﻿Event Speakers and schedule (SS) addon for EventON Plugin
====================================
v 0.6 (2016-11-28)
FIXED: +- icons switched to match scenario
FIXED: Speaker lightbox z value
FIXED: RTL compatibility
FIXED: Schedule days not staying in sequence
UPDATED: Show hide class name getting override in themes

v 0.5 (2016-11-18)
FIXED: Repeat events date not showing for schedule dates

v 0.4 (2016-11-15)
ADDED: Support for repeating events
FIXED: asort days order for schedule 

v 0.3 (2016-11-14)
FIXED: Add new speaker tag from wp-admin not saving meta values

v 0.2 (2016-10-31)
FIXED: function error when plugin activated
FIXED: Missing language text on frontend

v 0.1 (2016-10-18)
Initial release