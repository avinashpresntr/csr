<?php
/**
 * Event Tickets Ajax Handletx
 *
 * Handles AJAX requests via wp_ajax hook (both admin and front-end events)
 *
 * @author 		AJDE
 * @category 	Core
 * @package 	EventON-TX/classes/AJAX
 * @vetxion     1.3.6
 */

class evo_tix_ajax{
	/**
	 * Hook into ajax events
	 */
	public function __construct(){
		$ajax_events = array(
			'evotx_woocommerce_add_to_cart'=>'evotx_woocommerce_ajax_add_to_cart',
			'evoTX_ajax_06'=>'submit_inquiry',
			'evoTX_ajax_08'=>'evoTX_ajax_08',
		);
		foreach ( $ajax_events as $ajax_event => $class ) {
			add_action( 'wp_ajax_'.  $ajax_event, array( $this, $class ) );
			add_action( 'wp_ajax_nopriv_'.  $ajax_event, array( $this, $class ) );
		}
	}

	// submit inquiry form
		function submit_inquiry(){

			$evoOpt = get_evoOPT_array(1);

			$event_id = $_POST['event_id'];
			$ri = $_POST['ri'];

			add_filter('wp_mail_content_type',create_function('', 'return "text/html";'));	
			$_event_pmv = get_post_custom($event_id);
			
			// get email address
			$_to_mail = (!empty($_event_pmv['_tx_inq_email']))? $_event_pmv['_tx_inq_email'][0]:
				( !empty($evoOpt['evotx_tix_inquiries_def_email'])? $evoOpt['evotx_tix_inquiries_def_email']:
					get_option('admin_email'));
			// get subject
			$subject = (!empty($_event_pmv['_tx_inq_subject']))? $_event_pmv['_tx_inq_subject'][0]:
				( !empty($evoOpt['evotx_tix_inquiries_def_subject'])? $evoOpt['evotx_tix_inquiries_def_subject']:'New Ticket Sale Inquery');

			$from_email = $_POST['email'];
			$headers = 'From: '.$_POST['email'];	


			$helper = new evo_helper();

			ob_start();
			?>
				<div style='padding:20px;color:#777777'>
				<p>Event: <b><?php echo get_the_title( $event_id ); ?></b></p>
				<p>From: <b><?php echo $_POST['name'].' ('.$_POST['email'].')';?></b></p>
				<p>Message: <br/><?php echo $_POST['message'];?></p>
				</div>
			<?php
			$body = ob_get_clean();

			$email_body = $helper->get_email_body_content($body);
			$send_email = $helper->send_email(array(
				'to'=> $_to_mail,
				'from'=>$from_email,
				'subject'=>$subject,
				'html'=>'yes',
				'message'=>$email_body
			));

		}

	// ADD to cart for variable items
		function evotx_woocommerce_ajax_add_to_cart() {
			global $woocommerce;
			 
			// Initial values
				$product_id        = apply_filters( 'woocommerce_add_to_cart_product_id', absint( $_POST['product_id'] ) );
				$variation_id     = apply_filters( 'woocommerce_add_to_cart_variation_id', absint( $_POST['variation_id'] ) );
				$quantity  = empty( $_POST['quantity'] ) ? 1 : apply_filters( 'woocommerce_stock_amount', $_POST['quantity'] );
				$passed_validation = apply_filters( 'woocommerce_add_to_cart_validation', true, $product_id, $quantity );
				
			// if variations are sent
				if(isset($_POST['variations'])){
					$att=array();
					foreach($_POST['variations'] as $varF=>$varV){
						$att[$varF]=$varV;
					}
				}
			

			if($passed_validation && !empty($variation_id)){
				$cart_item_key = WC()->cart->add_to_cart( $product_id, $quantity, $variation_id ,$att);
				do_action( 'woocommerce_ajax_added_to_cart', $product_id );

				$frags = new WC_AJAX( );
	        	$frags->get_refreshed_fragments( );
			}

			/*
				// if variation ID is given
				if(!empty($variation_id) && $variation_id > 0){
					
					$cart_item_key = $woocommerce->cart->add_to_cart( $product_id, $quantity, $variation_id ,$att);
					 
					do_action( 'woocommerce_ajax_added_to_cart', $product_id ,$quantity, $variation_id ,$variation);

					// Return fragments
					//$frags = new WC_AJAX( );
		        	//$frags->get_refreshed_fragments( );


					// if WC settings set to redirect after adding to cart
					if ( get_option( 'woocommerce_cart_redirect_after_add' ) == 'yes' ) {
						// show cart notification
					 	wc_add_to_cart_message( $product_id );
					 	$woocommerce->set_messages();
					}
				}else{
				 
					if ( $passed_validation && $woocommerce->cart->add_to_cart( $product_id, $quantity) ) {
						do_action( 'woocommerce_ajax_added_to_cart', $product_id );
						 
						if ( get_option( 'woocommerce_cart_redirect_after_add' ) == 'yes' ) {
						 	woocommerce_add_to_cart_message( $product_id );
						 	$woocommerce->set_messages();
						}
						 
						// Return fragments
						// $frags = new WC_AJAX( );
		        		// $frags->get_refreshed_fragments( );
					 
					} else {
					 
						header( 'Content-Type: application/json; charset=utf-8' );
						 
						// If there was an error adding to the cart, redirect to the product page to show any errors
						$data = array(
						 	'error' => true,
						 	'product_url' => apply_filters( 'woocommerce_cart_redirect_after_error', get_permalink( $product_id ), $product_id )
						);
						 
						$woocommerce->set_messages();
						 
						echo json_encode( $data );
					 
					}
					die();
				} // endif
			
			*/
		
			$output = array(
				'key'=>$cart_item_key,
				'variation'=>WC()->cart->cart_contents_total
			);
			echo json_encode( $output );
		 }
	
	// make sure proper amount of tickets are created for all past shop_orders
		function evoTX_ajax_08(){
			$shop_orders = new WP_Query(array(
				'post_type'=>'shop_order',
				'posts_per_page'=>-1,				
			));

			if($shop_orders->have_posts()):
				while($shop_orders->have_posts()): $shop_orders->the_post();
					if($shop_orders->post->post_status!='wc-completed') continue;

					$orderPMV = get_post_custom($shop_orders->post->ID);

					if(!empty($orderPMV['_tixids'])){
						$ticketnumbers = unserialize($orderPMV['_tixids'][0]);
						if(is_array($ticketnumbers)){

						}else{
							$ticketnumbers;
						}
					}else{
						// create tickets
					}
				endwhile;
			endif;
			wp_reset_postdata();
		}
}
new evo_tix_ajax();


?>