<?php
/** 
 * AJAX for only backend of the tickets
 * @version 
 */
class evotx_admin_ajax{
	public function __construct(){
		$ajax_events = array(
			'the_ajax_evotx_a1'=>'evotx_get_attendees',
			'the_ajax_evotx_a5'=>'evoTX_checkin_',
			'the_ajax_evotx_a3'=>'generate_csv',
			'the_ajax_evotx_a55'=>'admin_resend_confirmation',
			'evoTX_ajax_07'=>'get_ticektinfo_by_ID',
		);
		foreach ( $ajax_events as $ajax_event => $class ) {
			add_action( 'wp_ajax_'.  $ajax_event, array( $this, $class ) );
			add_action( 'wp_ajax_nopriv_'.  $ajax_event, array( $this, $class ) );
		}
	}
	// GET attendee list view for event
		function evotx_get_attendees(){	
			global $evotx;

			$nonce = $_POST['postnonce'];
			$status = 0;
			$message = $content = '';

			if(! wp_verify_nonce( $nonce, 'evotx_nonce' ) ){
				$status = 1;	$message ='Invalid Nonce';
			}else{

				ob_start();

				$ri = (!empty($_POST['ri']) || (!empty($_POST['ri']) && $_POST['ri']==0 ))? $_POST['ri']:'all'; // repeat interval

				$customer_ = $evotx->functions->get_customer_ticket_list($_POST['eid'], $_POST['wcid'], $ri);

				// customers with completed orders
				if($customer_){
					echo "<div class='evotx'>";
					echo "<p class='header'>".__('Attendee Details','eventon')." <span class='txcount'>".__('Ticket Count','eventon')."</span></p>";	
					
					echo "<div class='eventedit_tix_attendee_list'>";

				
					// each event on the repeat
					foreach($customer_ as $event_time=>$tickets){
											
						$index = 1;
						$content = array();
						$totalCompleteCount = 0;

						// each ticket Order item
						foreach($tickets as $ticketItem_){
							
							$output = '';

							$order_status = !empty($ticketItem_['order_status'])? $ticketItem_['order_status']: false;
							$key = ($order_status=='completed')?'good':'bad';
							$key_ = ($order_status=='completed')?'':'hidden';
							if($order_status=='completed')$totalCompleteCount += (int)$ticketItem_['qty'];

							// HTML parsing
							$output .= "<span class='evotx_ticketitem_customer ".($index%2==0? 'even':'odd')." {$key} {$key_}'>";
							$output .= "<span class='evotx_ticketitem_header'>"
								.'<b>'.$ticketItem_['name'].'</b>  ('.$ticketItem_['email'].') '.( !empty($ticketItem_['type'])? "- <b>{$ticketItem_['type']}</b>":''). 
								( $order_status? " <b class='orderStutus status_{$order_status}'>{$order_status}</b>":'') ."</span>";
							$output .= "<span class='evotx_ticketItem'><span class='txcount'>{$ticketItem_['qty']}</span>";

							$tid = $ticketItem_['tids']; // ticket ID array with status
							

							$output .= "<span class='tixid'>";

							// Ticket Holder information
								$order_ticket_holders = get_post_meta($ticketItem_['orderid'], '_tixholders', true);
								$ticket_holder = $evotx->functions->get_ticketholder_names( $_POST['eid'],$order_ticket_holders);
							
							// for each ticket ID
							$index = 0;
							foreach($tid as $id=>$_status){
								$langStatus = $evotx->functions->get_checkin_status($_status);
								$output .= "<span class='evotx_ticket'>".$id;
								if($order_status == 'completed'){
									$output .= "<span class='evotx_status {$_status}' data-tid='{$id}' data-status='{$_status}' data-tiid='{$ticketItem_['tiid']}'>".$langStatus."</span>";

									// Ticket holder name associated to 
									if($ticket_holder && !empty($ticket_holder[$index]))
										$output .= "<span class='evotx_ticket_holdername'>".$ticket_holder[$index]."</span>";
								}
								$output .= "</span>";
								$tidX = $id;
								$index++;
							}

							$tix = explode('-', $tidX);
							$orderID = $tix[1];

							$output .= "<span class='clear'></span>
								<em class='orderdate'>".__('Ordered Date','eventon').': '.$ticketItem_['postdata']."</em>";
								$output .= " <em>".__('Order ID:','eventon')." <a class='evo_btn' href='".admin_url('post.php?post='.$orderID.'&action=edit')."'>".$orderID."</a></em>";
							$output .= "</span>";


							$output .= "</span>";
							$output .= "</span>";

							
							$content[$key][] = $output;
							$index++;
						}


						echo "<p class='attendee'>";
						echo "<span class='event_time'>".__('Event Start:','eventon').' '.$event_time."<em>".__('Total','eventon')." {$totalCompleteCount}</em></span>";
						echo implode('', $content['good']);
						echo "<span class='separatation'>".__('Other incompleted orders','eventon')."</span>";
						if(!empty($content['bad'])) echo implode('', $content['bad']);

						echo "</p>";					
					}
					echo "</div>";
					echo "</div>";
				}else{
					echo "<div class='evotx'>";
					echo "<p class='header nada'>".__('Could not find attendees with completed orders.','eventon')."</p>";	
					echo "</div>";
				}
				
				$content = ob_get_clean();
			}
					
			$return_content = array(
				'message'=> $message,
				'status'=>$status,
				'content'=>$content,
			);
			
			echo json_encode($return_content);		
			exit;
		}

	// for evo-tix post page and from event edit page
		function evoTX_checkin_(){
			global $evotx;

			$ticketNumber = $_POST['tid'];

			// split ticket number
			$tixNum = explode('-', $ticketNumber);
			$OrderComplete = $evotx->functions->is_order_complete($tixNum[1]);
			$CheckinLang = $evotx->functions->get_statuses_lang(); // get both check status lang

			// order is not complete
			if($OrderComplete){
				$tixID = $tixNum[0];

				$current_status = $_POST['status'];

				$ticketItem = new evotx_TicketItem($tixID);

				$other_status = $ticketItem->get_other_status($current_status);
				$ticketItem->change_ticket_status($other_status[0], $ticketNumber, $tixID);

				$newTixStaus = $other_status[0];

			}else{
				$newTixStaus = $_POST['status'];
			}			

			$return_content = array(
				'new_status'=>$newTixStaus,
				'new_status_lang'=>$CheckinLang[$newTixStaus],
			);
			
			echo json_encode($return_content);		
			exit;
		}


	// Download csv list of attendees
		function generate_csv(){

			$e_id = $_REQUEST['e_id'];
			$event = get_post($e_id, ARRAY_A);

			header("Content-type: text/csv");
			header("Content-Disposition: attachment; filename=".$event['post_name']."_".date("d-m-y").".csv");
			header("Pragma: no-cache");
			header("Expires: 0");


			global $evotx;
			$customers = $evotx->functions->get_customer_ticket_list($e_id, $_REQUEST['pid'], 'all');

			if($customers){
				//$fp = fopen('file.csv', 'w');

				$csv_header = apply_filters('evotx_csv_headers',array(
					'Name','Email Address','Address','Phone','Ticket IDs',
					'Quantity','Ticket Type','Event Time','Order Status'
				));
				$csv_head = implode(',', $csv_header);
				echo $csv_head."\n";
				
				// each customer
				foreach($customers as $eventtime=>$cus){

					// each ticket item
					foreach($cus as $ticketItem_){
						
						// Ticket Holder information
							$order_ticket_holders = get_post_meta($ticketItem_['orderid'], '_tixholders', true);
							$ticket_holder = $evotx->functions->get_ticketholder_names( $e_id,$order_ticket_holders);

						$tid = $ticketItem_['tids']; // ticket ID array with status
						
						// for each ticket ID
						$index = 0;
						foreach($tid as $id=>$_status){						
							$langStatus = $evotx->functions->get_checkin_status($_status);

							$name = ($ticket_holder && !empty($ticket_holder[$index]))? 
								$ticket_holder[$index]: $ticketItem_['name'];

							$csv_data = apply_filters('evotx_csv_row',array(
								$name,
								$ticketItem_['email'],
								$ticketItem_['address'],
								$ticketItem_['phone'],
								$id,
								'1',
								$ticketItem_['type'],
								'"'.$eventtime.'"',
								$ticketItem_['order_status']
							));

							$csv_dt = implode(",", $csv_data);
							echo $csv_dt."\n";
							$index++;
						}			
					}
					
				}
			}

		}

	// Resend Ticket Email
		function admin_resend_confirmation(){
			global $evotx;
			$order_id = false;

			// get order ID
			$order_id = (!empty($_POST['orderid']))? $_POST['orderid']:false;			
			
			if($order_id){

				// check if custom email is passed
				$customemail = !empty($_POST['email'])? $_POST['email']: false;

				$email = new evotx_email();
				$send_mail = $email->send_ticket_email($order_id, false, false, $customemail);
			}else{
				$send_mail =$order_id;
			}		

			// return the results
			$return_content = array(
				'status'=> ( $send_mail?'good':'bad')
			);
			
			echo json_encode($return_content);		
			exit;
		}

	// get information for a ticket number
		function get_ticektinfo_by_ID(){

			$tickernumber = $_POST['tickernumber'];
			
			$content = $this->get_ticket_info($tickernumber);

			$return_content = array(
				'content'=>$content,
				'status'=> ($content? 'good':'bad'),
			);
			
			echo json_encode($return_content);		
			exit;

		}

		function get_ticket_info($tickernumber){
			if(strpos($tickernumber, '-') === false) return false;

			$tixNum = explode('-', $tickernumber);

			if(!get_post_status($tixNum[0])) return false;

			$tixPMV = get_post_custom($tixNum[0]);

			ob_start();

			$ticketItem = new evotx_TicketItem($tixNum[0]);

			$tixPOST = get_post($tixNum[0]);
			$orderStatus = get_post_status($tixPMV['_orderid'][0]);
				$orderStatus = str_replace('wc-', '', $orderStatus);

			$ticketStatus = $ticketItem->get_ticket_status($tickernumber);

			echo "<p><em>".__('Primary Ticket Holder','eventon').":</em> {$tixPMV['name'][0]}</p>
				<p><em>".__('Email Address','eventon').":</em> {$tixPMV['email'][0]}</p>
				<p><em>".__('Event','eventon').":</em> ".get_the_title($tixPMV['_eventid'][0])."</p>
				<p><em>".__('Purchase Date','eventon').":</em> ".$tixPOST->post_date."</p>
				<p><em>".__('Ticket Status','eventon').":</em> <span class='tix_status {$ticketStatus}' data-tiid='{$tixNum[0]}' data-tid='{$tickernumber}' data-status='{$ticketStatus}'>{$ticketStatus}</span></p>
				<p><em>".__('Payment Status','eventon').":</em> {$orderStatus}</p>";

				// other tickets in the same order
				$otherTickets = $ticketItem->get_other_tix_order($tickernumber);

				if(is_array($otherTickets) && count($otherTickets)>0){
					echo "<div class='evotx_other_tickets'>";
					echo "<p >".__('Other Tickets in the same Order','eventon')."</p>";
					foreach($otherTickets as $num=>$status){
						echo "<p><em>".__('Ticekt Number','eventon').":</em> ".$num."</p>";
						echo "<p style='padding-bottom:10px;'><em>".__('Ticekt Status','eventon').":</em> <span class='tix_status {$status}' data-tiid='{$tixNum[0]}' data-tid='{$num}' data-status='{$status}'>{$status}</span></p>";
					}
					echo "</div>";
				}

			return ob_get_clean();
		}


}
new evotx_admin_ajax();