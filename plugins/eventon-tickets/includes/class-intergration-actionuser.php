<?php
/**
 * Intergration with ActionUser Addon
 * @version 1.3
 * @actionuser_version 2.0.10
 */
class evotx_actionuser{
	public function __construct(){

		add_filter('evoau_form_fields', array($this, 'fields_to_form'), 10, 1);

		// only for frontend
		if(!is_admin()){
			// actionUser intergration
			add_action('evoau_frontform_evotx', array($this, 'fields'), 10, 6);			
		}
		add_action('evoau_save_formfields', array($this, 'save_values'), 10, 3);

		// event manager
		add_action('evoau_manager_row_title', array($this, 'event_manager_row_title'), 10, 2);
		add_action('evoau_manager_row', array($this, 'event_manager_row'), 10, 3);
		add_action('evoauem_custom_action', array($this, 'event_manager_show_data'), 10, 1);

		// only admin fields
		if(is_admin()){
			add_filter('eventonau_language_fields', array($this, 'language'), 10, 1);
		}
	}

	// include fields to submission form array
		function fields_to_form($array){
			$array['evotx']=array('Ticket Fields', 'evotx_tix', 'evotx','custom','');
			return $array;
		}

	// Frontend showing fields and saving values  
		function fields($field, $event_id, $default_val, $EPMV, $opt2, $lang){
			$helper = new evo_helper();

			echo "<div class='row evotx'><p>";
				$evotx_tix = ($EPMV && !empty($EPMV['evotx_tix']) && $EPMV['evotx_tix'][0]=='yes')? true: false;
				echo $helper->html_yesnobtn(array(
					'id'=>'evotx_tix',
					'input'=>true,
					'label'=>evo_lang_get('evoAUL_rsvp1', 'Sell tickets for this event', $lang, $opt2),
					'var'=> ($evotx_tix?'yes':'no'),
					'lang'=>$lang,
					'afterstatement'=>'evotx_data_section'
				));
			echo "</p></div>";

			// for editting
				$_regular_price = $_sale_price = $_stock = $_sku = '';
				$wc_ticket_product_id = !empty($EPMV['tx_woocommerce_product_id'])? $EPMV['tx_woocommerce_product_id'][0]: false;
				if($wc_ticket_product_id){
					$woometa = get_post_custom($wc_ticket_product_id);
					
					if(!empty($woometa['_regular_price']) )	$_regular_price = $woometa['_regular_price'][0];
					if(!empty($woometa['_sale_price']) )	$_sale_price = $woometa['_sale_price'][0];
					if(!empty($woometa['_stock']) )	$_stock = $woometa['_stock'][0];
					if(!empty($woometa['_sku']) )	$_sku = $woometa['_sku'][0];
				}
			
			echo "<div id='evotx_data_section' class='row evoau_sub_formfield' style='display:".($evotx_tix?'':'none')."'>
					<input type='hidden' name='tx_product_type' value='simple'/>
					<input type='hidden' name='visibility' value='visible'/>

					<div class='evotx_data_row'>
						<p class='label'><label for=''>".evo_lang('Ticket Price',$lang, $opt2)."</label></p>
						<p><input type='text' class='fullwidth ' name='_regular_price' value='".$_regular_price."' placeholder=''/></p>
					</div>
					<div class='evotx_data_row'>
						<p class='label'><label for=''>".evo_lang('Ticket Sales Price',$lang, $opt2)."</label></p>
						<p><input type='text' class='fullwidth ' name='_sale_price' value='".$_sale_price."' placeholder=''/></p>
					</div>
					<div class='evotx_data_row'>
						<p class='label'><label for=''>".evo_lang('SKU',$lang, $opt2)."</label></p>
						<p><input type='text' class='fullwidth ' name='_sku' value='".$_sku."' placeholder=''/></p>
					</div>
					<div class='evotx_data_row'>
						<p class='label'><label for=''>".evo_lang('Ticket Stock Capacity',$lang, $opt2)."</label></p>
						<p><input type='text' class='fullwidth ' name='_stock' value='".$_stock."' placeholder=''/></p>
					</div>
				</div>";
		}
		function save_values($field, $fn, $created_event_id){
			if( $field =='evotx'){					
				if(!empty($_POST['evotx_tix']) && $_POST['evotx_tix']=='yes'){

					global $evotx;

					// adjust $_POST array
						if(!empty($_POST['_stock']))
							$_POST['_manage_stock'] = 'yes';

					add_post_meta($created_event_id, 'evotx_tix', $_POST['evotx_tix']);
					
					// add associate WC product
						$evotx->functions->add_new_woocommerce_product($created_event_id);					
				}
			}
		}
	
	// event manager additions
		function event_manager_row_title($event_id, $EPMV){
			$wc_ticket_product_id = !empty($EPMV['tx_woocommerce_product_id'])? $EPMV['tx_woocommerce_product_id'][0]: false;
			
			if(!empty($EPMV['evotx_tix']) && $EPMV['evotx_tix'][0]=='yes' && $wc_ticket_product_id){
				echo "<tags style='background-color:#8BDBEC'>".evo_lang('Ticket Sales On')."</tags>";
			}
		}
		function event_manager_row($event_id, $EPMV, $current_page_link){
			$wc_ticket_product_id = !empty($EPMV['tx_woocommerce_product_id'])? $EPMV['tx_woocommerce_product_id'][0]: false;

			if(!empty($EPMV['evotx_tix']) && $EPMV['evotx_tix'][0]=='yes' && $wc_ticket_product_id ){
				echo "<a class='evoauem_additional_buttons' href='". EVO_get_url($current_page_link, array('customaction'=>'ticket-stats','eid'=>$event_id))."'>".evo_lang('View Ticket Stats')."</a>";
			}
		}
		function event_manager_show_data($backto_events_html){
			$event_id = $_REQUEST['eid'];

			if($_REQUEST['customaction']== 'ticket-stats'){
				$EPMV = get_post_custom($event_id);
				$wc_ticket_product_id = !empty($EPMV['tx_woocommerce_product_id'])? $EPMV['tx_woocommerce_product_id'][0]: false;

				if(!$wc_ticket_product_id) return;
				global $evotx;

				$woometa = get_post_custom($wc_ticket_product_id);
				$product_type = $evotx->functions->get_product_type($wc_ticket_product_id);
				$__woo_currencySYM = get_woocommerce_currency_symbol();

				echo $backto_events_html;
				?>
					<h3><?php evo_lang_e('Event Ticket Information & Stats for');?> <u><?php echo get_the_title($event_id);?></u></h3>	
					<div class="evoautx_data">
						<table>
							<tr><td><?php evo_lang_e('Price');?></td><td><?php
							if($product_type=='variable'){
								echo $__woo_currencySYM . ' '. evo_meta($woometa, '_min_variation_price') .' - '.evo_meta($woometa, '_max_variation_price');
							}else{
								echo $__woo_currencySYM . ' '. evo_meta($woometa, '_regular_price');
							}
							?></td></tr>
							<?php if(evo_check_yn($woometa,'_manage_stock')):?>
								<?php
									if($product_type == 'simple'):
										$tix_inStock = $evotx->functions->event_has_tickets($EPMV, $woometa, 0);
								?>
									<tr><td><?php evo_lang_e('Tickets in stock');?></td><td><?php echo  $tix_inStock;?></td></tr>
								<?php endif;?>
							<?php endif;?>
							<tr><td><?php evo_lang_e('Ticket Type');?></td><td><?php echo $product_type;?></td></tr>
							<tr><td><?php evo_lang_e('SKU');?></td><td><?php echo evo_meta($woometa, '_sku');?></td></tr>
							<?php if(evo_check_yn($woometa,'_manage_stock')):?>
								<tr><td><?php evo_lang_e('Stock Status');?></td><td><?php echo evo_meta($woometa, '_stock_status');?></td></tr>
							<?php endif;?>

							<?php 
								$guest_list = $evotx->frontend->guest_list($event_id, 'all');
								if($guest_list):  
							?>
								<tr><td><?php evo_lang_e('Attendees');?></td><td>
									<p class='evotx_whos_coming' style='padding-top:5px;margin:0'>
										<item><em><?php evo_lang_e('Confirmed Attendance');?></em>: <?php echo $guest_list['count'];?></item><br/>
									<?php echo $guest_list['guests'];?></p>
								</td></tr>
							<?php endif;?>					
						</table>
					</div>
				<?php

			}
		}

	// language
		function language($array){
			$newarray = array(
				array('label'=>'Ticket Fields','type'=>'subheader'),
					array('label'=>'Ticket Price','var'=>'1'),		
					array('label'=>'Ticket Sales Price','var'=>'1'),		
					array('label'=>'SKU','var'=>'1'),		
					array('label'=>'Ticket Stock Capacity','var'=>'1'),		
					array('label'=>'Event Ticket Information & Stats for','var'=>'1'),				
					array('label'=>'Price','var'=>'1'),				
					array('label'=>'Tickets in stock','var'=>'1'),				
					array('label'=>'Tickets Type','var'=>'1'),	
					array('label'=>'Stock Status','var'=>'1'),				
					array('label'=>'Attendees','var'=>'1'),				
					array('label'=>'Confirmed Attendance','var'=>'1'),				
				array('type'=>'togend'),
			);
			return array_merge($array, $newarray);
		}
}
new evotx_actionuser();