<?php 

class w2dc_search_controller extends w2dc_frontend_controller {

	public function init($args = array()) {
		parent::init($args);

		$shortcode_atts = array_merge(array(
				'columns' => 2,
				'advanced_open' => false,
				'uid' => null,
				'show_what_search' => true,
				'show_categories_search' => true,
				'show_keywords_search' => true,
				'category' => 0,
				'exact_categories' => array(),
				'what_search' => '',
				'show_radius_search' => true,
				'radius' => 0,
				'show_where_search' => true,
				'show_locations_search' => true,
				'show_address_search' => true,
				'address' => '',
				'location' => 0,
				'exact_locations' => array(),
				'search_fields' => '',
				'search_fields_advanced' => '',
		), $args);
		
		$this->args = $shortcode_atts;
		
		$hash = false;
		if ($this->args['uid'])
			$hash = md5($this->args['uid']);
		
		if (isset($this->args['exact_categories']) && !is_array($this->args['exact_categories']))
			if ($categories = array_filter(explode(',', $this->args['exact_categories']), 'trim'))
				$this->args['exact_categories'] = $categories;

		if (isset($this->args['exact_locations']) && !is_array($this->args['exact_locations']))
			if ($locations = array_filter(explode(',', $this->args['exact_locations']), 'trim'))
				$this->args['exact_locations'] = $locations;

		$this->search_form = new w2dc_search_form($hash, 'listings_controller', $this->args);
		
		apply_filters('w2dc_frontend_controller_construct', $this);
	}

	public function display() {
		ob_start();
		$this->search_form->display($this->args['columns'], $this->args['advanced_open']);
		$output = ob_get_clean();

		return $output;
	}
}

?>