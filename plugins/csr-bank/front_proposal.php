<?php
	global $wpdb;
	echo '<link rel="stylesheet" type="text/css" href="'.plugins_url().'/datatables/jquery.dataTables.min.css" />';
	echo '<script type="text/javascript" src="'.plugins_url().'/datatables/jquery.dataTables.min.js"></script>';
	//echo '<script type="text/javascript" src="'.plugins_url().'/datatables/dataTables.bootstrap.min.js"></script>';
?>
<style>
table.dataTable tbody tr.highlight{
	background-color: #b3e5e1;
}
</style>
<link href="<?php echo plugins_url(); ?>/csr-bank/css/csr_bank_custom.css" rel="stylesheet" type="text/css" />
<link href="<?php echo plugins_url(); ?>/select2/select2.css" rel="stylesheet" type="text/css" />
<script src="<?php echo plugins_url(); ?>/select2/select2.min.js" type="text/javascript"></script>
<div class="wrap">
	<br>
	<?php 
		if(isset($_SESSION['msg'])){
			echo '<div class="updated" style="padding:5px 20px;"><b>'.$_SESSION['msg'].'</b></div>';
			unset($_SESSION['msg']);
		}
	?>
	<div class="row"><div class="col-sm-12 display_alert"></div></div>
	<div class="row">
		<form id="search_proposal" method="post" action="" class="" role="form" >
			<div class="col-lg-12 col-md-12">
				<div class="">
					<div class="form-group search_select_div mr10">
						<label class="sr-only" for="thematic_areas_work_id">Thematic areas Work</label>
						<select name="thematic_areas_work_id" id="thematic_areas_work_id" class="form-control select2 w200" >
							<option value=""> - All Thematic areas Work - </option>
							<?php
								$table_name = $wpdb->prefix . "thematic_areas_work";
								$res = $wpdb->get_results("SELECT * FROM $table_name WHERE `isDelete` = 0"); 
								foreach($res as $row){
									echo '<option value="'.$row->id.'" >'.$row->thematic_area.'</option>';
								}
							?>
						</select>
					</div>
				</div>
				<div class="">
					<div class="form-group search_select_div mr10">
						<label class="sr-only" for="work_locations">Work Location</label>
						<select name="work_location" id="work_locations" class="form-control select2 w200" >
							<option value=""> - All Work Locations - </option>
							<?php
								$table_name = $wpdb->prefix . "work_locations";
								$res = $wpdb->get_results("SELECT * FROM $table_name WHERE `isDelete` = 0"); 
								foreach($res as $row){
									echo '<option value="'.$row->id.'" >'.$row->location_name.'</option>';
								}
							?>
						</select>
					</div>
				</div>
				<div class="">
					<div class="form-group search_select_div mr10">
						<label class="sr-only" for="tag_id">Tags</label>
						<select name="tag_id" id="tag_id" class="form-control select2 w200" multiple >
							<?php /* <option value="" selected="selected"> - All Tags - </option> */ ?>
							<?php
								$table_name = $wpdb->prefix . "tag";
								$res = $wpdb->get_results("SELECT * FROM $table_name WHERE `isDelete` = 0"); 
								foreach($res as $row){
									echo '<option value="'.$row->id.'" >'.$row->tag_name.'</option>';
								}
							?>
						</select>
					</div>
				</div>
				<div class="">
					<input type="submit" name="search" style="margin:0px;" class="button form-control button-medium" value="Search" />
				</div>
			</div>
		</form>
	</div>
	<table id="proposal_table" width="100%" class="widefat dataTable">
		<thead>
			<tr>
				<th>Sr. No.</th>
				<th>Project Title</th>
				<th>NGO Name</th>
				<th>Thematic Area</th>								<th>Location</th>
				<th>Date</th>
				<th>Wishlist</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>
<script type="text/javascript">
	var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
	var corporate_id = '<?php echo isset($user_data->corporate_id)? $user_data->corporate_id : '0'; ?>';
	jQuery(document).ready( function(){
		jQuery('.select2').select2({ placeholder: "- All Tags -",  allowClear: true });
		
		dataTable = jQuery('#proposal_table').DataTable( {
			"processing": true,
			"serverSide": true,
			"pageLength": 25,
			"columnDefs": [ 
				{ "targets": 0, "orderable": false }, 
				{ "targets": 1, "orderable": false },
				{ "targets": 2, "orderable": false },
				{ "targets": 3, "orderable": false },
				{ "targets": 4, "orderable": false },
				{ "targets": 5, "orderable": false },
				{ "targets": 6, "orderable": false },
			],
			"createdRow": function( row, data, dataIndex ) {
				if ( data[6] == "Added to Wishlist" ) {
					jQuery(row).addClass( 'highlight' );
				}
			},
			"ajax":{
				data : function(d) {},
				url: ajaxurl + '?action=front_proposal_filter_list',
				type: "post", 
				error: function(){ 
					jQuery(".datatable-error").html("");
					jQuery("#datatable").append('<tbody class="datatable-error"><tr><th colspan="5">No data found in the server</th></tr></tbody>');
					jQuery("#datatable_processing").css("display","none");
				}
			},
			//"searching" : false,
			language: {
				searchPlaceholder: "Project Title"
			}
		} );
		
		jQuery(document).on('submit', '#search_proposal', function(){
			dataTable.destroy();
			var proposal_status = jQuery('#proposal_status').val();
			var thematic_areas_work_id = jQuery('#thematic_areas_work_id').val();						
			var work_location = jQuery('#work_locations').val();
			var tag_id = jQuery('#tag_id').val();
			if(tag_id == null){
				tag_id = '';
			}
			dataTable = jQuery('#proposal_table').DataTable( {
				"processing": true,
				"serverSide": true,
				"pageLength": 25,
				"columnDefs": [ 
					{ "targets": 0, "orderable": false }, 
					{ "targets": 1, "orderable": false },
					{ "targets": 2, "orderable": false },
					{ "targets": 3, "orderable": false },
					{ "targets": 4, "orderable": false },
					{ "targets": 5, "orderable": false },
					{ "targets": 6, "orderable": false },
				],
				"createdRow": function( row, data, dataIndex ) {
					if ( data[6] == "Added to Wishlist" ) {
						jQuery(row).addClass( 'highlight' );
					}
				},
				"ajax":{
					data : function(d) {},
					url: ajaxurl + '?action=front_proposal_filter_list&proposal_status=' + proposal_status + '&thematic_areas_work_id=' + thematic_areas_work_id + '&work_location=' + work_location + '&tag_id=' + tag_id,
					type: "post", 
					error: function(){ 
						jQuery(".datatable-error").html("");
						jQuery("#datatable").append('<tbody class="datatable-error"><tr><th colspan="5">No data found in the server</th></tr></tbody>');
						jQuery("#datatable_processing").css("display","none");
					}
				},
				//"searching" : false,
				language: {
					searchPlaceholder: "Project Title"
				}
			} );
			return false;
		});
	
	
		jQuery(document).on('click', '.add-to-wishlist', function(){
			var id = jQuery(this).data('id');
			var action = jQuery(this).data('action');
			var postData = new FormData();
            postData.append('action', 'corporate_wishlist_' + action);
            postData.append('proposal_id', id);
            postData.append('corporate_id', corporate_id);
			$.ajax({
                url: ajaxurl,
				type: "POST",
                processData: false,
                contentType: false,
                cache: false,
                data: postData,
                success: function (response) {
					var json = $.parseJSON(response);
					if (json['error'] == 'Exist') {
                        jQuery('.display_alert').html('<div class="alert alert-danger alert-dismissable"><strong>Error! </strong> You have already added this to wishlist.</div>');
                    }
                    if (json['success'] == 'Added') {
                        jQuery('.display_alert').html('<div class="alert alert-success alert-dismissable"><strong>Success! </strong> Proposal added to wishlist successfully.</div>');
						//jQuery('.wishlist-btn-'+id).data('action', 'remove');
						//jQuery('.wishlist-btn-'+id).html('Remove');
						jQuery('.add-to-wishlist-holder-'+id).html('Added to Wishlist');
                    }					
                    /*if (json['success'] == 'Removed') {
                        jQuery('.display_alert').html('<div class="alert alert-success alert-dismissable"><strong>Success! </strong> Proposal removed from wishlist successfully.</div>');
						jQuery('.wishlist-btn-'+id).data('action', 'add');
						jQuery('.wishlist-btn-'+id).html('Add');
                    }*/									
                    if (json['success'] == 'Nodata') {
                        jQuery('.display_alert').html('<div class="alert alert-danger alert-dismissable"><strong>Error! </strong> You have already removed this from wishlist.</div>');
                    }
					
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                    return false;
                },
            });

		});
		
	});


</script>
