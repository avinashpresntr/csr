<?php 
	global $wpdb; 
?>
<style>
.alert {
  padding: 5px 15px;
  margin-bottom: 20px;
  border: 1px solid transparent;
  border-radius: 4px;
}
.alert h4 {
  margin-top: 0;
  color: inherit;
}
.alert .alert-link {
  font-weight: bold;
}
.alert > p,
.alert > ul {
  margin-bottom: 0;
}
.alert > p + p {
  margin-top: 5px;
}
.alert-dismissable,
.alert-dismissible {
  padding-right: 35px;
}
.alert-dismissable .close,
.alert-dismissible .close {
  position: relative;
  top: -2px;
  right: -21px;
  color: inherit;
}
.alert-success {
  color: #3c763d;
  background-color: #dff0d8;
  border-color: #d6e9c6;
}
.alert-success hr {
  border-top-color: #c9e2b3;
}
.alert-success .alert-link {
  color: #2b542c;
}
.alert-info {
  color: #31708f;
  background-color: #d9edf7;
  border-color: #bce8f1;
}
.alert-info hr {
  border-top-color: #a6e1ec;
}
.alert-info .alert-link {
  color: #245269;
}
.alert-warning {
  color: #8a6d3b;
  background-color: #fcf8e3;
  border-color: #faebcc;
}
.alert-warning hr {
  border-top-color: #f7e1b5;
}
.alert-warning .alert-link {
  color: #66512c;
}
.alert-danger {
  color: #a94442;
  background-color: #f2dede;
  border-color: #ebccd1;
}
.alert-danger hr {
  border-top-color: #e4b9c0;
}
.alert-danger .alert-link {
  color: #843534;
}
</style>

<form id="front_corporate_change_password" action="" method="post" class="custom_form" data-parsley-validate novalidate enctype="multipart/form-data" >
	<div class="row"><div class="col-sm-12 display_alert"></div></div>
	<p>
		<label>Old Password*:<br>
			<span class="">
				<input type="password" name="old_password" value="" class="" parsley-trigger="change" required >
			</span>
		</label>
	</p>
	<p>
		<label>New Password*<br>
			<span class="">
				<input data-minlength="4" type="password" name="password" value="" class="" parsley-trigger="change" required >
			</span>
		</label>
	</p>
	<p>
		<label>Confirm Password<br>
			<span class="">
				<input data-minlength="4" type="password" id="cpassword" name="cpassword" value="" class="" parsley-trigger="change" required >
			</span>
		</label>
	</p>
	<p>
		<input type="hidden" name="id" value="<?php echo (isset($user_data->id) && $user_data->id > 0)? $user_data->id : 0; ?>" />
		<input type="submit" value="Update" class="gem-button gem-button-size-small gem-button-style-outline gem-button-text-weight-normal gem-button-border-2 submit_btn"><span class="ajax-loader"></span>
	</p>
</form>
<script type="text/javascript">

	var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
	jQuery(function() {
		
		jQuery(document).on('submit', '#front_corporate_change_password', function () {
            jQuery('.submit_btn').attr('disabled','disabled');
            var postData = new FormData(this);
            postData.append('action', 'front_corporate_change_password');
            $.ajax({
                url: ajaxurl,
				type: "POST",
                processData: false,
                contentType: false,
                cache: false,
                data: postData,
                success: function (response) {
					var json = $.parseJSON(response);
					console.log(json);
                    if (json['error'] == 'Nodata') {
                        jQuery('.display_alert').html('<div class="alert alert-danger alert-dismissable"><strong>Error! </strong> Nodata found invalid current password!</div>');
                    }
                    if (json['error'] == 'Invalidlength') {
                        jQuery('.display_alert').html('<div class="alert alert-danger alert-dismissable"><strong>Error! </strong> New password must contain minimum 4 character!</div>');
                    }
                    if (json['error'] == 'Notsame') {
                        jQuery('.display_alert').html('<div class="alert alert-danger alert-dismissable"><strong>Error! </strong> New password and Confirm password are not same!</div>');
                    }
                    if (json['success'] == 'Updated') {
                        jQuery('.display_alert').html('<div class="alert alert-success alert-dismissable"><strong>Success! </strong> Password changed successfully!</div>');
						setTimeout(function(){
							location.reload();
						}, 2000);
                    }
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                    $('.submit_btn').removeAttr('disabled','disabled');
                    return false;
                },
            });
            return false;
        });
		
		// only number in phone number field
		jQuery("#contact_phone_number").keydown(function (e) {
			// Allow: backspace, delete, tab, escape, enter and
			if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
				 // Allow: Ctrl+A, Command+A
				(e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
				 // Allow: home, end, left, right, down, up
				(e.keyCode >= 35 && e.keyCode <= 40)) {
					 // let it happen, don't do anything
					 return;
			}
			// Ensure that it is a number and stop the keypress
			if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
				e.preventDefault();
			}
		});
		
	});

</script>
