<?php
	global $wpdb;
	echo '<link rel="stylesheet" type="text/css" href="'.plugins_url().'/datatables/jquery.dataTables.min.css" />';
	echo '<script type="text/javascript" src="'.plugins_url().'/datatables/jquery.dataTables.min.js"></script>';
	//echo '<script type="text/javascript" src="'.plugins_url().'/datatables/dataTables.bootstrap.min.js"></script>';
?>
<style>
.label {
    display: inline;
    padding: .2em .6em .3em;
    font-size: 75%;
    font-weight: 700;
    line-height: 1;
    color: #fff;
    text-align: center;
    white-space: nowrap;
    vertical-align: baseline;
    border-radius: .25em;
}
.alert-success,
.alert-danger,
.alert-error,
.alert-warning,
.alert-info,
.label-danger,
.label-info,
.label-warning,
.label-primary,
.label-success {
  color: #fff !important;
}
.alert-danger,
.alert-error,
.label-danger {
  background-color: #dd4b39 !important;
}
.alert-warning,
.label-warning {
  background-color: #f39c12 !important;
}
.alert-info,
.label-info {
  background-color: #00c0ef !important;
}
.label-primary {
  background-color: #3c8dbc !important;
}
.alert-success,
.label-success {
  background-color: #00a65a !important;
}
.text-info{
	color: #00c0ef !important;
}
.text-success{
	color: #00a65a !important;
}
.text-danger{
	color: #dd4b39 !important;
}
</style>
<link href="<?php echo plugins_url(); ?>/csr-bank/css/csr_bank_custom.css" rel="stylesheet" type="text/css" />
<link href="<?php echo plugins_url(); ?>/select2/select2.css" rel="stylesheet" type="text/css" />
<script src="<?php echo plugins_url(); ?>/select2/select2.min.js" type="text/javascript"></script>
<div class="wrap">
	<h1>
		Corporate Partners
		<ul class="subsubsub pull-right" style="margin: 0px;">
			<li class="trash"><a href="?page=corporate" class="text-success">Corporate List</a></li>
		</ul>
	</h1>
	<br>
	<?php
		if(isset($_SESSION['corporate_restored'])){
			echo '<div id="message" class="updated notice notice-success is-dismissible"><p>Corporate Restored.</p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div>';
			unset($_SESSION['corporate_restored']);
		}
		
		if(isset($_SESSION['corporate_deleted'])){
			echo '<div id="message" class="updated notice notice-success is-dismissible"><p>Corporate Deleted.</p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div>';
			unset($_SESSION['corporate_deleted']);
		}
	?>
	<div>
		<form id="search_corporate" method="post" action="" class="form-inline" role="form" >
			<div class="form-group search_select_div mr10">
				<label class="sr-only" for="corporate_type_id">Type of Organisation</label>
				<select name="corporate_type_id" id="corporate_type_id" class="form-control select2 w200" >
					<option value=""> - All Type of Organisation - </option>
					<?php
						$table_name = $wpdb->prefix . "corporate_type";
						$res = $wpdb->get_results("SELECT * FROM $table_name WHERE `isDelete` = 0"); 
						foreach($res as $row){
							echo '<option value="'.$row->id.'" >'.$row->type_name.'</option>';
						}
					?>
				</select>
			</div>
			<div class="form-group search_select_div mr10">
				<label class="sr-only" for="work_location">Work Locations</label>
				<select name="work_location" id="work_location" class="form-control select2 w200" >
					<option value=""> - All Work Locations - </option>
					<?php
						$table_name = $wpdb->prefix . "work_locations";
						$res = $wpdb->get_results("SELECT * FROM $table_name WHERE `isDelete` = 0"); 
						foreach($res as $row){
							echo '<option value="'.$row->id.'" >'.$row->location_name.'</option>';
						}
					?>
				</select>
			</div>
			<input type="submit" name="search" class="button button-medium" value="Search" />
		</form>
		
	</div>
	<table id="corporate_table" width="100%" class="widefat dataTable">
		<thead>
			<tr>
				<th>Sr. No.</th>
				<th>Corporate Organisation Name</th>
				<th>Created Date</th>
				<th>Accepted Proposal Requests</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>
<script type="text/javascript">
	var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
	jQuery(document).ready( function(){
		jQuery('.select2').select2();
		dataTable = jQuery('#corporate_table').DataTable( {
			"processing": true,
			"serverSide": true,
			"pageLength": 25,
			"columnDefs": [ 
				{ "targets": 0, "orderable": false }, 
				{ "targets": 1, "orderable": false },
				{ "targets": 2, "orderable": false },
				{ "targets": 3, "orderable": false },
				{ "targets": 4, "orderable": false },
			],
			"ajax":{
				data : function(d) {},
				url: ajaxurl + '?action=corporate_filter_list&corporate_type=trashed',
				type: "post", 
				error: function(){ 
					jQuery(".datatable-error").html("");
					jQuery("#datatable").append('<tbody class="datatable-error"><tr><th colspan="5">No data found in the server</th></tr></tbody>');
					jQuery("#datatable_processing").css("display","none");
				}
			},
			//"searching" : false,
			language: {
				searchPlaceholder: "Organisation Name"
			}
		} );
		
		jQuery(document).on('submit', '#search_corporate', function(){
			dataTable.destroy();
			var corporate_type_id = jQuery('#corporate_type_id').val();
			var work_location = jQuery('#work_location').val();
			dataTable = jQuery('#corporate_table').DataTable( {
				"processing": true,
				"serverSide": true,
				"pageLength": 25,
				"columnDefs": [ 
					{ "targets": 0, "orderable": false }, 
					{ "targets": 1, "orderable": false },
					{ "targets": 2, "orderable": false },
					{ "targets": 3, "orderable": false },
					{ "targets": 4, "orderable": false },
				],
				"ajax":{
					data : function(d) {},
					url: ajaxurl + '?action=corporate_filter_list&corporate_type=trashed&corporate_type_id=' + corporate_type_id + '&work_location=' + work_location,
					type: "post", 
					error: function(){ 
						jQuery(".datatable-error").html("");
						jQuery("#datatable").append('<tbody class="datatable-error"><tr><th colspan="5">No data found in the server</th></tr></tbody>');
						jQuery("#datatable_processing").css("display","none");
					}
				},
				//"searching" : false,
				language: {
					searchPlaceholder: "Organisation Name"
				}
			} );
			return false;
		});
	});
	
	function corporate_restore(corporate_id){
		if (confirm('Are you sure, You want to restore this record?')) {
			var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
			jQuery.ajax({
				url: ajaxurl,
				data : {action: "corporate_restore", corporate_id: corporate_id },
				success: function(response){
					if (response == 'corporate_restored') {
						jQuery("html, body").animate({ scrollTop: 0 }, "slow");
						window.location.reload();
					}
					return false;
				},
			});
		}
	}
	
	function corporate_delete(corporate_id){
		if (confirm('Are you sure, You want to delete this record?')) {
			var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
			jQuery.ajax({
				url: ajaxurl,
				data : {action: "corporate_delete", corporate_id: corporate_id },
				success: function(response){
					if (response == 'corporate_deleted') {
						jQuery("html, body").animate({ scrollTop: 0 }, "slow");
						window.location.reload();
					}
					return false;
				},
			});
		}
	}
</script>
