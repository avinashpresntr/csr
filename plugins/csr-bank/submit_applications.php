<?php 
	global $wpdb; 
?>
<link href="<?php echo plugins_url(); ?>/select2/select2.css" rel="stylesheet" type="text/css" />
<script src="<?php echo plugins_url(); ?>/select2/select2.min.js" type="text/javascript"></script>
<style>
.alert {
  padding: 5px 15px;
  margin-bottom: 20px;
  border: 1px solid transparent;
  border-radius: 4px;
}
.alert h4 {
  margin-top: 0;
  color: inherit;
}
.alert .alert-link {
  font-weight: bold;
}
.alert > p,
.alert > ul {
  margin-bottom: 0;
}
.alert > p + p {
  margin-top: 5px;
}
.alert-dismissable,
.alert-dismissible {
  padding-right: 35px;
}
.alert-dismissable .close,
.alert-dismissible .close {
  position: relative;
  top: -2px;
  right: -21px;
  color: inherit;
}
.alert-success {
  color: #3c763d;
  background-color: #dff0d8;
  border-color: #d6e9c6;
}
.alert-success hr {
  border-top-color: #c9e2b3;
}
.alert-success .alert-link {
  color: #2b542c;
}
.alert-info {
  color: #31708f;
  background-color: #d9edf7;
  border-color: #bce8f1;
}
.alert-info hr {
  border-top-color: #a6e1ec;
}
.alert-info .alert-link {
  color: #245269;
}
.alert-warning {
  color: #8a6d3b;
  background-color: #fcf8e3;
  border-color: #faebcc;
}
.alert-warning hr {
  border-top-color: #f7e1b5;
}
.alert-warning .alert-link {
  color: #66512c;
}
.alert-danger {
  color: #a94442;
  background-color: #f2dede;
  border-color: #ebccd1;
}
.alert-danger hr {
  border-top-color: #e4b9c0;
}
.alert-danger .alert-link {
  color: #843534;
}
</style>
<div class="custom_form">
	<div class="row"><div class="col-sm-12 display_alert"></div></div>
	<form id="application_type_selection">
		<p>
			<label class="radio-inline" for="new_app" style="cursor:pointer; padding: 5px;">
				<input type="radio" class="application_type" value="0" name="application_type" id="new_app" checked />
				&nbsp;New Application
			</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<label class="radio-inline" for="renew_app" style="cursor:pointer; padding: 5px;">
				<input type="radio" class="application_type" value="1" name="application_type" id="renew_app" />
				&nbsp;Re-new Application
			</label>
		</p>
	</form>
</div>
		
<div class="hub_code_area" style="display:none;">
	<form id="get_ngo_details" action="" method="post" class="custom_form" data-parsley-validate novalidate enctype="multipart/form-data" >
		<p>
			<label>Your NGO Hubcode*:<br>
				<span class="">
					<input type="text" name="ngo_hub_code" value="" class="" required>
				</span>
			</label>
		</p>
		<p><input type="submit" value="Find" class="gem-button gem-button-size-small gem-button-style-outline gem-button-text-weight-normal gem-button-border-2 submit_btn"><span class="ajax-loader"></span></p>
	
	</form>
</div>

<div class="application_area">
	<form id="add_new_ngo" action="" method="post" class="custom_form" data-parsley-validate novalidate enctype="multipart/form-data" >
		<p>
			<label>Name of the organisation*:<br>
				<span class="">
					<input type="text" id="organisation_name" name="organisation_name" value="" class="" data-parsley-pattern="^[A-Za-z ]*$" required >
				</span>
			</label>
		</p>	
		<p>
			<label>Scale of Operation*:<br>
				<span class="">
					<select class="form-dropdown select2" id="operation_scale_id" name="operation_scale_id" parsley-trigger="change" required >
						<option value=""> - Select scale of Operation - </option>
						<?php
							$table_name = $wpdb->prefix . "operation_scale";
							$res = $wpdb->get_results("select * from $table_name where isDelete=0"); 
							foreach($res as $row){
								echo '<option value="'.$row->id.'">'.$row->operation_scale.'</option>';
							}
						?>
					</select>
				</span>
			</label>
		</p>
		<p>
			<label>Type of Organisation*:<br>
				<span class="">
					<select class="form-dropdown select2" id="organisation_type_id" name="organisation_type_id" parsley-trigger="change" required >
						<option value=""> - Select type of Organisation - </option>
						<?php
							$table_name = $wpdb->prefix . "organisation_type";
							$res = $wpdb->get_results("select * from $table_name where isDelete=0"); 
							foreach($res as $row){
								echo '<option value="'.$row->id.'">'.$row->type_name.'</option>';
							}
						?>
					</select>
				</span>
			</label>
		</p>
		<?php /* <p>
			<label>Thematic areas of work*:<br>
				<span class="">
					<select class="form-dropdown select2" id="thematic_areas_work_id" name="thematic_areas_work_id" parsley-trigger="change" required >
						<option value=""> - Select Thematic area - </option>
						<?php
							$table_name = $wpdb->prefix . "thematic_areas_work";
							$res = $wpdb->get_results("select * from $table_name where isDelete=0"); 
							foreach($res as $row){
								echo '<option value="'.$row->id.'">'.$row->thematic_area.'</option>';
							}
						?>
					</select>
				</span>
			</label>
		</p> */ ?>
		<p>
			<label>Thematic areas of work*<br>
				<span class="">
					<input type="hidden" name="thematic_areas_works" id="thematic_areas_works"  parsley-trigger="change" required >
					<ul class="parsley-errors-list filled tag_required_thematic_areas"  style="display: none;"><li class="parsley-required">This value is required.</li></ul>
				</span>
			</label>
		</p>
		<p>
			<label>Sub Thematic areas:<br>
				<span class="">
					<input type="text" id="sub_thematic_area" name="sub_thematic_area" value="" class="" >
					<small>Add multiple value by comma (,) separated.</small>
				</span>
			</label>
		</p>
		<p>
			<label>Locations of work: ( State / District / City )*<br>
				<span class="">
					<input type="hidden" name="work_location" id="work_locations"  parsley-trigger="change" required >
					<ul class="parsley-errors-list filled tag_required"  style="display: none;"><li class="parsley-required">This value is required.</li></ul>
				</span>
			</label>
		</p>
		<p>
			<label>Postal Address*:<br>
				<span class=""><textarea id="postal_address" name="postal_address" rows="4" class="" parsley-trigger="change" required ></textarea></span> 
			</label>
		</p>
		<h5>Contact Details</h5>
		<p>
			<label>Contact person name*:<br>
				<span class="">
					<input type="text" id="person_firstname" name="person_firstname" value="" class="" placeholder="First Name" data-parsley-pattern="^[A-Za-z ]*$" required ><br />
					<input type="text" id="person_lastname" name="person_lastname" value="" class="" placeholder="Last Name" data-parsley-pattern="^[A-Za-z ]*$" required >
				</span>
			</label>
		</p>
		<p>
			<label>Email*:<br>
				<span class="">
					<input type="email" id="email_id" name="email_id" value="" class="" parsley-trigger="change" required >
				</span>
			</label>
		</p>
		<p>
			<label>Pan Card Number*:<br>
				<span class="">
					<input type="text" id="pan_no" name="pan_no" value="" class="" required >
				</span>
			</label>
		</p>
		<p>
			<label>Phone Number:<br>
				<span class="">
					<input type="text" id="phone_number" name="phone_number" value="" class="" placeholder="Ex.(0123)456789" data-parsley-maxlength="15" >
					<small>Ex.(0123)456789</small>
				</span>
			</label>
		</p>
		<p>
			<label>Mobile Number:<br>
				<span class="">
					<input type="number" id="mobile_number" name="mobile_number" value="" class="" data-parsley-minlength="10" data-parsley-maxlength="10" >
				</span>
			</label>
		</p>
		<p>
			<label>Upload application form*:<br>
				<span class="">
					<input type="file" id="application_form_name" name="application_form_name" value="" class="" parsley-trigger="change" required >
					<div id="application_form_name_div"></div>
				</span>
			</label>
		</p>
		<p>
			<label>Upload payment receipt*:<br>
				<span class="">
					<input type="file" id="payment_receipt_name" name="payment_receipt_name" value="" class="" parsley-trigger="change" required >
					<div id="payment_receipt_name_div"></div>
				</span>
			</label>
		</p>
		<p>
			<input type="submit" value="Submit" class="gem-button gem-button-size-small gem-button-style-outline gem-button-text-weight-normal gem-button-border-2 submit_btn"><span class="ajax-loader"></span>
			<span class="loading_div" style="display: none;"><img src="<?php echo plugins_url(); ?>/csr-bank/img/loading.gif" alt="Loading..." width="25" /> <span>Please Wait...</span></span>
		</p>
	</form>		
</div>

<script type="text/javascript">
	$('.tag_required').hide();
	$('.tag_required_thematic_areas').hide();
	var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
	var filepath = "<?php echo home_url().'/wp-content/uploads/ngo_files/'; ?>";
	jQuery(function() {
		
		//for geting ngo details
		jQuery('.application_type').on('change', function(){
			if(jQuery(this).val() == '0'){
				window.location.reload();
			}else{
				jQuery('.hub_code_area').show();
				jQuery('.application_area').hide();
				
				jQuery(document).on('submit', '#get_ngo_details', function () {
					var ngo_hub_code = $('#ngo_hub_code').val();
					if(ngo_hub_code == ''){
						return false;
					}
					var postData = new FormData(this);
					postData.append('action', 'front_get_ngo_details');
					$.ajax({
						url: ajaxurl,
						type: "POST",
						processData: false,
						contentType: false,
						cache: false,
						data: postData,
						success: function (response) {
							var json = $.parseJSON(response);
							
							if (json['error'] == 'Nodata') {
								jQuery('.display_alert').html('<div class="alert alert-danger alert-dismissable"><strong>Error! </strong> No data found for this hub code in our empanelled NGO List.</div>');
								jQuery('.application_area').hide();
								return false;
							}
							if (json['success'] == 'NoRenewal') {
								jQuery('.display_alert').html('<div class="alert alert-danger alert-dismissable"><strong>Error! </strong> Your Application does not required renewal now! Because Your applocation is already running.</div>');
								jQuery('.application_area').hide();
								return false;
							}
							if (json['success'] == 'Found') {
								jQuery('.display_alert').html('<div class="alert alert-success alert-dismissable"><strong>Success! </strong> Your Application Found.</div>');
								jQuery('#add_new_ngo')[0].reset();
								jQuery('.select2-search-choice-close').click();
								jQuery('#select2-chosen-1').html(' - Select scale of Operation - ');
								jQuery('#select2-chosen-2').html(' - Select type of Organisation - ');
								jQuery('#select2-chosen-3').html(' - Select Thematic area - ');
								jQuery('.application_area').show();
								var data = json['data'];
								if(data.id > 0){
									jQuery('#organisation_name').val(data.organisation_name);
									jQuery('#operation_scale_id').val(data.operation_scale_id).trigger("change");
									jQuery('#organisation_type_id').val(data.organisation_type_id).trigger("change");
									jQuery("#thematic_areas_works").select2('val', data.thematic_areas_works).trigger("change");
									jQuery("#work_locations").select2('val', data.work_locations).trigger("change");
									jQuery("#sub_thematic_area").val(data.sub_thematic_area);
									jQuery("#postal_address").val(data.postal_address);
									jQuery("#person_firstname").val(data.person_firstname);
									jQuery("#person_lastname").val(data.person_lastname);
									jQuery("#email_id").val(data.email_id);
									jQuery("#pan_no").val(data.pan_no);
									jQuery("#phone_number").val(data.phone_number);
									jQuery("#mobile_number").val(data.mobile_number);
									var tempApplicationHtml = '';
									var tempPaymentHtml = '';
									var tempHubcodeHtml = '<input type="hidden" name="hub_code" value="' + data.hub_code + '" />';
									var tempNgoidHtml = '<input type="hidden" name="ngo_id" value="' + data.id + '" />';
									var tempStatusHtml = '<input type="hidden" name="ngo_status" value="' + data.ngo_status + '" />';
									var tempFrontCallHtml = '<input type="hidden" name="is_front_call" value="1" />';
									var tempLast3MonthHtml = '';
									if (json['last_3_month'] == 'last_3_month') {
										var tempLast3MonthHtml = '<input type="hidden" name="last_3_month" value="last_3_month" />';
									}
									if(data.application_form_name != ""){
										tempApplicationHtml = '<a href="' + filepath + data.application_form_name + '" download class="button" ><i class="fa fa-download"></i> Download </a>';
										jQuery('#application_form_name').removeAttr('required');
									}else{
										tempApplicationHtml = 'N/A';
									}
									if(data.payment_receipt_name != ""){
										tempPaymentHtml = '<a href="' + filepath + data.payment_receipt_name + '" download class="button" ><i class="fa fa-download"></i> Download </a>';
										jQuery('#payment_receipt_name').removeAttr('required');
									}else{
										tempPaymentHtml = 'N/A';
									}
									jQuery("#application_form_name_div").html(tempApplicationHtml);
									jQuery("#payment_receipt_name_div").html(tempPaymentHtml);
									
									jQuery("#add_new_ngo").append(tempHubcodeHtml+tempNgoidHtml+tempStatusHtml+tempFrontCallHtml+tempLast3MonthHtml);
								}
							}
							return false;
						},
					});
					return false;
				});
				
			}
		});
		
		jQuery('.select2').select2();
		tag_load();
		
		jQuery(document).on('click', '.alert-dismissable', function () {
			jQuery('.alert-dismissable').hide();
		});
		
		jQuery(document).on('submit', '#add_new_ngo', function () {
            var work_locations = $('#work_locations').val();
            if(work_locations == ''){
                jQuery('.tag_required').show();
                return false;
            }
            var thematic_areas_works = $('#thematic_areas_works').val();
            if(thematic_areas_works == ''){
                jQuery('.tag_required_thematic_areas').show();
                return false;
            }
            jQuery('.loading_div').show();
            var postData = new FormData(this);
			if(jQuery("input[name='application_type']:checked").val() == 1){
				postData.append('action', 'update_ngo');
			}else{
				postData.append('action', 'add_new_ngo');
			}
            $.ajax({
                url: ajaxurl,
				type: "POST",
                processData: false,
                contentType: false,
                cache: false,
                data: postData,
                success: function (response) {
					jQuery('.loading_div').hide();
					var json = $.parseJSON(response);
					
                    if (json['error'] == 'After_2year') {
                        jQuery('.display_alert').html('<div class="alert alert-danger alert-dismissable"><strong>Error! </strong> You are rejected, So you can reapply for empanelment only after two years.</div>');
                    }
                    if (json['error'] == 'Exist') {
                        jQuery('.display_alert').html('<div class="alert alert-danger alert-dismissable"><strong>Error! </strong> Application Already Exist.</div>');
                    }
                    if (json['error'] == 'FileType') {
                        jQuery('.display_alert').html('<div class="alert alert-danger alert-dismissable"><strong>Error! </strong> Only allow pdf,graphics and zip file upload.</div>');
                    }
                    if (json['error'] == 'FileSize') {
                        jQuery('.display_alert').html('<div class="alert alert-danger alert-dismissable"><strong>Error! </strong> Max file upload size limit 15MB.</div>');
                    }
                    if (json['success'] == 'Re_open') {
                        jQuery('.display_alert').html('<div class="alert alert-success alert-dismissable"><strong>Success! </strong> Your Application Re-open.</div>');
                        jQuery('#add_new_ngo')[0].reset();
                        jQuery('.select2-search-choice-close').click();
                        jQuery('#select2-chosen-1').html(' - Select scale of Operation - ');
                        jQuery('#select2-chosen-2').html(' - Select type of Organisation - ');
                        jQuery('#select2-chosen-3').html(' - Select Thematic area - ');
                    }
                    if (json['success'] == 'Added') {
                        jQuery('.display_alert').html('<div class="alert alert-success alert-dismissable"><strong>Success! </strong> Application Added.</div>');
                        jQuery('#add_new_ngo')[0].reset();
                        jQuery('.select2-search-choice-close').click();
                        jQuery('#select2-chosen-1').html(' - Select scale of Operation - ');
                        jQuery('#select2-chosen-2').html(' - Select type of Organisation - ');
                        jQuery('#select2-chosen-3').html(' - Select Thematic area - ');
                    }
                    if (json['success'] == 'Updated') {
                        jQuery('.display_alert').html('<div class="alert alert-success alert-dismissable"><strong>Success! </strong> Application Added for renewal.</div>');
                        jQuery('#add_new_ngo')[0].reset();
                        jQuery('#get_ngo_details')[0].reset();
                        jQuery('#application_type_selection')[0].reset();
                        jQuery('.select2-search-choice-close').click();
                        jQuery('#select2-chosen-1').html(' - Select scale of Operation - ');
                        jQuery('#select2-chosen-2').html(' - Select type of Organisation - ');
                        jQuery('#select2-chosen-3').html(' - Select Thematic area - ');
						setTimeout(function(){ 
							window.location.reload();
						}, 1500);
                    }
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                    $('.tag_required').hide();
                    $('.tag_required_thematic_areas').hide();
                    //$('.submit_btn').removeAttr('disabled','disabled');
                    tag_load();
                    return false;
                },
            });
            return false;
        });
		
	});
	
	function tag_load(){
		
		jQuery.ajax({
            url: ajaxurl,
			dataType : "JSON",
			data : {action: "get_work_locations"},
            success: function(result){
				$("#work_locations").select2({
                    createSearchChoice:function(term, data) { if ($(data).filter(function() { return this.text.localeCompare(term)===0; }).length===0) {return {id:term, text:term};} },
                    multiple: true,
                    //maximumSelectionSize: 1,
                    data: result,
                });
                function log(e) {
                    var e=$("<li>"+e+"</li>");
                    $("#events_11").append(e);
                    e.animate({opacity:1}, 10000, 'linear', function() { e.animate({opacity:0}, 2000, 'linear', function() {e.remove(); }); });
                }
                $("#work_locations")
                    .on("change", function(e) { log("change "+JSON.stringify({val:e.val, added:e.added, removed:e.removed})); })
                    .on("select2-opening", function() { log("opening"); })
                    .on("select2-open", function() { log("open"); })
                    .on("select2-close", function() { log("close"); })
                    .on("select2-highlight", function(e) { log ("highlighted val="+ e.val+" choice="+ JSON.stringify(e.choice));})
                    .on("select2-selecting", function(e) { log ("selecting val="+ e.val+" choice="+ JSON.stringify(e.choice));})
                    .on("select2-removing", function(e) { log ("removing val="+ e.val+" choice="+ JSON.stringify(e.choice));})
                    .on("select2-removed", function(e) { log ("removed val="+ e.val+" choice="+ JSON.stringify(e.choice));})
                    .on("select2-loaded", function(e) { log ("loaded (data property omitted for brevity)");})
                    .on("select2-focus", function(e) { log ("focus");})
                    .on("select2-blur", function(e) { log ("blur");});
            }
        });
    
		jQuery.ajax({
            url: ajaxurl,
			dataType : "JSON",
			data : {action: "get_thematic_areas_works"},
            success: function(result){
				$("#thematic_areas_works").select2({
                    //createSearchChoice:function(term, data) { if ($(data).filter(function() { return this.text.localeCompare(term)===0; }).length===0) {return {id:term, text:term};} },
                    multiple: true,
                    //maximumSelectionSize: 1,
                    data: result,
                });
                function log(e) {
                    var e=$("<li>"+e+"</li>");
                    $("#events_11").append(e);
                    e.animate({opacity:1}, 10000, 'linear', function() { e.animate({opacity:0}, 2000, 'linear', function() {e.remove(); }); });
                }
                $("#thematic_areas_works")
                    .on("change", function(e) { log("change "+JSON.stringify({val:e.val, added:e.added, removed:e.removed})); })
                    .on("select2-opening", function() { log("opening"); })
                    .on("select2-open", function() { log("open"); })
                    .on("select2-close", function() { log("close"); })
                    .on("select2-highlight", function(e) { log ("highlighted val="+ e.val+" choice="+ JSON.stringify(e.choice));})
                    .on("select2-selecting", function(e) { log ("selecting val="+ e.val+" choice="+ JSON.stringify(e.choice));})
                    .on("select2-removing", function(e) { log ("removing val="+ e.val+" choice="+ JSON.stringify(e.choice));})
                    .on("select2-removed", function(e) { log ("removed val="+ e.val+" choice="+ JSON.stringify(e.choice));})
                    .on("select2-loaded", function(e) { log ("loaded (data property omitted for brevity)");})
                    .on("select2-focus", function(e) { log ("focus");})
                    .on("select2-blur", function(e) { log ("blur");});
            }
        });
    
	}
    
</script>
