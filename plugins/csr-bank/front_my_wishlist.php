<?php
	global $wpdb;
	echo '<link rel="stylesheet" type="text/css" href="'.plugins_url().'/datatables/jquery.dataTables.min.css" />';
	echo '<script type="text/javascript" src="'.plugins_url().'/datatables/jquery.dataTables.min.js"></script>';
	//echo '<script type="text/javascript" src="'.plugins_url().'/datatables/dataTables.bootstrap.min.js"></script>';
?>
<link href="<?php echo plugins_url(); ?>/csr-bank/css/csr_bank_custom.css" rel="stylesheet" type="text/css" />
<link href="<?php echo plugins_url(); ?>/select2/select2.css" rel="stylesheet" type="text/css" />
<script src="<?php echo plugins_url(); ?>/select2/select2.min.js" type="text/javascript"></script>
<div class="wrap">
	<br>
	<?php 
		if(isset($_SESSION['msg'])){
			echo '<div class="updated" style="padding:5px 20px;"><b>'.$_SESSION['msg'].'</b></div>';
			unset($_SESSION['msg']);
		}
	?>
	<div class="row"><div class="col-sm-12 display_alert"></div></div>
	
	<table id="proposal_table" width="100%" class="widefat dataTable">
		<thead>
			<tr>
				<th>Sr. No.</th>				<th>Project Title</th>				<th>NGO Name</th>				<th>Thematic Area</th>								<th>Location</th>				<th>Date</th>				<th>Wishlist</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>
<script type="text/javascript">
	var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
	var corporate_id = '<?php echo isset($user_data->corporate_id)? $user_data->corporate_id : '0'; ?>';
	var wishlistStatus = 0;
	jQuery(document).ready( function(){
		jQuery('.select2').select2();
		
		dataTable = jQuery('#proposal_table').DataTable( {
			"processing": true,
			"serverSide": true,
			"pageLength": 25,
			"columnDefs": [ 
				{ "targets": 0, "orderable": false }, 
				{ "targets": 1, "orderable": false },
				{ "targets": 2, "orderable": false },
				{ "targets": 3, "orderable": false },
				{ "targets": 4, "orderable": false },
				{ "targets": 5, "orderable": false },
			],
			"ajax":{
				data : function(d) {},
				url: ajaxurl + '?action=front_proposal_wishlist_filter&status='+wishlistStatus,
				type: "post", 
				error: function(){ 
					jQuery(".datatable-error").html("");
					jQuery("#datatable").append('<tbody class="datatable-error"><tr><th colspan="5">No data found in the server</th></tr></tbody>');
					jQuery("#datatable_processing").css("display","none");
				}
			},
			//"searching" : false,
			language: {
				searchPlaceholder: "Project Title"
			}
		} );
		
		jQuery(document).on('click', '.add-to-wishlist', function(){
			var id = jQuery(this).data('id');
			var action = jQuery(this).data('action');
			var postData = new FormData();
            postData.append('action', 'corporate_wishlist_' + action);
            postData.append('proposal_id', id);
            postData.append('corporate_id', corporate_id);
			$.ajax({
                url: ajaxurl,
				type: "POST",
                processData: false,
                contentType: false,
                cache: false,
                data: postData,
                success: function (response) {
					var json = $.parseJSON(response);
					if (json['error'] == 'Exist') {
                        jQuery('.display_alert').html('<div class="alert alert-danger alert-dismissable"><strong>Error! </strong> You have already added this to wishlist.</div>');
                    }
                    if (json['success'] == 'Added') {
                        jQuery('.display_alert').html('<div class="alert alert-success alert-dismissable"><strong>Success! </strong> Proposal added to wishlist successfully.</div>');
						jQuery('.wishlist-btn-'+id).data('action', 'remove');
						jQuery('.wishlist-btn-'+id).html('Remove');
                    }					
                    if (json['success'] == 'Removed') {
                        jQuery('.display_alert').html('<div class="alert alert-success alert-dismissable"><strong>Success! </strong> Proposal removed from wishlist successfully.</div>');
						setTimeout(function(){
							location.reload();
						}, 1000);
                    }										
                    if (json['success'] == 'Nodata') {
                        jQuery('.display_alert').html('<div class="alert alert-danger alert-dismissable"><strong>Error! </strong> You have already removed this from wishlist.</div>');
                    }
					
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                    return false;
                },
            });

		});
		
	});


</script>
