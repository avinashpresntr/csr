<?php
	global $wpdb;
	echo '<link rel="stylesheet" type="text/css" href="'.plugins_url().'/datatables/jquery.dataTables.min.css" />';
	echo '<script type="text/javascript" src="'.plugins_url().'/datatables/jquery.dataTables.min.js"></script>';
	//echo '<script type="text/javascript" src="'.plugins_url().'/datatables/dataTables.bootstrap.min.js"></script>';
?>
<style>
.label {
    display: inline;
    padding: .2em .6em .3em;
    font-size: 75%;
    font-weight: 700;
    line-height: 1;
    color: #fff;
    text-align: center;
    white-space: nowrap;
    vertical-align: baseline;
    border-radius: .25em;
}
.alert-success,
.alert-danger,
.alert-error,
.alert-warning,
.alert-info,
.label-danger,
.label-info,
.label-warning,
.label-primary,
.label-success {
  color: #fff !important;
}
.alert-danger,
.alert-error,
.label-danger {
  background-color: #dd4b39 !important;
}
.alert-warning,
.label-warning {
  background-color: #f39c12 !important;
}
.alert-info,
.label-info {
  background-color: #00c0ef !important;
}
.label-primary {
  background-color: #3c8dbc !important;
}
.alert-success,
.label-success {
  background-color: #00a65a !important;
}
.text-info{
	color: #00c0ef !important;
}
.text-success{
	color: #00a65a !important;
}
.text-danger{
	color: #dd4b39 !important;
}
</style>
<link href="<?php echo plugins_url(); ?>/csr-bank/css/csr_bank_custom.css" rel="stylesheet" type="text/css" />
<link href="<?php echo plugins_url(); ?>/select2/select2.css" rel="stylesheet" type="text/css" />
<script src="<?php echo plugins_url(); ?>/select2/select2.min.js" type="text/javascript"></script>
<div class="wrap">
	<h1>
		Trashed NGO
		<ul class="subsubsub pull-right" style="margin: 0px;">
			<li class=""><a href="?page=ngo" class="text-success">NGO List</a></li>
		</ul>
	</h1>
	<br>
	<div class="">
		<?php 
			if(isset($_SESSION['ngo_restored'])){
				echo '<div id="message" class="updated notice notice-success is-dismissible"><p>NGO Restored.</p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div>';
				unset($_SESSION['ngo_restored']);
			}
			
			if(isset($_SESSION['ngo_deleted'])){
				echo '<div id="message" class="updated notice notice-success is-dismissible"><p>NGO Deleted.</p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div>';
				unset($_SESSION['ngo_deleted']);
			}
		?>
	</div>
	<div>
		<form id="search_ngo" method="post" action="" class="form-inline" role="form" >
			<div class="form-group search_select_div mr10">
				<label class="sr-only" for="operation_scale_id">Scale of Operation</label>
				<select name="operation_scale_id" id="operation_scale_id" class="form-control select2 w200" >
					<option value=""> - All Scale of Operation - </option>
					<?php
						$table_name = $wpdb->prefix . "operation_scale";
						$res = $wpdb->get_results("SELECT * FROM $table_name WHERE `isDelete` = 0"); 
						foreach($res as $row){
							echo '<option value="'.$row->id.'" >'.$row->operation_scale.'</option>';
						}
					?>
				</select>
			</div>
			<div class="form-group search_select_div mr10">
				<label class="sr-only" for="organisation_type_id">Type of Organisation</label>
				<select name="organisation_type_id" id="organisation_type_id" class="form-control select2 w200" >
					<option value=""> - All Type of Organisation - </option>
					<?php
						$table_name = $wpdb->prefix . "organisation_type";
						$res = $wpdb->get_results("SELECT * FROM $table_name WHERE `isDelete` = 0"); 
						foreach($res as $row){
							echo '<option value="'.$row->id.'" >'.$row->type_name.'</option>';
						}
					?>
				</select>
			</div>
			<div class="form-group search_select_div mr10">
				<label class="sr-only" for="thematic_areas_work_id">Thematic areas Work</label>
				<select name="thematic_areas_work_id" id="thematic_areas_work_id" class="form-control select2 w200" >
					<option value=""> - All Thematic areas Work - </option>
					<?php
						$table_name = $wpdb->prefix . "thematic_areas_work";
						$res = $wpdb->get_results("SELECT * FROM $table_name WHERE `isDelete` = 0"); 
						foreach($res as $row){
							echo '<option value="'.$row->id.'" >'.$row->thematic_area.'</option>';
						}
					?>
				</select>
			</div>
			<div class="form-group search_select_div mr10">
				<label class="sr-only" for="work_location">Work Locations</label>
				<select name="work_location" id="work_location" class="form-control select2 w200" >
					<option value=""> - All Work Locations - </option>
					<?php
						$table_name = $wpdb->prefix . "work_locations";
						$res = $wpdb->get_results("SELECT * FROM $table_name WHERE `isDelete` = 0"); 
						foreach($res as $row){
							echo '<option value="'.$row->id.'" >'.$row->location_name.'</option>';
						}
					?>
				</select>
			</div>
			<div class="form-group search_select_div mr10 w150">
				<label class="sr-only" for="ngo_status">Status</label>
				<select name="ngo_status" id="ngo_status" class="form-control select2 w150" >
					<option value=""> - All Status - </option>
					<option value="Rejected" > Rejected </option>
					<option value="Applicant" > Applicant </option>
					<option value="Empanelled" > Empanelled </option>
					<option value="Empanelment Live - Applied for Renewal" > Empanelment Live - Applied for Renewal </option>
					<option value="Expired - Applied for Renewal" > Expired - Applied for Renewal </option>
					<option value="expired_renewal_pending" > Expired – Renewal Pending </option>
					<option value="Field_Visit" > Field Visit </option>
				</select>
			</div>
			<input type="submit" name="search" class="button button-medium" value="Search" />
		</form>
	</div>
	<table id="ngo_table" width="100%" class="widefat dataTable">
		<thead>
			<tr>
				<th>Sr. No.</th>
				<th>Organisation Name</th>
				<th>HUB Code</th>
				<th>Status</th>
				<th>Date</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>
<script type="text/javascript">
	var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
	jQuery(document).ready( function(){
		jQuery('.select2').select2();
		
		dataTable = jQuery('#ngo_table').DataTable( {
			"processing": true,
			"serverSide": true,
			"pageLength": 25,
			"columnDefs": [ 
				{ "targets": 0, "orderable": false }, 
				{ "targets": 1, "orderable": false },
				{ "targets": 2, "orderable": false },
				{ "targets": 3, "orderable": false },
				{ "targets": 4, "orderable": false },
			],
			"ajax":{
				data : function(d) {},
				url: ajaxurl + '?action=ngo_filter_list&ngo_type=trashed',
				type: "post", 
				error: function(){ 
					jQuery(".datatable-error").html("");
					jQuery("#datatable").append('<tbody class="datatable-error"><tr><th colspan="5">No data found in the server</th></tr></tbody>');
					jQuery("#datatable_processing").css("display","none");
				}
			},
			//"searching" : false,
			language: {
				searchPlaceholder: "Organisation & HUB Code"
			}
		} );
		
		jQuery(document).on('submit', '#search_ngo', function(){
			dataTable.destroy();
			var ngo_status = jQuery('#ngo_status').val();
			var work_location = jQuery('#work_location').val();
			var operation_scale_id = jQuery('#operation_scale_id').val();
			var thematic_areas_work_id = jQuery('#thematic_areas_work_id').val();
			var organisation_type_id = jQuery('#organisation_type_id').val();
			dataTable = jQuery('#ngo_table').DataTable( {
				"processing": true,
				"serverSide": true,
				"pageLength": 25,
				"columnDefs": [ 
					{ "targets": 0, "orderable": false }, 
					{ "targets": 1, "orderable": false },
					{ "targets": 2, "orderable": false },
					{ "targets": 3, "orderable": false },
					{ "targets": 4, "orderable": false },
				],
				"ajax":{
					data : function(d) {},
					url: ajaxurl + '?action=ngo_filter_list&ngo_type=trashed&ngo_status=' + ngo_status + '&work_location=' + work_location + '&operation_scale_id=' + operation_scale_id + '&organisation_type_id=' + organisation_type_id + '&thematic_areas_work_id=' + thematic_areas_work_id,
					type: "post", 
					error: function(){ 
						jQuery(".datatable-error").html("");
						jQuery("#datatable").append('<tbody class="datatable-error"><tr><th colspan="5">No data found in the server</th></tr></tbody>');
						jQuery("#datatable_processing").css("display","none");
					}
				},
				//"searching" : false,
				language: {
					searchPlaceholder: "Organisation & HUB Code"
				}
			} );
			return false;
		});
	});
	
	function ngo_restore(ngo_id){
		if (confirm('Are you sure, You want to restore this record?')) {
			var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
			jQuery.ajax({
				url: ajaxurl,
				data : {action: "ngo_restore", ngo_id: ngo_id },
				success: function(response){
					if (response == 'ngo_restored') {
						jQuery("html, body").animate({ scrollTop: 0 }, "slow");
						window.location.reload();
					}
					return false;
				},
			});
		}
	}
	
	
	function ngo_delete(ngo_id){
		if (confirm('Are you sure, You want to delete this record?')) {
			var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
			jQuery.ajax({
				url: ajaxurl,
				data : {action: "ngo_delete", ngo_id: ngo_id },
				success: function(response){
					if (response == 'ngo_deleted') {
						jQuery("html, body").animate({ scrollTop: 0 }, "slow");
						window.location.reload();
					}
					return false;
				},
			});
		}
	}
</script>
