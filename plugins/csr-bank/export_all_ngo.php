<?php
	include_once('../../../wp-config.php');
	global $wpdb; 
	$sql = "SELECT `csr_ngo`.`id`, `csr_ngo`.`organisation_name`, `csr_ngo`.`hub_code`, `csr_ngo`.`ngo_status`, `csr_ngo`.`created_date`, `csr_ngo`.`empanelled_start_date`, `csr_ngo`.`empanelled_end_date`, `csr_ngo`.`rejected_date`, ";
	$sql .= " `csr_ngo`.`l1_status`, `csr_ngo`.`l2_status`, `csr_ngo`.`l3_status`, `csr_ngo`.`l4_status`, `csr_ngo`.`l5_status` ";
	$sql .= " FROM `csr_ngo` ";
	$sql .= " LEFT JOIN `csr_ngo_work_locations` ON `csr_ngo`.`id`=`csr_ngo_work_locations`.`ngo_id` ";
	$sql .= " LEFT JOIN `csr_ngo_thematic_areas_works` ON `csr_ngo`.`id`=`csr_ngo_thematic_areas_works`.`ngo_id` ";
	$sql .= " WHERE `csr_ngo`.`isTrash`= 0 ";
	$sql .= " GROUP BY `csr_ngo_work_locations`.`ngo_id` ";
	$sql .= " ORDER BY `csr_ngo`.`created_date` DESC ";
	$result = $wpdb->get_results($sql);
	$output = '';
	// Get The Field Name
	$output .= '"Sr. No.","Organisation Name","HUB Code","NGO Status"';
	$output .="\n";

	$inc = '1';
	foreach($result as $row){
		$output .='"'.$inc.' ",';
		$output .='"'.$row->organisation_name.' ",';
		$output .='"'.$row->hub_code.' ",';
		$output .= str_replace('_', ' ', $row->ngo_status);
		$output .="\n";
		$inc++;
	}
	
	// Download the file
	$filename = "NGO_list_".date('d_m_Y H_i_s').".csv";
	header('Content-type: application/csv');
	header('Content-Disposition: attachment; filename='.$filename);
	echo $output;
	exit;
?>
