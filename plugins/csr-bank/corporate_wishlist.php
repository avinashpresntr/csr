<?php
	global $wpdb;
	
	$corporate_id = (isset($_REQUEST['id']) && is_numeric($_REQUEST['id']) && $_REQUEST['id'] > 0)? $_REQUEST['id'] : '0';
	
	echo '<link rel="stylesheet" type="text/css" href="'.plugins_url().'/datatables/jquery.dataTables.min.css" />';
	echo '<script type="text/javascript" src="'.plugins_url().'/datatables/jquery.dataTables.min.js"></script>';
	//echo '<script type="text/javascript" src="'.plugins_url().'/datatables/dataTables.bootstrap.min.js"></script>';
?>
<style>
.alert {
  padding: 5px 15px;
  margin-bottom: 20px;
  border: 1px solid transparent;
  border-radius: 4px;
}
.alert h4 {
  margin-top: 0;
  color: inherit;
}
.alert .alert-link {
  font-weight: bold;
}
.alert > p,
.alert > ul {
  margin-bottom: 0;
}
.alert > p + p {
  margin-top: 5px;
}
.alert-dismissable,
.alert-dismissible {
  padding-right: 35px;
}
.alert-dismissable .close,
.alert-dismissible .close {
  position: relative;
  top: -2px;
  right: -21px;
  color: inherit;
}
.alert-success {
  color: #3c763d;
  background-color: #dff0d8;
  border-color: #d6e9c6;
}
.alert-success hr {
  border-top-color: #c9e2b3;
}
.alert-success .alert-link {
  color: #2b542c;
}
.alert-info {
  color: #31708f;
  background-color: #d9edf7;
  border-color: #bce8f1;
}
.alert-info hr {
  border-top-color: #a6e1ec;
}
.alert-info .alert-link {
  color: #245269;
}
.alert-warning {
  color: #8a6d3b;
  background-color: #fcf8e3;
  border-color: #faebcc;
}
.alert-warning hr {
  border-top-color: #f7e1b5;
}
.alert-warning .alert-link {
  color: #66512c;
}
.alert-danger {
  color: #a94442;
  background-color: #f2dede;
  border-color: #ebccd1;
}
.alert-danger hr {
  border-top-color: #e4b9c0;
}
.alert-danger .alert-link {
  color: #843534;
}
</style>

<link href="<?php echo plugins_url(); ?>/csr-bank/css/csr_bank_custom.css" rel="stylesheet" type="text/css" />
<link href="<?php echo plugins_url(); ?>/select2/select2.css" rel="stylesheet" type="text/css" />
<script src="<?php echo plugins_url(); ?>/select2/select2.min.js" type="text/javascript"></script>
<div class="wrap">
	<h1><a href="javascript:void(0);" onclick="window:history.back();" class="button button-primary button-medium"><i class="fa fa-angle-double-left"></i> Back</a> Corporate Proposal Wishlist/Confirmed</h1>
	<br>
	<div class="row"><div class="col-sm-12 display_alert"></div></div>
	<?php
		if(isset($_SESSION['msg'])){
			echo '<div class="updated" style="padding:5px 20px;"><b>'.$_SESSION['msg'].'</b></div>';
			unset($_SESSION['msg']);
		}
	?>
	<table id="corporate_table" width="100%" class="widefat dataTable">
		<thead>
			<tr>
				<th>Sr. No.</th>
				<th>Corporate Organisation Name</th>
				<th>Project Title</th>
				<th>Hubcode</th>
				<th width="300">Summary</th>
				<th>Added Date</th>
				<th>Actions</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>
<script type="text/javascript">
	var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
	jQuery(document).ready( function(){
		jQuery('.select2').select2();
		var corporate_id = '<?php echo $corporate_id; ?>';
		dataTable = jQuery('#corporate_table').DataTable( {
			"processing": true,
			"serverSide": true,
			"pageLength": 25,
			"columnDefs": [ 
				{ "targets": 0, "orderable": false }, 
				{ "targets": 1, "orderable": false },
				{ "targets": 2, "orderable": false },
				{ "targets": 3, "orderable": false },
				{ "targets": 4, "orderable": false },
				{ "targets": 5, "orderable": false },
				{ "targets": 6, "orderable": false },
			],
			"ajax":{
				data : function(d) {},
				url: ajaxurl + '?action=corporate_wishlist_filter_list&corporate_id=' + corporate_id,
				type: "post", 
				error: function(){ 
					jQuery(".datatable-error").html("");
					jQuery("#datatable").append('<tbody class="datatable-error"><tr><th colspan="5">No data found in the server</th></tr></tbody>');
					jQuery("#datatable_processing").css("display","none");
				}
			},
			//"searching" : false,
			language: {
				searchPlaceholder: "Project Title"
			}
		} );
					
		jQuery(document).on('click', '.add-to-wishlist', function(){
			var id = jQuery(this).data('id');
			var action = jQuery(this).data('action');
			var postData = new FormData();
            postData.append('action', 'corporate_wishlist_' + action);
            postData.append('id', id);
			jQuery.ajax({
                url: ajaxurl,
				type: "POST",
                processData: false,
                contentType: false,
                cache: false,
                data: postData,
                success: function (response) {
					var json = jQuery.parseJSON(response);
					if (json['error'] == 'Exist') {
                        jQuery('.display_alert').html('<div class="alert alert-danger alert-dismissable"><strong>Error! </strong> You have already added this to wishlist.</div>');
                    }
                    if (json['success'] == 'Added') {
                        jQuery('.display_alert').html('<div class="alert alert-success alert-dismissable"><strong>Success! </strong> Proposal accepted for wishlist successfully.</div>');
						setTimeout(function(){
							location.reload();
						}, 1000);
                    }					
                    if (json['success'] == 'Removed') {
                        jQuery('.display_alert').html('<div class="alert alert-success alert-dismissable"><strong>Success! </strong> Proposal rejected from wishlist successfully.</div>');
						setTimeout(function(){
							location.reload();
						}, 1000);
                    }										
                    if (json['success'] == 'Nodata') {
                        jQuery('.display_alert').html('<div class="alert alert-danger alert-dismissable"><strong>Error! </strong> You have already removed this from wishlist.</div>');
                    }
					
                    //jQuery("html, body").animate({ scrollTop: 0 }, "slow");
                    return false;
                },
            });

		});
		
	
	});
</script>
