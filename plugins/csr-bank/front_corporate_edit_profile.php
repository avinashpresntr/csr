<?php 
	global $wpdb;
	if(isset($_SESSION['corporate_login_id']) && $_SESSION['corporate_login_id'] > 0){
		wp_redirect(site_url().'/corporate-account/');
	}
	
	$corporate_id = $user_data->corporate_id;
	$corporate_result = $wpdb->get_results("SELECT * FROM `csr_corporate_partner` WHERE `id` = '" . $corporate_id . "' " ); 
	$corporate_result = $corporate_result[0];
	$wlids_arr = array();
	$work_locations_arr = array();
	$wlids_result = $wpdb->get_results("SELECT `work_locations_id` FROM `csr_corporate_work_locations` WHERE `corporate_id` = '" . $corporate_id . "' ");
	foreach($wlids_result as $wlids_row){
		$wlids_arr[] = $wlids_row->work_locations_id;
	}
	$wlids = implode(', ', $wlids_arr);
	$work_locations_result = $wpdb->get_results("SELECT * FROM `csr_work_locations` WHERE `id` IN ( ".$wlids." ) " ); 
	foreach($work_locations_result as $work_locations_row){
		$work_locations_arr[$work_locations_row->id] = $work_locations_row->location_name;
	}
	
	$corporate_contact_result = $wpdb->get_results("SELECT * FROM `csr_corporate_contact` WHERE `corporate_id` = '" . $corporate_id . "' " ); 
?>
<link href="<?php echo plugins_url(); ?>/csr-bank/css/csr_bank_custom.css" rel="stylesheet" type="text/css" />
<link href="<?php echo plugins_url(); ?>/select2/select2.css" rel="stylesheet" type="text/css" />
<script src="<?php echo plugins_url(); ?>/select2/select2.min.js" type="text/javascript"></script>
<style>
/*#s2id_autogen1{
	width: 210px !important;
}
.select2-container{
	width: 220px !important;
}*/
.alert {
  padding: 5px 15px;
  margin-bottom: 20px;
  border: 1px solid transparent;
  border-radius: 4px;
}
.alert h4 {
  margin-top: 0;
  color: inherit;
}
.alert .alert-link {
  font-weight: bold;
}
.alert > p,
.alert > ul {
  margin-bottom: 0;
}
.alert > p + p {
  margin-top: 5px;
}
.alert-dismissable,
.alert-dismissible {
  padding-right: 35px;
}
.alert-dismissable .close,
.alert-dismissible .close {
  position: relative;
  top: -2px;
  right: -21px;
  color: inherit;
}
.alert-success {
  color: #3c763d;
  background-color: #dff0d8;
  border-color: #d6e9c6;
}
.alert-success hr {
  border-top-color: #c9e2b3;
}
.alert-success .alert-link {
  color: #2b542c;
}
.alert-info {
  color: #31708f;
  background-color: #d9edf7;
  border-color: #bce8f1;
}
.alert-info hr {
  border-top-color: #a6e1ec;
}
.alert-info .alert-link {
  color: #245269;
}
.alert-warning {
  color: #8a6d3b;
  background-color: #fcf8e3;
  border-color: #faebcc;
}
.alert-warning hr {
  border-top-color: #f7e1b5;
}
.alert-warning .alert-link {
  color: #66512c;
}
.alert-danger {
  color: #a94442;
  background-color: #f2dede;
  border-color: #ebccd1;
}
.alert-danger hr {
  border-top-color: #e4b9c0;
}
.alert-danger .alert-link {
  color: #843534;
}

</style>

	<h4 class="hndle ui-sortable-handle text-center" style="padding: 5px 10px; margin: 0;text-align:center; "><span>Edit Profile</span></h4>
	<div class="display_alert">
		<?php 
			if(isset($_SESSION['corporate_updated'])){
				echo '<div class="alert alert-success alert-dismissable"><strong>Success! </strong> Corporate updated successfully.</div>';
				unset($_SESSION['corporate_updated']);
			}
		?>
	</div>

	<form id="update_corporate" class="" action="" method="post" enctype="multipart/form-data" >
		<input type="hidden" name="corporate_id" id="corporate_id" value="<?php echo $corporate_result->id; ?>">
		<div class="custom_form">
		<p>
			<label for="organisation_name">Name of the organisation*:<br/>
				<span class="">
					<input type="text" name="organisation_name" id="organisation_name" value="<?php echo $corporate_result->organisation_name; ?>" class="" required >
				</span>
			</label>
		</p>
		<p>
			<label for="organisation_name">Type of Corporate*:<br/>
				<span class="">
					<select class="form-dropdown select2" id="corporate_type_id" name="corporate_type_id" required >
						<?php
							$table_name = $wpdb->prefix . "corporate_type";
							$res = $wpdb->get_results("select * from $table_name where isDelete=0"); 
							foreach($res as $row){
								echo '<option value="'.$row->id.'" ';
								if( $corporate_result->corporate_type_id == $row->id ) { echo ' Selected '; }
								echo ' >'.$row->type_name.'</option>';
							}
						?>
					</select>
				</span>
			</label>
		</p>
		
		<p>			
			<label for="address1">Address 1*:<br/>
				<span class="">
					<input type="text" name="address1" id="address1" value="<?php echo $corporate_result->address1; ?>" class="" required >
				</span>
			</label>
		</p>
		
		<p>			
			<label for="address2">Address 2*:<br/>
				<span class="">
					<input type="text" name="address2" id="address2" value="<?php echo $corporate_result->address2; ?>" class="" required >
				</span>
			</label>
		</p>
		
		<p>			
			<label for="zip_code">Zip Code*:<br/>
				<span class="">										
					<input type="number" name="zip_code" id="zip_code" value="<?php echo $corporate_result->zip_code; ?>" class="" required >
				</span>
			</label>
		</p>
		
		<p>			
			<label for="work_location">Locations of work: ( State / District / City )*:<br/>
				<span class="">		
					<input type="hidden" name="work_location" id="work_locations" required />
					<ul class="parsley-errors-list filled tag_required"  style="display: none;"><li class="parsley-required">This value is required.</li></ul>
				</span>
			</label>
		</p>
		
		<p>			
			<label for="company_logo">Company Logo*:<br/>
				<span class="">
					<input type="file" name="company_logo" id="company_logo" class="" />
					<?php $filepath = home_url()."/wp-content/uploads/corporate_company_logo/"; ?>
					<?php if(!empty($corporate_result->company_logo)){ ?>
					<img src="<?php echo $filepath.$corporate_result->company_logo ; ?>" style="border: 1px solid #999; padding: 5px; width:100px;" />
					<?php } else { echo '<span style="border: 1px solid #999; padding: 5px; width:100px; ">N/A</span>'; } ?>
				</span>
			</label>
		</p>
		
		</div>
		<p>			
			<label for="">Multiple contact entries*:<br/>
				<span class="">					
					<table>
					   <thead>
						  <tr>
							 <th>Name</th>
							 <th>Designation</th>
							 <th>Email Id</th>
							 <th>Phone Number</th>
							 <th width="50">Main contact person</th>
						  </tr>
						</thead>
						<tbody id="addmore_contact">
						<?php 
							$inc = 0; 
							foreach($corporate_contact_result as $corporate_contact_row){ 
						?>
							<tr class="corporat_contact">
								<td><input type="text" name="contact_name[<?php echo $inc; ?>]" value="<?php echo $corporate_contact_row->contact_name; ?>" required placeholder="Enter Name"></td>
								<td><input type="text" name="contact_desc[<?php echo $inc; ?>]" value="<?php echo $corporate_contact_row->contact_desc; ?>" required placeholder="Enter Designation"></td>
								<td><input type="email" name="contact_email_id[<?php echo $inc; ?>]" value="<?php echo $corporate_contact_row->contact_email_id; ?>" required placeholder="Enter Email id"></td>
								<td><input type="number" name="contact_phone_number[<?php echo $inc; ?>]" value="<?php echo $corporate_contact_row->contact_phone_number; ?>" required placeholder="Enter Mob No." data-parsley-minlength="10" data-parsley-maxlength="10" ></td>
								<td width="50">
									<input type="checkbox" name="main_person[<?php echo $inc; ?>]" id="main_person" value="1" <?php if($corporate_contact_row->main_person == 1){ echo 'Checked'; } ?> >
									<div class="remove_contact" style="<?php if($inc == 0){ echo 'visibility: hidden;'; } ?> float: right; margin: 0px 0px 0px 20px;"><input type="button" class="button button-medium" style="background: #e8e8e8;" value="x" /></div>
								</td>
							</tr>
						<?php $inc++; } ?>
						</tbody>
					</table>
					<div><input type="button" class="button button-primary button-large addnew" style="float: right; margin-right: 3px;" value="+"></div>
				</span>
			</label>
		</p>

		<p class="custom_form" style="text-align:center;">
			<input type="submit" value="Save" class="gem-button gem-button-size-small gem-button-style-outline gem-button-text-weight-normal gem-button-border-2 submit_btn">
		</p>

	</form>



<script type="text/javascript">
	jQuery('.tag_required').hide();
	var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
	jQuery('.select2').select2();
	tag_load();
    jQuery(document).ready(function () {

		jQuery(document).on('click', '.remove_contact', function () {
            jQuery(this).closest('tr.corporat_contact').remove();
        });
		jQuery(document).on('click', '.addnew', function () {
			var section_index = jQuery('tr.corporat_contact').length;
			var newDiv = jQuery(".corporat_contact:first").clone()
				.find(':input')
				.each(function () {
					this.name = this.name.replace(/\[(\d+)\]/, function (str) {
						return '[' + section_index + ']';
					});
				})
				.end()
				.appendTo("#addmore_contact");
			newDiv.find('.remove_contact').css('visibility', 'visible');
			newDiv.find('input[type=checkbox]:checked').prop('checked', false);
			newDiv.find('input[type=text]').val('');
			newDiv.find('input[type=email]').val('');
			newDiv.find('input[type=number]').val('');
		});
		
		jQuery(document).on('submit', '#update_corporate', function () {
			
			if(jQuery('input[type=checkbox]:checked').length <= 0){
				jQuery('.display_alert').html('<div class="alert alert-danger alert-dismissable"><strong>Error! </strong> Please select one main person.</div>');
				jQuery("html, body").animate({ scrollTop: 0 }, "slow");
				return false;
			}
			
			if(jQuery('input[type=checkbox]:checked').length > 1){
				jQuery('.display_alert').html('<div class="alert alert-danger alert-dismissable"><strong>Error! </strong> Only one main person allowed.</div>');
				jQuery("html, body").animate({ scrollTop: 0 }, "slow");
				return false;
			}
			
            var work_locations = jQuery('#work_locations').val();
            if(work_locations == ''){
                jQuery('.tag_required').show();
                return false;
            }
            var postData = new FormData(this);
            postData.append('action', 'update_corporate');
            jQuery.ajax({
                url: ajaxurl,
				type: "POST",
                processData: false,
                contentType: false,
                cache: false,
                data: postData,
                success: function (response) {
					var json = jQuery.parseJSON(response);					
                    if (json['error'] == 'Exist') {
                        jQuery('.display_alert').html('<div class="alert alert-danger alert-dismissable"><strong>Error! </strong> Corporate Already Exist.</div>');
                    }					
                    if (json['error'] == 'CotnactExist') {
                        jQuery('.display_alert').html('<div class="alert alert-danger alert-dismissable"><strong>Error! </strong> Corporate Contact Already Exist.</div>');
                    }					
                    if (json['success'] == 'Updated') {
						location.reload();
						return false;
						jQuery('.display_alert').html('<div class="alert alert-success alert-dismissable"><strong>Success! </strong> Corporate updated successfully.</div>');
                        jQuery('#update_corporate')[0].reset();
                        jQuery('.select2-search-choice-close').click();
                    }
                    jQuery("html, body").animate({ scrollTop: 0 }, "slow");
                    tag_load();
                    return false;
                },
            });
            return false;
        });
		
	});
	
	function tag_load(){
		jQuery.ajax({
            url: ajaxurl,
			dataType : "JSON",
			data : {action: "get_work_locations"},
            success: function(result){
				jQuery("#work_locations").select2({
                    createSearchChoice:function(term, data) { if (jQuery(data).filter(function() { return this.text.localeCompare(term)===0; }).length===0) {return {id:term, text:term};} },
                    multiple: true,
                    <?php if(isset($corporate_id)){ ?>
                    initSelection: function (element, callback) {
                        callback(jQuery.map(element.val().split(','), function (id) {
                            return { id: id, text: id };
                        }));
                    },
                    <?php } ?>
                    //maximumSelectionSize: 1,
                    data: result,
                });
                <?php if(isset($corporate_id)){ ?>
                    <?php if(!empty($work_locations_arr)){ ?>
                        jQuery("#work_locations").select2('val', ["<?php echo implode('","', $work_locations_arr); ?>"], true);
                    <?php } ?>
                <?php } ?>
                function log(e) {
                    var e=jQuery("<li>"+e+"</li>");
                    jQuery("#events_11").append(e);
                    e.animate({opacity:1}, 10000, 'linear', function() { e.animate({opacity:0}, 2000, 'linear', function() {e.remove(); }); });
                }
                jQuery("#work_locations")
                    .on("change", function(e) { log("change "+JSON.stringify({val:e.val, added:e.added, removed:e.removed})); })
                    .on("select2-opening", function() { log("opening"); })
                    .on("select2-open", function() { log("open"); })
                    .on("select2-close", function() { log("close"); })
                    .on("select2-highlight", function(e) { log ("highlighted val="+ e.val+" choice="+ JSON.stringify(e.choice));})
                    .on("select2-selecting", function(e) { log ("selecting val="+ e.val+" choice="+ JSON.stringify(e.choice));})
                    .on("select2-removing", function(e) { log ("removing val="+ e.val+" choice="+ JSON.stringify(e.choice));})
                    .on("select2-removed", function(e) { log ("removed val="+ e.val+" choice="+ JSON.stringify(e.choice));})
                    .on("select2-loaded", function(e) { log ("loaded (data property omitted for brevity)");})
                    .on("select2-focus", function(e) { log ("focus");})
                    .on("select2-blur", function(e) { log ("blur");});
            }
        });
    }

	jQuery(window).load(function(){
		jQuery(document).on('change', 'input[type=checkbox]', function() {
			jQuery('input[type=checkbox]:checked').not(this).prop('checked', false);
		});
	});
</script>
