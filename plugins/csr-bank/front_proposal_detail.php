<?php
	global $wpdb;
	if(isset($_GET['page']) && $_GET['page'] == 'proposal-detail' && isset($_GET['proposal_id']) && $_GET['proposal_id'] > 0):
	
		$proposal_id = $_GET['proposal_id'];
		$query = "	SELECT cp.*,cn.organisation_name,cn.postal_address,cn.person_firstname,cn.person_lastname,cn.email_id,cn.phone_number,cn.mobile_number,cn.ngo_status
					FROM `csr_proposal` cp
					LEFT JOIN `csr_ngo` cn ON cn.hub_code = cp.hub_code
					WHERE cp.id = '".$proposal_id."' LIMIT 1		
				";
		$proposal_detail = $wpdb->get_results($query); 
		if(is_array($proposal_detail) && count($proposal_detail) > 0):
			$proposal_detail = $proposal_detail[0];						
			
			// thematic_area selection box
			$tlids_arr = array();
			$thematic_areas_works_arr = array();
			$tlids_result = $wpdb->get_results("SELECT `thematic_areas_id` FROM `csr_proposal_thematic_areas_work` WHERE `proposal_id` = '" . $proposal_id . "' ");
			foreach($tlids_result as $tlids_row){
				$tlids_arr[] = $tlids_row->thematic_areas_id;
			}
			$tlids = implode(', ', $tlids_arr);
			$thematic_areas_work_result = $wpdb->get_results("SELECT * FROM `csr_thematic_areas_work` WHERE `id` IN ( ".$tlids." ) " ); 
			foreach($thematic_areas_work_result as $thematic_row){
				$thematic_areas_works_arr[$thematic_row->id] = str_replace(',', ' ', $thematic_row->thematic_area);
			}
			
			//work locations array		
			$wlids_arr = array();
			$work_locations_arr = array();
			$wlids_result = $wpdb->get_results("SELECT `work_locations_id` FROM `csr_proposal_work_locations` WHERE `proposal_id` = '" . $proposal_id . "' ");
			foreach($wlids_result as $wlids_row){
				$wlids_arr[] = $wlids_row->work_locations_id;
			}
			$wlids = implode(', ', $wlids_arr);
			$work_locations_result = $wpdb->get_results("SELECT * FROM `csr_work_locations` WHERE `id` IN ( ".$wlids." ) " ); 
			foreach($work_locations_result as $work_locations_row){
				$work_locations_arr[$work_locations_row->id] = $work_locations_row->location_name;
			}
			
			// tags array
			$tags_arr = array();
			$tagids_arr = array();
			$tagids_result = $wpdb->get_results("SELECT `tag_id` FROM `csr_proposal_tags` WHERE `proposal_id` = '" . $proposal_id . "' ");
			foreach($tagids_result as $tagids_row){
				$tagids_arr[] = $tagids_row->tag_id;
			}
			$tagids = implode(', ', $tagids_arr);
			$tags_result = $wpdb->get_results("SELECT * FROM `csr_tag` WHERE `id` IN ( ".$tagids." ) " ); 
			foreach($tags_result as $tags_row){
				$tags_arr[$tags_row->id] = $tags_row->tag_name;
			}
			
?>
			<h4 style="margin: 0px;">Project Title: <?php echo ucfirst($proposal_detail->project_title); ?></h4>
			<hr style="margin: 10px 0px;" />
			<div class="pull-right" style="width:320px; margin-left: 30px;" >
				<h6>NGO Details : </h6>
				<strong>Organisation Name : </strong><?php echo ucfirst($proposal_detail->organisation_name); ?><br/>
				<strong>Address : </strong><?php echo ucfirst($proposal_detail->postal_address); ?><br/>
				<strong>Contact Person : </strong><?php echo ucfirst($proposal_detail->person_firstname) . ' ' . ucfirst($proposal_detail->person_lastname); ?><br/>
				<strong>Email : </strong><?php echo $proposal_detail->email_id; ?><br/>
				<strong>Phone Number : </strong><?php echo $proposal_detail->phone_number; ?><br/>
				<strong>Mobile Number : </strong><?php echo $proposal_detail->mobile_number; ?><br /><br />
			</div>
			<div>
				<h6>Proposal Details : </h6>
				<strong>Work Location : </strong><?php echo implode(', ', $work_locations_arr); ?><br/>
				<strong>Thematic Area : </strong><?php echo implode(', ', $thematic_areas_works_arr); ?><br/>
				<strong>Tags : </strong><?php echo implode(', ', $tags_arr); ?><br/>
				<strong>Proposal Pdf : </strong>
				<?php $filepath = home_url()."/wp-content/uploads/proposal_pdf/"; ?>
				<?php if(!empty($proposal_detail->proposal_pdf)){ ?>
					<a href="<?php echo $filepath.$proposal_detail->proposal_pdf; ?>" download class="button" ><i class="fa fa-download"></i> Download </a> 
				<?php  } else { echo '<span style="border: 1px solid #999; padding: 1px 5px; width:100px; ">N/A</span>'; } ?>
				<br/>	
				<strong>Project Summary : </strong><br/><?php echo nl2br($proposal_detail->project_summary); ?><br/>
			</div>
<?php
		endif;
	endif;	
?>
