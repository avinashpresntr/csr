<?php 
	echo '<link rel="stylesheet" type="text/css" href="'.plugins_url().'/datatables/jquery.dataTables.min.css" />';
	echo '<script type="text/javascript" src="'.plugins_url().'/datatables/jquery.dataTables.min.js"></script>';
?>
<style>
.dataTables_filter input { 
	width: 400px;
    margin-bottom: 10px;
}
.fa-spin-custom {
    -webkit-animation: spin 1000ms infinite linear;
    animation: spin 1000ms infinite linear;
}
@-webkit-keyframes spin {
    0% {
        -webkit-transform: rotate(0deg);
        transform: rotate(0deg);
    }
    100% {
        -webkit-transform: rotate(359deg);
        transform: rotate(359deg);
    }
}
@keyframes spin {
    0% {
        -webkit-transform: rotate(0deg);
        transform: rotate(0deg);
    }
    100% {
        -webkit-transform: rotate(359deg);
        transform: rotate(359deg);
    }
}
</style>
<link href="<?php echo plugins_url(); ?>/select2/select2.css" rel="stylesheet" type="text/css" />
<script src="<?php echo plugins_url(); ?>/select2/select2.min.js" type="text/javascript"></script>

	<div>
		<form id="search_ngo" method="post" action="" style="width:100%;" class="custom_form" role="form" >
			<div class="col-md-4">
				<label class="sr-only" for="thematic_areas_work_id">Thematic areas Work</label>
				<select name="thematic_areas_work_id" id="thematic_areas_work_id" class="form-control select2 w200" >
					<option value=""> - All Thematic areas Work - </option>
					<?php
						$table_name = $wpdb->prefix . "thematic_areas_work";
						$res = $wpdb->get_results("SELECT * FROM $table_name WHERE `isDelete` = 0"); 
						foreach($res as $row){
							echo '<option value="'.$row->id.'" >'.$row->thematic_area.'</option>';
						}
					?>
				</select>
			</div>
			<div class="col-md-4">
				<label class="sr-only" for="work_location">Work Locations</label>
				<select name="work_location" id="work_location" class="form-control select2 w200" >
					<option value=""> - All Work Locations - </option>
					<?php
						$table_name = $wpdb->prefix . "work_locations";
						$res = $wpdb->get_results("SELECT * FROM $table_name WHERE `isDelete` = 0"); 
						foreach($res as $row){
							echo '<option value="'.$row->id.'" >'.$row->location_name.'</option>';
						}
					?>
				</select>
			</div>
			<input type="submit" name="search" class="gem-button gem-button-size-small gem-button-style-outline gem-button-text-weight-normal gem-button-border-2 submit_btn" value="Search" />
		</form>
	</div>
	<div class="clearfix">&nbsp;</div>
<table id="ngo_table" style="width: 100%;">
	<thead>
		<tr>
			<th>Sr. No.</th>
			<th>Organisation Name</th>
			<th>Hub Code</th>
			<th>Contact Person</th>
			<th>Locations of work</th>
			<th>Type</th>
			<th>Thematic Area</th>
		</tr>
	</thead>
	<tbody>		
	</tbody>
</table>


<script type="text/javascript">
	jQuery('.select2').select2();
	var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
	var ngo_table = null;
	jQuery(document).ready( function(){
		ngo_table = jQuery('#ngo_table').DataTable( {
			"processing": true,
			"serverSide": true,
			"pageLength": 25,
			"ordering": false,
			"ajax":{
				data : function(d) {},
				url: ajaxurl + '?action=front_empanelled_ngo_filter',
				type: "post", 
				error: function(){ 
					jQuery(".datatable-error").html("");
					jQuery("#datatable").append('<tbody class="datatable-error"><tr><th colspan="5">No data found in the server</th></tr></tbody>');
					jQuery("#datatable_processing").css("display","none");
				}
			},
			//"searching" : false,
			"language": {
				"searchPlaceholder": "Organisation Name ",
				"processing": "<i class='fa fa-refresh fa-spin-custom'></i>",
				//"sProcessing": "DataTables is currently busy"
			}
		} );
		
		$(".dataTables_filter input")
			.unbind()
			.bind('keyup change', function (e) {
			if (e.keyCode == 13 || this.value == "") {
				ngo_table.search(this.value)
					.draw();
			}
		});
		
		jQuery(document).on('submit', '#search_ngo', function(){
			ngo_table.destroy();
			var work_location = jQuery('#work_location').val();
			var thematic_areas_work_id = jQuery('#thematic_areas_work_id').val();
			ngo_table = jQuery('#ngo_table').DataTable( {
				"processing": true,
				"serverSide": true,
				"pageLength": 25,
				"ordering": false,
				"ajax":{
					data : function(d) {},
					url: ajaxurl + '?action=front_empanelled_ngo_filter&work_location=' + work_location + "&thematic_areas_work_id=" + thematic_areas_work_id,
					type: "post", 
					error: function(){ 
						jQuery(".datatable-error").html("");
						jQuery("#datatable").append('<tbody class="datatable-error"><tr><th colspan="5">No data found in the server</th></tr></tbody>');
						jQuery("#datatable_processing").css("display","none");
					}
				},
				//"searching" : false,
				"language": {
					"searchPlaceholder": "Organisation Name ",
					"processing": "<i class='fa fa-refresh fa-spin-custom'></i>",
					//"sProcessing": "DataTables is currently busy"
				}
			} );
			return false;
		});
	
		
	});

</script>
