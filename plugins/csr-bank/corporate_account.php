<?php 
	global $wpdb;
?>
<style>
.alert {
  padding: 5px 15px;
  margin-bottom: 20px;
  border: 1px solid transparent;
  border-radius: 4px;
}
.alert h4 {
  margin-top: 0;
  color: inherit;
}
.alert .alert-link {
  font-weight: bold;
}
.alert > p,
.alert > ul {
  margin-bottom: 0;
}
.alert > p + p {
  margin-top: 5px;
}
.alert-dismissable,
.alert-dismissible {
  padding-right: 35px;
}
.alert-dismissable .close,
.alert-dismissible .close {
  position: relative;
  top: -2px;
  right: -21px;
  color: inherit;
}
.alert-success {
  color: #3c763d;
  background-color: #dff0d8;
  border-color: #d6e9c6;
}
.alert-success hr {
  border-top-color: #c9e2b3;
}
.alert-success .alert-link {
  color: #2b542c;
}
.alert-info {
  color: #31708f;
  background-color: #d9edf7;
  border-color: #bce8f1;
}
.alert-info hr {
  border-top-color: #a6e1ec;
}
.alert-info .alert-link {
  color: #245269;
}
.alert-warning {
  color: #8a6d3b;
  background-color: #fcf8e3;
  border-color: #faebcc;
}
.alert-warning hr {
  border-top-color: #f7e1b5;
}
.alert-warning .alert-link {
  color: #66512c;
}
.alert-danger {
  color: #a94442;
  background-color: #f2dede;
  border-color: #ebccd1;
}
.alert-danger hr {
  border-top-color: #e4b9c0;
}
.alert-danger .alert-link {
  color: #843534;
}
</style>

<?php if(isset($_SESSION['corporate_login_id']) && $_SESSION['corporate_login_id'] > 0): ?>


<h3 style="margin: 0px 0px 10px;" >Welcome <?php echo ucfirst($_SESSION['corporate_name']); ?></h3>
<?php $user_data = $_SESSION['user_data']; ?>
<div class="row">

	<div class="sidebar col-lg-3 col-md-3 col-sm-12" role="complementary">
		<div class="widget-area">
			<aside class="widget widget_pages widget_flexipages flexipages_widget">
				<ul>
					<li class="page_item current_page_ancestor current_page_parent"><a href="<?php echo site_url(); ?>/corporate-account/" title="Corporate Account">Corporate Account</a>	
						<ul class="children">
							<li class="page_item <?php echo (isset($_GET['page']) && $_GET['page'] == 'edit-profile')? 'current_page_item' : ''; ?>"><a href="<?php echo site_url(); ?>/corporate-account/?page=edit-profile" title="Edit Profile">Edit Profile</a></li>
							<li class="page_item <?php echo (isset($_GET['page']) && $_GET['page'] == 'proposal-bank')? 'current_page_item' : ''; ?>"><a href="<?php echo site_url(); ?>/corporate-account/?page=proposal-bank" title="Proposal Bank">Proposal Bank</a></li>
							<li class="page_item <?php echo (isset($_GET['page']) && $_GET['page'] == 'wishlist')? 'current_page_item' : ''; ?>"><a href="<?php echo site_url(); ?>/corporate-account/?page=wishlist" title="Wishlist">My Wishlist</a></li>
							<li class="page_item <?php echo (isset($_GET['page']) && $_GET['page'] == 'confirmed-projects')? 'current_page_item' : ''; ?>"><a href="<?php echo site_url(); ?>/corporate-account/?page=confirmed-projects" title="Confirmed Projects">Confirmed Projects</a></li>
							<li class="page_item <?php echo (isset($_GET['page']) && $_GET['page'] == 'change-password')? 'current_page_item' : ''; ?>"><a href="<?php echo site_url(); ?>/corporate-account/?page=change-password" title="Change Password">Change Password</a></li>
							<li class="page_item"><a href="<?php echo site_url(); ?>/corporate-account/?page=corporate-logout" class="corporate_logout" title="Mechanism">Logout</a></li>
						</ul>
					</li>
				</ul>
			</aside>
		</div>
	</div>
	<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
		<?php 
			if(isset($_GET['page']) && $_GET['page'] == 'proposal-bank'){
				include("front_proposal.php"); 
			}else if(isset($_GET['page']) && $_GET['page'] == 'proposal-detail'){
				include("front_proposal_detail.php"); 
			}else if(isset($_GET['page']) && $_GET['page'] == 'wishlist'){
				include("front_my_wishlist.php"); 
			}else if(isset($_GET['page']) && $_GET['page'] == 'edit-profile'){
				include("front_corporate_edit_profile.php");
			}else if(isset($_GET['page']) && $_GET['page'] == 'change-password'){
				include("front_corporate_change_password.php");
			}else if(isset($_GET['page']) && $_GET['page'] == 'confirmed-projects'){
				include("front_confirmed_projects.php");
			}else{
				
			}
		?>
	</div>
</div>		

<?php else: ?>

<form id="corporate_login" action="" method="post" class="custom_form" data-parsley-validate novalidate enctype="multipart/form-data" >
	<div class="row"><div class="col-sm-12 display_alert"></div></div>
	<h3>Login to your account.</h3>
	<p>
		<label>Email*:<br>
			<span class="">
				<input type="email" name="email_id" value="" class="" parsley-trigger="change" required >
			</span>
		</label>
	</p>
	<p>
		<label>Password*:<br>
			<span class="">
				<input type="password" name="pass" value="" class="" parsley-trigger="change" required >
			</span>
		</label>
	</p>
	
	<p><input type="submit" value="Submit" class="gem-button gem-button-size-small gem-button-style-outline gem-button-text-weight-normal gem-button-border-2 submit_btn"><span class="ajax-loader"></span></p>
</form>
<script type="text/javascript">

	var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
	jQuery(function() {

		jQuery(document).on('submit', '#corporate_login', function () {
	
            jQuery('.submit_btn').attr('disabled','disabled');
            var postData = new FormData(this);
            postData.append('action', 'corporate_login');
            $.ajax({
                url: ajaxurl,
				type: "POST",
                processData: false,
                contentType: false,
                cache: false,
                data: postData,
                success: function (response) {
					var json = $.parseJSON(response);
					
					if (json['error'] == 'Invalid') {
                        jQuery('.display_alert').html('<div class="alert alert-danger alert-dismissable"><strong>Error! </strong> Invalid login details.</div>');
						 $("html, body").animate({ scrollTop: 0 }, "slow");
                    }
                    if (json['success'] == 'Exist') {
                        //jQuery('.display_alert').html('<div class="alert alert-success alert-dismissable"><strong>Success! </strong> You have logged in successfully.</div>');
                        jQuery('#corporate_login')[0].reset();
						window.location.href = '<?php echo site_url(); ?>/corporate-account/?page=proposal-bank';
                    }
                   
                    $('.submit_btn').removeAttr('disabled','disabled');
                    return false;
                },
            });
            return false;
        });

	});
	
</script>

<?php endif; ?>


<script type="text/javascript">
	/*
	var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
	jQuery(document).on('click', '.corporate_logout', function () {

		$.ajax({
			url: ajaxurl,
			type: "POST",
			processData: false,
			contentType: false,
			cache: false,
			data: {'action': 'corporate_logout'},
			success: function (response) {
				var json = $.parseJSON(response);
				
				if (json['error'] == 'Invalid') {
					jQuery('.display_alert').html('<div class="alert alert-danger alert-dismissable"><strong>Error! </strong> Invalid details.</div>');
				}
				if (json['success'] == 'Exist') {
					jQuery('.display_alert').html('<div class="alert alert-success alert-dismissable"><strong>Success! </strong> You have logged out successfully.</div>');
					setTimeout(function(){
						location.reload();
					}, 1000);
				}
				$("html, body").animate({ scrollTop: 0 }, "slow");
				return false;
			},
		});
		return false;
	});
	*/
</script>

