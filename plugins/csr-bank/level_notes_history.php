<?php 
	global $wpdb;
	if(empty($_GET['id']) || !is_numeric($_GET['id'])){
		wp_redirect(admin_url().'admin.php?page=ngo');
	}
	$ngo_id = $_GET['id'];
	$ngo_result = $wpdb->get_results("SELECT * FROM `csr_ngo` WHERE `id` = '" . $ngo_id . "' " ); 
	$ngo_result = $ngo_result[0];
	
	if(empty($ngo_result)){
		wp_redirect(admin_url().'admin.php?page=ngo');
	}
?>
<div class="wrap">
	<h2 class="hndle ui-sortable-handle" style="padding: 5px 10px; margin: 0; "><a href="?page=ngo&action=ngo_details&id=<?php echo $ngo_id; ?>" class="button button-primary button-medium"> Back </a> &nbsp;<span>Scrutiny Level Notes History</span></h2>
	<div class="postbox" style="padding: 10px;">
		<table id="level_notes_table" width="100%" class="widefat dataTable">
			<thead>
				<tr>
					<th>Sr. No.</th>
					<th>User</th>
					<th>Level</th>
					<th>Status</th>
					<th>Note</th>
					<th>Date</th>
				</tr>
			</thead>
			<tbody>
				<?php $inc = 1;
					$logs_res = $wpdb->get_results("SELECT * FROM `csr_ngo_level_logs` WHERE `ngo_id` = '" . $ngo_id . "' ORDER BY `created_date` DESC " );
					foreach($logs_res as $logs_row){
						$user_info = get_userdata($logs_row->user_id);
						$username = $user_info->user_login;
					?>
						<tr>
							<td><?php echo $inc; ?></td>
							<td><?php echo $username; ?></td>
							<td><?php echo $logs_row->level; ?></td>
							<td><?php echo $logs_row->level_status; ?></td>
							<td><?php echo nl2br($logs_row->level_note); ?></td>
							<td><?php echo date('d/m/Y H:i:s', strtotime($logs_row->created_date)); ?></td>
						</tr>
					<?php
						$inc++;
					}
				?>
			</tbody>
		</table>
	</div>
</div>

<link rel="stylesheet" type="text/css" href="<?php echo plugins_url(); ?>/datatables/jquery.dataTables.min.css" />
<script type="text/javascript" src="<?php echo plugins_url(); ?>/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript">
	var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
    jQuery(document).ready(function () {
		jQuery('#level_notes_table').DataTable({
			"pageLength": 25,
		});
	});
</script>
