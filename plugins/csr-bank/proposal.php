<?php
	global $wpdb;
	echo '<link rel="stylesheet" type="text/css" href="'.plugins_url().'/datatables/jquery.dataTables.min.css" />';
	echo '<script type="text/javascript" src="'.plugins_url().'/datatables/jquery.dataTables.min.js"></script>';
	//echo '<script type="text/javascript" src="'.plugins_url().'/datatables/dataTables.bootstrap.min.js"></script>';
?>
<style>
.label {
    display: inline;
    padding: .2em .6em .3em;
    font-size: 75%;
    font-weight: 700;
    line-height: 1;
    color: #fff;
    text-align: center;
    white-space: nowrap;
    vertical-align: baseline;
    border-radius: .25em;
}
.alert-success,
.alert-danger,
.alert-error,
.alert-warning,
.alert-info,
.label-danger,
.label-info,
.label-warning,
.label-primary,
.label-success {
  color: #fff !important;
}
.alert-danger,
.alert-error,
.label-danger {
  background-color: #dd4b39 !important;
}
.alert-warning,
.label-warning {
  background-color: #f39c12 !important;
}
.alert-info,
.label-info {
  background-color: #00c0ef !important;
}
.label-primary {
  background-color: #3c8dbc !important;
}
.alert-success,
.label-success {
  background-color: #00a65a !important;
}
.text-danger{
	color: #dd4b39 !important;
}
</style>
<link href="<?php echo plugins_url(); ?>/csr-bank/css/csr_bank_custom.css" rel="stylesheet" type="text/css" />
<link href="<?php echo plugins_url(); ?>/select2/select2.css" rel="stylesheet" type="text/css" />
<script src="<?php echo plugins_url(); ?>/select2/select2.min.js" type="text/javascript"></script>
<div class="wrap">
	<h1>
		Proposal
		<ul class="subsubsub pull-right" style="margin: 0px;">
			<li class="trash"><a href="?page=proposal&action=proposal_trashed" class="text-danger">Trash</a></li>
		</ul>
	</h1>
	<br>
	<?php 
		if(isset($_SESSION['proposal_moved'])){
			echo '<div id="message" class="updated notice notice-success is-dismissible"><p>Proposal Moved to Trash.</p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div>';
			unset($_SESSION['proposal_moved']);
		}
	?>
	<div>
		<form id="search_proposal" method="post" action="" class="form-inline" role="form" >
			<div class="form-group search_select_div mr10">
				<label class="sr-only" for="thematic_areas_work_id">Thematic areas Work</label>
				<select name="thematic_areas_work_id" id="thematic_areas_work_id" class="form-control select2 w200" >
					<option value=""> - All Thematic areas Work - </option>
					<?php
						$table_name = $wpdb->prefix . "thematic_areas_work";
						$res = $wpdb->get_results("SELECT * FROM $table_name WHERE `isDelete` = 0"); 
						foreach($res as $row){
							echo '<option value="'.$row->id.'" >'.$row->thematic_area.'</option>';
						}
					?>
				</select>
			</div>
			<div class="form-group search_select_div mr10">
				<label class="sr-only" for="work_location">Work Locations</label>
				<select name="work_location" id="work_location" class="form-control select2 w200" >
					<option value=""> - All Work Locations - </option>
					<?php
						$table_name = $wpdb->prefix . "work_locations";
						$res = $wpdb->get_results("SELECT * FROM $table_name WHERE `isDelete` = 0"); 
						foreach($res as $row){
							echo '<option value="'.$row->id.'" >'.$row->location_name.'</option>';
						}
					?>
				</select>
			</div>
			<div class="form-group search_select_div mr10">
				<label class="sr-only" for="tag_id">Tags</label>
				<select name="tag_id" id="tag_id" class="form-control select2 w200" >
					<option value=""> - All Tag - </option>
					<?php
						$table_name = $wpdb->prefix . "tag";
						$res = $wpdb->get_results("SELECT * FROM $table_name WHERE `isDelete` = 0"); 
						foreach($res as $row){
							echo '<option value="'.$row->id.'" >'.$row->tag_name.'</option>';
						}
					?>
				</select>
			</div>
			<div class="form-group search_select_div mr10 w150">
				<label class="sr-only" for="proposal_status">Status</label>
				<select name="proposal_status" id="proposal_status" class="form-control select2 w150" >
					<option value=""> - All Status - </option>
					<option value="Rejected" > Rejected </option>
					<option value="Accepted" > Accepted </option>
				</select>
			</div>
			<input type="submit" name="search" class="button button-medium" value="Search" />
		</form>
	</div>
	<table id="proposal_table" width="100%" class="widefat dataTable">
		<thead>
			<tr>
				<th>Sr. No.</th>
				<th>Project Title</th>
				<th>NGO Organisation Name</th>
				<th>HUB Code</th>
				<th>Corporate Company Name</th>
				<th>Status</th>
				<th>Created Date</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>
<script type="text/javascript">
	var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
	jQuery(document).ready( function(){
		jQuery('.select2').select2();
		
		dataTable = jQuery('#proposal_table').DataTable( {
			"processing": true,
			"serverSide": true,
			"pageLength": 25,
			"columnDefs": [ 
				{ "targets": 0, "orderable": false }, 
				{ "targets": 1, "orderable": false },
				{ "targets": 2, "orderable": false },
				{ "targets": 3, "orderable": false },
				{ "targets": 4, "orderable": false },
				{ "targets": 5, "orderable": false },
				{ "targets": 6, "orderable": false },
				{ "targets": 7, "orderable": false },
			],
			"ajax":{
				data : function(d) {},
				url: ajaxurl + '?action=proposal_filter_list',
				type: "post", 
				error: function(){ 
					jQuery(".datatable-error").html("");
					jQuery("#datatable").append('<tbody class="datatable-error"><tr><th colspan="5">No data found in the server</th></tr></tbody>');
					jQuery("#datatable_processing").css("display","none");
				}
			},
			//"searching" : false,
			language: {
				searchPlaceholder: "Project Title & HUB Code"
			}
		} );
		
		jQuery(document).on('submit', '#search_proposal', function(){
			dataTable.destroy();
			var proposal_status = jQuery('#proposal_status').val();
			var thematic_areas_work_id = jQuery('#thematic_areas_work_id').val();
			var tag_id = jQuery('#tag_id').val();
			var work_location = jQuery('#work_location').val();
			dataTable = jQuery('#proposal_table').DataTable( {
				"processing": true,
				"serverSide": true,
				"pageLength": 25,
				"columnDefs": [ 
					{ "targets": 0, "orderable": false }, 
					{ "targets": 1, "orderable": false },
					{ "targets": 2, "orderable": false },
					{ "targets": 3, "orderable": false },
					{ "targets": 4, "orderable": false },
					{ "targets": 5, "orderable": false },
					{ "targets": 6, "orderable": false },
					{ "targets": 7, "orderable": false },
				],
				"ajax":{
					data : function(d) {},
					url: ajaxurl + '?action=proposal_filter_list&proposal_status=' + proposal_status + '&thematic_areas_work_id=' + thematic_areas_work_id + '&tag_id=' + tag_id + '&work_location=' + work_location,
					type: "post", 
					error: function(){ 
						jQuery(".datatable-error").html("");
						jQuery("#datatable").append('<tbody class="datatable-error"><tr><th colspan="5">No data found in the server</th></tr></tbody>');
						jQuery("#datatable_processing").css("display","none");
					}
				},
				//"searching" : false,
				language: {
					searchPlaceholder: "Project Title & HUB Code"
				}
			} );
			return false;
		});
	});
	
	function proposal_move_to_trash(proposal_id){
		if (confirm('Are you sure, You want move to trash this record?')) {
			var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
			jQuery.ajax({
				url: ajaxurl,
				data : {action: "proposal_move_to_trash", proposal_id: proposal_id },
				success: function(response){
					if (response == 'proposal_moved') {
						jQuery("html, body").animate({ scrollTop: 0 }, "slow");
						window.location.reload();
					}
					return false;
				},
			});
		}
	}
</script>
