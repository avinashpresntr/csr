<?php 
	global $wpdb;
	$ngo_res = $wpdb->get_results("SELECT * FROM `csr_ngo` WHERE 1 ORDER BY `id` DESC LIMIT 1" );
	$new_ngo_id = $ngo_res[0]->id + 1;
?>
<link href="<?php echo plugins_url(); ?>/select2/select2.css" rel="stylesheet" type="text/css" />
<script src="<?php echo plugins_url(); ?>/select2/select2.min.js" type="text/javascript"></script>
<style>
	input[type=text], input[type=number], input[type=email], #postal_address, #s2id_autogen1, .select2-container{
		width: 100% !important;
	}
</style>
<div class="wrap">
	<h2 class="hndle ui-sortable-handle" style="padding: 5px 10px; margin: 0; "><a href="?page=ngo" class="button button-primary button-medium"> Back </a> &nbsp;<span>Add New NGO </span></h2>
	<div class="display_alert">
		<?php 
			if(isset($_SESSION['ngo_added'])){
				echo '<div id="message" class="updated notice notice-success is-dismissible"><p>NGO Added.</p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div>';
				unset($_SESSION['ngo_added']);
			}
		?>
	</div>
	<div class="postbox" style="padding: 10px;">
		<form id="admin_ngo_add" action="" method="post" enctype="multipart/form-data" >
			<input type="hidden" name="ngo_id" id="ngo_id" value="<?php echo $new_ngo_id; ?>">
			<table width="70%">
				<tr>
					<td><label for="organisation_name">Name of the organisation*:</label></td>
					<td><input type="text" name="organisation_name" id="organisation_name" value="" data-parsley-pattern="^[A-Za-z ]*$" class="" required ></td>
				</tr>
				<tr>
					<td><label for="operation_scale_id">Scale of Operation*:</label></td>
					<td>
						<select class="form-dropdown select2" id="operation_scale_id" name="operation_scale_id" required >
							<option value=""> - Select scale of Operation - </option>
							<?php
								$table_name = $wpdb->prefix . "operation_scale";
								$res = $wpdb->get_results("select * from $table_name where isDelete=0"); 
								foreach($res as $row){
									echo '<option value="'.$row->id.'" >'.$row->operation_scale.'</option>';
								}
							?>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="organisation_type_id">Type of Organisation*:</label></td>
					<td>
						<select class="form-dropdown select2" id="organisation_type_id" name="organisation_type_id" required >
							<option value=""> - Select type of Organisation - </option>
							<?php
								$table_name = $wpdb->prefix . "organisation_type";
								$res = $wpdb->get_results("select * from $table_name where isDelete=0"); 
								foreach($res as $row){
									echo '<option value="'.$row->id.'" >'.$row->type_name.'</option>';
								}
							?>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="thematic_areas_works">Thematic areas of work*:</label></td>
					<td>
						<input type="hidden" name="thematic_areas_works" id="thematic_areas_works" required >
						<ul class="parsley-errors-list filled tag_required_thematic_areas"  style="color: #f00;"><li class="parsley-required">This value is required.</li></ul>
					</td>
				</tr>
				<tr>
					<td><label for="sub_thematic_area">Sub Thematic areas:</label></td>
					<td><input type="text" name="sub_thematic_area" id="sub_thematic_area" value="" ><br /><small>Add multiple value by comma (,) separated.</small></td>
				</tr>
				<tr>
					<td><label for="work_location">Locations of work: ( State / District / City )*:</label></td>
					<td>
						<input type="hidden" name="work_location" id="work_locations" required >
						<ul class="parsley-errors-list filled tag_required"  style="color: #f00;"><li class="parsley-required">This value is required.</li></ul>
					</td>
				</tr>
				<tr>
					<td><label for="postal_address">Postal Address*:</label></td>
					<td><textarea name="postal_address" id="postal_address" rows="4" class="" required ></textarea></td>
				</tr>
				<tr>
					<td><label for="status">Status:</label></td>
					<td>
						<select class="form-dropdown select2" id="ngo_status" name="ngo_status" >
							<option value=""> - Select Status - </option>
							<option value="Rejected" > Rejected </option>
							<option value="Applicant" > Applicant </option>
							<option value="Empanelled" > Empanelled </option>
							<option value="expired_renewal_pending" > Expired – Renewal Pending </option>
							<option value="Field_Visit" > Field Visit </option>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="hub_code">HUB Code:</label></td>
					<td>
						<input type="text" name="hub_code" id="hub_code" value="" class="" />
						<input type="button" name="generate_hub_code_btn" id="generate_hub_code_btn" value="Generate HUB Code" />
						<span class="generate_loading" style="display: none;"><img src="<?php echo plugins_url(); ?>/csr-bank/img/loading.gif" alt="Loading..." width="25" /></span>
					</td>
				</tr>
				<tr>
					<td><label for="empanellment_certificate">Empanellment certificate:</label></td>
					<td><input type="file" name="empanellment_certificate" id="empanellment_certificate" class="" ></td>
				</tr>
				<tr>
					<td><label for="empanelled_start_date">Empanelled Start Date:</label></td>
					<td><input type="text" name="empanelled_start_date" id="empanelled_start_date" class="datepicker" ></td>
				</tr>
				<tr>
					<td><label for="empanelled_end_date">Empanelled End Date:</label></td>
					<td><input type="text" name="empanelled_end_date" id="empanelled_end_date" class="datepicker" ></td>
				</tr>
				<tr>
					<td colspan="2"><h5>Contact Details</h5></td>
				</tr>
				<tr>
					<td><label for="email_id">Contact person name*:</label></td>
					<td>
						<input type="text" name="person_firstname" value="" class="" placeholder="First Name" data-parsley-pattern="^[A-Za-z ]*$" required ><br />
						<input type="text" name="person_lastname" value="" class="" placeholder="Last Name" data-parsley-pattern="^[A-Za-z ]*$" required >
					</td>
				</tr>
				<tr>
					<td><label for="email_id">Email Id*:</label></td>
					<td><input type="email" name="email_id" id="email_id" value="" class="" required ></td>
				</tr>
				<tr>
					<td><label for="email_id">Pan Card Number*:</label></td>
					<td><input type="text" name="pan_no" id="pan_no" value="" class="" required ></td>
				</tr>
				<tr>
					<td><label for="phone_number">Phone Number*:</label></td>
					<td><input type="text" name="phone_number" id="phone_number" value="" class="" placeholder="Ex.(0123)456789" data-parsley-maxlength="15" ><br /><small>Ex.(0123)456789</small></td>
				</tr>
				<tr>
					<td><label for="mobile_number">Mobile Number*:</label></td>
					<td><input type="number" name="mobile_number" id="mobile_number" value="" class="" data-parsley-minlength="10" data-parsley-maxlength="10" ></td>
				</tr>
				<tr>
					<td><label for="application_form_name">Upload application form*:</label></td>
					<td><input type="file" name="application_form_name" id="application_form_name" class="" required ></td>
				</tr>
				<tr>
					<td><label for="payment_receipt_name">Upload payment receipt*:</label></td>
					<td><input type="file" name="payment_receipt_name" id="payment_receipt_name" class="" required ></td>
				</tr>
				<tr>
					<td></td>
					<td><hr />
						<input type="submit" value="Update" class="button button-primary button-large submit_btn">
					</td>
				</tr>
			</table>
		</form>
	</div>
</div>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/js/parsleyjs/src/parsley.css">
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/parsleyjs/dist/parsley.min.js"></script>
<link href="<?php echo plugins_url(); ?>/datepicker/datepicker.css"" rel="stylesheet" type="text/css" />
<script src="<?php echo plugins_url(); ?>/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
<script>
  jQuery(document).ready(function () {
    jQuery('form').parsley();
    jQuery('.datepicker').datepicker({
		dateFormat: 'dd/mm/yy',
	});
  });
</script>
<script type="text/javascript">
	jQuery('.tag_required').hide();
	jQuery('.tag_required_thematic_areas').hide();
	var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
	jQuery('.select2').select2();
	tag_load();
    jQuery(document).ready(function () {
		jQuery(document).on('change', '#ngo_status', function(){
			var ngo_status = jQuery('#ngo_status').val();
			if(ngo_status != 'Empanelled'){
				jQuery('#empanellment_certificate').prop('required',false);
				jQuery('#empanelled_start_date').prop('required',false);
				jQuery('#empanelled_end_date').prop('required',false);
			} else {
				jQuery('#empanellment_certificate').prop('required',true);
				jQuery('#empanelled_start_date').prop('required',true);
				jQuery('#empanelled_end_date').prop('required',true);
			}
			if(ngo_status != 'Applicant'){
				jQuery('#hub_code').prop('required',false);
			} else {
				jQuery('#hub_code').prop('required',true);
			}			
		});
		
		jQuery(document).on('click', '#generate_hub_code_btn', function () {
			jQuery('.generate_loading').show();
			var postData = {action: 'generate_hub_code', ngo_id: jQuery('#ngo_id').val()};
		    jQuery.ajax({
                url: ajaxurl,
				dataType : "JSON",
                data: postData,
                success: function (response) {
					jQuery('.generate_loading').hide();
                    if (response.success == 'Generated') {
						jQuery('#hub_code').val(response.hub_code);
                    }                    
                    return false;
                },
            });
		});
		
		jQuery(document).on('click', '.is-dismissible', function () {
			jQuery('.is-dismissible').hide();
		});
		
		jQuery(document).on('submit', '#admin_ngo_add', function () {
            var work_locations = jQuery('#work_locations').val();
            if(work_locations == ''){
                jQuery('.tag_required').show();
                return false;
            }
            var thematic_areas_works = jQuery('#thematic_areas_works').val();
            if(thematic_areas_works == ''){
                jQuery('.tag_required_thematic_areas').show();
                return false;
            }
            jQuery('.loading_div').show();
            var postData = new FormData(this);
            postData.append('action', 'admin_ngo_add');
            jQuery.ajax({
                url: ajaxurl,
				type: "POST",
                processData: false,
                contentType: false,
                cache: false,
                data: postData,
                success: function (response) {
					jQuery('.loading_div').hide();
					var json = jQuery.parseJSON(response);
                    if (json['error'] == 'Exist') {
                        jQuery('.display_alert').html('<div id="message" class="error notice notice-success is-dismissible"><p>NGO Already Exist.</p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div>');
                    }
                    if (json['error'] == 'HubCodeExist') {
                        jQuery('.display_alert').html('<div id="message" class="error notice notice-success is-dismissible"><p>Below HUB Code Already Exist.</p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div>');
                    }
                    if (json['error'] == 'FileType') {
                        jQuery('.display_alert').html('<div id="message" class="error notice notice-success is-dismissible"><p><strong>Error! </strong> Only allow pdf,graphics and zip file upload.</p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div>');
                    }
                    if (json['error'] == 'FileSize') {
                        jQuery('.display_alert').html('<div id="message" class="error notice notice-success is-dismissible"><p><strong>Error! </strong> Max file upload size limit 10MB.</p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div>');
                    }
                    if (json['success'] == 'Added') {
						window.location.href = '?page=ngo-add'; 
						return false;
                    }
                    jQuery("html, body").animate({ scrollTop: 0 }, "slow");
                    tag_load();
                    return false;
                },
            });
            return false;
        });
		
	});
	
	function tag_load(){
		
		jQuery.ajax({
            url: ajaxurl,
			dataType : "JSON",
			data : {action: "get_work_locations"},
            success: function(result){
				jQuery("#work_locations").select2({
                    createSearchChoice:function(term, data) { if (jQuery(data).filter(function() { return this.text.localeCompare(term)===0; }).length===0) {return {id:term, text:term};} },
                    multiple: true,
                    data: result,
                });
                function log(e) {
                    var e=jQuery("<li>"+e+"</li>");
                    jQuery("#events_11").append(e);
                    e.animate({opacity:1}, 10000, 'linear', function() { e.animate({opacity:0}, 2000, 'linear', function() {e.remove(); }); });
                }
                jQuery("#work_locations")
                    .on("change", function(e) { log("change "+JSON.stringify({val:e.val, added:e.added, removed:e.removed})); })
                    .on("select2-opening", function() { log("opening"); })
                    .on("select2-open", function() { log("open"); })
                    .on("select2-close", function() { log("close"); })
                    .on("select2-highlight", function(e) { log ("highlighted val="+ e.val+" choice="+ JSON.stringify(e.choice));})
                    .on("select2-selecting", function(e) { log ("selecting val="+ e.val+" choice="+ JSON.stringify(e.choice));})
                    .on("select2-removing", function(e) { log ("removing val="+ e.val+" choice="+ JSON.stringify(e.choice));})
                    .on("select2-removed", function(e) { log ("removed val="+ e.val+" choice="+ JSON.stringify(e.choice));})
                    .on("select2-loaded", function(e) { log ("loaded (data property omitted for brevity)");})
                    .on("select2-focus", function(e) { log ("focus");})
                    .on("select2-blur", function(e) { log ("blur");});
            }
        });
		
		jQuery.ajax({
            url: ajaxurl,
			dataType : "JSON",
			data : {action: "get_thematic_areas_works"},
            success: function(result){
				jQuery("#thematic_areas_works").select2({
                    //createSearchChoice:function(term, data) { if (jQuery(data).filter(function() { return this.text.localeCompare(term)===0; }).length===0) {return {id:term, text:term};} },
                    multiple: true,
                    data: result,
                });
                function log(e) {
                    var e=jQuery("<li>"+e+"</li>");
                    jQuery("#events_11").append(e);
                    e.animate({opacity:1}, 10000, 'linear', function() { e.animate({opacity:0}, 2000, 'linear', function() {e.remove(); }); });
                }
                jQuery("#thematic_areas_works")
                    .on("change", function(e) { log("change "+JSON.stringify({val:e.val, added:e.added, removed:e.removed})); })
                    .on("select2-opening", function() { log("opening"); })
                    .on("select2-open", function() { log("open"); })
                    .on("select2-close", function() { log("close"); })
                    .on("select2-highlight", function(e) { log ("highlighted val="+ e.val+" choice="+ JSON.stringify(e.choice));})
                    .on("select2-selecting", function(e) { log ("selecting val="+ e.val+" choice="+ JSON.stringify(e.choice));})
                    .on("select2-removing", function(e) { log ("removing val="+ e.val+" choice="+ JSON.stringify(e.choice));})
                    .on("select2-removed", function(e) { log ("removed val="+ e.val+" choice="+ JSON.stringify(e.choice));})
                    .on("select2-loaded", function(e) { log ("loaded (data property omitted for brevity)");})
                    .on("select2-focus", function(e) { log ("focus");})
                    .on("select2-blur", function(e) { log ("blur");});
            }
        });
		
    }
</script>
