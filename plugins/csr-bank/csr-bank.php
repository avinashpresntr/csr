<?php
/*
Plugin Name: CSR Bank
Description: CSR Bank
Author: CSR
Version: 0.1
*/

add_action('admin_menu', 'ngo_setup_menu');

if(!isset($_SESSION)){ 
	session_start();
}

function ngo_setup_menu(){
	add_menu_page( 'NGO Page', 'NGO', 'manage_options', 'ngo', 'ngo', 'dashicons-admin-page' , 30  );
	add_submenu_page( 'ngo', 'Add NGO', 'Add NGO','manage_options', 'ngo-add', 'ngo_add');
	add_menu_page( 'Proposal Page', 'NGO Proposal', 'manage_options', 'proposal', 'proposal', 'dashicons-admin-page' , 30  );
	add_menu_page( 'Corporate Page', 'Corporate', 'manage_options', 'corporate', 'corporate', 'dashicons-admin-page' , 30  );
	add_submenu_page( 'corporate', 'Add Corporate', 'Add Corporate','manage_options', 'corporate-add', 'corporate_add');
}
/*******************************  NGO List *************************/ 
function ngo(){
	if(isset($_GET['action']) && $_GET['action'] == 'ngo_details'){
		include("ngo_details.php");
	} else if(isset($_GET['action']) && $_GET['action'] == 'ngo_trashed'){
		include("ngo_trashed.php");
	} else if(isset($_GET['action']) && $_GET['action'] == 'level_notes_history'){
		include("level_notes_history.php");
	} else {
		include("ngo.php");
	}
}
add_shortcode( 'ngo', 'ngo' );

/*******************************  Add NGO *************************/ 
function ngo_add(){
	include("ngo_add.php");
}
add_shortcode( 'ngo_add', 'ngo_add' );

/*******************************  Proposal List *************************/ 
function proposal(){
	if(isset($_GET['action']) && $_GET['action'] == 'proposal_details'){
		include("proposal_details.php");
	} else if(isset($_GET['action']) && $_GET['action'] == 'proposal_trashed'){
		include("proposal_trashed.php");
	} else {
		include("proposal.php");
	}
}
add_shortcode( 'proposal', 'proposal' );

/*******************************  Corporate List *************************/ 
function corporate(){
	if(isset($_GET['action']) && $_GET['action'] == 'corporate_wishlist'){
		include("corporate_wishlist.php");
	}else if(isset($_GET['action']) && $_GET['action'] == 'corporate_details'){
		include("corporate_details.php");
	} else if(isset($_GET['action']) && $_GET['action'] == 'corporate_trashed'){
		include("corporate_trashed.php");
	} else {
		include("corporate.php");
	}
}
add_shortcode( 'corporate', 'corporate' );

/*******************************  Add Corporate *************************/ 
function corporate_add(){
	include("corporate_add.php");
}
add_shortcode( 'corporate_add', 'corporate_add' );


if(isset($_GET['id']) && $_GET['page'] == "ngo_details"){
	include("ngo_details.php");
}

/*******************************  Corporate Login *************************/

function corporate_account_func(){
	include("corporate_account.php");
}
add_shortcode( 'corporate_account', 'corporate_account_func' );

if(isset($_GET['page']) && $_GET['page'] == 'corporate-logout'){
	
	if(isset($_SESSION['corporate_login_id'])){
		unset($_SESSION['corporate_login_id']);
	}

	if(isset($_SESSION['user_data'])){
		unset($_SESSION['user_data']);
	}

	header('Location: '.site_url().'/corporate-account/');
	exit();
}

/*******************************  Submit Application *************************/

function submit_application_form_func(){
	include("submit_applications.php");
}
add_shortcode( 'submit_application_form', 'submit_application_form_func' );

function get_work_locations(){
	global $wpdb; 
	$work_locations_arr = array();
	$table_name = $wpdb->prefix . "work_locations";
	$res = $wpdb->get_results("SELECT `id`,`location_name` as text FROM " . $table_name . " WHERE `isDelete` = 0"); 
	foreach($res as $row){
		$work_locations_arr[] = $row;
	}
	echo json_encode($work_locations_arr);
	exit;
}
add_action( 'wp_ajax_get_work_locations', 'get_work_locations');
add_action( 'wp_ajax_nopriv_get_work_locations', 'get_work_locations');

function get_thematic_areas_works(){
	global $wpdb; 
	$thematic_areas_works_arr = array();
	$table_name = $wpdb->prefix . "thematic_areas_work";
	$res = $wpdb->get_results("SELECT `id`,`thematic_area` as text FROM " . $table_name . " WHERE `isDelete` = 0"); 
	foreach($res as $row){
		$thematic_areas_works_arr[] = $row;
	}
	echo json_encode($thematic_areas_works_arr);
	exit;
}
add_action( 'wp_ajax_get_thematic_areas_works', 'get_thematic_areas_works');
add_action( 'wp_ajax_nopriv_get_thematic_areas_works', 'get_thematic_areas_works');

function add_new_ngo_email($ngo_id, $app_type, $attachments = array()) {
	global $wpdb;
	$sql = "SELECT `csr_ngo`.`id`, `csr_ngo`.`organisation_name`, `csr_ngo`.`sub_thematic_area`, `csr_ngo`.`hub_code`, `csr_ngo`.`ngo_status`, `csr_ngo`.`created_date`,
			`csr_ngo`.`postal_address`, `csr_ngo`.`person_firstname`, `csr_ngo`.`person_lastname`, `csr_ngo`.`email_id`, `csr_ngo`.`pan_no`, 
			`csr_ngo`.`phone_number`, `csr_ngo`.`mobile_number`,
			`csr_operation_scale`.`operation_scale`, `csr_organisation_type`.`type_name` ";
	$sql .= " FROM `csr_ngo` ";
	$sql .= " JOIN `csr_operation_scale` ON `csr_ngo`.`operation_scale_id`=`csr_operation_scale`.`id` ";
	$sql .= " JOIN `csr_organisation_type` ON `csr_ngo`.`organisation_type_id`=`csr_organisation_type`.`id` ";
	$sql .= " WHERE `csr_ngo`.`id` = '".$ngo_id."' ";
	$ngo_result = $wpdb->get_results($sql);
	$wlids_arr = array();
	$work_locations_arr = array();
	$wlids_result = $wpdb->get_results("SELECT `work_locations_id` FROM `csr_ngo_work_locations` WHERE `ngo_id` = '" . $ngo_id . "' ");
	foreach($wlids_result as $wlids_row){
		$wlids_arr[] = $wlids_row->work_locations_id;
	}
	$wlids = implode(', ', $wlids_arr);
	$work_locations_result = $wpdb->get_results("SELECT * FROM `csr_work_locations` WHERE `id` IN ( ".$wlids." ) " ); 
	foreach($work_locations_result as $work_locations_row){
		$work_locations_arr[$work_locations_row->id] = $work_locations_row->location_name;
	}
	$work_locations = implode(', ', $work_locations_arr);
	
	$area_sql = "SELECT `thematic_area` FROM `csr_thematic_areas_work` WHERE `id` IN ( SELECT `thematic_areas_id` FROM `csr_ngo_thematic_areas_works` WHERE `ngo_id` = '".$ngo_id."' )";
	$areas = $wpdb->get_results($area_sql);
	$area_names = array();
	if(!empty($areas)){
		foreach($areas as $area){
			$area_names[] = $area->thematic_area;
		}
	}
	$area_names = implode(', ', $area_names);
	
	if($app_type == 'Renew'){
		$args = array(
		  'name'        => 'submitting-renew-application-for-empanelment',
		  'post_type'   => 'csr_email_format',
		  'post_status' => 'publish',
		  'numberposts' => 1
		);
		$post = get_posts($args);
	} else {
		$args = array(
		  'name'        => 'submitting-application-for-empanelment',
		  'post_type'   => 'csr_email_format',
		  'post_status' => 'publish',
		  'numberposts' => 1
		);
		$post = get_posts($args);
	}
    $to = $ngo_result[0]->email_id;
	$subject = $post[0]->post_title;
	$message = '';
	$message .= $post[0]->post_content;
	$message .= '<table>
					<tr>
						<td>Organisation Name </td>
						<td> : '.$ngo_result[0]->organisation_name.'</td>
					</tr>
					<tr>
						<td>Operation Scale </td>
						<td> : '.$ngo_result[0]->operation_scale.'</td>
					</tr>
					<tr>
						<td>Type Name </td>
						<td> : '.$ngo_result[0]->type_name.'</td>
					</tr>
					<tr>
						<td>Thematic Area </td>
						<td> : '.$area_names.'</td>
					</tr>
					<tr>
						<td>Sub Thematic Area </td>
						<td> : '.$ngo_result[0]->sub_thematic_area.'</td>
					</tr>
					<tr>
						<td>Location Of work </td>
						<td> : '.$work_locations.'</td>
					</tr>
					<tr>
						<td>Postal Address </td>
						<td> : '.$ngo_result[0]->postal_address.'</td>
					</tr>
					<tr>
						<td>Person Firstname </td>
						<td> : '.$ngo_result[0]->person_firstname.'</td>
					</tr>
					<tr>
						<td>Person Lastname </td>
						<td> : '.$ngo_result[0]->person_lastname.'</td>
					</tr>
					<tr>
						<td>Email Id </td>
						<td> : '.$ngo_result[0]->email_id.'</td>
					</tr>
					<tr>
						<td>Pan Card Number </td>
						<td> : '.$ngo_result[0]->pan_no.'</td>
					</tr>
					<tr>
						<td>Phone Number </td>
						<td> : '.$ngo_result[0]->phone_number.'</td>
					</tr>
					<tr>
						<td>Mobile Number </td>
						<td> : '.$ngo_result[0]->mobile_number.'</td>
					</tr>
				</table>';
	$headers[] = 'From: '.get_option( 'blogname' ).' <'.get_option( 'admin_email' ).'>';
    add_filter( 'wp_mail_content_type', 'add_new_ngo_email_content_type' );
    $mail = wp_mail( $to, $subject, $message, $headers, $attachments );
}
function add_new_ngo_email_content_type() {
    return 'text/html';
}

function add_new_ngo(){
	global $wpdb;
	$organisation_name = $_REQUEST['organisation_name'];
	$operation_scale_id = $_REQUEST['operation_scale_id'];
	$organisation_type_id = $_REQUEST['organisation_type_id'];
	//$thematic_areas_work_id = $_REQUEST['thematic_areas_work_id'];
	$sub_thematic_area = $_REQUEST['sub_thematic_area'];
	$postal_address = $_REQUEST['postal_address'];
	$person_firstname = $_REQUEST['person_firstname'];
	$person_lastname = $_REQUEST['person_lastname'];
	$email_id = $_REQUEST['email_id'];
	$pan_no = $_REQUEST['pan_no'];
	$phone_number = $_REQUEST['phone_number'];
	$mobile_number = $_REQUEST['mobile_number'];
	
    $res = $wpdb->get_results("SELECT * FROM `csr_ngo` WHERE `pan_no` = '" . $pan_no . "' " ); 
	if(!empty($res)){
		$created_date = $res[0]->created_date;
		$expiry_date = date('Y-m-d H:i:s', strtotime('+2 years', strtotime($created_date)));
		$current_date = date('Y-m-d H:i:s');
		if ( $current_date < $expiry_date && $res[0]->ngo_status == 'Rejected'){
			$return['error'] = "After_2year";
		} else if ( $current_date > $expiry_date && $res[0]->ngo_status == 'Rejected'){
			$wpdb->update('csr_ngo', 
				array(
					'ngo_status' => $ngo_status,
					'created_date' => date('Y-m-d H:i:s')
				), 
				array('pan_no'=>$pan_no)
			);
			$return['success'] = "Re_open";
		} else {
			$return['error'] = "Exist";
		}
		print json_encode($return);
		exit;
	}
	
	//file size and type validations
	if(isset($_FILES["application_form_name"]["name"]) && !empty($_FILES["application_form_name"]["name"])){
		if ($_FILES['application_form_name']['size'] >= (15 * 1048576)){
			$return['error'] = "FileSize";
			print json_encode($return);
			exit;
		}
		$name = $_FILES["application_form_name"]["name"];
		$extension = pathinfo($name, PATHINFO_EXTENSION);
		$extension = strtolower($extension);
		if(!($extension == 'pdf' || $extension == 'zip' || $extension == 'jpg' || $extension == 'png' || $extension == 'jpeg' || $extension == 'gif' || $extension == 'bmp')){
			$return['error'] = "FileType";
			print json_encode($return);
			exit;
		}
	}
	if(isset($_FILES["payment_receipt_name"]["name"]) && !empty($_FILES["payment_receipt_name"]["name"])){
		if ($_FILES['payment_receipt_name']['size'] >= (15 * 1048576)){
			$return['error'] = "FileSize";
			print json_encode($return);
			exit;
		}
		$name = $_FILES["payment_receipt_name"]["name"];
		$extension = pathinfo($name, PATHINFO_EXTENSION);
		$extension = strtolower($extension);
		if(!($extension == 'pdf' || $extension == 'zip' || $extension == 'jpg' || $extension == 'png' || $extension == 'jpeg' || $extension == 'gif' || $extension == 'bmp')){
			$return['error'] = "FileType";
			print json_encode($return);
			exit;
		}
	}
    
    $work_location_id_arr = array();
    $work_locations = array();
	if(!empty($_POST['work_location'])){
		$table_name = $wpdb->prefix . "work_locations";
		$work_locations = explode(',', $_POST['work_location']);
		foreach($work_locations as $work_location ){
			if(is_numeric($work_location)){
				$work_location_id_arr[] = $work_location;
				$res = $wpdb->get_results("SELECT * FROM " . $table_name . " WHERE `id` = '" . $work_location . "' " ); 
				if(!empty($res)){
					$work_locations[] = $res[0]->location_name;
				}
			} else {
				$work_location = trim($work_location);
				$res = $wpdb->get_results("SELECT * FROM " . $table_name . " WHERE `location_name` = '" . $work_location . "' " ); 
				if(empty($res)){
					$wpdb->insert($table_name, array('location_name' => $work_location) ); 
					$id = $wpdb->insert_id;
					$work_location_id_arr[] = $id;
					$work_locations[] = $work_location;
				} else {
					$work_location_id_arr[] = $res[0]->id;
					$work_locations[] = $res[0]->location_name;
				}
			}
		}
	}
	
    $thematic_areas_work_id_arr = array();
    $thematic_areas_works = array();
	if(!empty($_POST['thematic_areas_works'])){
		$table_name = $wpdb->prefix . "thematic_areas_work";
		$thematic_areas_works = explode(',', $_POST['thematic_areas_works']);
		foreach($thematic_areas_works as $thematic_areas_work ){
			if(is_numeric($thematic_areas_work)){
				$thematic_areas_work_id_arr[] = $thematic_areas_work;
				$res = $wpdb->get_results("SELECT * FROM " . $table_name . " WHERE `id` = '" . $thematic_areas_work . "' " ); 
				if(!empty($res)){
					$thematic_areas_works[] = $res[0]->thematic_area;
				}
			} else {
				$thematic_areas_work = trim($thematic_areas_work);
				$res = $wpdb->get_results("SELECT * FROM " . $table_name . " WHERE `thematic_area` = '" . $thematic_areas_work . "' " ); 
				if(empty($res)){
					$wpdb->insert($table_name, array('thematic_area' => $thematic_areas_work) ); 
					$id = $wpdb->insert_id;
					$thematic_areas_work_id_arr[] = $id;
					$thematic_areas_works[] = $thematic_areas_work;
				} else {
					$thematic_areas_work_id_arr[] = $res[0]->id;
					$thematic_areas_works[] = $res[0]->thematic_area;
				}
			}
		}
	}
	
	$return = ''; 
	$wpdb->insert('csr_ngo', 
		array(
			'organisation_name' => $organisation_name,
			'operation_scale_id' => $operation_scale_id,
			'organisation_type_id' => $organisation_type_id,
			//'thematic_areas_work_id' => $thematic_areas_work_id,
			'sub_thematic_area' => $sub_thematic_area,
			'postal_address' => $postal_address,
			'person_firstname' => $person_firstname,
			'person_lastname' => $person_lastname,
			'email_id' => $email_id,
			'pan_no' => $pan_no,
			'phone_number' => $phone_number,
			'mobile_number' => $mobile_number,
			'ngo_status' => 'New',
			'created_date' => date('Y-m-d H:i:s')
		) 
	); 
	$ngo_id = $wpdb->insert_id;
	foreach($work_location_id_arr as $work_location_id){
		$wpdb->insert('csr_ngo_work_locations', 
			array(
				'ngo_id' => $ngo_id,
				'work_locations_id' => $work_location_id
			) 
		);
	} 
	foreach($thematic_areas_work_id_arr as $thematic_areas_work_id){
		$wpdb->insert('csr_ngo_thematic_areas_works', 
			array(
				'ngo_id' => $ngo_id,
				'thematic_areas_id' => $thematic_areas_work_id
			) 
		);
	}
	$app_type = 'New';
	//$upload_dir = wp_upload_dir(); 
	//$upload_dir = $upload_dir['baseurl'];
	//$targetPath = $upload_dir.'/ngo_files/';
	$path = wp_upload_dir();
	$attachments = array();
	if(isset($_FILES["application_form_name"]["name"]) && !empty($_FILES["application_form_name"]["name"])){
		$name = $_FILES["application_form_name"]["name"];
		$tmpName = $_FILES["application_form_name"]["tmp_name"];
		$extension = pathinfo($name, PATHINFO_EXTENSION);
		$name = $ngo_id.'_application_'.time().'.'.$extension;
		$a = $path['basedir']."/ngo_files/".$name; 
		$targetPath = str_replace("\\", "/", $a);
		move_uploaded_file($tmpName,$targetPath);
		$attachments[] = WP_CONTENT_DIR . '/uploads/ngo_files/' . $name;
		$wpdb->query("UPDATE `csr_ngo` SET `application_form_name`='".$name."' WHERE `id`='".$ngo_id."' " ); 
	}
	if(isset($_FILES["payment_receipt_name"]["name"]) && !empty($_FILES["payment_receipt_name"]["name"])){
		$name = $_FILES["payment_receipt_name"]["name"];
		$tmpName = $_FILES["payment_receipt_name"]["tmp_name"];
		$extension = pathinfo($name, PATHINFO_EXTENSION);
		$name = $ngo_id.'_payment_'.time().'.'.$extension;
		$a = $path['basedir']."/ngo_files/".$name; 
		$targetPath = str_replace("\\", "/", $a);
		move_uploaded_file($tmpName,$targetPath);
		$attachments[] = WP_CONTENT_DIR . '/uploads/ngo_files/' . $name;
		$wpdb->query("UPDATE `csr_ngo` SET `payment_receipt_name`='".$name."' WHERE `id`='".$ngo_id."' " ); 
	}
	add_new_ngo_email($ngo_id, $app_type, $attachments);
    $res = $wpdb->get_results("SELECT * FROM `csr_ngo` WHERE `id` = '" . $ngo_id . "' LIMIT 1 " ); 
	if(!empty($res)){		
		$wpdb->insert('csr_ngo_details', 
			array(
				'ngo_id' => $ngo_id,
				'hub_code' => $res[0]->hub_code,
				'application_form_name' => $res[0]->application_form_name,
				'payment_receipt_name' => $res[0]->payment_receipt_name,
				'created_date' => $res[0]->created_date
			) 
		);
	}
	$return['success'] = "Added";
	print json_encode($return);
	exit;
}
add_action( 'wp_ajax_add_new_ngo', 'add_new_ngo');
add_action( 'wp_ajax_nopriv_add_new_ngo', 'add_new_ngo');

function csr_ngo_rejected_email($email_id) {
	global $wpdb;
	$args = array(
	  'name'        => 'csr-ngo-rejected',
	  'post_type'   => 'csr_email_format',
	  'post_status' => 'publish',
	  'numberposts' => 1
	);
	$post = get_posts($args);
	
    $to = $email_id;
	$subject = $post[0]->post_title;
	$message = '';
	$message .= $post[0]->post_content;
	$message .= '<br /> You can apply after : '.date('d/m/Y', strtotime('+2 years'));;
	$headers[] = 'From: '.get_option( 'blogname' ).' <'.get_option( 'admin_email' ).'>';
    add_filter( 'wp_mail_content_type', 'csr_ngo_rejected_email_content_type' );
    $mail = wp_mail( $to, $subject, $message, $headers );
}
function csr_ngo_rejected_email_content_type() {
    return 'text/html';
}

function csr_ngo_renewal_email($email_id, $expiry_date) {
	global $wpdb;
	$args = array(
	  'name'        => 'csr-ngo-renewal-notice',
	  'post_type'   => 'csr_email_format',
	  'post_status' => 'publish',
	  'numberposts' => 1
	);
	$post = get_posts($args);
	
    $to = $email_id;
	$subject = $post[0]->post_title;
	$message = '';
	$message .= $post[0]->post_content;
	$message .= '<br/><strong>Expiry Date:</strong> '. $expiry_date;
	$headers[] = 'From: '.get_option( 'blogname' ).' <'.get_option( 'admin_email' ).'>';
    add_filter( 'wp_mail_content_type', 'csr_ngo_renewal_email_content_type' );
    $mail = wp_mail( $to, $subject, $message, $headers );
}
function csr_ngo_renewal_email_content_type() {
    return 'text/html';
}

function csr_ngo_expired_email($email_id, $expiry_date) {
	global $wpdb;
	$args = array(
	  'name'        => 'csr-ngo-application-expired',
	  'post_type'   => 'csr_email_format',
	  'post_status' => 'publish',
	  'numberposts' => 1
	);
	$post = get_posts($args);
	
    $to = $email_id;
	$subject = $post[0]->post_title;
	$message = '';
	$message .= $post[0]->post_content;
	$message .= '<br/><strong>Expired Date:</strong> '. $expiry_date;
	$headers[] = 'From: '.get_option( 'blogname' ).' <'.get_option( 'admin_email' ).'>';
    add_filter( 'wp_mail_content_type', 'csr_ngo_expired_email_content_type' );
    $mail = wp_mail( $to, $subject, $message, $headers );
}
function csr_ngo_expired_email_content_type() {
    return 'text/html';
}

function csr_ngo_applicant_email($email_id, $hub_code) {
	global $wpdb;
	$args = array(
	  'name'        => 'csr-ngo-applicant',
	  'post_type'   => 'csr_email_format',
	  'post_status' => 'publish',
	  'numberposts' => 1
	);
	$post = get_posts($args);
	
    $to = $email_id;
	$subject = $post[0]->post_title;
	$message = '';
	$message .= "<br /> Your HUB Code : ". $hub_code." <br />";
	$message .= $post[0]->post_content;
	$headers[] = 'From: '.get_option( 'blogname' ).' <'.get_option( 'admin_email' ).'>';
    add_filter( 'wp_mail_content_type', 'csr_ngo_applicant_email_content_type' );
    $mail = wp_mail( $to, $subject, $message, $headers );
}
function csr_ngo_applicant_email_content_type() {
    return 'text/html';
}

function csr_ngo_empanelled_email($email_id, $hub_code, $empanelled_start_date, $empanelled_end_date, $attachments = array()) {
	global $wpdb;
	$args = array(
	  'name'        => 'csr-ngo-empanelled',
	  'post_type'   => 'csr_email_format',
	  'post_status' => 'publish',
	  'numberposts' => 1
	);
	$post = get_posts($args);
	
    $to = $email_id;
	$subject = $post[0]->post_title;
	$message = '';
	$message .= $post[0]->post_content;
	$message .= "<br /><br /> Your HUB Code : ". $hub_code." <br />";
	$message .= "Empanelled Start Date : ". $empanelled_start_date." <br />";
	$message .= "Empanelled End Date : ". $empanelled_end_date." <br />";
	$headers[] = 'From: '.get_option( 'blogname' ).' <'.get_option( 'admin_email' ).'>';
    add_filter( 'wp_mail_content_type', 'csr_ngo_empanelled_email_content_type' );

    $mail = wp_mail( $to, $subject, $message, $headers, $attachments);
}
function csr_ngo_empanelled_email_content_type() {
    return 'text/html';
}

function update_ngo(){
	global $wpdb; 
	$ngo_id = $_REQUEST['ngo_id'];
	$organisation_name = $_REQUEST['organisation_name'];
	$operation_scale_id = $_REQUEST['operation_scale_id'];
	$organisation_type_id = $_REQUEST['organisation_type_id'];
	//$thematic_areas_work_id = $_REQUEST['thematic_areas_work_id'];
	$sub_thematic_area = $_REQUEST['sub_thematic_area'];
	$postal_address = $_REQUEST['postal_address'];
	$person_firstname = $_REQUEST['person_firstname'];
	$person_lastname = $_REQUEST['person_lastname'];
	$email_id = $_REQUEST['email_id'];
	$pan_no = $_REQUEST['pan_no'];
	$phone_number = $_REQUEST['phone_number'];
	$mobile_number = $_REQUEST['mobile_number'];
	$ngo_status = $_REQUEST['ngo_status'];
	$hub_code = $_REQUEST['hub_code'];
	$score_total = $_REQUEST['score_total'];
	$score_received = $_REQUEST['score_received'];
	$rating = $_REQUEST['rating'];
	$is_front_call = (isset($_REQUEST['is_front_call']) && $_REQUEST['is_front_call'] == "1")? "1" : "0";
	$last_3_month = (isset($_REQUEST['last_3_month']) && $_REQUEST['last_3_month'] == "last_3_month")? "last_3_month" : "";
    
    $res = $wpdb->get_results("SELECT * FROM `csr_ngo` WHERE `pan_no` = '" . $pan_no . "' AND `id` != '".$ngo_id."' " ); 
	if(!empty($res)){
		$return['error'] = "Exist";
		print json_encode($return);
		exit;
	}
	if(!empty($hub_code)){
		$res = $wpdb->get_results("SELECT * FROM `csr_ngo` WHERE `hub_code` = '" . $hub_code . "' AND `id` != '".$ngo_id."' " ); 
		if(!empty($res)){
			$return['error'] = "HubCodeExist";
			print json_encode($return);
			exit;
		}
	}
	
	if( $ngo_status == 'Empanelled'){
		$res = $wpdb->get_results("SELECT * FROM `csr_ngo` WHERE `id` = '".$ngo_id."' " ); 
		$rejected_level = array();
		if($res[0]->l1_status == "Rejected"){
			$rejected_level[] = 'L1';
		}
		if($res[0]->l2_status == "Rejected"){
			$rejected_level[] = 'L2';
		}
		if($res[0]->l3_status == "Rejected"){
			$rejected_level[] = 'L3';
		}
		if($res[0]->l4_status == "Rejected"){
			$rejected_level[] = 'L4';
		}
		if($res[0]->l5_status == "Rejected"){
			$rejected_level[] = 'L5';
		}
		$rejected_level = implode(',', $rejected_level);
		if(!empty($rejected_level)){
			$return['errorMessage'] = "Scrutiny level ".$rejected_level." has been rejected. Kindly accept it and then change the NGO status to empanelled.";
			$return['error'] = "allLevelNotAccepted";
			print json_encode($return);
			exit;
		} 
	}
	
	//file size and type validations
	if(isset($_FILES["empanellment_certificate"]["name"]) && !empty($_FILES["empanellment_certificate"]["name"])){
		if ($_FILES['empanellment_certificate']['size'] >= (15 * 1048576)){
			$return['error'] = "FileSize";
			print json_encode($return);
			exit;
		}
		$name = $_FILES["empanellment_certificate"]["name"];
		$extension = pathinfo($name, PATHINFO_EXTENSION);
		$extension = strtolower($extension);
		if(!($extension == 'pdf' || $extension == 'zip' || $extension == 'jpg' || $extension == 'png' || $extension == 'jpeg' || $extension == 'gif' || $extension == 'bmp')){
			$return['error'] = "FileType";
			print json_encode($return);
			exit;
		}
	}
	if(isset($_FILES["analysis_report"]["name"]) && !empty($_FILES["analysis_report"]["name"])){
		if ($_FILES['analysis_report']['size'] >= (15 * 1048576)){
			$return['error'] = "FileSize";
			print json_encode($return);
			exit;
		}
		$name = $_FILES["analysis_report"]["name"];
		$extension = pathinfo($name, PATHINFO_EXTENSION);
		$extension = strtolower($extension);
		if(!($extension == 'pdf' || $extension == 'zip' || $extension == 'jpg' || $extension == 'png' || $extension == 'jpeg' || $extension == 'gif' || $extension == 'bmp')){
			$return['error'] = "FileType";
			print json_encode($return);
			exit;
		}
	}
	if(isset($_FILES["application_form_name"]["name"]) && !empty($_FILES["application_form_name"]["name"])){
		if ($_FILES['application_form_name']['size'] >= (15 * 1048576)){
			$return['error'] = "FileSize";
			print json_encode($return);
			exit;
		}
		$name = $_FILES["application_form_name"]["name"];
		$extension = pathinfo($name, PATHINFO_EXTENSION);
		$extension = strtolower($extension);
		if(!($extension == 'pdf' || $extension == 'zip' || $extension == 'jpg' || $extension == 'png' || $extension == 'jpeg' || $extension == 'gif' || $extension == 'bmp')){
			$return['error'] = "FileType";
			print json_encode($return);
			exit;
		}
	}
	if(isset($_FILES["payment_receipt_name"]["name"]) && !empty($_FILES["payment_receipt_name"]["name"])){
		if ($_FILES['payment_receipt_name']['size'] >= (15 * 1048576)){
			$return['error'] = "FileSize";
			print json_encode($return);
			exit;
		}
		$name = $_FILES["payment_receipt_name"]["name"];
		$extension = pathinfo($name, PATHINFO_EXTENSION);
		$extension = strtolower($extension);
		if(!($extension == 'pdf' || $extension == 'zip' || $extension == 'jpg' || $extension == 'png' || $extension == 'jpeg' || $extension == 'gif' || $extension == 'bmp')){
			$return['error'] = "FileType";
			print json_encode($return);
			exit;
		}
	}
    
    $work_location_id_arr = array();
    $work_locations = array();
	if(!empty($_POST['work_location'])){
		$table_name = $wpdb->prefix . "work_locations";
		$work_locations = explode(',', $_POST['work_location']);
		foreach($work_locations as $work_location ){
			if(is_numeric($work_location)){
				$work_location_id_arr[] = $work_location;
				$res = $wpdb->get_results("SELECT * FROM " . $table_name . " WHERE `id` = '" . $work_location . "' " ); 
				if(!empty($res)){
					$work_locations[] = $res[0]->location_name;
				}
			} else {
				$work_location = trim($work_location);
				$res = $wpdb->get_results("SELECT * FROM " . $table_name . " WHERE `location_name` = '" . $work_location . "' " ); 
				if(empty($res)){
					$wpdb->insert($table_name, array('location_name' => $work_location) ); 
					$id = $wpdb->insert_id;
					$work_location_id_arr[] = $id;
					$work_locations[] = $work_location;
				} else {
					$work_location_id_arr[] = $res[0]->id;
					$work_locations[] = $res[0]->location_name;
				}
			}
		}
	}
	
	$thematic_areas_work_id_arr = array();
    $thematic_areas_works = array();
	if(!empty($_POST['thematic_areas_works'])){
		$table_name = $wpdb->prefix . "thematic_areas_work";
		$thematic_areas_works = explode(',', $_POST['thematic_areas_works']);
		foreach($thematic_areas_works as $thematic_areas_work ){
			if(is_numeric($thematic_areas_work)){
				$thematic_areas_work_id_arr[] = $thematic_areas_work;
				$res = $wpdb->get_results("SELECT * FROM " . $table_name . " WHERE `id` = '" . $thematic_areas_work . "' " ); 
				if(!empty($res)){
					$thematic_areas_works[] = $res[0]->thematic_area;
				}
			} else {
				$thematic_areas_work = trim($thematic_areas_work);
				$res = $wpdb->get_results("SELECT * FROM " . $table_name . " WHERE `thematic_area` = '" . $thematic_areas_work . "' " ); 
				if(empty($res)){
					$wpdb->insert($table_name, array('thematic_area' => $thematic_areas_work) ); 
					$id = $wpdb->insert_id;
					$thematic_areas_work_id_arr[] = $id;
					$thematic_areas_works[] = $thematic_areas_work;
				} else {
					$thematic_areas_work_id_arr[] = $res[0]->id;
					$thematic_areas_works[] = $res[0]->thematic_area;
				}
			}
		}
	}
	
	if( $ngo_status == 'Rejected'){
		$res = $wpdb->get_results("SELECT * FROM `csr_ngo` WHERE `email_id` = '" . $email_id . "' AND `ngo_status` != 'Rejected' " ); 
		$ngo_d_res = $wpdb->get_results("SELECT * FROM `csr_ngo_details` WHERE `ngo_id` = '" . $ngo_id . "' ORDER BY `ngo_d_id` DESC LIMIT 1" );
		if(!empty($res)){
			$rejected_date = date('Y-m-d H:i:s');
			$wpdb->update('csr_ngo', 
				array(
					'rejected_date' => $rejected_date
				), 
				array('email_id'=>$email_id)
			);
			csr_ngo_rejected_email($email_id);
			$wpdb->update('csr_ngo_details', 
				array(
					'rejected_date' => $rejected_date
				), 
				array('ngo_d_id'=>$ngo_d_res[0]->ngo_d_id)
			);
		}
	}
	if( $ngo_status == 'Applicant'){
		$res = $wpdb->get_results("SELECT * FROM `csr_ngo` WHERE `email_id` = '" . $email_id . "' AND `ngo_status` != 'Applicant' " ); 
		$ngo_d_res = $wpdb->get_results("SELECT * FROM `csr_ngo_details` WHERE `ngo_id` = '" . $ngo_id . "' ORDER BY `ngo_d_id` DESC LIMIT 1" );
		if(!empty($res)){
			csr_ngo_applicant_email($email_id, $hub_code);
			$wpdb->update('csr_ngo_details', 
				array(
					'hub_code' => $res[0]->hub_code
				), 
				array('ngo_d_id'=>$ngo_d_res[0]->ngo_d_id)
			);
		}
	}
	if( $ngo_status == 'Empanelled'){
		$res = $wpdb->get_results("SELECT * FROM `csr_ngo` WHERE `email_id` = '" . $email_id . "' AND `ngo_status` != 'Empanelled' " ); 
		$ngo_d_res = $wpdb->get_results("SELECT * FROM `csr_ngo_details` WHERE `ngo_id` = '" . $ngo_id . "' ORDER BY `ngo_d_id` DESC LIMIT 1" ); 
		if(!empty($res)){
			$attachments = array();
			$path = wp_upload_dir();
			$empanellment_certificate_name = '';
			if(isset($_FILES["empanellment_certificate"]["name"]) && !empty($_FILES["empanellment_certificate"]["name"])){
				$name = $_FILES["empanellment_certificate"]["name"];
				$tmpName = $_FILES["empanellment_certificate"]["tmp_name"];
				$extension = pathinfo($name, PATHINFO_EXTENSION);
				$name = $ngo_id.'_certificate_'.time().'.'.$extension;
				$a = $path['basedir']."/ngo_files/empanellment_certificate/".$name; 
				$targetPath = str_replace("\\", "/", $a);
				move_uploaded_file($tmpName,$targetPath);
				$attachments[] = WP_CONTENT_DIR . '/uploads/ngo_files/empanellment_certificate/' . $name;
				$empanellment_certificate_name = $name;
				$wpdb->update('csr_ngo', 
					array(
						'empanellment_certificate' => $name
					), 
					array('id'=>$ngo_id)
				);
			}
			$empanelled_start_date = substr($_REQUEST['empanelled_start_date'], 6, 4).'-'.substr($_REQUEST['empanelled_start_date'], 3, 2).'-'.substr($_REQUEST['empanelled_start_date'], 0, 2);
			$empanelled_end_date = substr($_REQUEST['empanelled_end_date'], 6, 4).'-'.substr($_REQUEST['empanelled_end_date'], 3, 2).'-'.substr($_REQUEST['empanelled_end_date'], 0, 2);
			$wpdb->update('csr_ngo', 
				array(
					'empanelled_start_date' => $empanelled_start_date,
					'empanelled_end_date' => $empanelled_end_date
				), 
				array('email_id'=>$email_id)
			);
			csr_ngo_empanelled_email($email_id, $hub_code, $_REQUEST['empanelled_start_date'], $_REQUEST['empanelled_end_date'], $attachments);
			$wpdb->update('csr_ngo_details', 
				array(
					'hub_code' => $res[0]->hub_code,
					'empanellment_certificate' => $empanellment_certificate_name,
					'empanelled_start_date' => $empanelled_start_date,
					'empanelled_end_date' => $empanelled_end_date
				), 
				array('ngo_d_id'=>$ngo_d_res[0]->ngo_d_id)
			);
		}
	}
	
	$return = ''; 
	$wpdb->update('csr_ngo', 
		array(
			'organisation_name' => $organisation_name,
			'operation_scale_id' => $operation_scale_id,
			'organisation_type_id' => $organisation_type_id,
			//'thematic_areas_work_id' => $thematic_areas_work_id,
			'sub_thematic_area' => $sub_thematic_area,
			'postal_address' => $postal_address,
			'person_firstname' => $person_firstname,
			'person_lastname' => $person_lastname,
			'email_id' => $email_id,
			'pan_no' => $pan_no,
			'phone_number' => $phone_number,
			'mobile_number' => $mobile_number,
			'ngo_status' => $ngo_status,
			'hub_code' => $hub_code,
			'score_total' => $score_total,
			'score_received' => $score_received,
			'rating' => $rating,
		), 
		array('id'=>$ngo_id)
	);
	$ngo_d_res = $wpdb->get_results("SELECT * FROM `csr_ngo_details` WHERE `ngo_id` = '" . $ngo_id . "' ORDER BY `ngo_d_id` DESC LIMIT 1" ); 
	$wpdb->update('csr_ngo_details', 
		array(
			'score_total' => $score_total,
			'score_received' => $score_received,
			'rating' => $rating
		), 
		array('ngo_d_id'=>$ngo_d_res[0]->ngo_d_id)
	);
	
	$wpdb->query( "DELETE FROM `csr_ngo_work_locations` WHERE `ngo_id` = '".$ngo_id."' " );
	$wpdb->query( "DELETE FROM `csr_ngo_thematic_areas_works` WHERE `ngo_id` = '".$ngo_id."' " );
	foreach($work_location_id_arr as $work_location_id){
		$wpdb->insert('csr_ngo_work_locations', 
			array(
				'ngo_id' => $ngo_id,
				'work_locations_id' => $work_location_id
			) 
		);
	}
	foreach($thematic_areas_work_id_arr as $thematic_areas_work_id){
		$wpdb->insert('csr_ngo_thematic_areas_works', 
			array(
				'ngo_id' => $ngo_id,
				'thematic_areas_id' => $thematic_areas_work_id
			) 
		);
	}
	$_SESSION['ngo_updated'] = "ngo_updated";
	
	$path = wp_upload_dir();
	$attachments = array();
	if(isset($_FILES["application_form_name"]["name"]) && !empty($_FILES["application_form_name"]["name"])){
		$name = $_FILES["application_form_name"]["name"];
		$tmpName = $_FILES["application_form_name"]["tmp_name"];
		$extension = pathinfo($name, PATHINFO_EXTENSION);
		$name = $ngo_id.'_application_'.time().'.'.$extension;
		$a = $path['basedir']."/ngo_files/".$name; 
		$targetPath = str_replace("\\", "/", $a);
		move_uploaded_file($tmpName,$targetPath);
		$attachments[] = WP_CONTENT_DIR . '/uploads/ngo_files/' . $name;
		$wpdb->query("UPDATE `csr_ngo` SET `application_form_name`='".$name."' WHERE `id`='".$ngo_id."' " ); 
	}
	if(isset($_FILES["payment_receipt_name"]["name"]) && !empty($_FILES["payment_receipt_name"]["name"])){
		$name = $_FILES["payment_receipt_name"]["name"];
		$tmpName = $_FILES["payment_receipt_name"]["tmp_name"];
		$extension = pathinfo($name, PATHINFO_EXTENSION);
		$name = $ngo_id.'_payment_'.time().'.'.$extension;
		$a = $path['basedir']."/ngo_files/".$name; 
		$targetPath = str_replace("\\", "/", $a);
		move_uploaded_file($tmpName,$targetPath);
		$attachments[] = WP_CONTENT_DIR . '/uploads/ngo_files/' . $name;
		$wpdb->query("UPDATE `csr_ngo` SET `payment_receipt_name`='".$name."' WHERE `id`='".$ngo_id."' " ); 
	}
	
	// Analysis Report file upload
	if(isset($_FILES["analysis_report"]["name"]) && !empty($_FILES["analysis_report"]["name"])){
		$name = $_FILES["analysis_report"]["name"];
		$tmpName = $_FILES["analysis_report"]["tmp_name"];
		$extension = pathinfo($name, PATHINFO_EXTENSION);
		$name = $ngo_id.'_analysis_report_'.time().'.'.$extension;
		$a = $path['basedir']."/ngo_files/analysis_report/".$name; 
		$targetPath = str_replace("\\", "/", $a);
		move_uploaded_file($tmpName,$targetPath);
		$attachments[] = WP_CONTENT_DIR . '/uploads/ngo_files/analysis_report/' . $name;
		$empanellment_certificate_name = $name;
		$wpdb->update('csr_ngo', 
			array(
				'analysis_report' => $name
			), 
			array('id'=>$ngo_id)
		);
		$wpdb->update('csr_ngo_details', 
		array(
			'analysis_report' => $name,
		), 
		array('ngo_d_id'=>$ngo_d_res[0]->ngo_d_id)
	);
	}
	
	if($is_front_call == "1"){
		$app_type = 'Renew';
		add_new_ngo_email($ngo_id, $app_type, $attachments);
		$res = $wpdb->get_results("SELECT * FROM `csr_ngo` WHERE `id` = '" . $ngo_id . "' LIMIT 1 " ); 
		if(!empty($res)){
			if(!empty($last_3_month) && $last_3_month == 'last_3_month'){
				$wpdb->update('csr_ngo', 
					array(
						'hub_code' => '',
						'ngo_status' => 'Empanelment Live - Applied for Renewal',
						'created_date' => date('Y-m-d H:i:s'),
						'rejected_date' => '',
						'empanelled_start_date' => '',
						'empanelled_end_date' => ''
					), 
					array('id'=>$ngo_id)
				);
			} else {
				$wpdb->update('csr_ngo', 
					array(
						'hub_code' => '',
						'ngo_status' => 'Expired - Applied for Renewal',
						'created_date' => date('Y-m-d H:i:s'),
						'rejected_date' => '',
						'empanelled_start_date' => '',
						'empanelled_end_date' => ''
					), 
					array('id'=>$ngo_id)
				); 
			}
			$wpdb->insert('csr_ngo_details', 
				array(
					'ngo_id' => $ngo_id,
					'application_form_name' => $res[0]->application_form_name,
					'payment_receipt_name' => $res[0]->payment_receipt_name,
					'created_date' => date('Y-m-d H:i:s'),
				) 
			);
		}
	}
	
	$return['success'] = "Updated";
	print json_encode($return);
	exit;
}
add_action( 'wp_ajax_update_ngo', 'update_ngo');
add_action( 'wp_ajax_nopriv_update_ngo', 'update_ngo');

function change_level_status(){
	global $wpdb;
	$user_id = get_current_user_id();
	$ngo_id = $_REQUEST['ngo_id'];
	$level_status_field = $_REQUEST['level_status'];
	$level_note = substr($level_status_field, 0, 2).'_note';
	$level_status = '';
	if($_REQUEST['level_label'] == 'Accept'){
		$level_status = 'Accepted';
	}
	if($_REQUEST['level_label'] == 'Reject'){
		$level_status = 'Rejected';
	}
	$result = $wpdb->update('csr_ngo', 
		array(
			$level_status_field => $level_status,
			$level_note => $_REQUEST['level_note'],
		), 
		array('id'=>$ngo_id)
	);
	$result = $wpdb->insert('csr_ngo_level_logs', 
		array(
			'user_id' => $user_id,
			'ngo_id' => $ngo_id,
			'level' => ucfirst(substr($level_status_field, 0, 2)),
			'level_status' => $level_status,
			'level_note' => $_REQUEST['level_note'],
			'created_date' => date('Y-m-d H:i:s'),
		) 
	);
	$_SESSION['level_status_changed'] = 'Scrutiny Level Status '.ucfirst(substr($level_status_field, 0, 2)) . ' ' . $level_status ;
	$return['success'] = "Changed";
	print json_encode($return);
	exit;
}
add_action( 'wp_ajax_change_level_status', 'change_level_status');
function level_status_reset(){
	global $wpdb;
	$ngo_id = $_REQUEST['ngo_id'];
	$ls_field = $_REQUEST['ls_field'];
	$ln_field = $_REQUEST['ln_field'];
	$result = $wpdb->update('csr_ngo', 
		array(
			$ls_field => '',
			$ln_field => '',
		), 
		array('id'=>$ngo_id)
	);
	$_SESSION['level_status_changed'] = 'Scrutiny Level Status '.ucfirst(substr($ls_field, 0, 2)) . ' Reset Successfully' ;
	echo "level_status_reset";
	exit;
}
add_action( 'wp_ajax_level_status_reset', 'level_status_reset');

function admin_ngo_add(){
	global $wpdb; 
	$organisation_name = $_REQUEST['organisation_name'];
	$operation_scale_id = $_REQUEST['operation_scale_id'];
	$organisation_type_id = $_REQUEST['organisation_type_id'];
	$sub_thematic_area = $_REQUEST['sub_thematic_area'];
	$postal_address = $_REQUEST['postal_address'];
	$person_firstname = $_REQUEST['person_firstname'];
	$person_lastname = $_REQUEST['person_lastname'];
	$email_id = $_REQUEST['email_id'];
	$pan_no = $_REQUEST['pan_no'];
	$phone_number = $_REQUEST['phone_number'];
	$mobile_number = $_REQUEST['mobile_number'];
	$ngo_status = $_REQUEST['ngo_status'];
	$hub_code = $_REQUEST['hub_code'];
	
    $res = $wpdb->get_results("SELECT * FROM `csr_ngo` WHERE `pan_no` = '" . $pan_no . "' " ); 
	if(!empty($res)){
		$return['error'] = "Exist";
		print json_encode($return);
		exit;
	}

    $res = $wpdb->get_results("SELECT * FROM `csr_ngo` WHERE `hub_code` = '" . $hub_code . "' " ); 
	if(!empty($res)){
		$return['error'] = "HubCodeExist";
		print json_encode($return);
		exit;
	}
	
	//file size and type validations
	if(isset($_FILES["empanellment_certificate"]["name"]) && !empty($_FILES["empanellment_certificate"]["name"])){
		if ($_FILES['empanellment_certificate']['size'] >= (10 * 1048576)){
			$return['error'] = "FileSize";
			print json_encode($return);
			exit;
		}
		$name = $_FILES["empanellment_certificate"]["name"];
		$extension = pathinfo($name, PATHINFO_EXTENSION);
		$extension = strtolower($extension);
		if(!($extension == 'pdf' || $extension == 'zip' || $extension == 'jpg' || $extension == 'png' || $extension == 'jpeg' || $extension == 'gif' || $extension == 'bmp')){
			$return['error'] = "FileType";
			print json_encode($return);
			exit;
		}
	}
	if(isset($_FILES["application_form_name"]["name"]) && !empty($_FILES["application_form_name"]["name"])){
		if ($_FILES['application_form_name']['size'] >= (15 * 1048576)){
			$return['error'] = "FileSize";
			print json_encode($return);
			exit;
		}
		$name = $_FILES["application_form_name"]["name"];
		$extension = pathinfo($name, PATHINFO_EXTENSION);
		$extension = strtolower($extension);
		if(!($extension == 'pdf' || $extension == 'zip' || $extension == 'jpg' || $extension == 'png' || $extension == 'jpeg' || $extension == 'gif' || $extension == 'bmp')){
			$return['error'] = "FileType";
			print json_encode($return);
			exit;
		}
	}
	if(isset($_FILES["payment_receipt_name"]["name"]) && !empty($_FILES["payment_receipt_name"]["name"])){
		if ($_FILES['payment_receipt_name']['size'] >= (15 * 1048576)){
			$return['error'] = "FileSize";
			print json_encode($return);
			exit;
		}
		$name = $_FILES["payment_receipt_name"]["name"];
		$extension = pathinfo($name, PATHINFO_EXTENSION);
		$extension = strtolower($extension);
		if(!($extension == 'pdf' || $extension == 'zip' || $extension == 'jpg' || $extension == 'png' || $extension == 'jpeg' || $extension == 'gif' || $extension == 'bmp')){
			$return['error'] = "FileType";
			print json_encode($return);
			exit;
		}
	}
    
    $work_location_id_arr = array();
    $work_locations = array();
	if(!empty($_POST['work_location'])){
		$table_name = $wpdb->prefix . "work_locations";
		$work_locations = explode(',', $_POST['work_location']);
		foreach($work_locations as $work_location ){
			if(is_numeric($work_location)){
				$work_location_id_arr[] = $work_location;
				$res = $wpdb->get_results("SELECT * FROM " . $table_name . " WHERE `id` = '" . $work_location . "' " ); 
				if(!empty($res)){
					$work_locations[] = $res[0]->location_name;
				}
			} else {
				$work_location = trim($work_location);
				$res = $wpdb->get_results("SELECT * FROM " . $table_name . " WHERE `location_name` = '" . $work_location . "' " ); 
				if(empty($res)){
					$wpdb->insert($table_name, array('location_name' => $work_location) ); 
					$id = $wpdb->insert_id;
					$work_location_id_arr[] = $id;
					$work_locations[] = $work_location;
				} else {
					$work_location_id_arr[] = $res[0]->id;
					$work_locations[] = $res[0]->location_name;
				}
			}
		}
	}
	
	$thematic_areas_work_id_arr = array();
    $thematic_areas_works = array();
	if(!empty($_POST['thematic_areas_works'])){
		$table_name = $wpdb->prefix . "thematic_areas_work";
		$thematic_areas_works = explode(',', $_POST['thematic_areas_works']);
		foreach($thematic_areas_works as $thematic_areas_work ){
			if(is_numeric($thematic_areas_work)){
				$thematic_areas_work_id_arr[] = $thematic_areas_work;
				$res = $wpdb->get_results("SELECT * FROM " . $table_name . " WHERE `id` = '" . $thematic_areas_work . "' " ); 
				if(!empty($res)){
					$thematic_areas_works[] = $res[0]->thematic_area;
				}
			} else {
				$thematic_areas_work = trim($thematic_areas_work);
				$res = $wpdb->get_results("SELECT * FROM " . $table_name . " WHERE `thematic_area` = '" . $thematic_areas_work . "' " ); 
				if(empty($res)){
					$wpdb->insert($table_name, array('thematic_area' => $thematic_areas_work) ); 
					$id = $wpdb->insert_id;
					$thematic_areas_work_id_arr[] = $id;
					$thematic_areas_works[] = $thematic_areas_work;
				} else {
					$thematic_areas_work_id_arr[] = $res[0]->id;
					$thematic_areas_works[] = $res[0]->thematic_area;
				}
			}
		}
	}
	
	$return = ''; 
	$wpdb->insert('csr_ngo', 
		array(
			'organisation_name' => $organisation_name,
			'operation_scale_id' => $operation_scale_id,
			'organisation_type_id' => $organisation_type_id,
			'sub_thematic_area' => $sub_thematic_area,
			'postal_address' => $postal_address,
			'person_firstname' => $person_firstname,
			'person_lastname' => $person_lastname,
			'email_id' => $email_id,
			'pan_no' => $pan_no,
			'phone_number' => $phone_number,
			'mobile_number' => $mobile_number,
			'ngo_status' => 'New',
			'created_date' => date('Y-m-d H:i:s')
		) 
	); 
	$ngo_id = $wpdb->insert_id;
	foreach($work_location_id_arr as $work_location_id){
		$wpdb->insert('csr_ngo_work_locations', 
			array(
				'ngo_id' => $ngo_id,
				'work_locations_id' => $work_location_id
			) 
		);
	} 
	foreach($thematic_areas_work_id_arr as $thematic_areas_work_id){
		$wpdb->insert('csr_ngo_thematic_areas_works', 
			array(
				'ngo_id' => $ngo_id,
				'thematic_areas_id' => $thematic_areas_work_id
			) 
		);
	}
	$app_type = 'New';
	$path = wp_upload_dir();
	$attachments = array();
	if(isset($_FILES["application_form_name"]["name"]) && !empty($_FILES["application_form_name"]["name"])){
		$name = $_FILES["application_form_name"]["name"];
		$tmpName = $_FILES["application_form_name"]["tmp_name"];
		$extension = pathinfo($name, PATHINFO_EXTENSION);
		$name = $ngo_id.'_application_'.time().'.'.$extension;
		$a = $path['basedir']."/ngo_files/".$name; 
		$targetPath = str_replace("\\", "/", $a);
		move_uploaded_file($tmpName,$targetPath);
		$attachments[] = WP_CONTENT_DIR . '/uploads/ngo_files/' . $name;
		$wpdb->query("UPDATE `csr_ngo` SET `application_form_name`='".$name."' WHERE `id`='".$ngo_id."' " ); 
	}
	if(isset($_FILES["payment_receipt_name"]["name"]) && !empty($_FILES["payment_receipt_name"]["name"])){
		$name = $_FILES["payment_receipt_name"]["name"];
		$tmpName = $_FILES["payment_receipt_name"]["tmp_name"];
		$extension = pathinfo($name, PATHINFO_EXTENSION);
		$name = $ngo_id.'_payment_'.time().'.'.$extension;
		$a = $path['basedir']."/ngo_files/".$name; 
		$targetPath = str_replace("\\", "/", $a);
		move_uploaded_file($tmpName,$targetPath);
		$attachments[] = WP_CONTENT_DIR . '/uploads/ngo_files/' . $name;
		$wpdb->query("UPDATE `csr_ngo` SET `payment_receipt_name`='".$name."' WHERE `id`='".$ngo_id."' " ); 
	}
	add_new_ngo_email($ngo_id, $app_type, $attachments);
    $res = $wpdb->get_results("SELECT * FROM `csr_ngo` WHERE `id` = '" . $ngo_id . "' LIMIT 1 " ); 
	if(!empty($res)){		
		$wpdb->insert('csr_ngo_details', 
			array(
				'ngo_id' => $ngo_id,
				'hub_code' => $res[0]->hub_code,
				'application_form_name' => $res[0]->application_form_name,
				'payment_receipt_name' => $res[0]->payment_receipt_name,
				'created_date' => $res[0]->created_date
			) 
		);
	}
	if( $ngo_status == 'Rejected'){
		$res = $wpdb->get_results("SELECT * FROM `csr_ngo` WHERE `email_id` = '" . $email_id . "' AND `ngo_status` != 'Rejected' " ); 
		$ngo_d_res = $wpdb->get_results("SELECT * FROM `csr_ngo_details` WHERE `ngo_id` = '" . $ngo_id . "' ORDER BY `ngo_d_id` DESC LIMIT 1" );
		if(!empty($res)){
			$rejected_date = date('Y-m-d H:i:s');
			$wpdb->update('csr_ngo', 
				array(
					'rejected_date' => $rejected_date
				), 
				array('email_id'=>$email_id)
			);
			csr_ngo_rejected_email($email_id);
			$wpdb->update('csr_ngo_details', 
				array(
					'rejected_date' => $rejected_date
				), 
				array('ngo_d_id'=>$ngo_d_res[0]->ngo_d_id)
			);
		}
	}
	if( $ngo_status == 'Applicant'){
		$res = $wpdb->get_results("SELECT * FROM `csr_ngo` WHERE `email_id` = '" . $email_id . "' AND `ngo_status` != 'Applicant' " ); 
		$ngo_d_res = $wpdb->get_results("SELECT * FROM `csr_ngo_details` WHERE `ngo_id` = '" . $ngo_id . "' ORDER BY `ngo_d_id` DESC LIMIT 1" );
		if(!empty($res)){
			csr_ngo_applicant_email($email_id, $hub_code);
			$wpdb->update('csr_ngo_details', 
				array(
					'hub_code' => $res[0]->hub_code
				), 
				array('ngo_d_id'=>$ngo_d_res[0]->ngo_d_id)
			);
		}
	}
	if( $ngo_status == 'Empanelled'){
		$res = $wpdb->get_results("SELECT * FROM `csr_ngo` WHERE `email_id` = '" . $email_id . "' AND `ngo_status` != 'Empanelled' " ); 
		$ngo_d_res = $wpdb->get_results("SELECT * FROM `csr_ngo_details` WHERE `ngo_id` = '" . $ngo_id . "' ORDER BY `ngo_d_id` DESC LIMIT 1" ); 
		if(!empty($res)){
			$attachments = array();
			$path = wp_upload_dir();
			$empanellment_certificate_name = '';
			if(isset($_FILES["empanellment_certificate"]["name"]) && !empty($_FILES["empanellment_certificate"]["name"])){
				$name = $_FILES["empanellment_certificate"]["name"];
				$tmpName = $_FILES["empanellment_certificate"]["tmp_name"];
				$extension = pathinfo($name, PATHINFO_EXTENSION);
				$name = $ngo_id.'_certificate_'.time().'.'.$extension;
				$a = $path['basedir']."/ngo_files/empanellment_certificate/".$name; 
				$targetPath = str_replace("\\", "/", $a);
				move_uploaded_file($tmpName,$targetPath);
				$attachments[] = WP_CONTENT_DIR . '/uploads/ngo_files/empanellment_certificate/' . $name;
				$empanellment_certificate_name = $name;
				$wpdb->update('csr_ngo', 
					array(
						'empanellment_certificate' => $name
					), 
					array('id'=>$ngo_id)
				);
			}
			$empanelled_start_date = substr($_REQUEST['empanelled_start_date'], 6, 4).'-'.substr($_REQUEST['empanelled_start_date'], 3, 2).'-'.substr($_REQUEST['empanelled_start_date'], 0, 2);
			$empanelled_end_date = substr($_REQUEST['empanelled_end_date'], 6, 4).'-'.substr($_REQUEST['empanelled_end_date'], 3, 2).'-'.substr($_REQUEST['empanelled_end_date'], 0, 2);
			$wpdb->update('csr_ngo', 
				array(
					'empanelled_start_date' => $empanelled_start_date,
					'empanelled_end_date' => $empanelled_end_date
				), 
				array('email_id'=>$email_id)
			);
			csr_ngo_empanelled_email($email_id, $hub_code, $_REQUEST['empanelled_start_date'], $_REQUEST['empanelled_end_date'], $attachments);
			$wpdb->update('csr_ngo_details', 
				array(
					'hub_code' => $res[0]->hub_code,
					'empanellment_certificate' => $empanellment_certificate_name,
					'empanelled_start_date' => $empanelled_start_date,
					'empanelled_end_date' => $empanelled_end_date
				), 
				array('ngo_d_id'=>$ngo_d_res[0]->ngo_d_id)
			);
		}
	}
	$_SESSION['ngo_added'] = "ngo_added";
	$return['success'] = "Added";
	print json_encode($return);
	exit;
}
add_action( 'wp_ajax_admin_ngo_add', 'admin_ngo_add');


function generate_hub_code(){
	$prefix = 'A';
	$month = date('m');
	$year = date('y');
	$suffix = (isset($_REQUEST['ngo_id']) && $_REQUEST['ngo_id'] > 0)? $_REQUEST['ngo_id'] : '';
	
	$prefix_month = array('04','05','06','07','08','09');
	$prefix = (in_array($prefix, $prefix_month))? $prefix : 'B';
		
	$return['hub_code'] = $prefix.'/'.$year.'/'.$month.'/'.$suffix;
	$return['success'] = "Generated";
	print json_encode($return);
	exit;
}
add_action( 'wp_ajax_generate_hub_code', 'generate_hub_code');

function ngo_filter_list(){
	global $wpdb; 
	$requestData = $_REQUEST;
	$columns = array( 
		0 => 'organisation_name', 
		1 => 'hub_code', 
	);
	$table_name = $wpdb->prefix . "ngo";
	$result = $wpdb->get_results("SELECT * FROM $table_name");
	$totalData = $wpdb->num_rows;
	$totalFiltered = $totalData;

	$sql = "SELECT `csr_ngo`.`id`, `csr_ngo`.`organisation_name`, `csr_ngo`.`hub_code`, `csr_ngo`.`ngo_status`, `csr_ngo`.`created_date`, `csr_ngo`.`empanelled_start_date`, `csr_ngo`.`empanelled_end_date`, `csr_ngo`.`rejected_date`, ";
	$sql .= " `csr_ngo`.`l1_status`, `csr_ngo`.`l2_status`, `csr_ngo`.`l3_status`, `csr_ngo`.`l4_status`, `csr_ngo`.`l5_status` ";
	$sql .= " FROM `csr_ngo` ";
	$sql .= " LEFT JOIN `csr_ngo_work_locations` ON `csr_ngo`.`id`=`csr_ngo_work_locations`.`ngo_id` ";
	$sql .= " LEFT JOIN `csr_ngo_thematic_areas_works` ON `csr_ngo`.`id`=`csr_ngo_thematic_areas_works`.`ngo_id` ";
	if( $requestData['ngo_type'] == 'trashed' ){
		$sql .= " WHERE `csr_ngo`.`isTrash`= 1 ";
	} else {
		$sql .= " WHERE `csr_ngo`.`isTrash`= 0 ";
	}
	if(!empty($requestData['ngo_status'])){
		$sql .= " AND  `csr_ngo`.`ngo_status` LIKE '".$requestData['ngo_status']."' ";
	}
	if(!empty($requestData['operation_scale_id'])){
		$sql .= " AND  `csr_ngo`.`operation_scale_id` LIKE '".$requestData['operation_scale_id']."' ";
	}
	if(!empty($requestData['organisation_type_id'])){
		$sql .= " AND  `csr_ngo`.`organisation_type_id` LIKE '".$requestData['organisation_type_id']."' ";
	}
	if(!empty($requestData['thematic_areas_work_id'])){
		$sql .= " AND  `csr_ngo_thematic_areas_works`.`thematic_areas_id` LIKE '".$requestData['thematic_areas_work_id']."' ";
	}
	if(!empty($requestData['work_location'])){
		$sql .= " AND  `csr_ngo_work_locations`.`work_locations_id` LIKE '".$requestData['work_location']."' ";
	}
	if( !empty($requestData['search']['value']) ) {
		$sql .= " AND ( `csr_ngo`.`organisation_name` LIKE '%".$requestData['search']['value']."%' ";    
		$sql .= " OR `csr_ngo`.`hub_code` LIKE '%".$requestData['search']['value']."%' )";
	}
	$sql .= " GROUP BY `csr_ngo_work_locations`.`ngo_id` ";
	$result = $wpdb->get_results($sql);
	$totalData = $totalFiltered = $wpdb->num_rows;
	$sql .= " ORDER BY `csr_ngo`.`created_date` DESC LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
	$result = $wpdb->get_results($sql);
	$inc = 1;
	$data = array();
	foreach($result as $row){
		
		$display_date = '<span> Created : ';
		$display_date .= date('d/m/Y', strtotime($row->created_date));
		$display_date .= '</span>';
		if($row->ngo_status == 'Empanelled'){
			$display_date .= '<br /><span style="color: #3c763d;"> Empanelled : ';
			$display_date .= date('d/m/Y', strtotime($row->empanelled_start_date)).' - To - <br />';
			if($row->empanelled_end_date != '0000-00-00 00:00:00'){
				$display_date .= date('d/m/Y', strtotime($row->empanelled_end_date));
			}
			$display_date .= '</span>';
		}
		if($row->ngo_status == 'Rejected'){
			$display_date .= '<br /><span style="color: #a94442;"> Rejected : ';
			$display_date .= date('d/m/Y', strtotime($row->rejected_date));
			$display_date .= '</span>';
		}
		
		$hub_code = $row->hub_code;
		
		$nestedData = array(); 
		$nestedData[] = $inc;
		$nestedData[] = '<a href="?page=ngo&action=ngo_details&id='.$row->id.'">'.$row->organisation_name.'</a>';
		$nestedData[] = $hub_code;
		if($row->ngo_status == 'Empanelment Live - Applied for Renewal'){
			$row->ngo_status = '<span class="label label-danger">Empanelment Live - Applied for Renewal</span>';
		}
		if($row->ngo_status == 'Expired - Applied for Renewal'){
			$row->ngo_status = '<span class="label label-danger">Expired - Applied for Renewal</span>';
		}
		if($row->ngo_status == 'New'){
			$row->ngo_status = '<span class="label label-success">New</span>';
		}
		$nestedData[] = $row->ngo_status;
		$nestedData[] = $display_date;
		if( $requestData['ngo_type'] != 'trashed' ){
			$nestedData[] = '<ol class="scrutiny_status">
							  <li class="'.$row->l1_status.'">&nbsp;</li>
							  <li class="'.$row->l2_status.'">&nbsp;</li>
							  <li class="'.$row->l3_status.'">&nbsp;</li>
							  <li class="'.$row->l4_status.'">&nbsp;</li>
							  <li class="'.$row->l5_status.'">&nbsp;</li>
							</ol>';
		}
		$delete_icon = '';
		if($row->ngo_status == 'Rejected'){
			if( $requestData['ngo_type'] == 'trashed' ){
				$delete_icon = '<span class="text-info" style="cursor:pointer;" onClick="ngo_restore('.$row->id.')" >Restore</span>'.'|'.'<span class="text-info" style="cursor:pointer;" onClick="ngo_delete('.$row->id.')" >Delete</span>';
			} else {
				$delete_icon = '<span class="label label-danger" style="cursor:pointer;" onClick="ngo_move_to_trash('.$row->id.')" ><i class="fa fa-trash"></i></span>';
			}
		}
		$nestedData[] = $delete_icon;
		$data[] = $nestedData;
		$inc++;
	}
	$json_data = array(
		"draw"            => intval( $requestData['draw'] ), 
		"recordsTotal"    => intval( $totalData ),  
		"recordsFiltered" => intval( $totalFiltered ), 
		"data"            => $data
	);
	echo json_encode($json_data);
	exit;
}
add_action( 'wp_ajax_ngo_filter_list', 'ngo_filter_list');

function ngo_move_to_trash(){
	global $wpdb;
	$ngo_id = $_REQUEST['ngo_id'];
	$_SESSION['ngo_moved'] = "ngo_moved";
	$wpdb->update( 'csr_ngo', array('isTrash' => '1' ), array( 'id' => $ngo_id ) );
	echo 'ngo_moved';
	exit;
}
add_action( 'wp_ajax_ngo_move_to_trash', 'ngo_move_to_trash');

function ngo_restore(){
	global $wpdb;
	$ngo_id = $_REQUEST['ngo_id'];
	$_SESSION['ngo_restored'] = "ngo_restored";
	$wpdb->update( 'csr_ngo', array('isTrash' => '0' ), array( 'id' => $ngo_id ) );
	echo 'ngo_restored';
	exit;
}
add_action( 'wp_ajax_ngo_restore', 'ngo_restore');

function ngo_delete(){
	global $wpdb;
	$ngo_id = $_REQUEST['ngo_id'];
	$_SESSION['ngo_deleted'] = "ngo_deleted";
	$wpdb->query( "DELETE FROM `csr_ngo_details` WHERE `ngo_id` = '".$ngo_id."'" );
	$wpdb->query( "DELETE FROM `csr_level_logs` WHERE `ngo_id` = '".$ngo_id."'" );
	$wpdb->query( "DELETE FROM `csr_ngo_thematic_areas_works` WHERE `ngo_id` = '".$ngo_id."'" );
	$wpdb->query( "DELETE FROM `csr_ngo_work_locations` WHERE `ngo_id` = '".$ngo_id."'" );
	$wpdb->query( "DELETE FROM `csr_ngo` WHERE `id` = '".$ngo_id."'" );
	echo 'ngo_deleted';
	exit;
}
add_action( 'wp_ajax_ngo_delete', 'ngo_delete');

function front_empanelled_ngo_list_func(){
	global $wpdb;
	$sql = "SELECT `csr_ngo`.`id`, `csr_ngo`.`organisation_name`, `csr_ngo`.`hub_code`, `csr_ngo`.`ngo_status`, `csr_ngo`.`created_date`,
			`csr_ngo`.`postal_address`, `csr_ngo`.`person_firstname`, `csr_ngo`.`person_lastname`, `csr_ngo`.`email_id`, 
			`csr_ngo`.`phone_number`, `csr_ngo`.`mobile_number`,
			`csr_operation_scale`.`operation_scale`, `csr_organisation_type`.`type_name`, `csr_thematic_areas_work`.`thematic_area` ";
	$sql .= " FROM `csr_ngo` ";
	$sql .= " JOIN `csr_operation_scale` ON `csr_ngo`.`operation_scale_id`=`csr_operation_scale`.`id` ";
	$sql .= " JOIN `csr_organisation_type` ON `csr_ngo`.`organisation_type_id`=`csr_organisation_type`.`id` ";
	$sql .= " JOIN `csr_thematic_areas_work` ON `csr_ngo`.`thematic_areas_work_id`=`csr_thematic_areas_work`.`id` ";
	$sql .= " WHERE `csr_ngo`.`ngo_status` = 'Empanelled' ";
	$ngo_result = $wpdb->get_results($sql);
	
	$ngo_arr = array();
	$table = '';
	
	if(is_array($ngo_result) && count($ngo_result) > 0){
		$table .= '<table style="width: 100%;"><thead><tr><th style="background-color: #ffde97;">Sr. No.</th><th style="background-color: #e4f4a5;">Organisation Name</th><th style="background-color: #ffde97;">Contact Person</th><th style="background-color: #a3e7f0;">Mobile</th><th style="background-color: #e4f4a5;">Address</th><th style="background-color: #a8aaf4;">Type</th><th style="background-color: #ffde97;">Thematic Area</th></tr><tbody>';
		$i = 1;
		foreach($ngo_result as $ngo){
			
			$tr = '<tr>';
				$tr .= '<td>' . $i . '</td>';
				$tr .= '<td>' . $ngo->organisation_name . '</td>';
				$tr .= '<td>' . ucfirst($ngo->person_firstname) . ' ' . ucfirst($ngo->person_lastname) . '</td>';
				$tr .= '<td>' . $ngo->mobile_number . '</td>';
				$tr .= '<td>' . $ngo->postal_address . '</td>';
				$tr .= '<td>' . $ngo->type_name . '</td>';
				$tr .= '<td>' . $ngo->thematic_area . '</td>';
			$tr .= '</tr>';
			
			$table .= $tr;
		$i++;
		}
		$table .= '</tbody></table>';
	}
	include "front_empanelled_ngo_list.php";
	
}
add_shortcode('empanelled_ngo_list', 'front_empanelled_ngo_list_func');

function front_empanelled_ngo_filter(){
	global $wpdb; 
	$requestData = $_REQUEST;
	$columns = array( 
		0 => 'organisation_name', 
		1 => 'hub_code', 
	);
	$table_name = $wpdb->prefix . "ngo";
	$result = $wpdb->get_results("SELECT * FROM $table_name");
	$totalData = $wpdb->num_rows;
	$totalFiltered = $totalData;

	$sql = "SELECT `csr_ngo`.`id`, `csr_ngo`.`organisation_name`, `csr_ngo`.`person_firstname`, `csr_ngo`.`person_lastname`, `csr_ngo`.`hub_code`, `csr_ngo`.`ngo_status`, `csr_ngo`.`created_date`, `csr_organisation_type`.`type_name` ";
	$sql .= " FROM `csr_ngo` ";
	$sql .= " LEFT JOIN `csr_ngo_work_locations` ON `csr_ngo`.`id`=`csr_ngo_work_locations`.`ngo_id` ";
	$sql .= " LEFT JOIN `csr_ngo_thematic_areas_works` ON `csr_ngo`.`id`=`csr_ngo_thematic_areas_works`.`ngo_id` ";
	$sql .= " LEFT JOIN `csr_organisation_type` ON `csr_ngo`.`organisation_type_id`=`csr_organisation_type`.`id` ";
	$sql .= " WHERE 1 ";

	if(!empty($requestData['organisation_type_id'])){
		$sql .= " AND  `csr_ngo`.`organisation_type_id` LIKE '".$requestData['organisation_type_id']."' ";
	}
	if(!empty($requestData['thematic_areas_work_id'])){
		$sql .= " AND  `csr_ngo_thematic_areas_works`.`thematic_areas_id` LIKE '".$requestData['thematic_areas_work_id']."' ";
	}
	if(!empty($requestData['work_location'])){
		$sql .= " AND  `csr_ngo_work_locations`.`work_locations_id` LIKE '".$requestData['work_location']."' ";
	}
	if( !empty($requestData['search']['value']) ) {
		$sql .= " AND ( `csr_ngo`.`organisation_name` LIKE '%".$requestData['search']['value']."%' ) ";
	}
	$sql .= " AND `csr_ngo`.`ngo_status` = 'Empanelled' ";
	
	$sql .= " GROUP BY `csr_ngo_work_locations`.`ngo_id` ";
	
	$result = $wpdb->get_results($sql);
	$totalData = $totalFiltered = $wpdb->num_rows;
	$sql .= " ORDER BY `csr_ngo`.`created_date` DESC LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
	$result = $wpdb->get_results($sql);
	$inc = 1;
	$data = array();
	foreach($result as $row){

		$location_sql = "SELECT `location_name` FROM `csr_work_locations` WHERE `id` IN ( SELECT `work_locations_id` FROM `csr_ngo_work_locations` WHERE `ngo_id` = '".$row->id."' )";
		$locations = $wpdb->get_results($location_sql);
		$location_names = array();
		if(!empty($locations)){
			foreach($locations as $loc){
				$location_names[] = $loc->location_name;
			}
		}
		$location_names = implode(', ', $location_names);
		
		$area_sql = "SELECT `thematic_area` FROM `csr_thematic_areas_work` WHERE `id` IN ( SELECT `thematic_areas_id` FROM `csr_ngo_thematic_areas_works` WHERE `ngo_id` = '".$row->id."' )";
		$areas = $wpdb->get_results($area_sql);
		$area_names = array();
		if(!empty($areas)){
			foreach($areas as $area){
				$area_names[] = $area->thematic_area;
			}
		}
		$area_names = implode(', ', $area_names);
		
		
		$hub_code = $row->hub_code;		
		$nestedData = array(); 
		$nestedData[] = $inc;
		$nestedData[] = $row->organisation_name;
		$nestedData[] = $hub_code;
		$nestedData[] = ucfirst($row->person_firstname) . ' ' . ucfirst($row->person_lastname);
		$nestedData[] = $location_names;
		$nestedData[] = $row->type_name;
		$nestedData[] = $area_names;
		$data[] = $nestedData;
		$inc++;
	}
	$json_data = array(
		"draw"            => intval( $requestData['draw'] ), 
		"recordsTotal"    => intval( $totalData ),  
		"recordsFiltered" => intval( $totalFiltered ), 
		"data"            => $data
	);
	echo json_encode($json_data);
	exit;
}
add_action( 'wp_ajax_front_empanelled_ngo_filter', 'front_empanelled_ngo_filter');
add_action( 'wp_ajax_nopriv_front_empanelled_ngo_filter', 'front_empanelled_ngo_filter');

function front_get_ngo_details(){
	global $wpdb;
	$return = array();
	$requestData = $_REQUEST;
	$table_name = $wpdb->prefix . "ngo";
	$result = $wpdb->get_results("SELECT * FROM $table_name");
	$hub_code = (isset($requestData['ngo_hub_code']) && trim($requestData['ngo_hub_code']))? $requestData['ngo_hub_code'] : '';
	$res = $wpdb->get_results("SELECT * FROM `csr_ngo` WHERE `hub_code` = '" . $hub_code . "' AND `ngo_status` = 'Empanelled' LIMIT 1 " );
	if(!empty($res) && $hub_code != ""){
		$ngo = $res[0];
		$ngo_id = $ngo->id;
		
		$themeSql = "SELECT `csr_thematic_areas_work`.`id`,`csr_thematic_areas_work`.`thematic_area` AS text FROM `csr_thematic_areas_work` ";
		$themeSql .= " LEFT JOIN `csr_ngo_thematic_areas_works` ON `csr_thematic_areas_work`.`id` = `csr_ngo_thematic_areas_works`.`thematic_areas_id` ";
		$themeSql .= " WHERE `csr_ngo_thematic_areas_works`.`ngo_id` = '" . $ngo_id . "' ";
		$themeSql .= " AND `csr_thematic_areas_work`.`isDelete` = '0' GROUP BY `csr_ngo_thematic_areas_works`.`thematic_areas_id` " ;
		$themeRes = $wpdb->get_results($themeSql);
		$thematic_areas_works_arr = array();
		if(!empty($themeRes)){
			foreach($themeRes as $trow){
				$thematic_areas_works_arr[] = $trow->id;
			}
		}
		$workSql = "SELECT `csr_work_locations`.`id`,`csr_work_locations`.`location_name` AS text FROM `csr_work_locations` ";
		$workSql .= " LEFT JOIN `csr_ngo_work_locations` ON `csr_work_locations`.`id` = `csr_ngo_work_locations`.`work_locations_id` ";
		$workSql .= " WHERE `csr_ngo_work_locations`.`ngo_id` = '" . $ngo_id . "' ";
		$workSql .= " AND `csr_work_locations`.`isDelete` = '0' GROUP BY `csr_ngo_work_locations`.`work_locations_id` " ;
		$workRes = $wpdb->get_results($workSql);
		$works_locations_arr = array();
		if(!empty($workRes)){
			foreach($workRes as $wrow){
				$works_locations_arr[] = $wrow->id;
			}
		}
		
		$ngo->thematic_areas_works = $thematic_areas_works_arr;
		$ngo->work_locations = $works_locations_arr;

		$cur_date = strtotime(date('Y-m-d'));
		$empanelled_end_date = strtotime(substr($ngo->empanelled_end_date, 0, 10));
		$empanelled_end_date_last_3_month = strtotime(substr($ngo->empanelled_end_date, 0, 10).' -3 month');		
		if($cur_date > $empanelled_end_date){
			$return['success'] = "Found";
		} else if($cur_date > $empanelled_end_date_last_3_month){
			$return['last_3_month'] = "last_3_month";
			$return['success'] = "Found";
		} else {
			$return['success'] = "NoRenewal";
		}
		
		$return['data'] = $ngo;
		print json_encode($return);
		exit;
	}else{
		$return['error'] = "Nodata";
		print json_encode($return);
		exit;	
	}
}
add_action( 'wp_ajax_front_get_ngo_details', 'front_get_ngo_details');
add_action( 'wp_ajax_nopriv_front_get_ngo_details', 'front_get_ngo_details');

/*******************************  Submit Propodsal *************************/

function submit_proposal_form_func(){
	include("submit_proposals.php");
}
add_shortcode( 'submit_proposal_form', 'submit_proposal_form_func' );

function get_p_tags(){
	global $wpdb; 
	$tag_arr = array();
	$table_name = $wpdb->prefix . "tag";
	$res = $wpdb->get_results("SELECT `id`,`tag_name` as text FROM " . $table_name . " WHERE `isDelete` = 0"); 
	foreach($res as $row){
		$tag_arr[] = $row;
	}
	echo json_encode($tag_arr);
	exit;
}
add_action( 'wp_ajax_get_p_tags', 'get_p_tags');
add_action( 'wp_ajax_nopriv_get_p_tags', 'get_p_tags');

function add_new_proposal_email($proposal_id, $hub_code, $attachments = array()) {
	global $wpdb;
	$query = "	SELECT cp.*,cn.email_id
				FROM `csr_proposal` cp
				LEFT JOIN `csr_ngo` cn ON cn.hub_code = cp.hub_code
				WHERE cp.id = '".$proposal_id."' LIMIT 1		
			";
	$proposal_detail = $wpdb->get_results($query); 
	$proposal_detail = $proposal_detail[0];						
	// thematic_area selection box
	$tlids_arr = array();
	$thematic_areas_works_arr = array();
	$tlids_result = $wpdb->get_results("SELECT `thematic_areas_id` FROM `csr_proposal_thematic_areas_work` WHERE `proposal_id` = '" . $proposal_id . "' ");
	foreach($tlids_result as $tlids_row){
		$tlids_arr[] = $tlids_row->thematic_areas_id;
	}
	$tlids = implode(', ', $tlids_arr);
	$thematic_areas_work_result = $wpdb->get_results("SELECT * FROM `csr_thematic_areas_work` WHERE `id` IN ( ".$tlids." ) " ); 
	foreach($thematic_areas_work_result as $thematic_row){
		$thematic_areas_works_arr[$thematic_row->id] = str_replace(',', ' ', $thematic_row->thematic_area);
	}
	//work locations array		
	$wlids_arr = array();
	$work_locations_arr = array();
	$wlids_result = $wpdb->get_results("SELECT `work_locations_id` FROM `csr_proposal_work_locations` WHERE `proposal_id` = '" . $proposal_id . "' ");
	foreach($wlids_result as $wlids_row){
		$wlids_arr[] = $wlids_row->work_locations_id;
	}
	$wlids = implode(', ', $wlids_arr);
	$work_locations_result = $wpdb->get_results("SELECT * FROM `csr_work_locations` WHERE `id` IN ( ".$wlids." ) " ); 
	foreach($work_locations_result as $work_locations_row){
		$work_locations_arr[$work_locations_row->id] = $work_locations_row->location_name;
	}
	// tags array
	$tags_arr = array();
	$tagids_arr = array();
	$tagids_result = $wpdb->get_results("SELECT `tag_id` FROM `csr_proposal_tags` WHERE `proposal_id` = '" . $proposal_id . "' ");
	foreach($tagids_result as $tagids_row){
		$tagids_arr[] = $tagids_row->tag_id;
	}
	$tagids = implode(', ', $tagids_arr);
	$tags_result = $wpdb->get_results("SELECT * FROM `csr_tag` WHERE `id` IN ( ".$tagids." ) " ); 
	foreach($tags_result as $tags_row){
		$tags_arr[$tags_row->id] = $tags_row->tag_name;
	}

	$args = array(
	  'name'        => 'submitting-proposal-for-empanelment',
	  'post_type'   => 'csr_email_format',
	  'post_status' => 'publish',
	  'numberposts' => 1
	);
	$post = get_posts($args);
	
    $to = $proposal_detail->email_id;
	$subject = $post[0]->post_title;
	$message = '';
	$message .= $post[0]->post_content;
	$message .= '<table>
					<tr>
						<td width="120">HUB Code </td>
						<td> : '.$hub_code.'</td>
					</tr>
					<tr>
						<td width="120">Project Title </td>
						<td> : '.$proposal_detail->project_title.'</td>
					</tr>
					<tr>
						<td width="120">Location Of work </td>
						<td> : '.implode(', ', $work_locations_arr).'</td>
					</tr>
					<tr>
						<td width="120">Thematic Area </td>
						<td> : '.implode(', ', $thematic_areas_works_arr).'</td>
					</tr>
					<tr>
						<td width="120">Tags </td>
						<td> : '.implode(', ', $tags_arr).'</td>
					</tr>
					<tr>
						<td width="120">Project Summary </td>
						<td> : '.nl2br($proposal_detail->project_summary).'</td>
					</tr>
				</table>';
	$headers[] = 'From: '.get_option( 'blogname' ).' <'.get_option( 'admin_email' ).'>';
    add_filter( 'wp_mail_content_type', 'add_new_proposal_email_content_type' );
	$mail = wp_mail( $to, $subject, $message, $headers, $attachments );
}
function add_new_proposal_email_content_type() {
    return 'text/html';
}

function add_new_proposal(){
	global $wpdb; 
	$hub_code = $_REQUEST['hub_code'];
	$project_title = $_REQUEST['project_title'];
	//$thematic_areas_work_id = $_REQUEST['thematic_areas_work_id'];
	$project_summary = $_REQUEST['project_summary'];
	
    $res = $wpdb->get_results("SELECT * FROM `csr_proposal` WHERE `hub_code` = '" . $hub_code . "' AND `project_title`= '" . $project_title . "' " ); 
	if(!empty($res)){
		$return['error'] = "Exist";
		print json_encode($return);
		exit;
	}
	$ngo_res = $wpdb->get_results("SELECT `email_id` FROM `csr_ngo` WHERE `hub_code` = '" . $hub_code . "' AND `ngo_status` = 'Rejected' " ); 
	if(!empty($ngo_res)){
		$return['error'] = "Rejected_hub_code";
		print json_encode($return);
		exit;
	}
	$ngo_res = $wpdb->get_results("SELECT `email_id` FROM `csr_ngo` WHERE `hub_code` = '" . $hub_code . "' " ); 
	if(empty($ngo_res)){
		$return['error'] = "Wrong_hub_code";
		print json_encode($return);
		exit;
	}
    
    //file size and type validations
	if(isset($_FILES["application_form_name"]["name"]) && !empty($_FILES["application_form_name"]["name"])){
		if ($_FILES['application_form_name']['size'] >= (15 * 1048576)){
			$return['error'] = "FileSize";
			print json_encode($return);
			exit;
		}
		$name = $_FILES["application_form_name"]["name"];
		$extension = pathinfo($name, PATHINFO_EXTENSION);
		$extension = strtolower($extension);
		if(!($extension == 'pdf' || $extension == 'zip' || $extension == 'jpg' || $extension == 'png' || $extension == 'jpeg' || $extension == 'gif' || $extension == 'bmp')){
			$return['error'] = "FileType";
			print json_encode($return);
			exit;
		}
	}
	if(isset($_FILES["payment_receipt_name"]["name"]) && !empty($_FILES["payment_receipt_name"]["name"])){
		if ($_FILES['payment_receipt_name']['size'] >= (15 * 1048576)){
			$return['error'] = "FileSize";
			print json_encode($return);
			exit;
		}
		$name = $_FILES["payment_receipt_name"]["name"];
		$extension = pathinfo($name, PATHINFO_EXTENSION);
		$extension = strtolower($extension);
		if(!($extension == 'pdf' || $extension == 'zip' || $extension == 'jpg' || $extension == 'png' || $extension == 'jpeg' || $extension == 'gif' || $extension == 'bmp')){
			$return['error'] = "FileType";
			print json_encode($return);
			exit;
		}
	}
	
    $tag_id_arr = array();
    $tags = array();
	if(!empty($_POST['tags'])){
		$table_name = $wpdb->prefix . "tag";
		$tags = explode(',', $_POST['tags']);
		foreach($tags as $tag ){
			if(is_numeric($tag)){
				$tag_id_arr[] = $tag;
				$res = $wpdb->get_results("SELECT * FROM " . $table_name . " WHERE `id` = '" . $tag . "' " ); 
				if(!empty($res)){
					$tags[] = $res[0]->tag_name;
				}
			} else {
				$tag = trim($tag);
				$res = $wpdb->get_results("SELECT * FROM " . $table_name . " WHERE `tag_name` = '" . $tag . "' " ); 
				if(empty($res)){
					$wpdb->insert($table_name, array('tag_name' => $tag) ); 
					$id = $wpdb->insert_id;
					$tag_id_arr[] = $id;
					$tags[] = $tag;
				} else {
					$tag_id_arr[] = $res[0]->id;
					$tags[] = $res[0]->tag_name;
				}
			}
		}
	}
	$return = ''; 
	$wpdb->insert('csr_proposal', 
		array(
			'hub_code' => $hub_code,
			'project_title' => $project_title,
			//'thematic_areas_work_id' => $thematic_areas_work_id,
			'project_summary' => $project_summary,
			'created_date' => date('Y-m-d H:i:s')
		) 
	); 
	$proposal_id = $wpdb->insert_id;
	foreach($tag_id_arr as $tag_id){
		$wpdb->insert('csr_proposal_tags', 
			array(
				'proposal_id' => $proposal_id,
				'tag_id' => $tag_id
			) 
		);
	}
		
    $work_location_id_arr = array();
    $work_locations = array();
	if(!empty($_POST['work_location'])){
		$table_name = $wpdb->prefix . "work_locations";
		$work_locations = explode(',', $_POST['work_location']);
		foreach($work_locations as $work_location ){
			if(is_numeric($work_location)){
				$work_location_id_arr[] = $work_location;
				$res = $wpdb->get_results("SELECT * FROM " . $table_name . " WHERE `id` = '" . $work_location . "' " ); 
				if(!empty($res)){
					$work_locations[] = $res[0]->location_name;
				}
			} else {
				$work_location = trim($work_location);
				$res = $wpdb->get_results("SELECT * FROM " . $table_name . " WHERE `location_name` = '" . $work_location . "' " ); 
				if(empty($res)){
					$wpdb->insert($table_name, array('location_name' => $work_location) ); 
					$id = $wpdb->insert_id;
					$work_location_id_arr[] = $id;
					$work_locations[] = $work_location;
				} else {
					$work_location_id_arr[] = $res[0]->id;
					$work_locations[] = $res[0]->location_name;
				}
			}
		}
	}
	foreach($work_location_id_arr as $work_location_id){
		$wpdb->insert('csr_proposal_work_locations', 
			array(
				'proposal_id' => $proposal_id,
				'work_locations_id' => $work_location_id
			) 
		);
	}
	
	$thematic_areas_work_id_arr = array();
    $thematic_areas_works = array();
	if(!empty($_POST['thematic_areas_works'])){
		$table_name = $wpdb->prefix . "thematic_areas_work";
		$thematic_areas_works = explode(',', $_POST['thematic_areas_works']);
		foreach($thematic_areas_works as $thematic_areas_work ){
			if(is_numeric($thematic_areas_work)){
				$thematic_areas_work_id_arr[] = $thematic_areas_work;
				$res = $wpdb->get_results("SELECT * FROM " . $table_name . " WHERE `id` = '" . $thematic_areas_work . "' " ); 
				if(!empty($res)){
					$thematic_areas_works[] = $res[0]->thematic_area;
				}
			} else {
				$thematic_areas_work = trim($thematic_areas_work);
				$res = $wpdb->get_results("SELECT * FROM " . $table_name . " WHERE `thematic_area` = '" . $thematic_areas_work . "' " ); 
				if(empty($res)){
					$wpdb->insert($table_name, array('thematic_area' => $thematic_areas_work) ); 
					$id = $wpdb->insert_id;
					$thematic_areas_work_id_arr[] = $id;
					$thematic_areas_works[] = $thematic_areas_work;
				} else {
					$thematic_areas_work_id_arr[] = $res[0]->id;
					$thematic_areas_works[] = $res[0]->thematic_area;
				}
			}
		}
	}
	foreach($thematic_areas_work_id_arr as $thematic_areas_work_id){
		$wpdb->insert('csr_proposal_thematic_areas_work', 
			array(
				'proposal_id' => $proposal_id,
				'thematic_areas_id' => $thematic_areas_work_id
			) 
		);
	}
	
	//$upload_dir = wp_upload_dir(); 
	//$upload_dir = $upload_dir['baseurl'];
	//$targetPath = $upload_dir.'/proposal_files/';
	$path = wp_upload_dir();
	$attachments = array();
	if(isset($_FILES["application_form_name"]["name"]) && !empty($_FILES["application_form_name"]["name"])){
		$name = $_FILES["application_form_name"]["name"];
		$tmpName = $_FILES["application_form_name"]["tmp_name"];
		$extension = pathinfo($name, PATHINFO_EXTENSION);
		$name = $proposal_id.'_application_'.time().'.'.$extension;
		$a = $path['basedir']."/proposal_files/".$name; 
		$targetPath = str_replace("\\", "/", $a);
		move_uploaded_file($tmpName,$targetPath);
		$attachments[] = WP_CONTENT_DIR . '/uploads/proposal_files/' . $name;
		$wpdb->query("UPDATE `csr_proposal` SET `application_form_name`='".$name."' WHERE `id`='".$proposal_id."' " ); 
	}
	if(isset($_FILES["payment_receipt_name"]["name"]) && !empty($_FILES["payment_receipt_name"]["name"])){
		$name = $_FILES["payment_receipt_name"]["name"];
		$tmpName = $_FILES["payment_receipt_name"]["tmp_name"];
		$extension = pathinfo($name, PATHINFO_EXTENSION);
		$name = $proposal_id.'_payment_'.time().'.'.$extension;
		$a = $path['basedir']."/proposal_files/".$name; 
		$targetPath = str_replace("\\", "/", $a);
		move_uploaded_file($tmpName,$targetPath);
		$attachments[] = WP_CONTENT_DIR . '/uploads/proposal_files/' . $name;
		$wpdb->query("UPDATE `csr_proposal` SET `payment_receipt_name`='".$name."' WHERE `id`='".$proposal_id."' " ); 
	}
	add_new_proposal_email($proposal_id, $hub_code, $attachments);
	
	$return['success'] = "Added";
	print json_encode($return);
	exit;
}
add_action( 'wp_ajax_add_new_proposal', 'add_new_proposal');
add_action( 'wp_ajax_nopriv_add_new_proposal', 'add_new_proposal');

function csr_proposal_rejected_email($email_id, $project_title) {
	global $wpdb;
	$args = array(
	  'name'        => 'csr-proposal-rejected',
	  'post_type'   => 'csr_email_format',
	  'post_status' => 'publish',
	  'numberposts' => 1
	);
	$post = get_posts($args);
	
    $to = $email_id;
	$subject = $post[0]->post_title;
	$message = '';
	$message = 'Project Title : '.$project_title.' <br />';
	$message .= $post[0]->post_content;
	$headers[] = 'From: '.get_option( 'blogname' ).' <'.get_option( 'admin_email' ).'>';
    add_filter( 'wp_mail_content_type', 'csr_proposal_rejected_email_content_type' );
    $mail = wp_mail( $to, $subject, $message, $headers );
}
function csr_proposal_rejected_email_content_type() {
    return 'text/html';
}

function csr_proposal_accepted_email($email_id, $project_title) {
	global $wpdb;
	$args = array(
	  'name'        => 'csr-proposal-accepted',
	  'post_type'   => 'csr_email_format',
	  'post_status' => 'publish',
	  'numberposts' => 1
	);
	$post = get_posts($args);
	
    $to = $email_id;
	$subject = $post[0]->post_title;
	$message = '';
	$message = 'Project Title : '.$project_title.' <br />';
	$message .= $post[0]->post_content;
	$headers[] = 'From: '.get_option( 'blogname' ).' <'.get_option( 'admin_email' ).'>';
    add_filter( 'wp_mail_content_type', 'csr_proposal_accepted_email_content_type' );
    $mail = wp_mail( $to, $subject, $message, $headers );
}
function csr_proposal_accepted_email_content_type() {
    return 'text/html';
}

function update_proposal(){
	global $wpdb; 
	$proposal_id = $_REQUEST['proposal_id'];
	$hub_code = $_REQUEST['hub_code'];
	$project_title = $_REQUEST['project_title'];
	$thematic_areas_work_id = $_REQUEST['thematic_areas_work_id'];
	$project_summary = $_REQUEST['project_summary'];
	$proposal_status = $_REQUEST['proposal_status'];
	
	$res = $wpdb->get_results("SELECT * FROM `csr_proposal` WHERE `hub_code` = '" . $hub_code . "' AND `project_title`= '" . $project_title . "' AND `id` != '".$proposal_id."' " ); 
	if(!empty($res)){
		$return['error'] = "Exist";
		print json_encode($return);
		exit;
	}
	$ngo_res = $wpdb->get_results("SELECT `email_id` FROM `csr_ngo` WHERE `hub_code` = '" . $hub_code . "' " ); 
	if(empty($ngo_res)){
		$return['error'] = "Wrong_hub_code";
		print json_encode($return);
		exit;
	}
	
	//file size and type validations
	if(isset($_FILES["proposal_pdf"]["name"]) && !empty($_FILES["proposal_pdf"]["name"])){
		if ($_FILES['proposal_pdf']['size'] >= (10 * 1048576)){
			$return['error'] = "FileSize";
			print json_encode($return);
			exit;
		}
		$name = $_FILES["proposal_pdf"]["name"];
		$extension = pathinfo($name, PATHINFO_EXTENSION);
		$extension = strtolower($extension);
		if($extension != 'pdf'){
			$return['error'] = "FileType";
			print json_encode($return);
			exit;
		}
	}
	
	$tag_id_arr = array();
    $tags = array();
	if(!empty($_POST['tags'])){
		$table_name = $wpdb->prefix . "tag";
		$tags = explode(',', $_POST['tags']);
		foreach($tags as $tag ){
			if(is_numeric($tag)){
				$tag_id_arr[] = $tag;
				$res = $wpdb->get_results("SELECT * FROM " . $table_name . " WHERE `id` = '" . $tag . "' " ); 
				if(!empty($res)){
					$tags[] = $res[0]->tag_name;
				}
			} else {
				$tag = trim($tag);
				$res = $wpdb->get_results("SELECT * FROM " . $table_name . " WHERE `tag_name` = '" . $tag . "' " ); 
				if(empty($res)){
					$wpdb->insert($table_name, array('tag_name' => $tag) ); 
					$id = $wpdb->insert_id;
					$tag_id_arr[] = $id;
					$tags[] = $tag;
				} else {
					$tag_id_arr[] = $res[0]->id;
					$tags[] = $res[0]->tag_name;
				}
			}
		}
	}
	
	$ngo_res = $wpdb->get_results("SELECT `email_id` FROM `csr_ngo` WHERE `hub_code` = '" . $hub_code . "' " ); 
	if(!empty($ngo_res)){
		if( $proposal_status == 'Rejected'){
			$res = $wpdb->get_results("SELECT * FROM `csr_proposal` WHERE `id` = '" . $proposal_id . "' AND `proposal_status` != 'Rejected' " ); 
			if(!empty($res)){
				csr_proposal_rejected_email($ngo_res[0]->email_id, $project_title);
			}
		}
		if( $proposal_status == 'Accepted - Live'){
			$res = $wpdb->get_results("SELECT * FROM `csr_proposal` WHERE `id` = '" . $proposal_id . "' AND `proposal_status` != 'Accepted - Live' " ); 
			if(!empty($res)){
				csr_proposal_accepted_email($ngo_res[0]->email_id, $project_title);
			}
		}
	}
	
	$return = ''; 
	$wpdb->update('csr_proposal', 
		array(
			'hub_code' => $hub_code,
			'project_title' => $project_title,
			'thematic_areas_work_id' => $thematic_areas_work_id,
			'project_summary' => $project_summary,
			'proposal_status' => $proposal_status
		), 
		array('id'=>$proposal_id)
	); 
	$path = wp_upload_dir();
	if(isset($_FILES["proposal_pdf"]["name"]) && !empty($_FILES["proposal_pdf"]["name"])){
		$name = $_FILES["proposal_pdf"]["name"];
		$tmpName = $_FILES["proposal_pdf"]["tmp_name"];
		$extension = pathinfo($name, PATHINFO_EXTENSION);
		$name = $proposal_id.'_proposal_pdf_'.time().'.'.$extension;
		$a = $path['basedir']."/proposal_pdf/".$name; 
		$targetPath = str_replace("\\", "/", $a);
		move_uploaded_file($tmpName,$targetPath);
		$wpdb->update('csr_proposal', 
			array(
				'proposal_pdf' => $name
			), 
			array('id'=>$proposal_id)
		);
	}
	$wpdb->query( "DELETE FROM `csr_proposal_tags` WHERE `proposal_id` = '".$proposal_id."' " );
	foreach($tag_id_arr as $tag_id){
		$wpdb->insert('csr_proposal_tags', 
			array(
				'proposal_id' => $proposal_id,
				'tag_id' => $tag_id
			) 
		);
	}
	
			
    $work_location_id_arr = array();
    $work_locations = array();
	if(!empty($_POST['work_location'])){
		$table_name = $wpdb->prefix . "work_locations";
		$work_locations = explode(',', $_POST['work_location']);
		foreach($work_locations as $work_location ){
			if(is_numeric($work_location)){
				$work_location_id_arr[] = $work_location;
				$res = $wpdb->get_results("SELECT * FROM " . $table_name . " WHERE `id` = '" . $work_location . "' " ); 
				if(!empty($res)){
					$work_locations[] = $res[0]->location_name;
				}
			} else {
				$work_location = trim($work_location);
				$res = $wpdb->get_results("SELECT * FROM " . $table_name . " WHERE `location_name` = '" . $work_location . "' " ); 
				if(empty($res)){
					$wpdb->insert($table_name, array('location_name' => $work_location) ); 
					$id = $wpdb->insert_id;
					$work_location_id_arr[] = $id;
					$work_locations[] = $work_location;
				} else {
					$work_location_id_arr[] = $res[0]->id;
					$work_locations[] = $res[0]->location_name;
				}
			}
		}
	}
	$wpdb->query( "DELETE FROM `csr_proposal_work_locations` WHERE `proposal_id` = '".$proposal_id."' " );
	foreach($work_location_id_arr as $work_location_id){
		$wpdb->insert('csr_proposal_work_locations', 
			array(
				'proposal_id' => $proposal_id,
				'work_locations_id' => $work_location_id
			) 
		);
	}
	
	$thematic_areas_work_id_arr = array();
    $thematic_areas_works = array();
	if(!empty($_POST['thematic_areas_works'])){
		$table_name = $wpdb->prefix . "thematic_areas_work";
		$thematic_areas_works = explode(',', $_POST['thematic_areas_works']);
		foreach($thematic_areas_works as $thematic_areas_work ){
			if(is_numeric($thematic_areas_work)){
				$thematic_areas_work_id_arr[] = $thematic_areas_work;
				$res = $wpdb->get_results("SELECT * FROM " . $table_name . " WHERE `id` = '" . $thematic_areas_work . "' " ); 
				if(!empty($res)){
					$thematic_areas_works[] = $res[0]->thematic_area;
				}
			} else {
				$thematic_areas_work = trim($thematic_areas_work);
				$res = $wpdb->get_results("SELECT * FROM " . $table_name . " WHERE `thematic_area` = '" . $thematic_areas_work . "' " ); 
				if(empty($res)){
					$wpdb->insert($table_name, array('thematic_area' => $thematic_areas_work) ); 
					$id = $wpdb->insert_id;
					$thematic_areas_work_id_arr[] = $id;
					$thematic_areas_works[] = $thematic_areas_work;
				} else {
					$thematic_areas_work_id_arr[] = $res[0]->id;
					$thematic_areas_works[] = $res[0]->thematic_area;
				}
			}
		}
	}
	$wpdb->query( "DELETE FROM `csr_proposal_thematic_areas_work` WHERE `proposal_id` = '".$proposal_id."' " );
	foreach($thematic_areas_work_id_arr as $thematic_areas_work_id){
		$wpdb->insert('csr_proposal_thematic_areas_work', 
			array(
				'proposal_id' => $proposal_id,
				'thematic_areas_id' => $thematic_areas_work_id
			) 
		);
	}	
	
	$_SESSION['proposal_updated'] = "proposal_updated";
	$return['success'] = "Updated";
	print json_encode($return);
	exit;
}
add_action( 'wp_ajax_update_proposal', 'update_proposal');

function proposal_filter_list(){
	global $wpdb; 
	$requestData = $_REQUEST;
	$columns = array( 
		0 => 'project_title', 
		1 => 'hub_code', 
	);
	$table_name = $wpdb->prefix . "proposal";
	$result = $wpdb->get_results("SELECT * FROM $table_name");
	$totalData = $wpdb->num_rows;
	$totalFiltered = $totalData;

	$sql = "SELECT `csr_proposal`.`id`, `csr_proposal`.`project_title`, `csr_proposal`.`hub_code`, `csr_proposal`.`proposal_status`, `csr_proposal`.`created_date`, `csr_ngo`.`organisation_name`, `csr_corporate_partner`.`organisation_name` as corporate_company_name, `csr_corporate_wishlist`.`request_status` as corporate_request_status ";
	$sql .= " FROM `csr_proposal` ";
	$sql .= " LEFT JOIN `csr_proposal_thematic_areas_work` ON `csr_proposal`.`id`=`csr_proposal_thematic_areas_work`.`proposal_id` ";
	$sql .= " LEFT JOIN `csr_proposal_work_locations` ON `csr_proposal`.`id`=`csr_proposal_work_locations`.`proposal_id` ";
	$sql .= " LEFT JOIN `csr_proposal_tags` ON `csr_proposal`.`id`=`csr_proposal_tags`.`proposal_id` ";
	$sql .= " LEFT JOIN `csr_ngo` ON `csr_proposal`.`hub_code`=`csr_ngo`.`hub_code` ";
	$sql .= " LEFT JOIN `csr_corporate_wishlist` ON `csr_corporate_wishlist`.`proposal_id`=`csr_proposal`.`id` ";
	$sql .= " LEFT JOIN `csr_corporate_partner` ON `csr_corporate_partner`.`id`=`csr_corporate_wishlist`.`corporate_id` ";
	$sql .= " WHERE 1 ";
	if( $requestData['proposal_type'] == 'trashed' ){
		$sql .= " AND `csr_proposal`.`isTrash`= 1 ";
	} else {
		$sql .= " AND `csr_proposal`.`isTrash`= 0 ";
	}
	if(!empty($requestData['proposal_status'])){
		$sql .= " AND  `csr_proposal`.`proposal_status` LIKE '".$requestData['proposal_status']."' ";
	}
	if(!empty($requestData['thematic_areas_work_id'])){
		$sql .= " AND  `csr_proposal_thematic_areas_work`.`thematic_areas_id` LIKE '".$requestData['thematic_areas_work_id']."' ";
	}
	if(!empty($requestData['work_location'])){
		$sql .= " AND  `csr_proposal_work_locations`.`work_locations_id` LIKE '".$requestData['work_location']."' ";
	}
	if(!empty($requestData['tag_id'])){
		$sql .= " AND  `csr_proposal_tags`.`tag_id` LIKE '".$requestData['tag_id']."' ";
	}
	if( !empty($requestData['search']['value']) ) {
		$sql .= " AND ( `csr_proposal`.`project_title` LIKE '%".$requestData['search']['value']."%' ";    
		$sql .= " OR `csr_proposal`.`hub_code` LIKE '%".$requestData['search']['value']."%' )";
	}
	$sql .= " GROUP BY `csr_proposal_tags`.`proposal_id` ";
	$result = $wpdb->get_results($sql);
	$totalData = $totalFiltered = $wpdb->num_rows;
	$sql .= " ORDER BY `created_date` DESC LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
	$result = $wpdb->get_results($sql);
	$inc = 1;
	$data = array();
	foreach($result as $row){
		$nestedData = array(); 
		$nestedData[] = $inc;
		$nestedData[] = '<a href="?page=proposal&action=proposal_details&id='.$row->id.'">'.$row->project_title.'</a>';
		$nestedData[] = $row->organisation_name;
		$nestedData[] = $row->hub_code;
		if ($requestData['proposal_type'] != 'trashed'){
			if($row->corporate_request_status == 1){
				$nestedData[] = $row->corporate_company_name;
			} else {
				$nestedData[] = '';
			}
		}
		$nestedData[] = $row->proposal_status;
		$nestedData[] = date('d/m/Y H:i:s', strtotime($row->created_date));
		$delete_icon = '';
		if($row->proposal_status == 'Rejected'){
			if( $requestData['proposal_type'] == 'trashed' ){
				$delete_icon = '<span class="text-info" style="cursor:pointer;" onClick="proposal_restore('.$row->id.')" >Restore</span>'.'|'.'<span class="text-info" style="cursor:pointer;" onClick="proposal_delete('.$row->id.')" >Delete</span>';
			} else {
				$delete_icon = '<span class="label label-danger" style="cursor:pointer;" onClick="proposal_move_to_trash('.$row->id.')" ><i class="fa fa-trash"></i></span>';
			}			
		}	
		$nestedData[] = $delete_icon;
		$data[] = $nestedData;
		$inc++;
	}
	$json_data = array(
		"draw"            => intval( $requestData['draw'] ), 
		"recordsTotal"    => intval( $totalData ),  
		"recordsFiltered" => intval( $totalFiltered ), 
		"data"            => $data
	);
	echo json_encode($json_data);
	exit;
}
add_action( 'wp_ajax_proposal_filter_list', 'proposal_filter_list');

function proposal_move_to_trash(){
	global $wpdb;
	$proposal_id = $_REQUEST['proposal_id'];
	$_SESSION['proposal_moved'] = "proposal_moved";
	$wpdb->update( 'csr_proposal', array('isTrash' => '1' ), array( 'id' => $proposal_id ) );
	echo 'proposal_moved';
	exit;
}
add_action( 'wp_ajax_proposal_move_to_trash', 'proposal_move_to_trash');

function proposal_restore(){
	global $wpdb;
	$proposal_id = $_REQUEST['proposal_id'];
	$_SESSION['proposal_restored'] = "proposal_restored";
	$wpdb->update( 'csr_proposal', array('isTrash' => '0' ), array( 'id' => $proposal_id ) );
	echo 'proposal_restored';
	exit;
}
add_action( 'wp_ajax_proposal_restore', 'proposal_restore');

function proposal_delete(){
	global $wpdb;
	$proposal_id = $_REQUEST['proposal_id'];
	$_SESSION['proposal_delete'] = "proposal_deleted";
	$wpdb->query( "DELETE FROM `csr_proposal_tags` WHERE `proposal_id` = '".$proposal_id."'");
	$wpdb->query( "DELETE FROM `csr_proposal_thematic_areas_work` WHERE `proposal_id` = '".$proposal_id."'");
	$wpdb->query( "DELETE FROM `csr_proposal_work_locations` WHERE `proposal_id` = '".$proposal_id."'");
	$wpdb->query( "DELETE FROM `csr_proposal` WHERE `id` = '".$proposal_id."'");
	echo 'proposal_deleted';
	exit;
}
add_action( 'wp_ajax_proposal_delete', 'proposal_delete');

function front_proposal_filter_list(){
	global $wpdb; 
	$requestData = $_REQUEST;
	$columns = array( 
		0 => 'project_title', 
		1 => 'hub_code', 
	);
	$table_name = $wpdb->prefix . "proposal";
	$result = $wpdb->get_results("SELECT * FROM $table_name");
	$totalData = $wpdb->num_rows;
	$totalFiltered = $totalData;

	$sql = "SELECT `csr_proposal`.`id`, `csr_proposal`.`project_title`, `csr_ngo`.`organisation_name`, `csr_proposal`.`hub_code`, `csr_proposal`.`project_summary`, `csr_proposal`.`created_date` ";
	$sql .= " FROM `csr_proposal` ";
	$sql .= " LEFT JOIN `csr_ngo` ON `csr_ngo`.`hub_code`=`csr_proposal`.`hub_code` ";
	$sql .= " LEFT JOIN `csr_proposal_work_locations` ON `csr_proposal`.`id`=`csr_proposal_work_locations`.`proposal_id` ";
	$sql .= " LEFT JOIN `csr_proposal_thematic_areas_work` ON `csr_proposal`.`id`=`csr_proposal_thematic_areas_work`.`proposal_id` ";
	$sql .= " LEFT JOIN `csr_proposal_tags` ON `csr_proposal`.`id`=`csr_proposal_tags`.`proposal_id` ";
	$sql .= " WHERE 1 ";
	if(!empty($requestData['thematic_areas_work_id'])){
		$sql .= " AND  `csr_proposal_thematic_areas_work`.`thematic_areas_id` LIKE '".$requestData['thematic_areas_work_id']."' ";
	}
	if(!empty($requestData['work_location'])){
		$sql .= " AND  `csr_proposal_work_locations`.`work_locations_id` LIKE '".$requestData['work_location']."' ";
	}
	if(!empty($requestData['tag_id'])){
		$sql .= " AND  `csr_proposal_tags`.`tag_id` IN (".$requestData['tag_id'].") ";
	}
	if( !empty($requestData['search']['value']) ) {
		$sql .= " AND ( `csr_proposal`.`project_title` LIKE '%".$requestData['search']['value']."%' ";    
		$sql .= " OR `csr_proposal`.`hub_code` LIKE '%".$requestData['search']['value']."%' )";
	}
	
	$sql .= " AND `csr_proposal`.`proposal_status` LIKE 'Accepted - Live'";
	$sql .= " AND `csr_proposal`.`id` NOT IN (SELECT `proposal_id` FROM `csr_corporate_wishlist` WHERE `request_status` = '1' GROUP BY `proposal_id`)";
	
	$sql .= " GROUP BY `csr_proposal_tags`.`proposal_id` ";
	$result = $wpdb->get_results($sql);
	$totalData = $totalFiltered = $wpdb->num_rows;
	$sql .= " ORDER BY `created_date` DESC LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
	$result = $wpdb->get_results($sql);
	$inc = 1;
	$data = array();
	foreach($result as $row){
		
		$corporate_id = $_SESSION['user_data']->corporate_id;
		$proposal_id = $row->id;
		
		// thematic_area selection box
		$tlids_arr = array();
		$thematic_areas_works_arr = array();
		$tlids_result = $wpdb->get_results("SELECT `thematic_areas_id` FROM `csr_proposal_thematic_areas_work` WHERE `proposal_id` = '" . $proposal_id . "' ");
		foreach($tlids_result as $tlids_row){
			$tlids_arr[] = $tlids_row->thematic_areas_id;
		}
		$tlids = implode(', ', $tlids_arr);
		$thematic_areas_work_result = $wpdb->get_results("SELECT * FROM `csr_thematic_areas_work` WHERE `id` IN ( ".$tlids." ) " ); 
		foreach($thematic_areas_work_result as $thematic_row){
			$thematic_areas_works_arr[$thematic_row->id] = str_replace(',', ' ', $thematic_row->thematic_area);
		}
		
		//work locations array		
		$wlids_arr = array();
		$work_locations_arr = array();
		$wlids_result = $wpdb->get_results("SELECT `work_locations_id` FROM `csr_proposal_work_locations` WHERE `proposal_id` = '" . $proposal_id . "' ");
		foreach($wlids_result as $wlids_row){
			$wlids_arr[] = $wlids_row->work_locations_id;
		}
		$wlids = implode(', ', $wlids_arr);
		$work_locations_result = $wpdb->get_results("SELECT * FROM `csr_work_locations` WHERE `id` IN ( ".$wlids." ) " ); 
		foreach($work_locations_result as $work_locations_row){
			$work_locations_arr[$work_locations_row->id] = $work_locations_row->location_name;
		}
		
		$innerSql = "SELECT * FROM `csr_corporate_wishlist` WHERE `corporate_id` = $corporate_id AND `proposal_id` = $proposal_id LIMIT 1";
		$foundRecords = $wpdb->get_results($innerSql);
		$status = (!empty($foundRecords))? 1 : 0;
		//$wishlistHtml = ($status == 1)? '<a data-action="remove" data-id="'.$row->id.'" class="add-to-wishlist wishlist-btn-'.$row->id.'" href="javascript:void(0);">Remove</a>' : '<a data-action="add" data-id="'.$row->id.'" class="add-to-wishlist wishlist-btn-'.$row->id.'" href="javascript:void(0);">Add</a>';
		if($status == 1){
			$wishlistHtml = 'Added to Wishlist';
		} else {
			$wishlistHtml = '<span class="add-to-wishlist-holder-' . $row->id . '"><a data-action="add" data-id="'.$row->id.'" class="add-to-wishlist wishlist-btn-'.$row->id.'" href="javascript:void(0);">Add</a></span>';
		}
		
		$nestedData = array(); 
		$nestedData[] = $inc;
		$nestedData[] = '<a href="'.site_url().'/corporate-account/?page=proposal-detail&proposal_id='.$proposal_id.'">'.$row->project_title.'</a>';
		$nestedData[] = ucfirst($row->organisation_name);
		$nestedData[] = implode(', ', $thematic_areas_works_arr);
		$nestedData[] = implode(', ', $work_locations_arr);
		$nestedData[] = date('d/m/Y H:i:s', strtotime($row->created_date));
		$nestedData[] = $wishlistHtml;
		$data[] = $nestedData;
		$inc++;
	}
	$json_data = array(
		"draw"            => intval( $requestData['draw'] ), 
		"recordsTotal"    => intval( $totalData ),  
		"recordsFiltered" => intval( $totalFiltered ), 
		"data"            => $data
	);
	echo json_encode($json_data);
	exit;
}
add_action( 'wp_ajax_front_proposal_filter_list', 'front_proposal_filter_list');
add_action( 'wp_ajax_nopriv_front_proposal_filter_list', 'front_proposal_filter_list');

function front_proposal_wishlist_filter(){
	global $wpdb; 
	$requestData = $_REQUEST;
	$columns = array( 
		0 => 'project_title', 
		1 => 'hub_code', 
	);
	$table_name = $wpdb->prefix . "csr_corporate_wishlist";
	$result = $wpdb->get_results("SELECT * FROM $table_name");
	$totalData = $wpdb->num_rows;
	$totalFiltered = $totalData;
	$corporate_id = $_SESSION['user_data']->corporate_id;
	
	$sql = "SELECT `csr_corporate_wishlist`.`id`, `csr_corporate_wishlist`.`request_status`, `csr_corporate_wishlist`.`corporate_id`, `csr_corporate_wishlist`.`proposal_id`, `csr_corporate_wishlist`.`created_date`,`csr_proposal`.`project_summary`,`csr_proposal`.`project_title`,`csr_proposal`.`hub_code`, `csr_ngo`.`organisation_name`";
	$sql .= " FROM `csr_corporate_wishlist` ";
	$sql .= " LEFT JOIN `csr_proposal` ON `csr_corporate_wishlist`.`proposal_id`=`csr_proposal`.`id` ";
	$sql .= " LEFT JOIN `csr_ngo` ON `csr_ngo`.`hub_code`=`csr_proposal`.`hub_code` ";	
	$sql .= " WHERE 1 ";

	if( !empty($requestData['search']['value']) ) {
		$sql .= " AND ( `csr_proposal`.`project_title` LIKE '%".$requestData['search']['value']."%' ";    
		$sql .= " OR `csr_proposal`.`hub_code` LIKE '%".$requestData['search']['value']."%' )";
	}
	
	if( isset($requestData['status']) && is_numeric($requestData['status']) ) {
		$sql .= " AND `csr_corporate_wishlist`.`request_status` = '" . $requestData['status'] . "'";
		$sql .= " AND (`csr_proposal`.`proposal_status` LIKE 'Locked' OR `csr_proposal`.`proposal_status` LIKE 'Accepted - Live')";
	} else {
		$sql .= " AND `csr_proposal`.`proposal_status` LIKE 'Accepted - Live'";
	}
	$sql .= " AND `csr_corporate_wishlist`.`corporate_id` = '$corporate_id' ";
	
	$sql .= " GROUP BY `csr_corporate_wishlist`.`proposal_id` ";
	
	$result = $wpdb->get_results($sql);
	$totalData = $totalFiltered = $wpdb->num_rows;
	$sql .= " ORDER BY `csr_corporate_wishlist`.`created_date` DESC LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
	
	$result = $wpdb->get_results($sql);
	$inc = 1;
	$data = array();
	foreach($result as $row){
				
		$proposal_id = $row->proposal_id;
		
		// thematic_area selection box
		$tlids_arr = array();
		$thematic_areas_works_arr = array();
		$tlids_result = $wpdb->get_results("SELECT `thematic_areas_id` FROM `csr_proposal_thematic_areas_work` WHERE `proposal_id` = '" . $proposal_id . "' ");
		foreach($tlids_result as $tlids_row){
			$tlids_arr[] = $tlids_row->thematic_areas_id;
		}
		$tlids = implode(', ', $tlids_arr);
		$thematic_areas_work_result = $wpdb->get_results("SELECT * FROM `csr_thematic_areas_work` WHERE `id` IN ( ".$tlids." ) " ); 
		foreach($thematic_areas_work_result as $thematic_row){
			$thematic_areas_works_arr[$thematic_row->id] = str_replace(',', ' ', $thematic_row->thematic_area);
		}
		
		//work locations array		
		$wlids_arr = array();
		$work_locations_arr = array();
		$wlids_result = $wpdb->get_results("SELECT `work_locations_id` FROM `csr_proposal_work_locations` WHERE `proposal_id` = '" . $proposal_id . "' ");
		foreach($wlids_result as $wlids_row){
			$wlids_arr[] = $wlids_row->work_locations_id;
		}
		$wlids = implode(', ', $wlids_arr);
		$work_locations_result = $wpdb->get_results("SELECT * FROM `csr_work_locations` WHERE `id` IN ( ".$wlids." ) " ); 
		foreach($work_locations_result as $work_locations_row){
			$work_locations_arr[$work_locations_row->id] = $work_locations_row->location_name;
		}
		
		$status = $row->request_status;
		$wishlistHtml = ($status == 1)? '<a data-action="remove" data-id="'.$row->proposal_id.'" class="add-to-wishlist wishlist-btn-'.$row->proposal_id.'" href="javascript:void(0);">Remove</a>' : '<a data-action="remove" data-id="'.$row->proposal_id.'" class="add-to-wishlist wishlist-btn-'.$row->proposal_id.'" href="javascript:void(0);">Remove</a>';
		
		$nestedData = array(); 
		$nestedData[] = $inc;
		$nestedData[] = '<a href="'.site_url().'/corporate-account/?page=proposal-detail&proposal_id='.$proposal_id.'">'.$row->project_title.'</a>';
		$nestedData[] = $row->organisation_name;
		$nestedData[] = implode(', ', $thematic_areas_works_arr);
		$nestedData[] = implode(', ', $work_locations_arr);
		$nestedData[] = date('d/m/Y H:i:s', strtotime($row->created_date));
		if(!(isset($requestData['status']) && $requestData['status'] > 0 )) {
			$nestedData[] = $wishlistHtml;
		}
		$data[] = $nestedData;
		$inc++;
	}
	$json_data = array(
		"draw"            => intval( $requestData['draw'] ), 
		"recordsTotal"    => intval( $totalData ),  
		"recordsFiltered" => intval( $totalFiltered ), 
		"data"            => $data
	);
	echo json_encode($json_data);
	exit;
}
add_action( 'wp_ajax_front_proposal_wishlist_filter', 'front_proposal_wishlist_filter');
add_action( 'wp_ajax_nopriv_front_proposal_wishlist_filter', 'front_proposal_wishlist_filter');

/**************************** corporate front change password ***************************************/
function front_corporate_change_password(){
	global $wpdb; 
	$id = $_REQUEST['id'];
	$old_password = isset($_REQUEST['old_password'])? $_REQUEST['old_password'] : '';
	$old_password = md5($old_password);
	$password = isset($_REQUEST['password'])? $_REQUEST['password'] : '';
	$cpassword = isset($_REQUEST['cpassword'])? $_REQUEST['cpassword'] : '';
   
	$return = '';
    $res = $wpdb->get_results("SELECT * FROM `csr_corporate_contact` WHERE `id` = " . $id . " AND `contact_pass_id` = '" . $old_password . "' LIMIT 1" ); 
	if(!empty($res)){
		
		if(trim($password) != "" && strlen($password) > 3){

			if( trim($password) == trim($cpassword)){
				$wpdb->update('csr_corporate_contact', 
					array(
						'contact_pass_id' => md5($password)
					), 
					array('id'=>$id)
				);
				$return['success'] = "Updated";
				print json_encode($return);
				exit;
			}else{
				$return['error'] = "Notsame";
				print json_encode($return);
				exit;
			}
		}else{
			$return['error'] = "Invalidlength";
			print json_encode($return);
			exit;
		}
		
	}else{

		$return['error'] = "Nodata";
		print json_encode($return);
		exit;
	}

}
add_action( 'wp_ajax_front_corporate_change_password', 'front_corporate_change_password');
add_action( 'wp_ajax_nopriv_front_corporate_change_password', 'front_corporate_change_password');

/**************************** corporate add_to_wishlist ***************************************/
function corporate_wishlist_add(){
	global $wpdb; 
	$proposal_id = $_REQUEST['proposal_id'];
	$corporate_id = $_REQUEST['corporate_id'];
	$table_name = 'csr_corporate_wishlist';
	$created_date = date('Y-m-d H:i:s');
    $return = '';
	
	$sql = "SELECT * FROM `$table_name` WHERE `corporate_id` = $corporate_id AND `proposal_id` = $proposal_id LIMIT 1";
	$foundRecords = $wpdb->get_results($sql);
	if(empty($foundRecords)){
		$wpdb->insert($table_name, array('corporate_id' => $corporate_id, 'proposal_id' => $proposal_id, 'created_date' => $created_date) ); 
		$return['success'] = "Added";
	}else{
		$return['error'] = "Exist";
	}
	print json_encode($return);
	exit;
}

add_action( 'wp_ajax_corporate_wishlist_add', 'corporate_wishlist_add');
add_action( 'wp_ajax_nopriv_corporate_wishlist_add', 'corporate_wishlist_add');

/**************************** corporate remove_from_wishlist ***************************************/
function corporate_wishlist_remove(){
	global $wpdb; 
	$proposal_id = $_REQUEST['proposal_id'];
	$corporate_id = $_REQUEST['corporate_id'];
	$table_name = 'csr_corporate_wishlist';
	$created_date = date('Y-m-d H:i:s');
    $return = '';
	
	$sql = "SELECT * FROM `$table_name` WHERE `corporate_id` = $corporate_id AND `proposal_id` = $proposal_id LIMIT 1";
	$foundRecords = $wpdb->get_results($sql);
	if(empty($foundRecords)){
		$return['error'] = "Nodata";
	}else{
		$sql = "DELETE FROM `$table_name` WHERE `corporate_id` = $corporate_id AND `proposal_id` = $proposal_id";
		$foundRecords = $wpdb->get_results($sql);
		
		$sql = "DELETE FROM `csr_corporate_proposal` WHERE `corporate_id` = $corporate_id AND `proposal_id` = $proposal_id";
		$foundRecords = $wpdb->get_results($sql);
		
		$return['success'] = "Removed";
	}
	print json_encode($return);
	exit;
}

add_action( 'wp_ajax_corporate_wishlist_remove', 'corporate_wishlist_remove');
add_action( 'wp_ajax_nopriv_corporate_wishlist_remove', 'corporate_wishlist_remove');

/*******************************  Add Corporate *************************/

function add_new_corporate(){
	global $wpdb; 
	//print_r($_REQUEST); exit;
	$organisation_name = $_REQUEST['organisation_name'];
	$corporate_type_id = $_REQUEST['corporate_type_id'];
	$address1 = $_REQUEST['address1'];
	$address2 = $_REQUEST['address2'];
	$zip_code = $_REQUEST['zip_code'];
	
	$contact_name_arr = $_REQUEST['contact_name'];
	$contact_desc = $_REQUEST['contact_desc'];
	$contact_email_id = $_REQUEST['contact_email_id'];
	$contact_phone_number = $_REQUEST['contact_phone_number'];
	$main_person = $_REQUEST['main_person'];
    
    //check for contact email exists or not
	foreach($contact_name_arr as $key => $contact_name){		
		$res = $wpdb->get_results("SELECT * FROM `csr_corporate_contact` WHERE `contact_email_id` = '" . $contact_email_id[$key] . "' " ); 
		if(!empty($res)){
			$return['error'] = "Exist";
			print json_encode($return);
			exit;
		}
	}
	
    //file width validations
	if(isset($_FILES["company_logo"]["name"]) && !empty($_FILES["company_logo"]["name"])){
		$src = $_FILES['company_logo']['tmp_name'];
		list($width, $height) = getimagesize($src);
		if ($width > "100" || $height > "100") {
			$return['error'] = "FileWidthWrong";
			print json_encode($return);
			exit;
		}
	}

    $work_location_id_arr = array();
    $work_locations = array();
	if(!empty($_POST['work_location'])){
		$table_name = $wpdb->prefix . "work_locations";
		$work_locations = explode(',', $_POST['work_location']);
		foreach($work_locations as $work_location ){
			if(is_numeric($work_location)){
				$work_location_id_arr[] = $work_location;
				$res = $wpdb->get_results("SELECT * FROM " . $table_name . " WHERE `id` = '" . $work_location . "' " ); 
				if(!empty($res)){
					$work_locations[] = $res[0]->location_name;
				}
			} else {
				$work_location = trim($work_location);
				$res = $wpdb->get_results("SELECT * FROM " . $table_name . " WHERE `location_name` = '" . $work_location . "' " ); 
				if(empty($res)){
					$wpdb->insert($table_name, array('location_name' => $work_location) ); 
					$id = $wpdb->insert_id;
					$work_location_id_arr[] = $id;
					$work_locations[] = $work_location;
				} else {
					$work_location_id_arr[] = $res[0]->id;
					$work_locations[] = $res[0]->location_name;
				}
			}
		}
	}
	
	$return = ''; 
	$wpdb->insert('csr_corporate_partner', 
		array(
			'organisation_name' => $organisation_name,
			'corporate_type_id' => $corporate_type_id,
			'address1' => $address1,
			'address2' => $address2,
			'zip_code' => $zip_code,
			'created_date' => date('Y-m-d H:i:s')
		) 
	); 
	$corporate_id = $wpdb->insert_id;
	foreach($work_location_id_arr as $work_location_id){
		$wpdb->insert('csr_corporate_work_locations', 
			array(
				'corporate_id' => $corporate_id,
				'work_locations_id' => $work_location_id
			) 
		);
	}
	foreach($contact_name_arr as $key => $contact_name){
		
		$res = $wpdb->get_results("SELECT * FROM `csr_corporate_contact` WHERE `contact_email_id` = '" . $contact_email_id[$key] . "' " ); 
		if(!empty($res)){
			$return['error'] = "CotnactExist";
			print json_encode($return);
			exit;
		}
		
		$data = array(
			'corporate_id' => $corporate_id,
			'contact_name' => $contact_name,
			'contact_desc' => $contact_desc[$key],
			'contact_email_id' => $contact_email_id[$key],
			'contact_phone_number' => $contact_phone_number[$key],
		);
		if(!empty($main_person[$key])){
			$data['main_person'] = $main_person[$key];
			if($data['main_person'] == '1'){
				$data['contact_pass_id'] = rand(111111,999999);
				csr_corporate_password_email($data['contact_email_id'], $data['contact_pass_id']);
				$data['contact_pass_id'] =  md5($data['contact_pass_id']);
			}				
		}
		$wpdb->insert('csr_corporate_contact', $data);
	}
	$path = wp_upload_dir();
	if(isset($_FILES["company_logo"]["name"]) && !empty($_FILES["company_logo"]["name"])){
		$name = $_FILES["company_logo"]["name"];
		$tmpName = $_FILES["company_logo"]["tmp_name"];
		$extension = pathinfo($name, PATHINFO_EXTENSION);
		$name = $corporate_id.'_logo_'.time().'.'.$extension;
		$a = $path['basedir']."/corporate_company_logo/".$name; 
		$targetPath = str_replace("\\", "/", $a);
		move_uploaded_file($tmpName,$targetPath);
		$wpdb->query("UPDATE `csr_corporate_partner` SET `company_logo`='".$name."' WHERE `id`='".$corporate_id."' " ); 
	}
	$_SESSION['corporate_added'] = "corporate_added";
	$return['success'] = "Added";
	print json_encode($return);
	exit;
}
add_action( 'wp_ajax_add_new_corporate', 'add_new_corporate');

function update_corporate(){
	global $wpdb; 
	$corporate_id = $_REQUEST['corporate_id'];
	$organisation_name = $_REQUEST['organisation_name'];
	$corporate_type_id = $_REQUEST['corporate_type_id'];
	$address1 = $_REQUEST['address1'];
	$address2 = $_REQUEST['address2'];
	$zip_code = $_REQUEST['zip_code'];
	$contact_name_arr = $_REQUEST['contact_name'];
	$contact_desc = $_REQUEST['contact_desc'];
	$contact_email_id = $_REQUEST['contact_email_id'];
	$contact_phone_number = $_REQUEST['contact_phone_number'];
	$main_person = $_REQUEST['main_person'];
    
    //check for contact email exists or not
	$old_email = '';
	$old_password = '';
	foreach($contact_name_arr as $key => $contact_name){		
		$res = $wpdb->get_results("SELECT * FROM `csr_corporate_contact` WHERE `contact_email_id` = '" . $contact_email_id[$key] . "' AND main_person = '1' LIMIT 1" ); 
		if(!empty($res)){
			$record = $res[0];
			$old_email = $record->contact_email_id;
			$old_password = $record->contact_pass_id;
		}
	}
	
	//file width validations
	if(isset($_FILES["company_logo"]["name"]) && !empty($_FILES["company_logo"]["name"])){
		$src = $_FILES['company_logo']['tmp_name'];
		list($width, $height) = getimagesize($src);
		if ($width > "100" || $height > "100") {
			$return['error'] = "FileWidthWrong";
			print json_encode($return);
			exit;
		}
	}
	
    $work_location_id_arr = array();
    $work_locations = array();
	if(!empty($_POST['work_location'])){
		$table_name = $wpdb->prefix . "work_locations";
		$work_locations = explode(',', $_POST['work_location']);
		foreach($work_locations as $work_location ){
			if(is_numeric($work_location)){
				$work_location_id_arr[] = $work_location;
				$res = $wpdb->get_results("SELECT * FROM " . $table_name . " WHERE `id` = '" . $work_location . "' " ); 
				if(!empty($res)){
					$work_locations[] = $res[0]->location_name;
				}
			} else {
				$work_location = trim($work_location);
				$res = $wpdb->get_results("SELECT * FROM " . $table_name . " WHERE `location_name` = '" . $work_location . "' " ); 
				if(empty($res)){
					$wpdb->insert($table_name, array('location_name' => $work_location) ); 
					$id = $wpdb->insert_id;
					$work_location_id_arr[] = $id;
					$work_locations[] = $work_location;
				} else {
					$work_location_id_arr[] = $res[0]->id;
					$work_locations[] = $res[0]->location_name;
				}
			}
		}
	}
	$return = ''; 
	$csr_corporate_partner_data = array(
									'organisation_name' => $organisation_name,
									'corporate_type_id' => $corporate_type_id,
									'address1' => $address1,
									'address2' => $address2,
									'zip_code' => $zip_code,
								);
	$wpdb->update('csr_corporate_partner', $csr_corporate_partner_data, array('id'=>$corporate_id) );	
	$wpdb->query( "DELETE FROM `csr_corporate_work_locations` WHERE `corporate_id` = '".$corporate_id."' " );
	foreach($work_location_id_arr as $work_location_id){
		$wpdb->insert('csr_corporate_work_locations', 
			array(
				'corporate_id' => $corporate_id,
				'work_locations_id' => $work_location_id
			) 
		);
	}
	
	$wpdb->query( "DELETE FROM `csr_corporate_contact` WHERE `corporate_id` = '".$corporate_id."' " );
	foreach($contact_name_arr as $key => $contact_name){
		
		$res = $wpdb->get_results("SELECT * FROM `csr_corporate_contact` WHERE `contact_email_id` = '" . $contact_email_id[$key] . "' " ); 
		if(!empty($res)){
			$return['error'] = "CotnactExist";
			print json_encode($return);
			exit;
		}
		
		$data = array(
			'corporate_id' => $corporate_id,
			'contact_name' => $contact_name,
			'contact_desc' => $contact_desc[$key],
			'contact_email_id' => $contact_email_id[$key],
			'contact_phone_number' => $contact_phone_number[$key],
		);
		
		if(!empty($main_person[$key])){
			$data['main_person'] = $main_person[$key];
			if($data['main_person'] == '1'){
				if($old_email == $data['contact_email_id']){
					$data['contact_pass_id'] = $old_password;
				}else{
					$data['contact_pass_id'] = rand(111111,999999);
					csr_corporate_password_email($data['contact_email_id'], $data['contact_pass_id']);
					$data['contact_pass_id'] = md5($data['contact_pass_id']);
				}
			}				
		}
		
		$wpdb->insert('csr_corporate_contact', $data);
	}
	
	$path = wp_upload_dir();
	if(isset($_FILES["company_logo"]["name"]) && !empty($_FILES["company_logo"]["name"])){
		$name = $_FILES["company_logo"]["name"];
		$tmpName = $_FILES["company_logo"]["tmp_name"];
		$extension = pathinfo($name, PATHINFO_EXTENSION);
		$name = $corporate_id.'_logo_'.time().'.'.$extension;
		$a = $path['basedir']."/corporate_company_logo/".$name; 
		$targetPath = str_replace("\\", "/", $a);
		move_uploaded_file($tmpName,$targetPath);
		$wpdb->query("UPDATE `csr_corporate_partner` SET `company_logo`='".$name."' WHERE `id`='".$corporate_id."' " ); 
	}
	$_SESSION['corporate_updated'] = "corporate_updated";
	$return['success'] = "Updated";
	print json_encode($return);
	exit;
}
add_action( 'wp_ajax_update_corporate', 'update_corporate');

function csr_corporate_password_email($email_id, $password) {
	global $wpdb;
	$args = array(
	  'name'        => 'csr-corporate-account-created',
	  'post_type'   => 'csr_email_format',
	  'post_status' => 'publish',
	  'numberposts' => 1
	);
	$post = get_posts($args);
	
    $to = $email_id;
	$subject = $post[0]->post_title;
	$message = '';
	$message .= $post[0]->post_content;
	$message .= "<br /> Email : ". $email_id;
	$message .= "<br /> Password : ". $password;
	$headers[] = 'From: '.get_option( 'blogname' ).' <'.get_option( 'admin_email' ).'>';
    add_filter( 'wp_mail_content_type', 'csr_corporate_password_email_content_type' );
    $mail = wp_mail( $to, $subject, $message, $headers );
}
function csr_corporate_password_email_content_type() {
    return 'text/html';
}

function corporate_filter_list(){
	global $wpdb; 
	$requestData = $_REQUEST;
	$table_name = $wpdb->prefix . "corporate_partner";
	$result = $wpdb->get_results("SELECT * FROM $table_name");
	$totalData = $wpdb->num_rows;
	$totalFiltered = $totalData;

	$sql = "SELECT `csr_corporate_partner`.`id`, `csr_corporate_partner`.`organisation_name`, `csr_corporate_partner`.`created_date` ";
	$sql .= " FROM `csr_corporate_partner` ";
	//$sql .= " LEFT JOIN `csr_corporate_type` ON `csr_corporate_partner`.`corporate_type_id`=`csr_corporate_type`.`id` ";
	$sql .= " LEFT JOIN `csr_corporate_work_locations` ON `csr_corporate_partner`.`id`=`csr_corporate_work_locations`.`corporate_id` ";
	if( $requestData['corporate_type'] == 'trashed' ){
		$sql .= " WHERE `csr_corporate_partner`.`isTrash`= 1 ";
	} else {
		$sql .= " WHERE `csr_corporate_partner`.`isTrash`= 0 ";
	}
	if(!empty($requestData['corporate_type_id'])){
		$sql .= " AND  `csr_corporate_partner`.`corporate_type_id` LIKE '".$requestData['corporate_type_id']."' ";
	}
	if(!empty($requestData['work_location'])){
		$sql .= " AND  `csr_corporate_work_locations`.`work_locations_id` = '".$requestData['work_location']."' ";
	}
	if( !empty($requestData['search']['value']) ) {
		$sql .= " AND ( `csr_corporate_partner`.`organisation_name` LIKE '%".$requestData['search']['value']."%' ) ";    
		//$sql .= " OR `csr_corporate_type`.`type_name` LIKE '%".$requestData['search']['value']."%' )";
	}

	$sql .= " GROUP BY `csr_corporate_work_locations`.`corporate_id` ";
	$result = $wpdb->get_results($sql);
	$totalData = $totalFiltered = $wpdb->num_rows;
	$sql .= " ORDER BY `csr_corporate_partner`.`created_date` DESC LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
	$result = $wpdb->get_results($sql);
	$inc = 1;
	$data = array();
	foreach($result as $row){
		$nestedData = array(); 
		$nestedData[] = $inc;
		$nestedData[] = '<a href="?page=corporate&action=corporate_details&id='.$row->id.'">'.$row->organisation_name.'</a>';
		$nestedData[] = date('d/m/Y H:i:s', strtotime($row->created_date));
		$nestedData[] = '<a href="?page=corporate&action=corporate_wishlist&id='.$row->id.'">Proposals</a>';
		$delete_icon = '';
		$cp_result = $wpdb->get_results("SELECT * FROM `csr_corporate_proposal` WHERE `corporate_id`='".$row->id."' ");
		if(empty($cp_result)){
			if( $requestData['corporate_type'] == 'trashed' ){
				$delete_icon = '<span class="text-info" style="cursor:pointer;" onClick="corporate_restore('.$row->id.')" >Restore</span>'.'|'.'<span class="text-info" style="cursor:pointer;" onClick="corporate_delete('.$row->id.')" >Delete</span>';
			} else {
				$delete_icon = '<span class="label label-danger" style="cursor:pointer;" onClick="corporate_move_to_trash('.$row->id.')" ><i class="fa fa-trash"></i></span>';
			}			
		}	
		$nestedData[] = $delete_icon;
		$data[] = $nestedData;
		$inc++;
	}
	$json_data = array(
		"draw"            => intval( $requestData['draw'] ), 
		"recordsTotal"    => intval( $totalData ),  
		"recordsFiltered" => intval( $totalFiltered ), 
		"data"            => $data
	);
	echo json_encode($json_data);
	exit;
}
add_action( 'wp_ajax_corporate_filter_list', 'corporate_filter_list');

function corporate_move_to_trash(){
	global $wpdb;
	$corporate_id = $_REQUEST['corporate_id'];
	$_SESSION['corporate_moved'] = "corporate_moved";
	$wpdb->update( 'csr_corporate_partner', array('isTrash' => '1' ), array( 'id' => $corporate_id ) );
	echo 'corporate_moved';
	exit;
}
add_action( 'wp_ajax_corporate_move_to_trash', 'corporate_move_to_trash');

function corporate_restore(){
	global $wpdb;
	$corporate_id = $_REQUEST['corporate_id'];
	$_SESSION['corporate_restored'] = "corporate_restored";
	$wpdb->update( 'csr_corporate_partner', array('isTrash' => '0' ), array( 'id' => $corporate_id ) );
	echo 'corporate_restored';
	exit;
}
add_action( 'wp_ajax_corporate_restore', 'corporate_restore');

function corporate_delete(){
	global $wpdb;
	$corporate_id = $_REQUEST['corporate_id'];
	$_SESSION['corporate_deleted'] = "corporate_deleted";
	$wpdb->query( "DELETE FROM `csr_corporate_contact` WHERE `corporate_id` = '".$corporate_id."'" );
	$wpdb->query( "DELETE FROM `csr_corporate_proposal` WHERE `corporate_id` = '".$corporate_id."'" );
	$wpdb->query( "DELETE FROM `csr_corporate_wishlist` WHERE `corporate_id` = '".$corporate_id."'" );
	$wpdb->query( "DELETE FROM `csr_corporate_work_locations` WHERE `corporate_id` = '".$corporate_id."'" );
	$wpdb->query( "DELETE FROM `csr_corporate_partner` WHERE `id` = '".$corporate_id."'" );
	echo 'corporate_deleted';
	exit;
}
add_action( 'wp_ajax_corporate_delete', 'corporate_delete');

function corporate_wishlist_filter_list(){
	global $wpdb; 
	$requestData = $_REQUEST;
	$table_name = $wpdb->prefix . "proposal";
	$result = $wpdb->get_results("SELECT * FROM $table_name");
	$totalData = $wpdb->num_rows;
	$totalFiltered = $totalData;
	$corporate_id = isset($requestData['corporate_id'])? $requestData['corporate_id'] : '0';

	$sql = "SELECT `csr_corporate_wishlist`.`id`,`csr_corporate_wishlist`.`request_status`,`csr_corporate_wishlist`.`created_date`,`csr_corporate_wishlist`.`proposal_id`,`csr_corporate_wishlist`.`corporate_id`,`csr_proposal`.`project_title`,`csr_proposal`.`hub_code`,`csr_proposal`.`project_summary`, `csr_corporate_partner`.`organisation_name` ";
	$sql .= " FROM `csr_corporate_wishlist` ";
	$sql .= " LEFT JOIN `csr_proposal` ON `csr_corporate_wishlist`.`proposal_id` = `csr_proposal`.`id` ";
	$sql .= " LEFT JOIN `csr_corporate_partner` ON `csr_corporate_wishlist`.`corporate_id` = `csr_corporate_partner`.`id` ";
	$sql .= " WHERE 1 ";
	if( !empty($requestData['search']['value']) ) {
		$sql .= " AND ( `csr_proposal`.`project_title` LIKE '%".$requestData['search']['value']."%' ";    
		$sql .= " OR `csr_proposal`.`hub_code` LIKE '%".$requestData['search']['value']."%' )";
	}
	
	if($corporate_id > 0){
		$sql .= " AND `csr_corporate_wishlist`.`corporate_id` = '$corporate_id' ";
	}else{
		$sql .= " AND `csr_corporate_wishlist`.`request_status` = '0' ";
	}
	
	$result = $wpdb->get_results($sql);
	$totalData = $totalFiltered = $wpdb->num_rows;
	$sql .= " ORDER BY `csr_corporate_wishlist`.`created_date` DESC LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
	$result = $wpdb->get_results($sql);
	$inc = 1;
	$data = array();
	foreach($result as $row){
		$status = $row->request_status;
		$wishlistHtml = '';
		if($status == '1'){
			$wishlistHtml = '<a data-action="reject" data-id="'.$row->id.'" class="add-to-wishlist wishlist-btn-'.$row->id.'" href="javascript:void(0);">Reject</a> &nbsp;&nbsp;<strong>Confirmed</strong>';
		} else {
			$wishlistHtml = '<a data-action="accept" data-id="'.$row->id.'" class="add-to-wishlist wishlist-btn-'.$row->id.'" href="javascript:void(0);">Accept</a> | <a data-action="reject" data-id="'.$row->id.'" class="add-to-wishlist wishlist-btn-'.$row->id.'" href="javascript:void(0);">Reject</a> &nbsp;&nbsp;<strong>Wishlist</strong>';
		}
		$nestedData = array(); 
		$nestedData[] = $inc;
		$nestedData[] = '<a target="_blank" href="'.site_url().'/wp-admin/admin.php?page=corporate&action=corporate_details&id='.$row->corporate_id.'">'.$row->organisation_name.'</a>';
		$nestedData[] = '<a target="_blank" href="'.site_url().'/wp-admin/admin.php?page=proposal&action=proposal_details&id='.$row->proposal_id.'">'.$row->project_title.'</a>';
		$nestedData[] = $row->hub_code;
		$nestedData[] = mb_strimwidth($row->project_summary, 0, 200, '...');
		$nestedData[] = date('d/m/Y H:i:s', strtotime($row->created_date));
		$nestedData[] = $wishlistHtml;
		$data[] = $nestedData;
		$inc++;
	}
	$json_data = array(
		"draw"            => intval( $requestData['draw'] ), 
		"recordsTotal"    => intval( $totalData ),  
		"recordsFiltered" => intval( $totalFiltered ), 
		"data"            => $data
	);
	echo json_encode($json_data);
	exit;
}
add_action( 'wp_ajax_corporate_wishlist_filter_list', 'corporate_wishlist_filter_list');

function corporate_wishlist_accept(){
	global $wpdb; 
	$id = (isset($_REQUEST['id']) && is_numeric($_REQUEST['id']))? $_REQUEST['id'] : '0';
	$table_name = 'csr_corporate_wishlist';
	$created_date = date('Y-m-d H:i:s');
    $return = '';
	
	$sql = "SELECT 
				`$table_name`.`id`, `$table_name`.`corporate_id` , `$table_name`.`proposal_id`, `$table_name`.`created_date`, `csr_corporate_partner`.`organisation_name`, `csr_corporate_contact`.`contact_email_id`, `csr_corporate_contact`.`contact_name`, `csr_proposal`.`project_title`, `csr_proposal`.`hub_code`, `csr_ngo`.`email_id` AS ngo_email
			FROM `$table_name`
			LEFT JOIN `csr_proposal` ON `$table_name`.`proposal_id` = `csr_proposal`.`id`
			LEFT JOIN `csr_corporate_partner` ON `$table_name`.`corporate_id` = `csr_corporate_partner`.`id`
			LEFT JOIN `csr_corporate_contact` ON `$table_name`.`corporate_id` = `csr_corporate_contact`.`corporate_id`
			LEFT JOIN `csr_ngo` ON `csr_proposal`.`hub_code` = `csr_ngo`.`hub_code`
			WHERE `$table_name`.`id` = '$id' AND `csr_corporate_contact`.`main_person` = '1' GROUP BY `csr_corporate_contact`.`corporate_id`
			";
	$foundRecords = $wpdb->get_results($sql);
	if(empty($foundRecords)){
		$return['error'] = "Nodata";
	}else{
		
		$record = $foundRecords[0];
		$proposal_id = $record->proposal_id;
		$corporate_id = $record->corporate_id;
		$created_date = $record->created_date;
		$email_id = $record->contact_email_id;
		$project_title = $record->project_title;
		$organisation_name = $record->organisation_name;
		$contact_name = $record->contact_name;
		$ngo_email = $record->ngo_email;
		
		csr_corporate_wishlist_accept_email($email_id, $project_title);
		csr_corporate_wishlist_accept_ngo_email($ngo_email, $project_title, $organisation_name);
		
		// csr_proposal status set 'Locked'
		$csr_proposal_sql = "UPDATE `csr_proposal` SET `proposal_status` = 'Locked' WHERE `id` = '".$proposal_id."' ";
		$csr_proposal_insertRecord = $wpdb->get_results($csr_proposal_sql);
		
		$sql = "DELETE FROM `$table_name` WHERE `proposal_id` = '$proposal_id' AND `id` != '$id'";
		$foundRecords = $wpdb->get_results($sql);
		
		$insert_sql = "UPDATE `$table_name` SET `request_status` = '1' WHERE `id` = '$id' ";
		$insertRecord = $wpdb->get_results($insert_sql);
		
		$insert_sql = "INSERT INTO `csr_corporate_proposal` (`corporate_id`, `proposal_id`, `created_date`) VALUES('$corporate_id', '$proposal_id', '$created_date')";
		$insertRecord = $wpdb->get_results($insert_sql);
		
		$return['success'] = "Added";
	}
	print json_encode($return);
	exit;
}
add_action( 'wp_ajax_corporate_wishlist_accept', 'corporate_wishlist_accept');

function csr_corporate_wishlist_accept_ngo_email($email_id, $project_title, $organisation_name) {
	global $wpdb;
	$args = array(
	  'name'        => 'csr-ngo-proposal-wishlisted-by-corporate',
	  'post_type'   => 'csr_email_format',
	  'post_status' => 'publish',
	  'numberposts' => 1
	);
	$post = get_posts($args);
	
    $to = $email_id;
	$subject = $post[0]->post_title;
	$message = '';
	$message .= $post[0]->post_content;
	$message .= '<br/><strong>Project Name:</strong> '.$project_title;
	$message .= '<br/><strong>Corporate Organisation Name:</strong> '.$organisation_name;
	$headers[] = 'From: '.get_option( 'blogname' ).' <'.get_option( 'admin_email' ).'>';
    add_filter( 'wp_mail_content_type', 'csr_corporate_wishlist_accept_ngo_email_content_type' );
    $mail = wp_mail( $to, $subject, $message, $headers );
}
function csr_corporate_wishlist_accept_ngo_email_content_type() {
    return 'text/html';
}

function csr_corporate_wishlist_accept_email($email_id, $project_title) {
	global $wpdb;
	$args = array(
	  'name'        => 'csr-corporate-wishlist-request-accepted',
	  'post_type'   => 'csr_email_format',
	  'post_status' => 'publish',
	  'numberposts' => 1
	);
	$post = get_posts($args);
	
    $to = $email_id;
	$subject = $post[0]->post_title;
	$message = '';
	$message .= $post[0]->post_content;
	$message .= '<br/><strong>Project Name:</strong> '.$project_title;
	$headers[] = 'From: '.get_option( 'blogname' ).' <'.get_option( 'admin_email' ).'>';
    add_filter( 'wp_mail_content_type', 'csr_corporate_wishlist_accept_email_content_type' );
    $mail = wp_mail( $to, $subject, $message, $headers );
}
function csr_corporate_wishlist_accept_email_content_type() {
    return 'text/html';
}

function corporate_wishlist_reject(){
	global $wpdb; 
	$id = (isset($_REQUEST['id']) && is_numeric($_REQUEST['id']))? $_REQUEST['id'] : '0';
	$table_name = 'csr_corporate_wishlist';
	$created_date = date('Y-m-d H:i:s');
    $return = '';
	
	$sql = "SELECT 
				`csr_corporate_wishlist`.`id` , `csr_corporate_wishlist`.`corporate_id` , `csr_corporate_wishlist`.`proposal_id`, `csr_corporate_contact`.`contact_email_id`, `csr_proposal`.`project_title`
			FROM `csr_corporate_wishlist`
			LEFT JOIN `csr_proposal` ON `csr_corporate_wishlist`.`proposal_id` = `csr_proposal`.`id`
			LEFT JOIN `csr_corporate_contact` ON `csr_corporate_wishlist`.`corporate_id` = `csr_corporate_contact`.`corporate_id`
			WHERE `csr_corporate_wishlist`.`id` = '$id' AND `csr_corporate_contact`.`main_person` = '1' GROUP BY `csr_corporate_contact`.`corporate_id`
			";
	$foundRecords = $wpdb->get_results($sql);
	if(empty($foundRecords)){
		$return['error'] = "Nodata";
	}else{
		$record = $foundRecords[0];
		$corporate_id = $record->corporate_id;
		$proposal_id = $record->proposal_id;
		csr_corporate_wishlist_reject_email($record->contact_email_id, $record->project_title);
		
		$sql = "DELETE FROM `$table_name` WHERE `id` = '$id' LIMIT 1";
		$foundRecords = $wpdb->get_results($sql);
		
		$sql = "DELETE FROM `csr_corporate_proposal` WHERE `corporate_id` = '$corporate_id' AND `proposal_id` = '$proposal_id' LIMIT 1";
		$foundRecords = $wpdb->get_results($sql);
		
		$return['success'] = "Removed";
	}
	print json_encode($return);
	exit;
}
add_action( 'wp_ajax_corporate_wishlist_reject', 'corporate_wishlist_reject');

function csr_corporate_wishlist_reject_email($email_id, $project_title) {
	global $wpdb;
	$args = array(
	  'name'        => 'csr-corporate-wishlist-request-rejected',
	  'post_type'   => 'csr_email_format',
	  'post_status' => 'publish',
	  'numberposts' => 1
	);
	$post = get_posts($args);
	
    $to = $email_id;
	$subject = $post[0]->post_title;
	$message = '';
	$message .= $post[0]->post_content;
	$message .= '<br/><strong>Project Name:</strong> '.$project_title;
	$headers[] = 'From: '.get_option( 'blogname' ).' <'.get_option( 'admin_email' ).'>';
    add_filter( 'wp_mail_content_type', 'csr_corporate_wishlist_reject_email_content_type' );
    $mail = wp_mail( $to, $subject, $message, $headers );
}
function csr_corporate_wishlist_reject_email_content_type() {
    return 'text/html';
}


/***************************** Login / logout Corporate ***************************/
function corporate_login(){
	global $wpdb; 
	//print_r($_REQUEST); exit;
	$email_id = $_REQUEST['email_id'];
	//$pass = $_REQUEST['pass'];
	$pass = md5($_REQUEST['pass']);
    
	$sql = "SELECT * FROM `csr_corporate_contact` WHERE `contact_email_id` = '" . $email_id . "' AND `contact_pass_id` = '" . $pass . "' AND `main_person` = '1' LIMIT 1";	
    $res = $wpdb->get_results($sql); 
	
	if(!empty($res)){
		foreach($res as $record){
			$_SESSION['corporate_login_id'] = $record->id;
			$_SESSION['corporate_name'] = $record->contact_name;
			$_SESSION['user_data'] = $record;
			break;
		}
		$return['success'] = "Exist";
		print json_encode($return);
		exit;
	}
    
	$return['error'] = "Invalid";
	print json_encode($return);
	exit;
}
add_action( 'wp_ajax_corporate_login', 'corporate_login');
add_action( 'wp_ajax_nopriv_corporate_login', 'corporate_login');

// adding signin and signout menu to primary menu
add_filter( 'wp_nav_menu_items', 'add_corporate_loginout_link', 20, 2 );
function add_corporate_loginout_link( $items, $args ) {
    if (isset($_SESSION['corporate_login_id']) && $_SESSION['corporate_login_id'] > 0 && $args->theme_location == 'primary') {
        $items .= '
			<li class="dropdown-submenu menu-item menu-item-type-post_type menu-item-object-zanmenu menu-item-30889 zanmenu-menu-item dropdown menu-item-has-children zmb-position-relative">
				<a title="Corporate" class="zmb-item-title zmb-dropdown-toggle" data-title="Corporate" data-obj-id="30862" href="#" data-toggle="dropdown">Hello ' . $_SESSION['corporate_name'] . ' <span class="caret"></span></a>
				<div class="sub-menu zanmenu  zmb-dop-direction-left" data-width="225" data-style="" data-mega-obj-id="30862" style="background-color: rgb(255, 255, 255); width: 225px;">
					<div class="zanmenu-inner">
						<div class="zanmenu-content ">
							<div id="vc_row-58ba1ef8ad843" class="vc_row wpb_row vc_row-fluid">
								<div class="wpb_column vc_column_container vc_col-sm-12">
									<div class="vc_column-inner ">
										<div class="wpb_wrapper">
											<div class="zan-custom-menu-wrap zanmb-custom-menu-wrap ">
												<div class="vc_wp_custommenu wpb_content_element zan-custom-menu zanmb-custom-menu" data-menu-slug="corporate">
													<div class="widget widget_nav_menu">
														<div class="zmb-container zmb-container-">
															<div class="menu-corporate-container">
																<ul id="menu-corporate" class="menu">
																	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-29497"><a href="'. site_url() .'/corporate-account/?page=proposal-bank">Profile</a></li>
																	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-29497"><a href="'. site_url() .'/corporate-account/?page=corporate-logout">Sign out</a></li>
																</ul>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</li>
		';
        //$items .= '<li class="menu-item"><a href="'. site_url() .'/corporate-account">Hello ' . $_SESSION['corporate_name'] . '</a></li>';
        //$items .= '<li class="menu-item"><a href="'. site_url() .'/corporate-account/?page=corporate-logout">Sign out</a></li>';
    }
    elseif (!(isset($_SESSION['corporate_login_id']) && $_SESSION['corporate_login_id'] > 0) && $args->theme_location == 'primary') {
        $items .= '<li class="menu-item"><a href="'. site_url() .'/corporate-account/">Sign in</a></li>';
    }
    return $items;
}

?>
