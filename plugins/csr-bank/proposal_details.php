<?php 
	global $wpdb;
	if(empty($_GET['id']) || !is_numeric($_GET['id'])){
		wp_redirect(admin_url().'admin.php?page=proposal');
	}
	$proposal_id = $_GET['id'];
	$proposal_result = $wpdb->get_results("SELECT * FROM `csr_proposal` WHERE `id` = '" . $proposal_id . "' " ); 
	$proposal_result = $proposal_result[0];
	if(empty($proposal_result)){
		wp_redirect(admin_url().'admin.php?page=proposal');
	}
	// thematic_area selection box
	$tlids_arr = array();
	$thematic_areas_works_arr = array();
	$tlids_result = $wpdb->get_results("SELECT `thematic_areas_id` FROM `csr_proposal_thematic_areas_work` WHERE `proposal_id` = '" . $proposal_id . "' ");
	foreach($tlids_result as $tlids_row){
		$tlids_arr[] = $tlids_row->thematic_areas_id;
	}
	$tlids = implode(', ', $tlids_arr);
	$thematic_areas_work_result = $wpdb->get_results("SELECT * FROM `csr_thematic_areas_work` WHERE `id` IN ( ".$tlids." ) " ); 
	foreach($thematic_areas_work_result as $thematic_row){
		$thematic_areas_works_arr[$thematic_row->id] = str_replace(',', ' ', $thematic_row->thematic_area);
	}
	
	//work locations array		
	$wlids_arr = array();
	$work_locations_arr = array();
	$wlids_result = $wpdb->get_results("SELECT `work_locations_id` FROM `csr_proposal_work_locations` WHERE `proposal_id` = '" . $proposal_id . "' ");
	foreach($wlids_result as $wlids_row){
		$wlids_arr[] = $wlids_row->work_locations_id;
	}
	$wlids = implode(', ', $wlids_arr);
	$work_locations_result = $wpdb->get_results("SELECT * FROM `csr_work_locations` WHERE `id` IN ( ".$wlids." ) " ); 
	foreach($work_locations_result as $work_locations_row){
		$work_locations_arr[$work_locations_row->id] = $work_locations_row->location_name;
	}
	
	// tags array
	$tags_arr = array();
	$tagids_arr = array();
	$tagids_result = $wpdb->get_results("SELECT `tag_id` FROM `csr_proposal_tags` WHERE `proposal_id` = '" . $proposal_id . "' ");
	foreach($tagids_result as $tagids_row){
		$tagids_arr[] = $tagids_row->tag_id;
	}
	$tagids = implode(', ', $tagids_arr);
	$tags_result = $wpdb->get_results("SELECT * FROM `csr_tag` WHERE `id` IN ( ".$tagids." ) " ); 
	foreach($tags_result as $tags_row){
		$tags_arr[$tags_row->id] = $tags_row->tag_name;
	}
	
?>
<link href="<?php echo plugins_url(); ?>/select2/select2.css" rel="stylesheet" type="text/css" />
<script src="<?php echo plugins_url(); ?>/select2/select2.min.js" type="text/javascript"></script>
<style>
	input[type=text], input[type=number], input[type=email], #project_summary, #s2id_autogen1, .select2-container{
		width: 100% !important;
	}
</style>
<div class="wrap">
	<h2 class="hndle ui-sortable-handle" style="padding: 5px 10px; margin: 0; "><a href="?page=proposal" class="button button-primary button-medium"> Back </a> &nbsp;<span>Proposal Details</span></h2>
	<div class="display_alert">
		<?php 
			if(isset($_SESSION['proposal_updated'])){
				echo '<div id="message" class="updated notice notice-success is-dismissible"><p>Proposal Updated.</p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div>';
				unset($_SESSION['proposal_updated']);
			}
		?>
	</div>
	<div class="postbox" style="padding: 10px;">
		<form id="update_proposal" action="" method="post" enctype="multipart/form-data" >
			<input type="hidden" name="proposal_id" id="proposal_id" value="<?php echo $proposal_result->id; ?>">
			<table width="70%">
				<tr>
					<td><label for="hub_code">HUB Code*:</label></td>
					<td><input type="text" name="hub_code" id="hub_code" value="<?php echo $proposal_result->hub_code; ?>" class="" required ></td>
				</tr>
				<tr>
					<td><label for="project_title">Project Title*:</label></td>
					<td><input type="text" name="project_title" id="project_title" value="<?php echo $proposal_result->project_title; ?>" class="" required ></td>
				</tr>
				<?php /* <tr>
					<td><label for="thematic_areas_work_id">Thematic areas of work*:</label></td>
					<td>
						<select class="form-dropdown select2" id="thematic_areas_work_id" name="thematic_areas_work_id" required >
							<option value=""> - Select Thematic area - </option>
							<?php
								$table_name = $wpdb->prefix . "thematic_areas_work";
								$res = $wpdb->get_results("select * from $table_name where isDelete=0"); 
								foreach($res as $row){
									echo '<option value="'.$row->id.'" ';
									if( $row->id == $proposal_result->thematic_areas_work_id ) { echo 'Selected'; }
									echo ' >'.$row->thematic_area.'</option>';
								}
							?>
						</select>
					</td>
				</tr> */ ?>
				<tr>
					<td><label for="thematic_areas_works">Thematic areas of work*:</label></td>
					<td>
						<input type="hidden" name="thematic_areas_works" id="thematic_areas_works" required >
						<ul class="parsley-errors-list filled tag_required_thematic_areas"  style="color: #f00;"><li class="parsley-required">This value is required.</li></ul>
					</td>
				</tr>
				<tr>
					<td><label for="work_location">Locations of work: ( State / District / City )*:</label></td>
					<td>
						<input type="hidden" name="work_location" id="work_locations" required >
						<ul class="parsley-errors-list filled tag_required_work_locations"  style="color: #f00;"><li class="parsley-required">This value is required.</li></ul>
					</td>
				</tr>
				<tr>
					<td><label for="tags">Tag:</label></td>
					<td>
						<input type="hidden" name="tags" id="tags" value="" class=""  >
						<ul class="parsley-errors-list filled tag_required"  style="color: #f00;"><li class="parsley-required">This value is required.</li></ul>
					</td>
				</tr>
				<tr>
					<td><label for="project_summary">Project Summary*:</label></td>
					<td><textarea name="project_summary" id="project_summary" rows="4"><?php echo $proposal_result->project_summary; ?></textarea></td>
				</tr>
				<tr>
					<td><label for="status">Status:</label></td>
					<td>
						<?php if( $proposal_result->proposal_status == 'Locked' ) { ?>
							<input type="text" name="proposal_status" id="proposal_status" value="Locked" class="" readonly >
						<?php } else { ?>
							<select class="form-dropdown select2" id="proposal_status" name="proposal_status" >
								<option value=""> - Select Status - </option>
								<option value="Accepted - Live" <?php if( $proposal_result->proposal_status == 'Accepted - Live' ) { echo 'Selected'; } ?> > Accepted - Live </option>
								<option value="Accepted - Not Live" <?php if( $proposal_result->proposal_status == 'Accepted - Not Live' ) { echo 'Selected'; } ?> > Accepted - Not Live </option>
								<option value="Rejected" <?php if( $proposal_result->proposal_status == 'Rejected' ) { echo 'Selected'; } ?> > Rejected </option>
							</select>
						<?php } ?>
					</td>
				</tr>
				<tr>
					<td><label for="proposal_pdf">Proposal pdf:</label></td>
					<td>
						<input type="file" name="proposal_pdf" id="proposal_pdf" class="" >
						<?php $filepath = home_url()."/wp-content/uploads/proposal_pdf/"; ?>
						<?php if(!empty($proposal_result->proposal_pdf)){ ?>

								<a href="<?php echo $filepath.$proposal_result->proposal_pdf; ?>" download class="button" ><i class="fa fa-download"></i> Download </a> 

						<?php  } else { echo '<span style="border: 1px solid #999; padding: 5px; width:100px; ">N/A</span>'; } ?>
					</td>
				</tr>
				<tr>
					<td></td>
					<td><hr />
						<input type="submit" value="Update" class="button button-primary button-large submit_btn">
						<span class="loading_div" style="display: none;"><img src="<?php echo plugins_url(); ?>/csr-bank/img/loading.gif" alt="Loading..." width="25" /> <span>Please Wait...</span></span>
					</td>
				</tr>
			</table>
		</form>
	</div>
	<div class="postbox" style="padding: 10px;">
		<?php $filepath = home_url()."/wp-content/uploads/proposal_files/"; ?>
		<h3>
			Application form : &nbsp;&nbsp;&nbsp;
			<?php if(!empty($proposal_result->application_form_name)){ ?>
				<a href="<?php echo $filepath.$proposal_result->application_form_name; ?>" download class="button" ><i class="fa fa-download"></i> Download </a>
			<?php } else { echo 'N/A'; } ?>
		</h3> <hr />
		<h3>
			Payment form : &nbsp;&nbsp;&nbsp;
			<?php if(!empty($proposal_result->payment_receipt_name)){ ?>
				<a href="<?php echo $filepath.$proposal_result->payment_receipt_name; ?>" download class="button" ><i class="fa fa-download"></i> Download </a> 
			<?php } else { echo 'N/A'; } ?>
		</h3>
	</div>
</div>
<script type="text/javascript">
	jQuery('.tag_required').hide();
	jQuery('.tag_required_thematic_areas').hide();
	jQuery('.tag_required_work_locations').hide();
	var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
	jQuery(function() {
		jQuery('.select2').select2();
		tag_load();
		
		jQuery(document).on('click', '.is-dismissible', function () {
			jQuery('.is-dismissible').hide();
		});
		
		jQuery(document).on('submit', '#update_proposal', function () {
			var work_locations = jQuery('#work_locations').val();
            if(work_locations == ''){
                jQuery('.tag_required_work_locations').show();
                return false;
            }
            var thematic_areas_works = jQuery('#thematic_areas_works').val();
            if(thematic_areas_works == ''){
                jQuery('.tag_required_thematic_areas').show();
                return false;
            }
            var tags = jQuery('#tags').val();
            if(tags == ''){
                jQuery('.tag_required').show();
                return false;
            }
            //jQuery('.submit_btn').attr('disabled','disabled');
            jQuery('.loading_div').show();
            var postData = new FormData(this);
            postData.append('action', 'update_proposal');
            jQuery.ajax({
                url: ajaxurl,
				type: "POST",
                processData: false,
                contentType: false,
                cache: false,
                data: postData,
                success: function (response) {
					jQuery('.loading_div').hide();
					var json = jQuery.parseJSON(response);
                    if (json['error'] == 'Exist') {
                        jQuery('.display_alert').html('<div id="message" class="error notice notice-success is-dismissible"><p>Proposal Already Exist.</p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div>');
                    }
                    if (json['error'] == 'Wrong_hub_code') {
                        jQuery('.display_alert').html('<div id="message" class="error notice notice-success is-dismissible"><p>Below HUB Code is wrong, Please enter right HUB Code.</p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div>');
                    }
                    if (json['error'] == 'FileType') {
                        jQuery('.display_alert').html('<div id="message" class="error notice notice-success is-dismissible"><p><strong>Error! </strong> Only allow pdf file upload.</p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div>');
                    }
                    if (json['error'] == 'FileSize') {
                        jQuery('.display_alert').html('<div id="message" class="error notice notice-success is-dismissible"><p><strong>Error! </strong> Max file upload size limit 10MB.</p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div>');
                    }
                    if (json['success'] == 'Updated') {
						window.location.href = '?page=proposal&action=proposal_details&id=<?php echo $proposal_id; ?>'; 
						return false;
                        jQuery('.display_alert').html('<div id="message" class="updated notice notice-success is-dismissible"><p>NGO Updated.</p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div>');
                        jQuery('#update_proposal')[0].reset();
                        jQuery('.select2-search-choice-close').click();
                        jQuery('#select2-chosen-1').html(' - Select Thematic area - ');
                    }
                    jQuery("html, body").animate({ scrollTop: 0 }, "slow");
                    jQuery('.tag_required').hide();
                    //jQuery('.submit_btn').removeAttr('disabled','disabled');
                    tag_load();
                    return false;
                },
            });
            return false;
        });
		
	});
	
	function tag_load(){
		
		jQuery.ajax({
            url: ajaxurl,
			dataType : "JSON",
			data : {action: "get_work_locations"},
            success: function(result){
				jQuery("#work_locations").select2({
                    createSearchChoice:function(term, data) { if (jQuery(data).filter(function() { return this.text.localeCompare(term)===0; }).length===0) {return {id:term, text:term};} },
                    multiple: true,
                    <?php if(isset($proposal_id)){ ?>
                    initSelection: function (element, callback) {
                        callback(jQuery.map(element.val().split(','), function (id) {
                            return { id: id, text: id };
                        }));
                    },
                    <?php } ?>
                    //maximumSelectionSize: 1,
                    data: result,
                });
                <?php if(isset($proposal_id)){ ?>
                    <?php if(!empty($work_locations_arr)){ ?>
                        jQuery("#work_locations").select2('val', ["<?php echo implode('","', $work_locations_arr); ?>"], true);
                    <?php } ?>
                <?php } ?>
                function log(e) {
                    var e=jQuery("<li>"+e+"</li>");
                    jQuery("#events_11").append(e);
                    e.animate({opacity:1}, 10000, 'linear', function() { e.animate({opacity:0}, 2000, 'linear', function() {e.remove(); }); });
                }
                jQuery("#work_locations")
                    .on("change", function(e) { log("change "+JSON.stringify({val:e.val, added:e.added, removed:e.removed})); })
                    .on("select2-opening", function() { log("opening"); })
                    .on("select2-open", function() { log("open"); })
                    .on("select2-close", function() { log("close"); })
                    .on("select2-highlight", function(e) { log ("highlighted val="+ e.val+" choice="+ JSON.stringify(e.choice));})
                    .on("select2-selecting", function(e) { log ("selecting val="+ e.val+" choice="+ JSON.stringify(e.choice));})
                    .on("select2-removing", function(e) { log ("removing val="+ e.val+" choice="+ JSON.stringify(e.choice));})
                    .on("select2-removed", function(e) { log ("removed val="+ e.val+" choice="+ JSON.stringify(e.choice));})
                    .on("select2-loaded", function(e) { log ("loaded (data property omitted for brevity)");})
                    .on("select2-focus", function(e) { log ("focus");})
                    .on("select2-blur", function(e) { log ("blur");});
            }
        });
		
		jQuery.ajax({
            url: ajaxurl,
			dataType : "JSON",
			data : {action: "get_thematic_areas_works"},
            success: function(result){
				jQuery("#thematic_areas_works").select2({
                    //createSearchChoice:function(term, data) { if (jQuery(data).filter(function() { return this.text.localeCompare(term)===0; }).length===0) {return {id:term, text:term};} },
                    multiple: true,
                    <?php if(isset($proposal_id)){ ?>
                    initSelection: function (element, callback) {
                        callback(jQuery.map(element.val().split(','), function (id) {
                            return { id: id, text: id };
                        }));
                    },
                    <?php } ?>
                    //maximumSelectionSize: 1,
                    data: result,
                });
                <?php if(isset($proposal_id)){ ?>
                    <?php if(!empty($thematic_areas_works_arr)){ ?>
                        jQuery("#thematic_areas_works").select2('val', ["<?php echo implode('","', $thematic_areas_works_arr); ?>"], true);
                    <?php } ?>
                <?php } ?>
                function log(e) {
                    var e=jQuery("<li>"+e+"</li>");
                    jQuery("#events_11").append(e);
                    e.animate({opacity:1}, 10000, 'linear', function() { e.animate({opacity:0}, 2000, 'linear', function() {e.remove(); }); });
                }
                jQuery("#thematic_areas_works")
                    .on("change", function(e) { log("change "+JSON.stringify({val:e.val, added:e.added, removed:e.removed})); })
                    .on("select2-opening", function() { log("opening"); })
                    .on("select2-open", function() { log("open"); })
                    .on("select2-close", function() { log("close"); })
                    .on("select2-highlight", function(e) { log ("highlighted val="+ e.val+" choice="+ JSON.stringify(e.choice));})
                    .on("select2-selecting", function(e) { log ("selecting val="+ e.val+" choice="+ JSON.stringify(e.choice));})
                    .on("select2-removing", function(e) { log ("removing val="+ e.val+" choice="+ JSON.stringify(e.choice));})
                    .on("select2-removed", function(e) { log ("removed val="+ e.val+" choice="+ JSON.stringify(e.choice));})
                    .on("select2-loaded", function(e) { log ("loaded (data property omitted for brevity)");})
                    .on("select2-focus", function(e) { log ("focus");})
                    .on("select2-blur", function(e) { log ("blur");});
            }
        });
		
		jQuery.ajax({
            url: ajaxurl,
			dataType : "JSON",
			data : {action: "get_p_tags"},
            success: function(result){
				jQuery("#tags").select2({
                    createSearchChoice:function(term, data) { if (jQuery(data).filter(function() { return this.text.localeCompare(term)===0; }).length===0) {return {id:term, text:term};} },
                    multiple: true,
                    <?php if(isset($proposal_id)){ ?>
                    initSelection: function (element, callback) {
                        callback(jQuery.map(element.val().split(','), function (id) {
                            return { id: id, text: id };
                        }));
                    },
                    <?php } ?>
                    //maximumSelectionSize: 1,
                    data: result,
                });
                <?php if(isset($proposal_id)){ ?>
                    <?php if(!empty($tags_arr)){ ?>
                        jQuery("#tags").select2('val', ["<?php echo implode('","', $tags_arr); ?>"], true);
                    <?php } ?>
                <?php } ?>
                function log(e) {
                    var e=jQuery("<li>"+e+"</li>");
                    jQuery("#events_11").append(e);
                    e.animate({opacity:1}, 10000, 'linear', function() { e.animate({opacity:0}, 2000, 'linear', function() {e.remove(); }); });
                }
                jQuery("#tags")
                    .on("change", function(e) { log("change "+JSON.stringify({val:e.val, added:e.added, removed:e.removed})); })
                    .on("select2-opening", function() { log("opening"); })
                    .on("select2-open", function() { log("open"); })
                    .on("select2-close", function() { log("close"); })
                    .on("select2-highlight", function(e) { log ("highlighted val="+ e.val+" choice="+ JSON.stringify(e.choice));})
                    .on("select2-selecting", function(e) { log ("selecting val="+ e.val+" choice="+ JSON.stringify(e.choice));})
                    .on("select2-removing", function(e) { log ("removing val="+ e.val+" choice="+ JSON.stringify(e.choice));})
                    .on("select2-removed", function(e) { log ("removed val="+ e.val+" choice="+ JSON.stringify(e.choice));})
                    .on("select2-loaded", function(e) { log ("loaded (data property omitted for brevity)");})
                    .on("select2-focus", function(e) { log ("focus");})
                    .on("select2-blur", function(e) { log ("blur");});
            }
        });
    }
    
</script>
