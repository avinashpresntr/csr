<?php 
	global $wpdb; 
?>
<link href="<?php echo plugins_url(); ?>/select2/select2.css" rel="stylesheet" type="text/css" />
<script src="<?php echo plugins_url(); ?>/select2/select2.min.js" type="text/javascript"></script>
<style>
.alert {
  padding: 5px 15px;
  margin-bottom: 20px;
  border: 1px solid transparent;
  border-radius: 4px;
}
.alert h4 {
  margin-top: 0;
  color: inherit;
}
.alert .alert-link {
  font-weight: bold;
}
.alert > p,
.alert > ul {
  margin-bottom: 0;
}
.alert > p + p {
  margin-top: 5px;
}
.alert-dismissable,
.alert-dismissible {
  padding-right: 35px;
}
.alert-dismissable .close,
.alert-dismissible .close {
  position: relative;
  top: -2px;
  right: -21px;
  color: inherit;
}
.alert-success {
  color: #3c763d;
  background-color: #dff0d8;
  border-color: #d6e9c6;
}
.alert-success hr {
  border-top-color: #c9e2b3;
}
.alert-success .alert-link {
  color: #2b542c;
}
.alert-info {
  color: #31708f;
  background-color: #d9edf7;
  border-color: #bce8f1;
}
.alert-info hr {
  border-top-color: #a6e1ec;
}
.alert-info .alert-link {
  color: #245269;
}
.alert-warning {
  color: #8a6d3b;
  background-color: #fcf8e3;
  border-color: #faebcc;
}
.alert-warning hr {
  border-top-color: #f7e1b5;
}
.alert-warning .alert-link {
  color: #66512c;
}
.alert-danger {
  color: #a94442;
  background-color: #f2dede;
  border-color: #ebccd1;
}
.alert-danger hr {
  border-top-color: #e4b9c0;
}
.alert-danger .alert-link {
  color: #843534;
}
</style>
<form id="add_new_proposal" action="" method="post" class="custom_form" data-parsley-validate novalidate enctype="multipart/form-data" >
	<div class="row"><div class="col-sm-12 display_alert"></div></div>
	<p>
		<label>HUB Code*:<br>
			<span class="">
				<input type="text" name="hub_code" value="" class="" parsley-trigger="change" required >
			</span>
		</label>
	</p>
	<p>
		<label>Project Title*:<br>
			<span class="">
				<input type="text" name="project_title" value="" class="" parsley-trigger="change" required >
			</span>
		</label>
	</p>
	<?php /*<p>
		<label>Thematic areas of work*:<br>
			<span class="">
				<select class="form-dropdown select2" id="thematic_areas_work_id" name="thematic_areas_work_id" parsley-trigger="change" required >
					<option value=""> - Select Thematic area - </option>
					<?php
						$table_name = $wpdb->prefix . "thematic_areas_work";
						$res = $wpdb->get_results("select * from $table_name where isDelete=0"); 
						foreach($res as $row){
							echo '<option value="'.$row->id.'">'.$row->thematic_area.'</option>';
						}
					?>
				</select>
			</span>
		</label>
	</p> */ ?>
	<p>
		<label>Locations of work: ( State / District / City )*<br>
			<span class="">
				<input type="hidden" name="work_location" id="work_locations"  parsley-trigger="change" required >
				<ul class="parsley-errors-list filled tag_required"  style="display: none;"><li class="parsley-required">This value is required.</li></ul>
			</span>
		</label>
	</p>
	<p>
		<label>Thematic areas of work*<br>
			<span class="">
				<input type="hidden" name="thematic_areas_works" id="thematic_areas_works"  parsley-trigger="change" required >
				<ul class="parsley-errors-list filled tag_required_thematic_areas"  style="display: none;"><li class="parsley-required">This value is required.</li></ul>
			</span>
		</label>
	</p>
	<p>
		<label>Tags<br>
			<span class="">
				<input type="hidden" name="tags" id="tags"  parsley-trigger="change" required >
				<ul class="parsley-errors-list filled tag_required"  style="display: none;"><li class="parsley-required">This value is required.</li></ul>
			</span>
		</label>
	</p>
	<p>
		<label>Project Summary*:<br>
			<span class=""><textarea name="project_summary" rows="4" class="" parsley-trigger="change" required ></textarea></span> 
		</label>
	</p>
	<p>
		<label>Upload application form*:<br>
			<span class="">
				<input type="file" name="application_form_name" value="" class="" parsley-trigger="change" required >
			</span>
		</label>
	</p>
	<p>
		<label>Upload payment receipt*:<br>
			<span class="">
				<input type="file" name="payment_receipt_name" value="" class="" parsley-trigger="change" required >
			</span>
		</label>
	</p>
	<p>
		<input type="submit" value="Submit" class="gem-button gem-button-size-small gem-button-style-outline gem-button-text-weight-normal gem-button-border-2 submit_btn"><span class="ajax-loader"></span>
		<span class="loading_div" style="display: none;"><img src="<?php echo plugins_url(); ?>/csr-bank/img/loading.gif" alt="Loading..." width="25" /> <span>Please Wait...</span></span>
	</p>
</form>
<script type="text/javascript">
	$('.tag_required').hide();
	$('.tag_required_thematic_areas').hide();
	var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
	jQuery(function() {
		jQuery('.select2').select2();
		tag_load();
		
		jQuery(document).on('click', '.alert-dismissable', function () {
			jQuery('.alert-dismissable').hide();
		});
		
		jQuery(document).on('submit', '#add_new_proposal', function () {
            var tags = $('#tags').val();
            if(tags == ''){
                jQuery('.tag_required').show();
                return false;
            }
			var work_locations = $('#work_locations').val();
            if(work_locations == ''){
                jQuery('.tag_required').show();
                return false;
            }
			var thematic_areas_works = $('#thematic_areas_works').val();
            if(thematic_areas_works == ''){
                jQuery('.tag_required_thematic_areas').show();
                return false;
            }
            //jQuery('.submit_btn').attr('disabled','disabled');
            jQuery('.loading_div').show();
            var postData = new FormData(this);
            postData.append('action', 'add_new_proposal');
            $.ajax({
                url: ajaxurl,
				type: "POST",
                processData: false,
                contentType: false,
                cache: false,
                data: postData,
                success: function (response) {
					jQuery('.loading_div').hide();
					var json = $.parseJSON(response);
                    if (json['error'] == 'Exist') {
                        jQuery('.display_alert').html('<div class="alert alert-danger alert-dismissable"><strong>Error! </strong> Proposal Already Exist.</div>');
                    }
                    if (json['error'] == 'Rejected_hub_code') {
                        jQuery('.display_alert').html('<div class="alert alert-danger alert-dismissable"><strong>Error! </strong> Below hub code is Rejected. Please enter the valid hubcode. </div>');
                    }
                    if (json['error'] == 'Wrong_hub_code') {
                        jQuery('.display_alert').html('<div class="alert alert-danger alert-dismissable"><strong>Error! </strong> Below hub code is incorrect. Please enter the valid hubcode. </div>');
                    }
                    if (json['error'] == 'FileType') {
                        jQuery('.display_alert').html('<div class="alert alert-danger alert-dismissable"><strong>Error! </strong> Only allow pdf,graphics and zip file upload.</div>');
                    }
                    if (json['error'] == 'FileSize') {
                        jQuery('.display_alert').html('<div class="alert alert-danger alert-dismissable"><strong>Error! </strong> Max file upload size limit 15MB.</div>');
                    }
                    if (json['success'] == 'Added') {
                        jQuery('.display_alert').html('<div class="alert alert-success alert-dismissable"><strong>Success! </strong> Proposal Added.</div>');
                        jQuery('#add_new_proposal')[0].reset();
                        jQuery('.select2-search-choice-close').click();
                        jQuery('#select2-chosen-1').html(' - Select Thematic area - ');
                    }
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                    $('.tag_required').hide();
                    //$('.submit_btn').removeAttr('disabled','disabled');
                    tag_load();
                    return false;
                },
            });
            return false;
        });
		
	});
	
	function tag_load(){
		
		jQuery.ajax({
            url: ajaxurl,
			dataType : "JSON",
			data : {action: "get_work_locations"},
            success: function(result){
				$("#work_locations").select2({
                    createSearchChoice:function(term, data) { if ($(data).filter(function() { return this.text.localeCompare(term)===0; }).length===0) {return {id:term, text:term};} },
                    multiple: true,
                    //maximumSelectionSize: 1,
                    data: result,
                });
                function log(e) {
                    var e=$("<li>"+e+"</li>");
                    $("#events_11").append(e);
                    e.animate({opacity:1}, 10000, 'linear', function() { e.animate({opacity:0}, 2000, 'linear', function() {e.remove(); }); });
                }
                $("#work_locations")
                    .on("change", function(e) { log("change "+JSON.stringify({val:e.val, added:e.added, removed:e.removed})); })
                    .on("select2-opening", function() { log("opening"); })
                    .on("select2-open", function() { log("open"); })
                    .on("select2-close", function() { log("close"); })
                    .on("select2-highlight", function(e) { log ("highlighted val="+ e.val+" choice="+ JSON.stringify(e.choice));})
                    .on("select2-selecting", function(e) { log ("selecting val="+ e.val+" choice="+ JSON.stringify(e.choice));})
                    .on("select2-removing", function(e) { log ("removing val="+ e.val+" choice="+ JSON.stringify(e.choice));})
                    .on("select2-removed", function(e) { log ("removed val="+ e.val+" choice="+ JSON.stringify(e.choice));})
                    .on("select2-loaded", function(e) { log ("loaded (data property omitted for brevity)");})
                    .on("select2-focus", function(e) { log ("focus");})
                    .on("select2-blur", function(e) { log ("blur");});
            }
        });
    
		jQuery.ajax({
            url: ajaxurl,
			dataType : "JSON",
			data : {action: "get_p_tags"},
            success: function(result){
				$("#tags").select2({
                    createSearchChoice:function(term, data) { if ($(data).filter(function() { return this.text.localeCompare(term)===0; }).length===0) {return {id:term, text:term};} },
                    multiple: true,
                    //maximumSelectionSize: 1,
                    data: result,
                });
                function log(e) {
                    var e=$("<li>"+e+"</li>");
                    $("#events_11").append(e);
                    e.animate({opacity:1}, 10000, 'linear', function() { e.animate({opacity:0}, 2000, 'linear', function() {e.remove(); }); });
                }
                $("#tags")
                    .on("change", function(e) { log("change "+JSON.stringify({val:e.val, added:e.added, removed:e.removed})); })
                    .on("select2-opening", function() { log("opening"); })
                    .on("select2-open", function() { log("open"); })
                    .on("select2-close", function() { log("close"); })
                    .on("select2-highlight", function(e) { log ("highlighted val="+ e.val+" choice="+ JSON.stringify(e.choice));})
                    .on("select2-selecting", function(e) { log ("selecting val="+ e.val+" choice="+ JSON.stringify(e.choice));})
                    .on("select2-removing", function(e) { log ("removing val="+ e.val+" choice="+ JSON.stringify(e.choice));})
                    .on("select2-removed", function(e) { log ("removed val="+ e.val+" choice="+ JSON.stringify(e.choice));})
                    .on("select2-loaded", function(e) { log ("loaded (data property omitted for brevity)");})
                    .on("select2-focus", function(e) { log ("focus");})
                    .on("select2-blur", function(e) { log ("blur");});
            }
        });
		    
		jQuery.ajax({
            url: ajaxurl,
			dataType : "JSON",
			data : {action: "get_thematic_areas_works"},
            success: function(result){
				$("#thematic_areas_works").select2({
                    //createSearchChoice:function(term, data) { if ($(data).filter(function() { return this.text.localeCompare(term)===0; }).length===0) {return {id:term, text:term};} },
                    multiple: true,
                    //maximumSelectionSize: 1,
                    data: result,
                });
                function log(e) {
                    var e=$("<li>"+e+"</li>");
                    $("#events_11").append(e);
                    e.animate({opacity:1}, 10000, 'linear', function() { e.animate({opacity:0}, 2000, 'linear', function() {e.remove(); }); });
                }
                $("#thematic_areas_works")
                    .on("change", function(e) { log("change "+JSON.stringify({val:e.val, added:e.added, removed:e.removed})); })
                    .on("select2-opening", function() { log("opening"); })
                    .on("select2-open", function() { log("open"); })
                    .on("select2-close", function() { log("close"); })
                    .on("select2-highlight", function(e) { log ("highlighted val="+ e.val+" choice="+ JSON.stringify(e.choice));})
                    .on("select2-selecting", function(e) { log ("selecting val="+ e.val+" choice="+ JSON.stringify(e.choice));})
                    .on("select2-removing", function(e) { log ("removing val="+ e.val+" choice="+ JSON.stringify(e.choice));})
                    .on("select2-removed", function(e) { log ("removed val="+ e.val+" choice="+ JSON.stringify(e.choice));})
                    .on("select2-loaded", function(e) { log ("loaded (data property omitted for brevity)");})
                    .on("select2-focus", function(e) { log ("focus");})
                    .on("select2-blur", function(e) { log ("blur");});
            }
        });
    
    }
    
</script>
