<?php 
	global $wpdb; 
?>
<link href="<?php echo plugins_url(); ?>/csr-bank/css/csr_bank_custom.css" rel="stylesheet" type="text/css" />
<link href="<?php echo plugins_url(); ?>/select2/select2.css" rel="stylesheet" type="text/css" />
<script src="<?php echo plugins_url(); ?>/select2/select2.min.js" type="text/javascript"></script>
<style>
	input[type=text], input[type=number], #s2id_autogen1, .select2-container{
		width: 50% !important;
	}
	.multiple_contact_entries input[type=text], .multiple_contact_entries input[type=number]{
		width: 100% !important;
	}
</style>
<div class="wrap">
	<h2 class="hndle ui-sortable-handle" style="padding: 5px 10px; margin: 0; "><span>Add New Corporate</span></h2>
	<div class="display_alert">
		<?php 
			if(isset($_SESSION['corporate_added'])){
				echo '<div id="message" class="updated notice notice-success is-dismissible"><p>Corporate Added.</p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div>';
				unset($_SESSION['corporate_added']);
			}
		?>
	</div>
	<div class="postbox" style="padding: 10px;">
		<form id="add_new_corporate" action="" method="post" enctype="multipart/form-data" >
			<table>
				<tr>
					<td><label for="organisation_name">Name of the organisation*:</label></td>
					<td><input type="text" name="organisation_name" id="organisation_name" value="" class="" required ></td>
				</tr>
				<tr>
					<td><label for="organisation_name">Type of Corporate*:</label></td>
					<td>
						<select class="form-dropdown select2" id="corporate_type_id" name="corporate_type_id" required >
							<?php
								$table_name = $wpdb->prefix . "corporate_type";
								$res = $wpdb->get_results("select * from $table_name where isDelete=0"); 
								foreach($res as $row){
									echo '<option value="'.$row->id.'">'.$row->type_name.'</option>';
								}
							?>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="address1">Address 1*:</label></td>
					<td><input type="text" name="address1" id="address1" value="" class="" required ></td>
				</tr>
				<tr>
					<td><label for="address2">Address 2*:</label></td>
					<td><input type="text" name="address2" id="address2" value="" class="" required ></td>
				</tr>
				<tr>
					<td><label for="zip_code">Zip Code*:</label></td>
					<td><input type="number" name="zip_code" id="zip_code" value="" class="" required ></td>
				</tr>
				<tr>
					<td><label for="work_location">Locations of work: ( State / District / City )*:</label></td>
					<td>
						<input type="hidden" name="work_location" id="work_locations" required >
						<ul class="parsley-errors-list filled tag_required"  style="display: none;"><li class="parsley-required">This value is required.</li></ul>
					</td>
				</tr>
				<tr>
					<td><label for="company_logo">Company Logo*:</label></td>
					<td><input type="file" name="company_logo" id="company_logo" class="" required ></td>
				</tr>
				<tr>
					<td style="padding-top: 20px; position: absolute;"><label for="">Multiple contact entries*:</label></td>
					<td>
						<table id="list" style="" class="horizontal multiple_contact_entries">
						   <thead>
							  <tr>
								 <th>Name</th>
								 <th>Designation</th>
								 <th>Email Id</th>
								 <th>Phone Number</th>
								 <th width="50">Main contact person</th>
							  </tr>
							</thead>
							<tbody id="addmore_contact">
							  <tr class="corporat_contact">
								 <td><input type="text" name="contact_name[0]" required placeholder="Enter Name"></td>
								 <td><input type="text" name="contact_desc[0]" required placeholder="Enter Designation"></td>
								 <td><input type="email" name="contact_email_id[0]" required placeholder="Enter Email id"></td>
								 <td><input type="number" name="contact_phone_number[0]" required placeholder="Enter Mob No." data-parsley-minlength="10" data-parsley-maxlength="10"></td>
								 <td width="50">
									<input type="checkbox" name="main_person[0]" id="main_person" value="1">
									<div class="remove_contact" style="visibility: hidden; float: right; margin: -22px 0px 0px 20px;"><span class="button button-medium" style="background: #e8e8e8;" title="Remove Contact">x</span></div>
								 </td>
							  </tr>
						   </tbody>
						</table>
						<div><span class="button button-primary button-medium addnew" style="float: right; margin-right: 3px;" title="Add Contact"> + </span></div>
					</td>
				</tr>
				<tr>
					<td></td>
					<td><hr />
						<input type="submit" value="Save" class="button button-primary button-large submit_btn">
					</td>
				</tr>
			</table>
		</form>
	</div>
</div>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/js/parsleyjs/src/parsley.css">
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/parsleyjs/dist/parsley.min.js"></script>
<script>
  jQuery(document).ready(function () {
    jQuery('form').parsley();
  });
</script>
<script type="text/javascript">
	jQuery('.tag_required').hide();
	var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
	jQuery('.select2').select2();
	tag_load();
    jQuery(document).ready(function () {
		jQuery(document).on('change', 'input[type=checkbox]', function() {
			jQuery('input[type=checkbox]:checked').not(this).prop('checked', false);
		});
		jQuery(document).on('click', '.remove_contact', function () {
            jQuery(this).closest('tr.corporat_contact').remove();
        });
		jQuery(document).on('click', '.addnew', function () {
			var section_index = jQuery('tr.corporat_contact').length;
			var newDiv = jQuery(".corporat_contact:first").clone()
				.find(':input')
				.each(function () {
					this.name = this.name.replace(/\[(\d+)\]/, function (str) {
						return '[' + section_index + ']';
					});
					console.log(this.name);
				})
				.end()
				.appendTo("#addmore_contact");
			newDiv.find('.remove_contact').css('visibility', 'visible');
			newDiv.find(':input[type=checkbox]').prop('checked', false);
		});
		
		jQuery(document).on('click', '.is-dismissible', function () {
			jQuery('.is-dismissible').hide();
		});
		
		jQuery(document).on('submit', '#add_new_corporate', function () {
            var work_locations = jQuery('#work_locations').val();
            if(work_locations == ''){
                jQuery('.tag_required').show();
                return false;
            }
            var postData = new FormData(this);
            postData.append('action', 'add_new_corporate');
            jQuery.ajax({
                url: ajaxurl,
				type: "POST",
                processData: false,
                contentType: false,
                cache: false,
                data: postData,
                success: function (response) {
					var json = jQuery.parseJSON(response);
                    
					if (json['error'] == 'Exist') {
                        jQuery('.display_alert').html('<div id="message" class="error notice notice-success is-dismissible"><p>Corporate Already Exist.</p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div>');
                    }
					
					if (json['error'] == 'CotnactExist') {
                        jQuery('.display_alert').html('<div id="message" class="error notice notice-success is-dismissible"><p>Corporate Contact Already Exist.</p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div>');
                    }
					if (json['error'] == 'FileWidthWrong') {
                        jQuery('.display_alert').html('<div id="message" class="error notice notice-success is-dismissible"><p><strong>Error! </strong> Image size less than 100 x 100 pixels.</p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div>');
                    }
                    if (json['success'] == 'Added') {
						location.reload(); return false;
                        jQuery('.display_alert').html('<div id="message" class="updated notice notice-success is-dismissible"><p>Corporate Added.</p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div>');
                        jQuery('#add_new_corporate')[0].reset();
                        jQuery('.select2-search-choice-close').click();
                    }
                    jQuery("html, body").animate({ scrollTop: 0 }, "slow");
                    tag_load();
                    return false;
                },
            });
            return false;
        });
		
	});
	
	function tag_load(){
		jQuery.ajax({
            url: ajaxurl,
			dataType : "JSON",
			data : {action: "get_work_locations"},
            success: function(result){
				jQuery("#work_locations").select2({
                    createSearchChoice:function(term, data) { if (jQuery(data).filter(function() { return this.text.localeCompare(term)===0; }).length===0) {return {id:term, text:term};} },
                    multiple: true,
                    //maximumSelectionSize: 1,
                    data: result,
                });
                function log(e) {
                    var e=jQuery("<li>"+e+"</li>");
                    jQuery("#events_11").append(e);
                    e.animate({opacity:1}, 10000, 'linear', function() { e.animate({opacity:0}, 2000, 'linear', function() {e.remove(); }); });
                }
                jQuery("#work_locations")
                    .on("change", function(e) { log("change "+JSON.stringify({val:e.val, added:e.added, removed:e.removed})); })
                    .on("select2-opening", function() { log("opening"); })
                    .on("select2-open", function() { log("open"); })
                    .on("select2-close", function() { log("close"); })
                    .on("select2-highlight", function(e) { log ("highlighted val="+ e.val+" choice="+ JSON.stringify(e.choice));})
                    .on("select2-selecting", function(e) { log ("selecting val="+ e.val+" choice="+ JSON.stringify(e.choice));})
                    .on("select2-removing", function(e) { log ("removing val="+ e.val+" choice="+ JSON.stringify(e.choice));})
                    .on("select2-removed", function(e) { log ("removed val="+ e.val+" choice="+ JSON.stringify(e.choice));})
                    .on("select2-loaded", function(e) { log ("loaded (data property omitted for brevity)");})
                    .on("select2-focus", function(e) { log ("focus");})
                    .on("select2-blur", function(e) { log ("blur");});
            }
        });
    }
</script>
