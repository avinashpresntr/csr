<?php 
	global $wpdb;
	if(empty($_GET['id']) || !is_numeric($_GET['id'])){
		wp_redirect(admin_url().'admin.php?page=ngo');
	}
	$ngo_id = $_GET['id'];
	$ngo_result = $wpdb->get_results("SELECT * FROM `csr_ngo` WHERE `id` = '" . $ngo_id . "' " ); 
	$ngo_result = $ngo_result[0];
	
	if(empty($ngo_result)){
		wp_redirect(admin_url().'admin.php?page=ngo');
	}
	
	$wlids_arr = array();
	$work_locations_arr = array();
	$wlids_result = $wpdb->get_results("SELECT `work_locations_id` FROM `csr_ngo_work_locations` WHERE `ngo_id` = '" . $ngo_id . "' ");
	foreach($wlids_result as $wlids_row){
		$wlids_arr[] = $wlids_row->work_locations_id;
	}
	$wlids = implode(', ', $wlids_arr);
	$work_locations_result = $wpdb->get_results("SELECT * FROM `csr_work_locations` WHERE `id` IN ( ".$wlids." ) " ); 
	foreach($work_locations_result as $work_locations_row){
		$work_locations_arr[$work_locations_row->id] = $work_locations_row->location_name;
	}
	
	// thematic_area selection box
	$tlids_arr = array();
	$thematic_areas_works_arr = array();
	$tlids_result = $wpdb->get_results("SELECT `thematic_areas_id` FROM `csr_ngo_thematic_areas_works` WHERE `ngo_id` = '" . $ngo_id . "' ");
	foreach($tlids_result as $tlids_row){
		$tlids_arr[] = $tlids_row->thematic_areas_id;
	}
	$tlids = implode(', ', $tlids_arr);
	$thematic_areas_work_result = $wpdb->get_results("SELECT * FROM `csr_thematic_areas_work` WHERE `id` IN ( ".$tlids." ) " ); 
	foreach($thematic_areas_work_result as $thematic_row){
		$thematic_areas_works_arr[$thematic_row->id] = str_replace(',', ' ', $thematic_row->thematic_area);
	}
?>
<link href="<?php echo plugins_url(); ?>/select2/select2.css"" rel="stylesheet" type="text/css" />
<script src="<?php echo plugins_url(); ?>/select2/select2.min.js" type="text/javascript"></script>
<style>
	input[type=text], input[type=number], input[type=email], #postal_address, #s2id_autogen1, .select2-container{
		width: 100% !important;
	}
	.level_status{
		padding-bottom: 5px;
	}
	.level_status label{
		line-height: 30px;
	}
	.level_status input[type=button].level_accept{
		background-color: #00a65a;
		color: #fff;
		width: 100px;
	}
	.level_status input[type=button].level_accepted{
		background-color: #dff0d8;
		color: #000;
		cursor: auto;
		width: 100px;
	}
	.level_status input[type=button].level_reject{
		background: #dd4b39;
		color: #fff;
		width: 100px;
	}
	.level_status input[type=button].level_rejected{
		background: #f2dede;
		color: #000;
		cursor: auto;
		width: 100px;
	}
	table.dataTable tbody td {
		border-bottom: 1px solid #e1e1e1;
	}
	
	.label {
		display: inline;
		padding: .2em .6em .3em;
		font-size: 75%;
		font-weight: 700;
		line-height: 1;
		color: #fff;
		text-align: center;
		white-space: nowrap;
		vertical-align: baseline;
		border-radius: .25em;
	}
	.alert-success,
	.alert-danger,
	.alert-error,
	.alert-warning,
	.alert-info,
	.label-danger,
	.label-info,
	.label-warning,
	.label-primary,
	.label-success {
	  color: #fff !important;
	}
	.alert-danger,
	.alert-error,
	.label-danger {
	  background-color: #dd4b39 !important;
	}
	.alert-warning,
	.label-warning {
	  background-color: #f39c12 !important;
	}
	.alert-info,
	.label-info {
	  background-color: #00c0ef !important;
	}
	.label-primary {
	  background-color: #3c8dbc !important;
	}
	.alert-success,
	.label-success {
	  background-color: #00a65a !important;
	}
	.text-danger{
		color: #dd4b39 !important;
	}
</style>
<div class="wrap">
	<h2 class="hndle ui-sortable-handle" style="padding: 5px 10px; margin: 0; "><a href="?page=ngo" class="button button-primary button-medium"> Back </a> &nbsp;<span>NGO Details</span></h2>
	<div class="display_alert">
		<?php 
			if(isset($_SESSION['ngo_updated'])){
				echo '<div id="message" class="updated notice notice-success is-dismissible"><p>NGO Updated.</p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div>';
				unset($_SESSION['ngo_updated']);
			}
			if(isset($_SESSION['level_status_changed'])){
				echo '<div id="message" class="updated notice notice-success is-dismissible"><p>'.$_SESSION['level_status_changed'].'.</p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div>';
				unset($_SESSION['level_status_changed']);
			}
		?>
	</div>
	<div class="postbox" style="padding: 10px;">
		<form id="update_ngo" action="" method="post" enctype="multipart/form-data" >
			<input type="hidden" name="ngo_id" id="ngo_id" value="<?php echo $ngo_result->id; ?>">
			<table width="70%">
				<tr>
					<td><label for="organisation_name">Name of the organisation*:</label></td>
					<td><input type="text" name="organisation_name" id="organisation_name" value="<?php echo $ngo_result->organisation_name; ?>" data-parsley-pattern="^[A-Za-z ]*$" class="" required ></td>
				</tr>
				<tr>
					<td><label for="operation_scale_id">Scale of Operation*:</label></td>
					<td>
						<select class="form-dropdown select2" id="operation_scale_id" name="operation_scale_id" required >
							<option value=""> - Select scale of Operation - </option>
							<?php
								$table_name = $wpdb->prefix . "operation_scale";
								$res = $wpdb->get_results("select * from $table_name where isDelete=0"); 
								foreach($res as $row){
									echo '<option value="'.$row->id.'" ';
									if( $row->id == $ngo_result->operation_scale_id ) { echo 'Selected'; }
									echo ' >'.$row->operation_scale.'</option>';
								}
							?>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="organisation_type_id">Type of Organisation*:</label></td>
					<td>
						<select class="form-dropdown select2" id="organisation_type_id" name="organisation_type_id" required >
							<option value=""> - Select type of Organisation - </option>
							<?php
								$table_name = $wpdb->prefix . "organisation_type";
								$res = $wpdb->get_results("select * from $table_name where isDelete=0"); 
								foreach($res as $row){
									echo '<option value="'.$row->id.'" ';
									if( $row->id == $ngo_result->organisation_type_id ) { echo 'Selected'; }
									echo ' >'.$row->type_name.'</option>';
								}
							?>
						</select>
					</td>
				</tr>
				<?php /* <tr>
					<td><label for="thematic_areas_work_id">Thematic areas of work*:</label></td>
					<td>
						<select class="form-dropdown select2" id="thematic_areas_work_id" name="thematic_areas_work_id" required >
							<option value=""> - Select Thematic area - </option>
							<?php
								$table_name = $wpdb->prefix . "thematic_areas_work";
								$res = $wpdb->get_results("select * from $table_name where isDelete=0"); 
								foreach($res as $row){
									echo '<option value="'.$row->id.'" ';
									if( $row->id == $ngo_result->thematic_areas_work_id ) { echo 'Selected'; }
									echo ' >'.$row->thematic_area.'</option>';
								}
							?>
						</select>
					</td>
				</tr> */ ?>
				<tr>
					<td><label for="thematic_areas_works">Thematic areas of work*:</label></td>
					<td>
						<input type="hidden" name="thematic_areas_works" id="thematic_areas_works" required >
						<ul class="parsley-errors-list filled tag_required_thematic_areas"  style="color: #f00;"><li class="parsley-required">This value is required.</li></ul>
					</td>
				</tr>
				<tr>
					<td><label for="sub_thematic_area">Sub Thematic areas:</label></td>
					<td><input type="text" name="sub_thematic_area" id="sub_thematic_area" value="<?php echo $ngo_result->sub_thematic_area; ?>" ><br /><small>Add multiple value by comma (,) separated.</small></td>
				</tr>
				<tr>
					<td><label for="work_location">Locations of work: ( State / District / City )*:</label></td>
					<td>
						<input type="hidden" name="work_location" id="work_locations" required >
						<ul class="parsley-errors-list filled tag_required"  style="color: #f00;"><li class="parsley-required">This value is required.</li></ul>
					</td>
				</tr>
				<tr>
					<td><label for="postal_address">Postal Address*:</label></td>
					<td><textarea name="postal_address" id="postal_address" rows="4" class="" required ><?php echo $ngo_result->postal_address; ?></textarea></td>
				</tr>
				<tr>
					<td><label for="status">Status:</label></td>
					<td>
						<?php
							if( $ngo_result->ngo_status == 'Empanelment Live - Applied for Renewal' ) { 
								echo '<div><span class="label label-danger">Empanelment Live - Applied for Renewal</span></div>';
							}
							if($ngo_result->ngo_status == 'Expired - Applied for Renewal'){
								echo '<div><span class="label label-danger">Expired - Applied for Renewal</span></div>';
							}
						?>
						<br />
						<select class="form-dropdown select2" id="ngo_status" name="ngo_status" >
							<option value=""> - Select Status - </option>
							<option value="Rejected" <?php if( $ngo_result->ngo_status == 'Rejected' ) { echo 'Selected'; } ?> > Rejected </option>
							<option value="Applicant" <?php if( $ngo_result->ngo_status == 'Applicant' ) { echo 'Selected'; } ?> > Applicant </option>
							<option value="Empanelled" <?php if( $ngo_result->ngo_status == 'Empanelled' ) { echo 'Selected'; } ?> > Empanelled </option>
							<option value="expired_renewal_pending" <?php if( $ngo_result->ngo_status == 'expired_renewal_pending' ) { echo 'Selected'; } ?> > Expired – Renewal Pending </option>
							<option value="Field_Visit" <?php if( $ngo_result->ngo_status == 'Field_Visit' ) { echo 'Selected'; } ?> > Field Visit </option>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="hub_code">HUB Code:</label></td>
					<td>
						<input type="text" name="hub_code" id="hub_code" value="<?php echo $ngo_result->hub_code; ?>" class="" />
						<input type="button" name="generate_hub_code_btn" id="generate_hub_code_btn" value="Generate HUB Code" />
						<span class="generate_loading" style="display: none;"><img src="<?php echo plugins_url(); ?>/csr-bank/img/loading.gif" alt="Loading..." width="25" /></span>
					</td>
				</tr>
				<tr>
					<td><label for="empanellment_certificate">Empanellment certificate:</label></td>
					<td>
						<input type="file" name="empanellment_certificate" id="empanellment_certificate" class="" >
						<?php $filepath = home_url()."/wp-content/uploads/ngo_files/"; ?>
						<?php if(!empty($ngo_result->empanellment_certificate) && $ngo_result->ngo_status == 'Empanelled'){ ?>

								<a href="<?php echo $filepath.'empanellment_certificate/'.$ngo_result->empanellment_certificate; ?>" download class="button" ><i class="fa fa-download"></i> Download </a> 

						<?php  } else { echo '<span style="border: 1px solid #999; padding: 5px; width:100px; ">N/A</span>'; } ?>
					</td>
				</tr>
				<tr>
					<td><label for="empanelled_start_date">Empanelled Start Date:</label></td>
					<td><input type="text" name="empanelled_start_date" id="empanelled_start_date" class="datepicker" value="<?php if($ngo_result->empanelled_start_date != '0000-00-00 00:00:00'){ echo substr($ngo_result->empanelled_start_date, 8, 2).'/'.substr($ngo_result->empanelled_start_date, 5, 2).'/'.substr($ngo_result->empanelled_start_date, 0, 4); } ?>" ></td>
				</tr>
				<tr>
					<td><label for="empanelled_end_date">Empanelled End Date:</label></td>
					<td>
						<input type="text" name="empanelled_end_date" id="empanelled_end_date" class="datepicker" 
							value="<?php if($ngo_result->empanelled_end_date != '0000-00-00 00:00:00'){ echo substr($ngo_result->empanelled_end_date, 8, 2).'/'.substr($ngo_result->empanelled_end_date, 5, 2).'/'.substr($ngo_result->empanelled_end_date, 0, 4); } ?>" >
					</td>
				</tr>
				<tr>
					<td colspan="2"><h5>Contact Details</h5></td>
				</tr>
				<tr>
					<td><label for="email_id">Contact person name*:</label></td>
					<td>
						<input type="text" name="person_firstname" value="<?php echo $ngo_result->person_firstname; ?>" class="" placeholder="First Name" data-parsley-pattern="^[A-Za-z ]*$" required ><br />
						<input type="text" name="person_lastname" value="<?php echo $ngo_result->person_lastname; ?>" class="" placeholder="Last Name" data-parsley-pattern="^[A-Za-z ]*$" required >
					</td>
				</tr>
				<tr>
					<td><label for="email_id">Email Id*:</label></td>
					<td><input type="email" name="email_id" id="email_id" value="<?php echo $ngo_result->email_id; ?>" class="" required ></td>
				</tr>
				<tr>
					<td><label for="email_id">Pan Card Number*:</label></td>
					<td><input type="text" name="pan_no" id="pan_no" value="<?php echo $ngo_result->pan_no; ?>" class="" required ></td>
				</tr>
				<tr>
					<td><label for="phone_number">Phone Number*:</label></td>
					<td><input type="text" name="phone_number" id="phone_number" value="<?php echo $ngo_result->phone_number; ?>" class="" placeholder="Ex.(0123)456789" data-parsley-maxlength="15" ><br /><small>Ex.(0123)456789</small></td>
				</tr>
				<tr>
					<td><label for="mobile_number">Mobile Number*:</label></td>
					<td><input type="number" name="mobile_number" id="mobile_number" value="<?php echo $ngo_result->mobile_number; ?>" class="" data-parsley-minlength="10" data-parsley-maxlength="10" ></td>
				</tr>
				<tr>
					<td><label for="analysis_report">Analysis Report:</label></td>
					<td>
						<input type="file" name="analysis_report" id="analysis_report" class="" >
						<?php $filepath = home_url()."/wp-content/uploads/ngo_files/"; ?>
						<?php if(!empty($ngo_result->analysis_report)){ ?>
								<a href="<?php echo $filepath.'analysis_report/'.$ngo_result->analysis_report; ?>" download class="button" ><i class="fa fa-download"></i> Download </a> 
						<?php  } else { echo '<span style="border: 1px solid #999; padding: 5px; width:100px; ">N/A</span>'; } ?>
					</td>
				</tr>
				<tr>
					<td><label for="score">Score:</label></td>
					<td>
						<input type="number" name="score_total" id="score_total" value="<?php echo $ngo_result->score_total; ?>" class="" style="width:100px !important;" > /
						<input type="number" name="score_received" id="score_received" value="<?php echo $ngo_result->score_received; ?>" class="" style="width:100px !important;" >
					</td>
				</tr>
				<tr>
					<td><label for="rating">Rating:</label></td>
					<td>
						<select class="form-dropdown select2" id="rating" name="rating" >
							<option value=""> - Select Rating - </option>
							<option value="Poor" <?php if( $ngo_result->rating == 'Poor' ) { echo 'Selected'; } ?> > Poor </option>
							<option value="Average" <?php if( $ngo_result->rating == 'Average' ) { echo 'Selected'; } ?> > Average </option>
							<option value="Good" <?php if( $ngo_result->rating == 'Good' ) { echo 'Selected'; } ?> > Good </option>
							<option value="Very Good" <?php if( $ngo_result->rating == 'Very Good' ) { echo 'Selected'; } ?> > Very Good </option>
						</select>
					</td>
				</tr>
				<tr>
					<td></td>
					<td><hr />
						<input type="submit" value="Update" class="button button-primary button-large submit_btn">
						<span class="loading_div" style="display: none;"><img src="<?php echo plugins_url(); ?>/csr-bank/img/loading.gif" alt="Loading..." width="25" /> <span>Please Wait...</span></span>
					</td>
				</tr>
				<tr>
					<td colspan="2"><hr /><label for="level_status"><strong>Scrutiny Level Status:</strong></label></td>
				<tr>
					<td colspan="2">
						<div class="level_status">
							<label for="l1">L1:</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	
							<input type="button" name="l1_accepted" id="l1_accepted" data-level="l1_status" <?php if( $ngo_result->l1_status == 'Accepted' ) { echo 'value="Accepted" class="button level_accepted" '; } else { echo 'data-toggle="modal" data-target="#lavelModal" value="Accept" class="button level_accept" '; } ?> >&nbsp;&nbsp;
							<input type="button" name="l1_rejected" id="l1_rejected" data-level="l1_status" <?php if( $ngo_result->l1_status == 'Rejected' ) { echo 'value="Rejected" class="button level_rejected" '; } else { echo 'data-toggle="modal" data-target="#lavelModal" value="Reject" class="button level_reject" '; } ?> >&nbsp;&nbsp;&nbsp;&nbsp;
							<input type="button" name="l1_reset" id="l1_reset" data-level="L1 Status" data-ls-field="l1_status" data-ln-field="l1_note" value="Reset" class="button" >
						</div>
						<div class="level_status">
							<label for="l2">L2:</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	
							<input type="button" name="l2_accepted" id="l2_accepted" data-level="l2_status" <?php if( $ngo_result->l2_status == 'Accepted' ) { echo 'value="Accepted" class="button level_accepted" '; } else { echo 'data-toggle="modal" data-target="#lavelModal" value="Accept" class="button level_accept" '; } ?> >&nbsp;&nbsp;
							<input type="button" name="l2_rejected" id="l2_rejected" data-level="l2_status" <?php if( $ngo_result->l2_status == 'Rejected' ) { echo 'value="Rejected" class="button level_rejected" '; } else { echo 'data-toggle="modal" data-target="#lavelModal" value="Reject" class="button level_reject" '; } ?> >&nbsp;&nbsp;&nbsp;&nbsp;
							<input type="button" name="l2_reset" id="l2_reset" data-level="L2 Status" data-ls-field="l2_status" data-ln-field="l2_note" value="Reset" class="button" >
						</div>
						<div class="level_status">
							<label for="l3">L3:</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	
							<input type="button" name="l3_accepted" id="l3_accepted" data-level="l3_status" <?php if( $ngo_result->l3_status == 'Accepted' ) { echo 'value="Accepted" class="button level_accepted" '; } else { echo 'data-toggle="modal" data-target="#lavelModal" value="Accept" class="button level_accept" '; } ?> >&nbsp;&nbsp;
							<input type="button" name="l3_rejected" id="l3_rejected" data-level="l3_status" <?php if( $ngo_result->l3_status == 'Rejected' ) { echo 'value="Rejected" class="button level_rejected" '; } else { echo 'data-toggle="modal" data-target="#lavelModal" value="Reject" class="button level_reject" '; } ?> >&nbsp;&nbsp;&nbsp;&nbsp;
							<input type="button" name="l3_reset" id="l3_reset" data-level="L3 Status" data-ls-field="l3_status" data-ln-field="l3_note" value="Reset" class="button" >
						</div>
						<div class="level_status">
							<label for="l4">L4:</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	
							<input type="button" name="l4_accepted" id="l4_accepted" data-level="l4_status" <?php if( $ngo_result->l4_status == 'Accepted' ) { echo 'value="Accepted" class="button level_accepted" '; } else { echo 'data-toggle="modal" data-target="#lavelModal" value="Accept" class="button level_accept" '; } ?> >&nbsp;&nbsp;
							<input type="button" name="l4_rejected" id="l4_rejected" data-level="l4_status" <?php if( $ngo_result->l4_status == 'Rejected' ) { echo 'value="Rejected" class="button level_rejected" '; } else { echo 'data-toggle="modal" data-target="#lavelModal" value="Reject" class="button level_reject" '; } ?> >&nbsp;&nbsp;&nbsp;&nbsp;
							<input type="button" name="l4_reset" id="l4_reset" data-level="L4 Status" data-ls-field="l4_status" data-ln-field="l4_note" value="Reset" class="button" >
						</div>
						<div class="level_status">
							<label for="l5">L5:</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	
							<input type="button" name="l5_accepted" id="l5_accepted" data-level="l5_status" <?php if( $ngo_result->l5_status == 'Accepted' ) { echo 'value="Accepted" class="button level_accepted" '; } else { echo 'data-toggle="modal" data-target="#lavelModal" value="Accept" class="button level_accept" '; } ?> >&nbsp;&nbsp;
							<input type="button" name="l5_rejected" id="l5_rejected" data-level="l5_status" <?php if( $ngo_result->l5_status == 'Rejected' ) { echo 'value="Rejected" class="button level_rejected" '; } else { echo 'data-toggle="modal" data-target="#lavelModal" value="Reject" class="button level_reject" '; } ?> >&nbsp;&nbsp;&nbsp;&nbsp;
							<input type="button" name="l5_reset" id="l5_reset" data-level="L5 Status" data-ls-field="l5_status" data-ln-field="l5_note" value="Reset" class="button" >
						</div>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<label for="notes_history" style="line-height: 30px;" >Note :</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	
						<a href="?page=ngo&action=level_notes_history&id=<?php echo $ngo_id; ?>" class="button button-primary" >Check Notes History</a><hr />
					</td>
				</tr>
			</table>
		</form>
	</div>
	<div class="postbox" style="padding: 10px;">
		<?php $filepath = home_url()."/wp-content/uploads/ngo_files/"; ?>
		<h3>Current NGO File : </h3> <hr />
		<h4>
			Application form : &nbsp;&nbsp;&nbsp;
			<?php if(!empty($ngo_result->application_form_name)){ ?>
				<a href="<?php echo $filepath.$ngo_result->application_form_name; ?>" download class="button button-small" ><i class="fa fa-download"></i> Download </a>
			<?php } else { echo 'N/A'; } ?>
		</h4>
		<h4>
			Payment form : &nbsp;&nbsp;&nbsp;
			<?php if(!empty($ngo_result->payment_receipt_name)){ ?>
				<a href="<?php echo $filepath.$ngo_result->payment_receipt_name; ?>" download class="button button-small" ><i class="fa fa-download"></i> Download </a> 
			<?php } else { echo 'N/A'; } ?>
		</h4> <hr />
	</div>
	<div class="postbox" style="padding: 10px;">
		<h3>All HUB Codes </h3>
		<table id="ngo_detail_table" width="100%" class="widefat dataTable">
			<thead>
				<tr>
					<th>Sr. No.</th>
					<th>HUB Code</th>
					<th>Files</th>
					<th>Score</th>
					<th>Rateing</th>
					<th width="300">Date</th>
				</tr>
			</thead>
			<tbody>
				<?php $inc = 1;
					$ngo_d_res = $wpdb->get_results("SELECT * FROM `csr_ngo_details` WHERE `ngo_id` = '" . $ngo_id . "' ORDER BY `ngo_d_id` DESC " );
					foreach($ngo_d_res as $ngo_d_row){
						$display_date = '<span> Created : ';
						$display_date .= date('d/m/Y H:i:s', strtotime($ngo_d_row->created_date));
						$display_date .= '</span>';
						if($ngo_d_row->empanelled_end_date != '0000-00-00 00:00:00'){
							$display_date .= '<br /><span style="color: #3c763d;"> Empanelled : ';
							$display_date .= date('d/m/Y', strtotime($ngo_d_row->empanelled_start_date)).' - To - ';
							$display_date .= date('d/m/Y', strtotime($ngo_d_row->empanelled_end_date));
							$display_date .= '</span>';
						}
						if($ngo_d_row->rejected_date != '0000-00-00 00:00:00'){
							$display_date .= '<br /><span style="color: #a94442;"> Rejected : ';
							$display_date .= date('d/m/Y H:i:s', strtotime($ngo_d_row->rejected_date));
							$display_date .= '</span>';
						}
					?>
						<tr>
							<td><?php echo $inc; ?></td>
							<td><?php echo $ngo_d_row->hub_code; ?></td>
							<td>
								<h5>
								<div style="margin-bottom: 5px;">
										Application form : &nbsp;&nbsp;&nbsp;
										<?php if(!empty($ngo_d_row->application_form_name)){ ?>
											<a href="<?php echo $filepath.$ngo_d_row->application_form_name; ?>" download class="button button-small" ><i class="fa fa-download"></i> Download </a>
										<?php } else { echo 'N/A'; } ?>
								</div>
								<div style="margin-bottom: 5px;">
									Payment form : &nbsp;&nbsp;&nbsp;
									<?php if(!empty($ngo_d_row->payment_receipt_name)){ ?>
										<a href="<?php echo $filepath.$ngo_d_row->payment_receipt_name; ?>" download class="button button-small" ><i class="fa fa-download"></i> Download </a> 
									<?php } else { echo 'N/A'; } ?>
								</div>
								<div style="margin-bottom: 5px;">
									Empanellment Certificate : &nbsp;&nbsp;&nbsp;
									<?php if(!empty($ngo_d_row->empanellment_certificate)){ ?>
										<a href="<?php echo $filepath.$ngo_d_row->empanellment_certificate; ?>" download class="button button-small" ><i class="fa fa-download"></i> Download </a> 
									<?php } else { echo 'N/A'; } ?>
								</div>
								<div style="margin-bottom: 5px;">
									Analysis Report : &nbsp;&nbsp;&nbsp;
									<?php if(!empty($ngo_d_row->analysis_report)){ ?>
										<a href="<?php echo $filepath.$ngo_d_row->analysis_report; ?>" download class="button button-small" ><i class="fa fa-download"></i> Download </a> 
									<?php } else { echo 'N/A'; } ?>
								</div>
								</h5>
							</td>
							<td><?php echo $ngo_d_row->score_total.' / '.$ngo_d_row->score_received; ?></td>
							<td><?php echo $ngo_d_row->rating; ?></td>
							<td><?php echo $display_date; ?></td>
						</tr>
					<?php
						$inc++;
					}
				?>
			</tbody>
		</table>
	</div>
</div>

<!-- Modal -->
<div id="lavelModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Scrutiny Level Status</h4>
      </div>
      <form id="change_level_status" action="" method="post" enctype="multipart/form-data" >
		  <div class="modal-body level_note_detail">
				<input type="hidden" name="ngo_id" id="ngo_id" value="<?php echo $ngo_result->id; ?>">
				<input type="hidden" name="level_status" id="level_status" value="">
				<input type="hidden" name="level_label" id="level_label" value="">
				<input type="hidden" name="level_btn_id" id="level_btn_id" value="">
				<label class="note_title" >Note : </label>
				<textarea name="level_note" id="level_note" style="width: 100%;" required ></textarea>
		  </div>
		  <div class="modal-footer">
			<button type="submit" class="button button-primary">Save</button>
			<button type="button" class="button" data-dismiss="modal">Close</button>
		  </div>
	  </form>
    </div>

  </div>
</div>

<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/js/parsleyjs/src/parsley.css">
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/parsleyjs/dist/parsley.min.js"></script>
<link href="<?php echo plugins_url(); ?>/datepicker/datepicker.css"" rel="stylesheet" type="text/css" />
<script src="<?php echo plugins_url(); ?>/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="<?php echo plugins_url(); ?>/datatables/jquery.dataTables.min.css" />
<script type="text/javascript" src="<?php echo plugins_url(); ?>/datatables/jquery.dataTables.min.js"></script>
<!-- Modal popup -->
<link rel="stylesheet" type="text/css" href="<?php echo plugins_url(); ?>/csr-bank/css/modal.css" />
<script type="text/javascript" src="<?php echo plugins_url(); ?>/csr-bank/js/modal.js"></script>
<script>
  jQuery(document).ready(function () {
    jQuery('form').parsley();
    jQuery('.datepicker').datepicker({
		dateFormat: 'dd/mm/yy',
	});
  });
</script>
<script type="text/javascript">
	jQuery('.tag_required').hide();
	jQuery('.tag_required_thematic_areas').hide();
	var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
	jQuery('.select2').select2();
	tag_load();
    jQuery(document).ready(function () {
		jQuery('#ngo_detail_table').DataTable();
		jQuery(document).on('change', '#ngo_status', function(){
			var ngo_status = jQuery('#ngo_status').val();
			if(ngo_status != 'Empanelled'){
				jQuery('#empanellment_certificate').prop('required',false);
				jQuery('#empanelled_start_date').prop('required',false);
				jQuery('#empanelled_end_date').prop('required',false);
			} else {
				jQuery('#empanellment_certificate').prop('required',true);
				jQuery('#empanelled_start_date').prop('required',true);
				jQuery('#empanelled_end_date').prop('required',true);
			}
			if(ngo_status != 'Applicant'){
				jQuery('#hub_code').prop('required',false);
			} else {
				jQuery('#hub_code').prop('required',true);
			}			
		});
		
		jQuery(document).on('click', '#generate_hub_code_btn', function () {
			jQuery('.generate_loading').show();
			var postData = {action: 'generate_hub_code', ngo_id: jQuery('#ngo_id').val()};
		    jQuery.ajax({
                url: ajaxurl,
				dataType : "JSON",
                data: postData,
                success: function (response) {
					jQuery('.generate_loading').hide();
                    if (response.success == 'Generated') {
						jQuery('#hub_code').val(response.hub_code);
                    }                    
                    return false;
                },
            });
		});
		
		jQuery(document).on('click', '.is-dismissible', function () {
			jQuery('.is-dismissible').hide();
		});
		
		jQuery(document).on('submit', '#update_ngo', function () {
            var work_locations = jQuery('#work_locations').val();
            if(work_locations == ''){
                jQuery('.tag_required').show();
                return false;
            }
            var thematic_areas_works = jQuery('#thematic_areas_works').val();
            if(thematic_areas_works == ''){
                jQuery('.tag_required_thematic_areas').show();
                return false;
            }
            jQuery('.loading_div').show();
            var postData = new FormData(this);
            postData.append('action', 'update_ngo');
            jQuery.ajax({
                url: ajaxurl,
				type: "POST",
                processData: false,
                contentType: false,
                cache: false,
                data: postData,
                success: function (response) {
					jQuery('.loading_div').hide();
					var json = jQuery.parseJSON(response);
                    if (json['error'] == 'Exist') {
                        jQuery('.display_alert').html('<div id="message" class="error notice notice-success is-dismissible"><p>NGO Already Exist.</p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div>');
                    }
                    if (json['error'] == 'HubCodeExist') {
                        jQuery('.display_alert').html('<div id="message" class="error notice notice-success is-dismissible"><p>Below HUB Code Already Exist.</p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div>');
                    }
                    if (json['error'] == 'allLevelNotAccepted') {
                        jQuery('.display_alert').html('<div id="message" class="error notice notice-success is-dismissible"><p>'+ json['errorMessage'] +'</p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div>');
                    }
                    if (json['error'] == 'FileType') {
                        jQuery('.display_alert').html('<div id="message" class="error notice notice-success is-dismissible"><p><strong>Error! </strong> Only allow pdf,graphics and zip file upload.</p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div>');
                    }
                    if (json['error'] == 'FileSize') {
                        jQuery('.display_alert').html('<div id="message" class="error notice notice-success is-dismissible"><p><strong>Error! </strong> Max file upload size limit 10MB.</p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div>');
                    }
                    if (json['success'] == 'Updated') {
						window.location.href = '?page=ngo&action=ngo_details&id=<?php echo $ngo_id; ?>'; 
						return false;
                        jQuery('.display_alert').html('<div id="message" class="updated notice notice-success is-dismissible"><p>NGO Updated.</p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div>');
                        jQuery('#update_ngo')[0].reset();
                        jQuery('.select2-search-choice-close').click();
                    }
                    jQuery("html, body").animate({ scrollTop: 0 }, "slow");
                    tag_load();
                    return false;
                },
            });
            return false;
        });
		
		jQuery(document).on('click', '.level_status input[type=button]', function () {
			var ngo_id = jQuery('#ngo_id').val();
			var level_status = jQuery(this).attr('data-level');
			var level_label = jQuery(this).val();
			if(level_label == "Reset"){
				var ls_field = jQuery(this).attr('data-ls-field');
				var ln_field = jQuery(this).attr('data-ln-field');
				if (confirm('Are you sure, You want to Reset ' + level_status + '?')) {
					var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
					jQuery.ajax({
						url: ajaxurl,
						data : {action: "level_status_reset", ngo_id: ngo_id, ls_field: ls_field, ln_field: ln_field },
						success: function(response){
							if (response == 'level_status_reset') {
								window.location.href = '?page=ngo&action=ngo_details&id=<?php echo $ngo_id; ?>'; 
								return false;
							}
						},
					});
				}
			} else {
				var level_btn_id = jQuery(this).attr('id');
				jQuery('#change_level_status #level_label').val(level_label);
				jQuery('#change_level_status #level_status').val(level_status);
				jQuery('#change_level_status #level_btn_id').val(level_btn_id);
				level_status = level_status.replace('_', ' ');
				level_status = level_status.toLowerCase().replace(/\b[a-z]/g, function(letter) {
					return letter.toUpperCase();
				});
				jQuery('#change_level_status .note_title').html('<strong> ' + level_status + ' >> ' + level_label + ' >> Note: </strong>');
				jQuery('#change_level_status #level_note').focus();
			}
		});
		jQuery(document).on('submit', '#change_level_status', function () {
			var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
			var postData = new FormData(this);
			postData.append('action', 'change_level_status');
			jQuery.ajax({
				url: ajaxurl,
				type: "POST",
				processData: false,
				contentType: false,
				cache: false,
				data: postData,
				success: function(response){
					var json = jQuery.parseJSON(response);
                    if (json['success'] == 'Changed') {
						window.location.href = '?page=ngo&action=ngo_details&id=<?php echo $ngo_id; ?>'; 
						return false;
					}
				},
			});
			return false;
		});
		
	});
	
	function tag_load(){
		
		jQuery.ajax({
            url: ajaxurl,
			dataType : "JSON",
			data : {action: "get_work_locations"},
            success: function(result){
				jQuery("#work_locations").select2({
                    createSearchChoice:function(term, data) { if (jQuery(data).filter(function() { return this.text.localeCompare(term)===0; }).length===0) {return {id:term, text:term};} },
                    multiple: true,
                    <?php if(isset($ngo_id)){ ?>
                    initSelection: function (element, callback) {
                        callback(jQuery.map(element.val().split(','), function (id) {
                            return { id: id, text: id };
                        }));
                    },
                    <?php } ?>
                    //maximumSelectionSize: 1,
                    data: result,
                });
                <?php if(isset($ngo_id)){ ?>
                    <?php if(!empty($work_locations_arr)){ ?>
                        jQuery("#work_locations").select2('val', ["<?php echo implode('","', $work_locations_arr); ?>"], true);
                    <?php } ?>
                <?php } ?>
                function log(e) {
                    var e=jQuery("<li>"+e+"</li>");
                    jQuery("#events_11").append(e);
                    e.animate({opacity:1}, 10000, 'linear', function() { e.animate({opacity:0}, 2000, 'linear', function() {e.remove(); }); });
                }
                jQuery("#work_locations")
                    .on("change", function(e) { log("change "+JSON.stringify({val:e.val, added:e.added, removed:e.removed})); })
                    .on("select2-opening", function() { log("opening"); })
                    .on("select2-open", function() { log("open"); })
                    .on("select2-close", function() { log("close"); })
                    .on("select2-highlight", function(e) { log ("highlighted val="+ e.val+" choice="+ JSON.stringify(e.choice));})
                    .on("select2-selecting", function(e) { log ("selecting val="+ e.val+" choice="+ JSON.stringify(e.choice));})
                    .on("select2-removing", function(e) { log ("removing val="+ e.val+" choice="+ JSON.stringify(e.choice));})
                    .on("select2-removed", function(e) { log ("removed val="+ e.val+" choice="+ JSON.stringify(e.choice));})
                    .on("select2-loaded", function(e) { log ("loaded (data property omitted for brevity)");})
                    .on("select2-focus", function(e) { log ("focus");})
                    .on("select2-blur", function(e) { log ("blur");});
            }
        });
		
		jQuery.ajax({
            url: ajaxurl,
			dataType : "JSON",
			data : {action: "get_thematic_areas_works"},
            success: function(result){
				jQuery("#thematic_areas_works").select2({
                    //createSearchChoice:function(term, data) { if (jQuery(data).filter(function() { return this.text.localeCompare(term)===0; }).length===0) {return {id:term, text:term};} },
                    multiple: true,
                    <?php if(isset($ngo_id)){ ?>
                    initSelection: function (element, callback) {
                        callback(jQuery.map(element.val().split(','), function (id) {
                            return { id: id, text: id };
                        }));
                    },
                    <?php } ?>
                    //maximumSelectionSize: 1,
                    data: result,
                });
                <?php if(isset($ngo_id)){ ?>
                    <?php if(!empty($thematic_areas_works_arr)){ ?>
                        jQuery("#thematic_areas_works").select2('val', ["<?php echo implode('","', $thematic_areas_works_arr); ?>"], true);
                    <?php } ?>
                <?php } ?>
                function log(e) {
                    var e=jQuery("<li>"+e+"</li>");
                    jQuery("#events_11").append(e);
                    e.animate({opacity:1}, 10000, 'linear', function() { e.animate({opacity:0}, 2000, 'linear', function() {e.remove(); }); });
                }
                jQuery("#thematic_areas_works")
                    .on("change", function(e) { log("change "+JSON.stringify({val:e.val, added:e.added, removed:e.removed})); })
                    .on("select2-opening", function() { log("opening"); })
                    .on("select2-open", function() { log("open"); })
                    .on("select2-close", function() { log("close"); })
                    .on("select2-highlight", function(e) { log ("highlighted val="+ e.val+" choice="+ JSON.stringify(e.choice));})
                    .on("select2-selecting", function(e) { log ("selecting val="+ e.val+" choice="+ JSON.stringify(e.choice));})
                    .on("select2-removing", function(e) { log ("removing val="+ e.val+" choice="+ JSON.stringify(e.choice));})
                    .on("select2-removed", function(e) { log ("removed val="+ e.val+" choice="+ JSON.stringify(e.choice));})
                    .on("select2-loaded", function(e) { log ("loaded (data property omitted for brevity)");})
                    .on("select2-focus", function(e) { log ("focus");})
                    .on("select2-blur", function(e) { log ("blur");});
            }
        });
		
    }
</script>
