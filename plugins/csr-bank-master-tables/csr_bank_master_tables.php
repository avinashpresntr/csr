<?php
/*
Plugin Name: CSR Bank Master Tables
Plugin URI: CSR
Description: CSR Bank Master Tables.
Author: CSR
Version: 1.6
Author URI: CSR
*/

require_once(ABSPATH . 'wp-config.php');

session_start();

if(isset($_GET['did']) && $_GET['page'] == "Custom-csr-operation-scale-Form"){
	delete_csr_operation_scale();
}
	
function delete_csr_operation_scale(){
	global $wpdb, $name;
	$table_name = $wpdb->prefix . "operation_scale";

	$del = $wpdb->query("update $table_name set isDelete=1 where id=".$_GET['did'] ); 
	$_SESSION['msg'] = "Delete Successfully";
	
	$url = "?page=Custom-csr-operation-scale-Form";
	header("location:$url");
	exit;
}

	
if(isset($_POST["btn_operation_scale"])){
	extract($_POST);
	if($_GET['id']){
		update_csr_operation_scale();
	} else {
		insert_csr_operation_scale();
	}
}

function insert_csr_operation_scale(){
	global $wpdb, $name;
	$table_name = $wpdb->prefix . "operation_scale";
	$res = $wpdb->get_results("select * from $table_name where operation_scale='".$name."' "); 
	if(empty($res)){
		$wpdb->insert($table_name, array('operation_scale' => $name) ); 
		$_SESSION['msg'] = "Insert Successfully";
	} else {
		if($res[0]->isDelete == 1){
			$upd = $wpdb->query("update $table_name set isDelete=0 where operation_scale='".$name."' " ); 
			$_SESSION['msg'] = "Insert Successfully";
		} else {
			$_SESSION['error_msg'] = "Already Exist";
		}
	}
}

function update_csr_operation_scale(){
	global $wpdb, $name;
	$table_name = $wpdb->prefix . "operation_scale";
	$res = $wpdb->get_results("select * from $table_name where operation_scale='".$name."' AND id!=".$_GET['id']); 
	if(empty($res)){
		$upd = $wpdb->query("update $table_name set operation_scale='$name' where id=".$_GET['id'] ); 
		$_SESSION['msg'] = "Update Successfully";
	} else {
		$_SESSION['error_msg'] = "Already Exist";
	}
	$url = "?page=Custom-csr-operation-scale-Form";
	header("location:$url");
	exit;
}

add_action('admin_menu', 'custom_csr_operation_scale_form');
function custom_csr_operation_scale_form(){
    add_menu_page( 'Csr Master Table', 'Csr Master Table', 'manage_options', 'Custom-csr-operation-scale-Form', 'csr_operation_scale', '' , 30 );
	add_submenu_page( 'Custom-csr-operation-scale-Form', 'Operation Scale', 'Operation Scale','manage_options', 'Custom-csr-operation-scale-Form');
	add_submenu_page( 'Custom-csr-operation-scale-Form', 'Organisation Type', 'Organisation Type','manage_options', 'organisation-type', 'organisation_type');
	add_submenu_page( 'Custom-csr-operation-scale-Form', 'Thematic Area', 'Thematic Area','manage_options', 'thematic-area', 'thematic_area');
	add_submenu_page( 'Custom-csr-operation-scale-Form', 'Tag', 'Tag','manage_options', 'tag', 'tag');
	add_submenu_page( 'Custom-csr-operation-scale-Form', 'Type Of Work', 'Type Of Work','manage_options', 'type-of-work', 'type_of_work');
	add_submenu_page( 'Custom-csr-operation-scale-Form', 'Corporate Type', 'Corporate Type','manage_options', 'corporate-type', 'corporate_type');
	add_submenu_page( 'Custom-csr-operation-scale-Form', 'Work Locations', 'Work Locations','manage_options', 'work-locations', 'work_locations');
}

function csr_operation_scale(){
	echo '<link rel="stylesheet" type="text/css" href="'.plugins_url().'/datatables/jquery.dataTables.min.css" />';
	echo '<script type="text/javascript" src="'.plugins_url().'/datatables/jquery.dataTables.min.js"></script>';
	//echo '<script type="text/javascript" src="'.plugins_url().'/datatables/dataTables.bootstrap.min.js"></script>';
?>
	<div style="padding-bottom:20px;" class="widefat">
		<table style="padding-top:20px;">
			<form method="post" action="#" >
				<tr>
					<th>Enter Operation Scale</th>
					<td>
						<?php
							global $wpdb;
							$table_name = $wpdb->prefix . "operation_scale";
							$res = $wpdb->get_results("select * from $table_name where id=".$_GET['id']); 
							$ressel = $res[0];
						?>
						<input type="text" value="<?php echo $ressel->operation_scale; ?>" name="name" required />
					</td>
					<td colspan="2" align="right">
						<input type="submit" name="btn_operation_scale" value="Submit" class="button button-primary button-small" />
					</td>
				</tr>
			</form>
		</table>
		<br>
		<?php 
			if(isset($_SESSION['msg'])){
				echo '<div class="updated" style="padding:5px 20px;"><b>'.$_SESSION['msg'].'</b></div>';
				unset($_SESSION['msg']);
			}
			if(isset($_SESSION['error_msg'])){
				echo '<div class="error" style="padding:5px 20px;"><b>'.$_SESSION['error_msg'].'</b></div>';
				unset($_SESSION['error_msg']);
			}
		?>
		<table id="csr_operation_scale_table" width="100%" class="widefat dataTable">
			<thead>
				<tr>
					<th>Sr. No.</th>
					<th>Name</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
			<?php
				global $wpdb; $i = 1;
				$table_name = $wpdb->prefix . "operation_scale";
				$res = $wpdb->get_results("select * from $table_name where isDelete=0"); 
				foreach($res as $row){
			?>
			
				<tr>
					<td><?php echo $i; ?></td>
					<td><?php echo $row->operation_scale; ?></td>
					<td><a href="?page=Custom-csr-operation-scale-Form&id=<?php echo $row->id; ?>">Edit</a> | 
						<a onclick="return confirm('Are you sure you want to delete this ?');" href="?page=Custom-csr-operation-scale-Form&did=<?php echo $row->id; ?>">Delete</a>
					</td>
				</tr>
			<?php $i++; } ?>
			</tbody>
		</table>
	</div>
	<script type="text/javascript">
		 jQuery(function() {
			jQuery('#csr_operation_scale_table').dataTable();
		});
	</script>	
<?php	
}
add_shortcode( 'csr_operation_scale', 'csr_operation_scale' );

/****************************************** Organisation Type **************************************/

if(isset($_GET['did']) && $_GET['page'] == "organisation-type"){
	delete_organisation_type();
}
	
function delete_organisation_type(){
	global $wpdb, $name;
	$table_name = $wpdb->prefix . "organisation_type";

	$del = $wpdb->query("update $table_name set isDelete=1 where id=".$_GET['did'] ); 
	$_SESSION['msg'] = "Delete Successfully";
	
	$url = "?page=organisation-type";
	header("location:$url");
	exit;
}
	
if(isset($_POST["btn_organisation_type"])){
	extract($_POST);
	if($_GET['id']){
		update_organisation_type();
	} else {
		insert_organisation_type();
	}
}
	
function insert_organisation_type(){
	global $wpdb, $name;
	$table_name = $wpdb->prefix . "organisation_type";
	$res = $wpdb->get_results("select * from $table_name where type_name='".$name."' "); 
	if(empty($res)){
		$wpdb->insert($table_name, array('type_name' => $name) ); 
		$_SESSION['msg'] = "Insert Successfully";
	} else {
		if($res[0]->isDelete == 1){
			$upd = $wpdb->query("update $table_name set isDelete=0 where type_name='".$name."' " ); 
			$_SESSION['msg'] = "Insert Successfully";
		} else {
			$_SESSION['error_msg'] = "Already Exist";
		}
	}
}

function update_organisation_type(){
	global $wpdb, $name;
	$table_name = $wpdb->prefix . "organisation_type";
	$res = $wpdb->get_results("select * from $table_name where type_name='".$name."' AND id!=".$_GET['id']); 
	if(empty($res)){
		$upd = $wpdb->query("update $table_name set type_name='$name' where id=".$_GET['id'] ); 
		$_SESSION['msg'] = "Update Successfully";
	} else {
		$_SESSION['error_msg'] = "Already Exist";
	}
	$url = "?page=organisation-type";
	header("location:$url");
	exit;
}

function organisation_type(){
	include("organisation_type.php");
}
add_shortcode( 'organisation_type', 'organisation_type' );

/****************************************** Thematic Area **************************************/

if(isset($_GET['did']) && $_GET['page'] == "thematic-area"){
	delete_thematic_area();
}
	
function delete_thematic_area(){
	global $wpdb, $name;
	$table_name = $wpdb->prefix . "thematic_areas_work";
	$del = $wpdb->query("update $table_name set isDelete=1 where id=".$_GET['did'] ); 
	$_SESSION['msg'] = "Delete Successfully";
	$url = "?page=thematic-area";
	header("location:$url");
	exit;
}
	
if(isset($_POST["btn_thematic_area"])){
	extract($_POST);
	if($_GET['id']){
		update_thematic_area();
	} else {
		insert_thematic_area();
	}
}
	
function insert_thematic_area(){
	global $wpdb, $name;
	$table_name = $wpdb->prefix . "thematic_areas_work";
	$res = $wpdb->get_results("select * from $table_name where thematic_area='".$name."' "); 
	if(empty($res)){
		$wpdb->insert($table_name, array('thematic_area' => $name) ); 
		$_SESSION['msg'] = "Insert Successfully";
	} else {
		if($res[0]->isDelete == 1){
			$upd = $wpdb->query("update $table_name set isDelete=0 where thematic_area='".$name."' " ); 
			$_SESSION['msg'] = "Insert Successfully";
		} else {
			$_SESSION['error_msg'] = "Already Exist";
		}
	}
}

function update_thematic_area(){
	global $wpdb, $name;
	$table_name = $wpdb->prefix . "thematic_areas_work";
	$res = $wpdb->get_results("select * from $table_name where thematic_area='".$name."' AND id!=".$_GET['id']); 
	if(empty($res)){
		$upd = $wpdb->query("update $table_name set thematic_area='$name' where id=".$_GET['id'] ); 
		$_SESSION['msg'] = "Update Successfully";
	} else {
		$_SESSION['error_msg'] = "Already Exist";
	}
	$url = "?page=thematic-area";
	header("location:$url");
	exit;
}

function thematic_area(){
	include("thematic_area.php");
}
add_shortcode( 'thematic_area', 'thematic_area' );

/****************************************** Tag **************************************/

if(isset($_GET['did']) && $_GET['page'] == "tag"){
	delete_tag();
}
	
function delete_tag(){
	global $wpdb, $name;
	$table_name = $wpdb->prefix . "tag";
	$del = $wpdb->query("update $table_name set isDelete=1 where id=".$_GET['did'] ); 
	$_SESSION['msg'] = "Delete Successfully";
	$url = "?page=tag";
	header("location:$url");
	exit;
}
	
if(isset($_POST["btn_tag"])){
	extract($_POST);
	if($_GET['id']){
		update_tag();
	} else {
		insert_tag();
	}
}
	
function insert_tag(){
	global $wpdb, $name;
	$table_name = $wpdb->prefix . "tag";
	$res = $wpdb->get_results("select * from $table_name where tag_name='".$name."' "); 
	if(empty($res)){
		$wpdb->insert($table_name, array('tag_name' => $name) ); 
		$_SESSION['msg'] = "Insert Successfully";
	} else {
		if($res[0]->isDelete == 1){
			$upd = $wpdb->query("update $table_name set isDelete=0 where tag_name='".$name."' " ); 
			$_SESSION['msg'] = "Insert Successfully";
		} else {
			$_SESSION['error_msg'] = "Already Exist";
		}
	}
}

function update_tag(){
	global $wpdb, $name;
	$table_name = $wpdb->prefix . "tag";
	$res = $wpdb->get_results("select * from $table_name where tag_name='".$name."' AND id!=".$_GET['id']); 
	if(empty($res)){
		$upd = $wpdb->query("update $table_name set tag_name='$name' where id=".$_GET['id'] ); 
		$_SESSION['msg'] = "Update Successfully";
	} else {
		$_SESSION['error_msg'] = "Already Exist";
	}
	$url = "?page=tag";
	header("location:$url");
	exit;
}

function tag(){
	include("tag.php");
}
add_shortcode( 'tag', 'tag' );

/****************************************** Type of Work **************************************/

if(isset($_GET['did']) && $_GET['page'] == "type-of-work"){
	delete_type_of_work();
}
	
function delete_type_of_work(){
	global $wpdb, $name;
	$table_name = $wpdb->prefix . "type_of_work";
	$del = $wpdb->query("update $table_name set isDelete=1 where id=".$_GET['did'] ); 
	$_SESSION['msg'] = "Delete Successfully";
	$url = "?page=type-of-work";
	header("location:$url");
	exit;
}
	
if(isset($_POST["btn_type_of_work"])){
	extract($_POST);
	if($_GET['id']) {
		update_type_of_work();
	} else {
		insert_type_of_work();
	}
}
	
function insert_type_of_work() {
	global $wpdb, $name;
	$table_name = $wpdb->prefix . "type_of_work";
	$res = $wpdb->get_results("select * from $table_name where work_type='".$name."' "); 
	if(empty($res)){
		$wpdb->insert($table_name, array('work_type' => $name) ); 
		$_SESSION['msg'] = "Insert Successfully";
	} else {
		if($res[0]->isDelete == 1){
			$upd = $wpdb->query("update $table_name set isDelete=0 where work_type='".$name."' " ); 
			$_SESSION['msg'] = "Insert Successfully";
		} else {
			$_SESSION['error_msg'] = "Already Exist";
		}
	}
}

function update_type_of_work() {
	global $wpdb, $name;
	$table_name = $wpdb->prefix . "type_of_work";
	$res = $wpdb->get_results("select * from $table_name where work_type='".$name."' AND id!=".$_GET['id']); 
	if(empty($res)){
		$upd = $wpdb->query("update $table_name set work_type='$name' where id=".$_GET['id'] ); 
		$_SESSION['msg'] = "Update Successfully";
	} else {
		$_SESSION['error_msg'] = "Already Exist";
	}
	$url = "?page=type-of-work";
	header("location:$url");
	exit;
}

function type_of_work(){
	include("type_of_work.php");
}
add_shortcode( 'type_of_work', 'type_of_work' );

/****************************************** Corporate Type **************************************/

if(isset($_GET['did']) && $_GET['page'] == "corporate-type"){
	delete_corporate_type();
}
	
function delete_corporate_type(){
	global $wpdb, $name;
	$table_name = $wpdb->prefix . "corporate_type";
	$del = $wpdb->query("update $table_name set isDelete=1 where id=".$_GET['did'] ); 
	$_SESSION['msg'] = "Delete Successfully";
	$url = "?page=corporate-type";
	header("location:$url");
	exit;
}

if(isset($_POST["btn_corporate_type"])){
	extract($_POST);
	if($_GET['id']){
		update_corporate_type();
	} else {
		insert_corporate_type();
	}
}

function insert_corporate_type(){
	global $wpdb, $name;
	$table_name = $wpdb->prefix . "corporate_type";
	$res = $wpdb->get_results("select * from $table_name where type_name='".$name."' "); 
	if(empty($res)){
		$wpdb->insert($table_name, array('type_name' => $name) ); 
		$_SESSION['msg'] = "Insert Successfully";
	} else {
		if($res[0]->isDelete == 1){
			$upd = $wpdb->query("update $table_name set isDelete=0 where type_name='".$name."' " ); 
			$_SESSION['msg'] = "Insert Successfully";
		} else {
			$_SESSION['error_msg'] = "Already Exist";
		}
	}
}

function update_corporate_type(){
	global $wpdb, $name;
	$table_name = $wpdb->prefix . "corporate_type";
	$res = $wpdb->get_results("select * from $table_name where type_name='".$name."' AND id!=".$_GET['id']); 
	if(empty($res)){
		$upd = $wpdb->query("update $table_name set type_name='$name' where id=".$_GET['id'] ); 
		$_SESSION['msg'] = "Update Successfully";
	} else {
		$_SESSION['error_msg'] = "Already Exist";
	}
	$url = "?page=corporate-type";
	header("location:$url");
	exit;
}

function corporate_type(){
	include("corporate_type.php");
}
add_shortcode( 'corporate_type', 'corporate_type' );

/****************************************** Work Locations **************************************/

if(isset($_GET['did']) && $_GET['page'] == "work-locations"){
	delete_work_locations();
}
	
function delete_work_locations(){
	global $wpdb, $name;
	$table_name = $wpdb->prefix . "work_locations";
	$del = $wpdb->query("update $table_name set isDelete=1 where id=".$_GET['did'] ); 
	$_SESSION['msg'] = "Delete Successfully";
	$url = "?page=work-locations";
	header("location:$url");
	exit;
}

if(isset($_POST["btn_work_locations"])){
	extract($_POST);
	if($_GET['id']){
		update_work_locations();
	} else {
		insert_work_locations();
	}
}

function insert_work_locations(){
	global $wpdb, $name;
	$table_name = $wpdb->prefix . "work_locations";
	$res = $wpdb->get_results("select * from $table_name where location_name='".$name."' "); 
	if(empty($res)){
		$wpdb->insert($table_name, array('location_name' => $name) ); 
		$_SESSION['msg'] = "Insert Successfully";
	} else {
		if($res[0]->isDelete == 1){
			$upd = $wpdb->query("update $table_name set isDelete=0 where location_name='".$name."' " ); 
			$_SESSION['msg'] = "Insert Successfully";
		} else {
			$_SESSION['error_msg'] = "Already Exist";
		}
	}
}

function update_work_locations(){
	global $wpdb, $name;
	$table_name = $wpdb->prefix . "work_locations";
	$res = $wpdb->get_results("select * from $table_name where location_name='".$name."' AND id!=".$_GET['id']); 
	if(empty($res)){
		$upd = $wpdb->query("update $table_name set location_name='$name' where id=".$_GET['id'] ); 
		$_SESSION['msg'] = "Update Successfully";
	} else {
		$_SESSION['error_msg'] = "Already Exist";
	}
	$url = "?page=work-locations";
	header("location:$url");
	exit;
}

function work_locations(){
	include("work_locations.php");
}
add_shortcode( 'work_locations', 'work_locations' );
?>
