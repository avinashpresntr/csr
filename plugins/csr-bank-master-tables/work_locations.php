<?php
	echo '<link rel="stylesheet" type="text/css" href="'.plugins_url().'/datatables/jquery.dataTables.min.css" />';
	echo '<script type="text/javascript" src="'.plugins_url().'/datatables/jquery.dataTables.min.js"></script>';
	//echo '<script type="text/javascript" src="'.plugins_url().'/datatables/dataTables.bootstrap.min.js"></script>';
?>
<div style="padding-bottom:20px;" class="widefat">
	<table style="padding-top:20px;">
		<form method="post">
			<tr>
				<td>Enter Work Locations</td>
				<td>
					<?php
						global $wpdb;
						$table_name = $wpdb->prefix . "work_locations";
						$resot = $wpdb->get_results("select * from $table_name where id=".$_GET['id']); 
						$resotsel = $resot[0];
					?>
					<input type="text" value="<?php echo $resotsel->location_name; ?>" name="name" required />
				</td>
				<td colspan="2" align="right">
					<input type="submit" name="btn_work_locations" value="Submit" class="button button-primary button-small" />
				</td>
			</tr>
		</form>
	</table>
	<br>
	<?php 
		if(isset($_SESSION['msg'])){
			echo '<div class="updated" style="padding:5px 20px;"><b>'.$_SESSION['msg'].'</b></div>';
			unset($_SESSION['msg']);
		}
		if(isset($_SESSION['error_msg'])){
			echo '<div class="error" style="padding:5px 20px;"><b>'.$_SESSION['error_msg'].'</b></div>';
			unset($_SESSION['error_msg']);
		}
	?>
	<table id="work_locations_table" width="100%" class="widefat dataTable">
		<thead>
			<th>Sr. No.</th>
			<th>Work Locations</th>
			<th>Action</th>
		</thead>
		<tbody>
		<?php
			global $wpdb; $i = 1;
			$table_name = $wpdb->prefix . "work_locations";
			$res = $wpdb->get_results("select * from $table_name where isDelete=0");
			foreach($res as $row){
		?>
			<tr>
				<td><?php echo $i; ?></td>
				<td><?php echo $row->location_name; ?></td>
				<td><a href="?page=work-locations&id=<?php echo $row->id; ?>">Edit</a> | 
					<a onclick="return confirm('Are you sure you want to delete this ?');" href="?page=work-locations&did=<?php echo $row->id; ?>">Delete</a>
				</td>
			</tr>
		<?php $i++; } ?>
		</tbody>
	</table>
</div>
<script type="text/javascript">
	 jQuery(function() {
		jQuery('#work_locations_table').dataTable();
	});
</script>
