<?php
	echo '<link rel="stylesheet" type="text/css" href="'.plugins_url().'/datatables/jquery.dataTables.min.css" />';
	echo '<script type="text/javascript" src="'.plugins_url().'/datatables/jquery.dataTables.min.js"></script>';
	//echo '<script type="text/javascript" src="'.plugins_url().'/datatables/dataTables.bootstrap.min.js"></script>';
?>
<div style="padding-bottom:20px;" class="widefat">
	<table style="padding-top:20px;">
		<form method="post">
			<tr>
				<td>Enter Tag</td>
				<td>
					<?php
						global $wpdb;
						$table_name = $wpdb->prefix . "tag";
						$resot = $wpdb->get_results("select * from $table_name where id=".$_GET['id']); 
						$resotsel = $resot[0];
					?>
					<input type="text" value="<?php echo $resotsel->tag_name; ?>" name="name" required />
				</td>
				<td colspan="2" align="right">
					<input type="submit" name="btn_tag" value="Submit" class="button button-primary button-small" />
				</td>
			</tr>
		</form>
	</table>
	<br>
	<?php 
		if(isset($_SESSION['msg'])){
			echo '<div class="updated" style="padding:5px 20px;"><b>'.$_SESSION['msg'].'</b></div>';
			unset($_SESSION['msg']);
		}
		if(isset($_SESSION['error_msg'])){
			echo '<div class="error" style="padding:5px 20px;"><b>'.$_SESSION['error_msg'].'</b></div>';
			unset($_SESSION['error_msg']);
		}
	?>
	<table id="tag_table" width="100%" class="widefat dataTable">
		<thead>
			<tr>
				<th>Sr. No.</th>
				<th>Tag</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
		<?php
			global $wpdb; $i = 1;
			$table_name = $wpdb->prefix . "tag";
			$res = $wpdb->get_results("select * from $table_name where isDelete=0");
			foreach($res as $row){
		?>
			<tr>
				<td><?php echo $i; ?></td>
				<td><?php echo $row->tag_name; ?></td>
				<td><a href="?page=tag&id=<?php echo $row->id; ?>">Edit</a> | 
					<a onclick="return confirm('Are you sure you want to delete this ?');" href="?page=tag&did=<?php echo $row->id; ?>">Delete</a>
				</td>
			</tr>
		<?php $i++; } ?>
		</tbody>
	</table>
</div>
<script type="text/javascript">
	 jQuery(function() {
		jQuery('#tag_table').dataTable();
	});
</script>
