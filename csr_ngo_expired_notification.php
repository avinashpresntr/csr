<?php

if($_REQUEST['key'] === 'R2KuTChcZ39hXzE0bvpEYU3GGW5SMA'){
	
	include_once('wp-config.php');
	$res = $wpdb->get_results("SELECT `id`, `empanelled_end_date`, `email_id`, DATEDIFF( now( ) , `empanelled_end_date` ) AS days FROM `csr_ngo` WHERE `ngo_status` = 'Empanelled' HAVING days = 1" ); 
	if(!empty($res)){
		foreach($res as $record){
			if($record->days == 1){
				$expiry_date = date('d M, Y', strtotime($record->empanelled_end_date));
				$ngo_id = $record->id;
				$ngo_status = "expired_renewal_pending";
				csr_ngo_expired_email($record->email_id, $expiry_date);
				
				$wpdb->update('csr_ngo', 
					array(
						'ngo_status' => $ngo_status
					), 
					array('id'=>$ngo_id)
				);
			}
		}
	}
}
	
?>